
module.exports = function(grunt) {
    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-clean');
    // grunt.loadNpmTasks('grunt-contrib-compress');
    // grunt.loadNpmTasks('grunt-css');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    // grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    // grunt.loadNpmTasks('grunt-contrib-qunit');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-html-validation');
    grunt.loadNpmTasks('grunt-contrib-compass');

    grunt.initConfig({

    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    
    banner: '/**\n' +
              '* Euro Accountancy Theme v<%= pkg.version %> by <%= pkg.author %>\n' +
              '* Copyright <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
              '* Licensed under <%= _.pluck(pkg.licenses, "url").join(", ") %>.\n' +
              '*\n' +
              '* Designed and built with all the love in the world by @auroralabs.\n' +
              '*/\n',
    jqueryCheck: 'if (typeof jQuery === "undefined") { throw new Error(\"Bootstrap requires jQuery\") }\n\n',

    dirs: {
        src: 'javascripts/bootstrap',
        dest: 'javascripts'
    }, //dirs

    copy: {
        fonts: {
            expand: true,
            cwd: 'bower_components/font-awesome/fonts/',
               src: ["**"],
               dest: 'css/fonts/font-awesome/',
               filter: 'isFile'
        } //fonts
     }, // copy

    concat: {
        options: {
            banner: '<%= banner %>',
            stripBanners: false
        }, //options
        bootstrap: {
            src: '<%= dirs.src %>/*.js',
            dest: '<%= dirs.dest %>/bootstrap.js'
        }, // bootstrap
        extras: {
            src: 'components/js/*.js',
            dest: 'components/scripts.js'
        } // extras
    },

    uglify: {
      options: {
          banner: '<%= banner %>',
          report: 'min',
          mangle: false
      }, //options
      bootstrap: {
        src: ['<%= concat.bootstrap.dest %>'],
        dest: 'js/bootstrap.min.js'
      }, //bootstrap
      extras: {
        src: ['<%= concat.extras.dest %>'],
        dest: 'js/scripts.min.js'
      } //extras
    }, // uglify

    compass: {
      dev: {
        options: {
          config: 'config.rb'
        } // options
      } // dev
    }, //compass task

    // Watch tasks to automate the process of site changes in designated folders.
    watch: {
        options: {
            spawm: true,
            livereload: {
                port: 9000
            } // port assigned
        }, // livereload
        sass: {
            files: ['sass/*.scss', 'components/sass/*.scss'],
            tasks: ['compass:dev']
        }, // sass
        html: {
            files: ['*.html']
        } // html
     } //watch


  }); // initConfig


  // JS distribution task.
  //grunt.registerTask('dist-js', ['concat', 'uglify']);

  // CSS distribution task.
  //grunt.registerTask('dist-css', ['compass:bootstrap', 'cssmin:bootstrap']);

  // Fonts distribution task.
  //grunt.registerTask('dist-fonts', ['copy:fonts']);

  // Full distribution task.
  //grunt.registerTask('dist', ['clean', 'dist-fonts', 'dist-css', 'dist-js', 'compress:dist']);

  // Default task.
  grunt.registerTask('default', 'watch');
  // grunt.registerTask('default', ['test-no-html', 'dist']);


}; // module.exports