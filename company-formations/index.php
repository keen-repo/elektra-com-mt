<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Company Formations : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="EAFS are a leading UK based company formation agent offering company formations of limited companies. Visit our Company Formations page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Company Formations :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="EAFS are a leading UK based company formation agent offering company formations of limited companies. Visit our Company Formations page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>company-formations/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Company Formations :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>company-formations/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="EAFS are a leading UK based company formation agent offering company formations of limited companies. Visit our Company Formations page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Limited Company Formation, Company Formations, Accountancy & Company Formations, Company Formations, Company Registration, Limited Company Formation" />
    <meta name="description" content="EAFS are a leading UK based company formation agent offering company formations of limited companies. Visit our Company Formations page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>company-formations/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body itemscope itemtype="https://schema.org/WebPage">
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Company Formation Services</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->



<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb"><a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current"><span itemprop="title">Company Formations</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemscope itemtype="https://schema.org/Article" itemprop="Company Formations" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Company Formations</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>Euro Accountancy &amp; Finance Services specialise in providing professional and timely accountancy services to a wide range of clients within the United Kingdom. These include Limited Company Contractors, Small to Medium Sized Owner Managed Limited Companies, Partnerships and Sole Traders. </strong></p>
                            <p>Our team of professional, fully qualified accountants are dedicated to meeting all your accountancy and tax obligations, ensuring all deadlines are met on time, every time. We not only make sure that you and your business are fully compliant, we also advise on the most tax efficient and legal way of organising your business affairs; through an EAFS UK Umbrella, Limited Company, Partnership or Sole Trader structure. </p>
                            <p>Our services are provided for a fixed competitive fixed annual fee and we can discuss your situation at our free initial, no obligation meeting.  At Euro Accountancy &amp; Finance Services, we fully understand that everyone&rsquo;s needs are different; we will take time to discuss with you and advise on the best solution for you. </p>
                            </div> <!-- end articleBody -->
                            <h4>UK Company Formations</h4>
                            <div class="custom-services clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 first">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-user fa-4x"></i>
                                                    <h3>Sole Trader</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Sole Trader</h3>
                                                    <p>At EAFS we offer a number of accountancy solutions to best assist you as a sole trader.  </p>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-user fa-4x"></i>
                                                    <i class="fa fa-user fa-4x"></i>
                                                    <h3>Partnership</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Partnership</h3>
                                                    <p>At EAFS, we offer a number of accountancy solutions to best assist your business partnership. </p>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-group fa-4x"></i>
                                                    <h3>Limited Company</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Limited Company</h3>
                                                    <p>At EAFS, we offer a number of accountancy solutions to best assist your Limited Company.  </p>
                                                    <a href="<?php echo SITE_URL;?>company-formations/limited-company/" title="Link to Limited Company Formations Page">Limited Company</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->
                            </div>

                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                        </article>
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
            <div id="Company Formation Links">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_coformations.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
            </div><!-- end formation -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>