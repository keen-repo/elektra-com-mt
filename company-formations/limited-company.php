<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Company Information : Limited Company Services : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Visit our About Us page to find out about EAFS as a company and the services we offer for Contractors and Freelancers." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Company Information : About Us : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Visit our About Us page to find out about EAFS as a company and the services we offer for Contractors and Freelancers." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>company-information/about-eafs/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Company Information : About Us : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>company-information/about-eafs/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Visit our About Us page to find out about EAFS &amp; as a company and the services we offer for Contractors and Freelancers."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="EAFS About Us, About Us" />
    <meta name="description" content="Visit our About Us page to find out about EAFS as a company and the services we offer for Contractors and Freelancers." />
    <link rel="canonical" href="<?php echo SITE_URL;?>company-information/about-eafs/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body itemscope itemtype="https://schema.org/WebPage">
    <?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Company Formations : Limited Company</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div role="main" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb"><a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb"class="unavailable"><a itemprop="url" href="<?php echo SITE_URL;?>company-formations/" title="Company Formations" alt="Company Formations"><span itemprop="title">Company Formations</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb"class="current"><span itemprop="title">Limited Company Formations</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemscope itemtype="https://schema.org/Article" itemprop="Limited Company Formations" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Company Formations : Limited Company</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>You can easily set up a private limited company in order to run your business through EAFS. You must appoint people to run the company, also known as directors, and register (or incorporate) it with Companies House. </strong></p>
                            <p>Once the company is registered, you&rsquo;ll receive a &lsquo;Certificate of Incorporation&rsquo; confirming that the company legally exists and will display the company number and the date of formation. Sole Traders are personally responsible for any business debts, but the liability within a private company is usually limited only to shareholders. The liabilities always depend on the type of company being used or created. </p>
                            <p><strong>All Limited Companies have to be registered or incorporated with Companies House. To do this, you&rsquo;ll need:</strong></p>
                            <ul type="disc">
                                <li>The      Company&rsquo;s Name and Registered Address.</li>
                                <li>At Least      One Director.</li>
                                <li>At Least      One Shareholder.</li>
                                <li>Details of      the Company&rsquo;s Shares - Known as &lsquo;Memorandum of Association&rsquo;.</li>
                                <li>Rules      about how the Company is run - Known as &lsquo;Articles of Association&rsquo;.</li>
                            </ul>
                            <h4><strong>Company Name.</strong></h4>
                            <p>The name of your Private Limited Company within the UK must always end either in &lsquo;Ltd&rsquo; or &lsquo;Limited&rsquo;. The Welsh equivalent of &lsquo;Limited&rsquo; and &lsquo;Ltd&rsquo; are &lsquo;Cyfyngedig&rsquo; and &lsquo;Cyf&rsquo;. The name can&rsquo;t: </p>
                            <ul type="disc">
                                <li>Be identical      to any other name on the Companies House <a href="https://www.companieshouse.gov.uk/toolsToHelp/findCompanyInfo.shtml" class="bs-example" data-placement="top" data-content="This is the popover message for this link" data-original-title="Index of Names">index of names</a>. </li>
                                <li>Contain any      &lsquo;sensitive&rsquo; words or expressions unless you <a href="https://www.companieshouse.gov.uk/about/gbhtml/gp1.shtml#appA" class="bs-example" data-placement="top" data-content="This is the popover message for this link" data-original-title="Sensitive Words">gain permission</a>.</li>
                                <li>Suggest a      Connection with Local Authorities or Councils or Governmental Departments.</li>
                                <li>Be      offensive in any way. </li>
                            </ul>
                            <p>Registering a company doesn&rsquo;t mean that any of your trademarks are protected. You have to register any trademarks separately. </p>
                            <h4><strong>Registered Address</strong></h4>
                            <p>The registered address for your office is where all official documentation and communication are sent. Taxation letters, Contact from Companies House and Revenue &amp; Customs certifications are all dispatched to the registered address. However, the address itself doesn&rsquo;t have to be where you operate your business from, but it must be:</p>
                            <ul type="disc">
                                <li>An actual      Physical Address.</li>
                                <li>In the      same country that your company is registered in , for example, a company registered in Holland must also therefore have a registered address in Holland.</li>
                            </ul>
                            <p>While you can use a PO Box, you must include the physical address and postcode when establishing communication to have documentation send to PO box. For Example, you couldn&rsquo;t simply say &lsquo;PO Box 456&rsquo;. The full PO Box address would be: &lsquo;PO Box 456, 1 Main Road, Acity,  SW12 3AB&rsquo;.</p>
                            <p>You can use your home address, if the address meets the set rules, or the address of the individual who would be managing your corporation tax – such as the service EAFS offer. </p>
                            <h4><strong>Directors</strong></h4>
                            <p>Your company must have at least one director when registering. The director is lawfully responsible for running the company and must be over 16 years of age, while not being an individual whom has been disqualified from being a director. <br>
                                It is possible to make another company a director – but at least 1 of your company&rsquo;s directors must be an actual person. </p>
                            <h4><strong>Directors - Company Secretaries</strong></h4>
                            <p>You don&rsquo;t need a company secretary for a Private Limited Company, but some organisations and businesses take on secretaries in order to share the director&rsquo;s responsibilities. The company&rsquo;s secretary can indeed be a director, but can&rsquo;t:</p>
                            <ul type="disc">
                                <li>Be the company&rsquo;s auditor</li>
                                <li>Or be an      &lsquo;undischarged bankrupt&rsquo; unless they have court permission.</li>
                            </ul>
                            <p>Despite the presence of any company secretary, the directors still remain legally and lawfully responsible for all aspects of the company.</p>
                            <h4>Shareholders</h4>
                            <p>When you register your company, you will need to make a &lsquo;statement of capital&rsquo;. This refers to the number of shares that the company has paired with their total value, known as the &lsquo;Share Capital&rsquo;. This also refers to the names and addresses of all the company shareholders, often dubbed &lsquo;members&rsquo; or &lsquo;subscribers&rsquo;. There are no maximum numbers limits sets for shareholders, of which Directors can also be, but the company must have at least one. A company with 1000 shares at £2 each has a share capital of £2,000. <br>
                                Shareholders are ultimately owners of the company and have certain rights. Any changes to the company must be put to vote in order to let shareholders voice their opinion.</p>
                            <h4>Articles of Association</h4>
                            <p>When you register your company, you must possess articles of association.  These refer to the rules around running the company that shareholders and &lsquo;officer&rsquo;s – directors or secretaries – have to agree to. Most companies use standard or model articles yet you can change your specific rules on the premise that no laws are broken. </p>
                            <h4>Set up your Company for Corporation Tax.</h4>
                            <p>Within three months of starting your business, you have to supply HMRC (Her Majesty&rsquo;s Revenue &amp; Customs) with specific information in regards to your company. This can be completed once you&rsquo;ve received your company&rsquo;s Unique Taxpayers Reference. This reference is used by HMRC to decide when your company must pay corporation tax.</p>
                            <p><strong>You must supply HMRC with:</strong></p>
                            <ul type="disc">
                                <li>The Date your Company was Founded.</li>
                                <li>Your Company Name and Registered Number.</li>
                                <li>The Main Address where you do      Business.</li>
                                <li>What Kind/Type of Business you do.</li>
                                <li>The Date you&rsquo;ll make your Annual      Accounts.</li>
                                <li>If you&rsquo;ve taken over a business or bought      them out. </li>
                            </ul>
                            <h4>Your Company&rsquo;s Unique Taxpayer Reference.</h4>
                            <p>Her Majesty&rsquo;s Revenue &amp; Customs will send out your companies Unique Taxpayer Reference to your registered office address. This will happen within the week your company is registered/incorporated.  </p>
                            <p>The letter tells you how to give HMRC all the information that they require about your company and set up the company online account with HMRC in regards to Company Tax Returns and all Corporation Taxes.  </p>
                            <p><cite>Source: HMRC</cite></p>
                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                                </div>

                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                        </article>
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_coformations.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
<script>
    $(function (){
        $(".bs-example").popover();
    });
</script>
</body>
</html>