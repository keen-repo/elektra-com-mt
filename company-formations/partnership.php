<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Company Formations : Partnership : <?php echo SITE_TITLE;?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <link href="../css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body itemscope itemtype="https://schema.org/WebPage">
    <?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Company Formations : Partnership</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb"><a itemprop="url"href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable"><a itemprop="url"href="<?php echo SITE_URL;?>company-formations/" title="Company Formations" alt="Company Formations"><span itemprop="title">Company Formations</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current"><span itemprop="title">Partnership</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemscope itemtype="https://schema.org/Article" itemprop="Company Partnership Formations" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Company Formations : Partnership</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>At Euro Accountancy &amp; Finance Services, we have dedicated accountants who can help your business partnership with all your financial needs. A business partnership shares control, responsibility and financial matters between two or more people. The partner or partners take on full liability for any debts incurred by the business and all profits would be shared equally. </strong></p>
                            <p class="has-pullquote pullquote-adelle" data-pullquote="EAFS can provide all your service needs at a highly competitive price.">Ordinary partners also take on equal responsibility and decision-making in the running of the business. There are advantages and disadvantages to entering a partnership, however, and these must be assessed before entering into a bond. </p>
                            <h4>Business Partnership Advantages:</h4>
                            <ul type="disc">
                                <li>Business      partnerships are relatively easy to establish. Make sure time is taken in      drafting partnership agreements to avoid future problems.</li>
                                <li>With more      than one owner the ability to raise funds can be increased.</li>
                                <li>The      business can benefit from&nbsp;using the knowledge base and experience of      all of the business partners. </li>
                            </ul>
                            <h4>Business Partnership Disadvantages:</h4>
                            <ul type="disc">
                                <li>Business      partners are jointly and individually liable for the actions of the other      partners.</li>
                                <li>Business      partners, like sole traders, are 100% liable for the actions of the      business.</li>
                                <li>Since      decisions are shared, disagreements can occur, and therefore the decision      making process can take longer.</li>
                                <li>Some      employee benefits are not deductible from business income on tax returns.</li>
                                <li>Many      recruitment agencies will not deal with contractors who work through a      partnership structure.</li>
                            </ul>
                            <h4>How we can help</h4>
                            <p>If you are interested in meeting with one of our accountants, we are always happy to discuss your current and potential business situation. Whether you are venturing into a new business partnership, or you are already established, we are qualified to assist you in all financial aspects of your business partnership. Take advantage of our 'value for money' expert knowledge and allow us to guide you through the potential pitfalls of a business partnership.</p>

                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                        </div><!-- end articleBody -->
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                        </article>
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_coformations.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
            </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>