<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
 <?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Company Information : Contact Us : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Visit our Contact Us page to get in touch about our services specifically for contractors and freelancers." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Company Information : Contact Us : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Visit our Contact Us page to get in touch about our services specifically for contractors and freelancers." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>company-information/contact-eafs/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Company Information : Contact Us : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>company-information/contact-eafs/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Visit our Contact Us page to get in touch about our services specifically for contractors and freelancers."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>company-information/contact-eafs/" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Contact Us, EAFS Contact Us" />
    <meta name="description" content="Visit our Contact Us page to get in touch about our services specifically for contractors and freelancers." />
    <link rel="canonical" href="<?php echo SITE_URL;?>company-information/contact-eafs/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Company Information : Contact EAFS</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->

<!-- START GOOGLE MAP-->
<section class="mapping clearfix hidden-xs">
        <div id="map-wrapper" style="height: 60px;">
            <a href="#" id="map-expand-button" class="map-expand-button" ></a>
            <div class="map" id="alpha"></div>
        </div>
</section>
<!-- END GOOGLE MAP-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb"><a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current"><span itemprop="title">Contact EAFS</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemscope itemtype="https://schema.org/Article" itemprop="Contacting EAFS" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Company Information : Contact EAFS</h2>
                        </header>
                            <p class="lead"><strong>We have established ourselves in the contractor payroll sector for over a decade, accumulating a wealth of experience and building a strong team to support your accounting needs.</strong></p>
                            <div itemprop="articleBody">
                            <p class="has-pullquote pullquote-adelle" data-pullquote="Specialist accounting services specifically for UK based contractors.">EAFS are proud to have received PCG accreditation as Specialist Contractor Accountants, paired with our expertise in contract reviews for IR35. Providing tailored Limited Company accounting services specifically for contractors, we allow you to concentrate on your area of expertise as we take care of all accountancy, tax and planning compliance. This leaves you free to enjoy your spare time or a well-deserved rest from your employment without the hassle of sorting out and ensuring legality over your tax affairs.</p>
                            </div>
                        </article>
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

            <!-- START ADDRESS FORM-->
            <section itemprop="General Enquiry Form" itemscope itemtype="https://schema.org/Article" class="doc">
                <?php require_once("../forms/enquiry.contact.inc.php");?>
            </section>
            <!-- END ADDRESS FORM-->

            <!-- START ADDRESS FORM-->
            <section itemprop="EAFS Address Block" itemscope itemtype="https://schema.org/Article" class="doc">
                <?php require_once("../partials/adresses.inc.php");?>
            </section>
            <!-- END ADDRESS FORM-->

            <!-- START ADDRESS FORM-->
            <section itemprop="Newsletter" itemscope itemtype="https://schema.org/Article" class="doc">
                <?php require_once("../partials/newsletter.inc.php");?>
            </section>
            <!-- END ADDRESS FORM-->


            <!-- START Social Widget-->
            <section itemprop="Social Links" itemscope itemtype="https://schema.org/Article" class="doc hidden-xs">
                <?php require_once("../partials/social.inc.php");?>
            </section>
            <!-- END Social Widget-->


        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_aboutlinks.inc.php");?>
                <!-- WIDGET END LINKS -->

                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->

<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts_contact.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
<script>
    var LocsA = [
        {
            lat: 55.949161,
            lon: -3.187802,
            title: 'Head Office',
            html: '<h5><strong>Euro Accountancy & Finance Services</strong></h5><p><strong>Head Office</strong><br>23 Blair Street<br>Edinburgh<br>EH 1 1QR<br>Scotland</p>',
            zoom: 8,
            icon: '<?php echo SITE_URL;?>assets/ico_scotland.png',
            show_infowindows: true,
            infowindow_type: 'bubble',
            visible: true,
            stopover: false
        },
        {
            lat: 50.840517,
            lon: 4.368860,
            title: 'Belgium Office',
            html: '<h5><strong>EAFS Consulting BVBA</strong></h5><p><strong>Belgium</strong><br>Square be Meeus 37,<br>Brussles<br>1000<br>Belgium</p>',
            zoom: 8,
            icon: '<?php echo SITE_URL;?>assets/ico_belgium.png',
            show_infowindows: true,
            infowindow_type: 'bubble',
            visible: true,
            stopover: false
        },
        {
            lat: 52.520998,
            lon: 4.961891,
            title: 'Netherlands Office',
            html: '<h5><strong>EAFS Consulting BV</strong></h5><p><strong>Netherlands</strong><br>Leeuwerikplein 101,<br>Purmerend<br>144HZ<br>Netherlands</p>',
            zoom: 8,
            icon: '<?php echo SITE_URL;?>assets/ico_holland.png',
            show_infowindows: true,
            infowindow_type: 'bubble',
            visible: true,
            stopover: false
        },
        {
            lat: 49.295020,
            lon: 8.648430,
            title: 'German Office',
            html: '<h5><strong>EAFS Consulting GmbH</strong></h5><p><strong>Germany</strong><br>Altrottstr. 31,<br>Walldorf<br>D-69190<br>Germany</p>',
            zoom: 8,
            icon: '<?php echo SITE_URL;?>assets/ico_germany.png',
            show_infowindows: true,
            infowindow_type: 'bubble',
            visible: true,
            stopover: false
        }
    ];
    new Maplace({
        map_div: document.getElementById('alpha'),
        locations: LocsA,
        controls_div: '#controls-polygon',
        controls_type: 'list',
        controls_on_map: false,
        view_all_text: "Choose Company Location",
        map_options: {
            disableDefaultUI: true
        }
    }).Load();
</script>


</body>
</html>