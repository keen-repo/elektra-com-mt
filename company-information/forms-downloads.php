<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Company Information: Forms and Downloads : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Visit our Forms and Downloads page to find all the forms you will need to start with EAFS." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Company Information : Forms and Downloads : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Visit our Forms and Downloads page to find all the forms you will need to start with EAFS." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>company-information/forms-downloads/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Company Information : Forms and Downloads : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>company-information/forms-downloads/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Visit our Forms and Downloads page to find all the forms you will need to start with EAFS."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Forms and Downloads, EAFS Forms and Downloads" />
    <meta name="description" content="Visit our Forms and Downloads page to find all the forms you will need to start with EAFS." />
    <link rel="canonical" href="<?php echo SITE_URL;?>company-information/forms-downloads/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Company Information : Forms and Downloads</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb"><a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="cjild" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb"class="current"><span itemprop="title">Forms and Downloads</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry">
                    <div class="col-lg-12">
                        <article itemprop="Forms and Downloads" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Company Information : Forms and Downloads</h2>
                        </header>

                            <p class="lead"><strong>Euro Accountancy &amp; Finance Services </strong><strong>was founded in 2004, in response to the demand for practical and professional help for new and already established Small and Medium Sized Enterprises for Contractors working in Europe.</strong></p>

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th><strong>Working in Europe Guides for Contractors</strong></th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Working in Belgium Guide</td>
                                    <td><a href="#" title="Being Updated Latest Working in Belgium Guide Available Soon" data-placement="bottom" data-toggle="tooltip" data-original-title="Guides Being Updated">Download PDF</a></td>
                                </tr>
                                <tr>
                                    <td>Working in Germany Guide</td>
                                    <td><a href="#" title="Being Updated Latest Working in Belgium Guide Available Soon" data-placement="bottom" data-toggle="tooltip" data-original-title="Guides Being Updated">Download PDF</a></td>
                                </tr>
                                <tr>
                                    <td>Working in Italy Guide</td>
                                    <td><a href="#" title="Being Updated Latest Working in Italy Guide Available Soon" data-placement="bottom" data-toggle="tooltip" data-original-title="Guides Being Updated">Download PDF</a></td>
                                </tr>
                                <tr>
                                    <td>Working in Luxembourg Guide</td>
                                    <td><a href="#" title="Being Updated Latest Working in Luxembourg Guide Available Soon" data-placement="bottom" data-toggle="tooltip" data-original-title="Guides Being Updated">Download PDF</a></td>
                                </tr>
                                <tr>
                                    <td>Working in Netherlands Guide</td>
                                    <td><a href="#" title="Being Updated Latest Working in Netherlands Guide Available Soon" data-placement="bottom" data-toggle="tooltip" data-original-title="Guides Being Updated">Download PDF</a></td>
                                </tr>
                                </tbody>
                            </table>
                            <p>&nbsp;</p>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th><strong>Application Forms, Links &amp; Downloads</strong></th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Cater Allen Bank Account (Limited Company Application Form)</td>
                                    <td><a href="#" title="Cater Allen Limited Company Application Form  Available Soon" data-placement="bottom" data-toggle="tooltip" data-original-title="Cater Allen Application">Download PDF</a></td>
                                </tr>
                                <tr>
                                    <td>Cater Allen Bank Account (ID Requirements Form)</td>
                                    <td><a href="#" title="Cater Allen ID Requirements Form Available Soon" data-placement="bottom" data-toggle="tooltip" data-original-title="Cater Allen ID">Download PDF</a></td>
                                </tr>
                                <tr>
                                    <td>Mortgages for Contractors</td>
                                    <td><a href="https://www.contractormortgagesuk.com/?utm_source=eafs&utm_medium=Link&utm_campaign=Partner" title="Link to Contractor Mortgages Website" target="_blank" data-placement="bottom" data-toggle="tooltip" data-original-title="Link to Contractor Mortgages Website">Contractor Mortgages Website</a></td>
                                </tr>
                                <tr>
                                    <td height="37">Insurance for Contractors</td>
                                    <td><a href="https://eafs.kpsol.co.uk/" title="Link to the Knightsbridge Professional Website" target="_blank" target="_blank" data-placement="bottom" data-toggle="tooltip" data-original-title="Link to the Knightsbridge Professional Website">Knightsbridge Professional Website</a></td>
                                </tr>
                                </tbody>
                            </table>

                            <strong>EAFS are proud to be approved by the PCG as an Accredited Accountancy firm for Contractors.</strong>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                        </article>
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->
        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">

                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_aboutlinks.inc.php");?>
                <!-- WIDGET END LINKS -->

                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->

        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->

<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->

<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->


</body>
</html>