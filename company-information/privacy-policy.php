<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Company Information: Privacy Policy : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>"" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Visit our Privacy Policy page to understand the stance taken in relation to your information used after input through our website." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Company Information : Privacy Policy : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Visit our Privacy Policy page to understand the stance taken in relation to your information used after input through our website." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>company-information/privacy-policy/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Company Information : Privacy Policy : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>company-information/privacy-policy/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Visit our Privacy Policy page to understand the stance taken in relation to your information used after input through our website."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Terms of Use, EAFS Terms of Use" />
    <meta name="description" content="Visit our Privacy Policy page to understand the stance taken in relation to your information used after input through our website." />
    <link rel="canonical" href="<?php echo SITE_URL;?>company-information/privacy-policy/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Company Information : Privacy Policy </h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb"><a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current"><span itemprop="title">Privacy Policy </span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemscope itemtype="https://schema.org/Article" itemprop="Privacy Policy" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Company Information : Privacy Policy </h2>
                        </header>
                            <div itemprop="articleBody">
                            <p>This privacy policy sets out how <strong>Euro Accountancy &amp; Finance Services </strong>uses and protects any information that you give <strong>Euro Accountancy &amp; Finance Services</strong>when you use this website. <strong>Euro Accountancy &amp; Finance Services</strong> is committed to ensuring that your privacy is protected. Should we ask  you to provide certain information by which you can be identified when  using this website, then you can be assured that it will only be used in  accordance with this privacy statement. <strong>Euro Accountancy &amp; Finance Services </strong>may change this policy from time to time by updating this page. You  should check this page from time to time to ensure that you are happy  with any changes.</p>
                            <h4>What we collect</h4>
                            <p>We may collect the following information:</p>
                            <ul>
                                <li>name</li>
                                <li>contact information including email address</li>
                                <li>demographic information such as postcode, preferences and interests</li>
                                <li>other information relevant to customer surveys and/or offers</li>
                            </ul>
                            <p>For the exhaustive list of cookies we collect see the <strong>List of cookies we collect</strong> section.</p>
                            <h4>What we do with the information we gather</h4>
                            <p>We require this information to understand your needs and provide you  with a better service, and in particular for the following reasons:</p>
                            <ul>
                                <li>Internal record keeping.</li>
                                <li>We may use the information to improve our products and services.</li>
                                <li>We may periodically send promotional emails about new products,  special offers or other information which we think you may find  interesting using the email address which you have provided.</li>
                                <li>From time to time, we may also use your information to contact you  for market research purposes. We may contact you by email, phone, fax or  mail. We may use the information to customise the website according to  your interests.</li>
                            </ul>
                            <h4>Security</h4>
                            <p>We are committed to ensuring that your information is secure. In  order to prevent unauthorised access or disclosure, we have put in place  suitable physical, electronic and managerial procedures to safeguard  and secure the information we collect online.</p>
                            <h4>How we use cookies</h4>
                            <p>A cookie is a small file which asks permission to be placed on your  computer’s hard drive. Once you agree, the file is added and the cookie  helps analyse web traffic or lets you know when you visit a particular  site. Cookies allow web applications to respond to you as an individual.  The web application can tailor its operations to your needs, likes and  dislikes by gathering and remembering information about your  preferences.</p>
                            <p>We use traffic log cookies to identify which pages are being used.  This helps us analyse data about web page traffic and improve our  website in order to tailor it to customer needs. We only use this  information for statistical analysis purposes and then the data is  removed from the system.</p>
                            <p>Overall, cookies help us provide you with a better website, by  enabling us to monitor which pages you find useful and which you do not.  A cookie in no way gives us access to your computer or any information  about you, other than the data you choose to share with us. You can  choose to accept or decline cookies. Most web browsers automatically  accept cookies, but you can usually modify your browser setting to  decline cookies if you prefer. This may prevent you from taking full  advantage of the website.</p>
                            <h4>Links to other websites</h4>
                            <p>Our website may contain links to other websites of interest. However,  once you have used these links to leave our site, you should note that  we do not have any control over that other website. Therefore, we cannot  be responsible for the protection and privacy of any information which  you provide whilst visiting such sites and such sites are not governed  by this privacy statement. You should exercise caution and look at the  privacy statement applicable to the website in question.</p>
                            <h4>Controlling your personal information</h4>
                            <p>You may choose to restrict the collection or use of your personal information in the following ways:</p>
                            <ul>
                                <li>whenever you are asked to fill in a form on the website, look for  the box that you can click to indicate that you do not want the  information to be used by anybody for direct marketing purposes.</li>
                                <li>if you have previously agreed to us using your personal information  for direct marketing purposes, you may change your mind at any time by  writing to or emailing us at <strong>privacy@eafs.eu</strong></li>
                            </ul>
                            <p>We will not sell, distribute or lease your personal information to  third parties unless we have your permission or are required by law to  do so. We may use your personal information to send you promotional  information about third parties which we think you may find interesting  if you tell us that you wish this to happen.</p>
                            <p>You may request details of personal information which we hold about  you under the Data Protection Act 1998. A small fee will be payable. If  you would like a copy of the information held on you please write to</p>
                            <p>Euro Accountancy &amp; Finance Services<br>
                                23 Blair Street,<br>
                                Edinburgh,<br>
                                EH1 1QR</p>
                            <p>If you believe that any information we are holding on you is  incorrect or incomplete, please write to or email us as soon as  possible, at the above address. We will promptly correct any information  found to be incorrect.</p>
                            <h4><a name="list"></a>List of cookies we collect</h4>
                            <p>The table below lists the cookies we collect and what information they store.</p>
                            <table class="table table-striped"> <thead> <tr> <th width="200">Table Header</th> <th>Table Header</th> </tr> </thead> <tbody> <tr> <td><strong>Google Analytics </strong></td>
                                <td>Google Analytics collects anonymous  statistical data about how you use our website. The information is used  to track the performance of aurora-consulting.biz and to help us improve  the service we offer to you <strong><br>
                                    To reject or delete these cookies:</strong> <a target="_blank" href="https://www.google.com/intl/en/privacypolicy.html" title="Link to Google Website">https://www.google.com/intl/en/privacypolicy.html</a></td> </tr> <tr> <td>GOOGLE__utma Cookie</td> <td>It tracks visitors. Metrics associated with  the Google __utma cookie include: first visit (unique visit), last visit  (returning visit).</td> </tr> <tr> <td>GOOGLE__utmb Cookie</td> <td>These cookies work in tandem to calculate visit length. Google __utmb cookie demarks the exact arrival time.</td> </tr>
                            <tr>
                                <td>GOOGLE__utmc Cookies</td>
                                <td>then Google __utmc registers the precise exit time of the user.</td>
                            </tr>
                            <tr>
                                <td>GOOGLE__utmz Cookie</td>
                                <td>Monitors the HTTP Referrer and notes where a  visitor arrived from, with the referrer siloed into type (Search engine  (organic or cpc), direct, social and unaccounted).</td>
                            </tr>
                            <tr>
                                <td>GOOGLE__utmv Cookie</td>
                                <td>It is a persistant cookie. It is used for  segmentation, data experimentation and the __utmv works hand in hand  with the __utmz cookie to improve cookie targeting capabilities.</td>
                            </tr>
                            <tr>
                                <td><strong>Twitter</strong></td>
                                <td>If you use a ‘Tweet’ or ‘Twitter’ link on our  website to share content on twitter, twitter sets a cookie to remember  that you’ve already shared this information. <strong><br>
                                    To reject or delete these cookies:</strong> <a target="_blank" href="https://twitter.com/privacy" title="Link to Twitter Website">https://twitter.com/privacy</a></td>
                            </tr>
                            <tr>
                                <td><strong>Google+</strong></td>
                                <td>If you use a ‘+1′ link on our website to share  content on Google +, Google sets a cookie to remember that you’ve  already shared this information. <strong><br>
                                    To reject or delete these cookies:</strong> <a target="_blank" href="https://www.google.co.uk/intl/en/policies/privacy/" title="Link to Google Website">https://www.google.co.uk/intl/en/policies/privacy/</a></td>
                            </tr>
                            <tr>
                                <td><strong>Linkedin</strong></td>
                                <td>If you use a ‘Linkedin’ link on our website to  share content on Linkedin, Linkedin sets a cookie to remember that  you’ve already shared this information.<strong><br>
                                    To reject or delete these cookies:</strong> <a target="_blank" href="https://www.linkedin.com/static?key=privacy_policy&amp;trk=hb_ft_priv" title="Link to Linkedin Website"> https://www.linkedin.com/static?key=privacy_policy&amp;trk=hb_ft_priv</a></td>
                            </tr>
                            <tr>
                                <td><strong>Facebook Like or Send</strong></td>
                                <td>If you use a ‘like’ or ‘send’ link on our  website to share content on Facebook, Facebook sets a cookie to remember  that you’ve already shared this information. <strong><br>
                                    To reject or delete these cookies:</strong> <a target="_blank" href="https://www.facebook.com/about/privacy/" title="Link to Facebook Website">https://www.facebook.com/about/privacy/</a></td>
                            </tr>
                            </tbody>
                            </table>
                            <h4>Internal cookies we collect</h4>
                            <p>The following cookie are set by our CMS and cookie approval script and are non intrusive.</p>

                            <hr class="sidedash">
                            <p><strong>Ready to start putting some distance between your company and its competitors?</strong> Lets talk about your requirements and see how we can help! We'll be more than happy to share some of our valuable knowledge with you over a coffee or online.</p>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article>
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->
        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">

                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_aboutlinks.inc.php");?>
                <!-- WIDGET END LINKS -->

                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->

<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->

<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->


</body>
</html>