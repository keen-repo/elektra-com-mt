<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Company Information: Terms of Use : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Visit our Terms of Use page to understand your position using our information presented on our website." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Company Information : Terms of Use : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Visit our Terms of Use page to understand your position using our information presented on our website." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>company-information/terms-conditions/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Company Information : Terms of Use : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>company-information/terms-conditions/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Visit our Terms of Use page to understand your position using our information presented on our website."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Terms of Use, EAFS Terms of Use" />
    <meta name="description" content="Visit our Terms of Use page to understand your position using our information presented on our website." />
    <link rel="canonical" href="<?php echo SITE_URL;?>company-information/terms-conditions/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Company Information : Terms Conditions </h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb"><a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current"><span itemprop="title">Terms Conditions </span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemscope itemtype="https://schema.org/Article" itemprop="Terms & Conditions" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Company Information : Terms Conditions </h2>
                        </header>
                            <div itemprop="articleBody">
                            <p>The contained information within this website is for guidance purposes on matters of interest only. The lawful impacts through application and implementation can be vastly different based on any specific facts involved. Taking into consideration the constant changing nature of laws, regulations and rules with the inherent pitfalls and hazards of communication through electronic devices, there could be delays alongside omissions or inaccuracies in relevant information contained within this website.</p>
                            <p>Relevantly, the information provided on this site is so with an understanding that the publisher and author are not engaged herein with rendering any legality, accountancy or other form of professional advice or services. Therefore, the information on this website should not be used as a substitute for consultation with an EAFS or other competent contracting or industry professional. Before making<em> any </em>decision or taking any action of any kind in regards to your financial contracting matters, we advise that you should consult an EAFS tax professional. </p>
                            <p>While every attempt to make sure that all information contained within this site has been obtained from governmental, banking and reliable sources, Euro Accountancy &amp; Finance Services (EAFS) is not responsible for any errors, mistakes, and omissions or out dated information or results obtained based on the information from this website.  All data and information within these pages is provided in an &ldquo;as is&rdquo; form with no guarantee of completeness, timeliness, accuracy or of any result obtained by using the aforementioned information, and without any warranty of any kind – expressed or implied – including, but not limited to, warranties of performance, fitness for use or merchantability for any particular purpose.</p>
                            <p>In no event can, or will, EAFS or its related corporations, partnerships, partners, agents or employees be held responsible or liable to you or anyone else for any action taken or decisions made in reliance on information from and in this website or any consequential, similar or special damages. This also covers even if advised of the possibility of any such damages.</p>
                            <p>Links in this website and through its subsequent webpages will, and do, connect to other websites which are therefore maintained and managed by third parties, of whom EAFS have no control over in any form. EAFS therefore makes absolutely no representations as to the accuracy or any other aspect in terms of information contained within other websites of which EAFS link to.</p>
                            <p>Euro Accountancy &amp; Finance Services (EAFS) provide social security and tax advice and related management services to contractors, agencies and others. Should any users of this website wish to rely on the content of the website in any way whatsoever, or on any responses received to enquiries made through this website in any way whatsoever, then this should be brought to the attention of EAFS employees by contacting the company, through which you should state your full personal details, the applicable facts and how the person wishes to use the content or response for his/her benefit personally.</p>
                            <p>The content of this site is general in nature and should not and may not be used for specific situations or circumstances without prior contact with aforementioned EAFS professionals, confirming if or if not these rulings, regulations or other factors indeed apply to your situation. Specific responses are given on specific facts provided. Any response is subject to all current applicability&rsquo;s of tax laws, regulations and practice, of which may change at any given time. </p>
                            <p>Therefore accordingly, any user of this site or any other service that may follow from the use of this site agrees and accepts that the user accepts the taxation consequences will, in all circumstantial situations, be &ldquo;uncertain&rdquo; and, hence, that no legal action whatsoever may be taken against the owners, associates, contributors, authors, publishers or any other party whatsoever in any way related to this website.</p>
                            <p>Continued use of this site after any changes are made to the terms of usage establishes acceptance of those posted changes. EAFS also reserves the right to take down or make this website unavailable at any given time or to restrict access to all or parts of the site without due notice.</p>
                            <p>If you should choose to register on this site, you will be asked to insert a username and password through your log in details. You are solely responsible for maintaining the confidentiality of these log in details and must not allow anyone else to use the information. You should immediately inform EAFS of any unauthorised use of your log in details.</p>
                            <p>Should the user wish to use content or engage in tax or financial planning in any way on which any reliance on content could be or may be placed, EAFS should be notified immediately thereof and an appropriate professional charge and amount will be agreed in order to obtain Euro Accountancy &amp; Finance Services (EAFS) professional opinion and/or advice prior to any such use, reliance or planning of any kind.</p>
                            <p>All these conditions of use apply hereby to all and any content on the domain <a href="https://www.eafs.eu" title="Linkto Euro Accountancy &amp; Finance Services" target="_self">www.eafs.eu</a> and subsequent blogs edinburgheafs.tumblr.com, eafsedinburgh.tumblr.com, edinburgheafs.wordpress.com and blog.eafs.eu. These may be updated from time to time and as may be seen and/or deemed appropriate and the user agrees to be bound by such updates agreeing to familiarise themselves therewith.</p>
                            <p>In line and accordance with recommendations from our professional bodies, these statements and this letter confirms the basis on which we will provide services to you and not to rely on information on this website solely without consulting with a professional from Euro Accountancy &amp; Finance Services (EAFS) in order to avoid misinterpretation, misunderstandings and general ignorance of our respective responsibilities.  </p>

                            <hr class="sidedash">
                            <p><strong>Ready to start putting some distance between your company and its competitors?</strong> Lets talk about your requirements and see how we can help! We'll be more than happy to share some of our valuable knowledge with you over a coffee or online.</p>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article>
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->
        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_aboutlinks.inc.php");?>
                <!-- WIDGET END LINKS -->

                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->

        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->

<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->

<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->


</body>
</html>