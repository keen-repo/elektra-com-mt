<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Company Information</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/about-eafs/" title="Link to About us Page"><div itemprop="name">About us</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/contact-eafs/" title="Link to Contact us Page"><div itemprop="name">Contact us</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/privacy-policy/" title="Link to Privacy Policy Page"><div itemprop="name">Privacy Policy</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/terms-conditions/" title="Link to Terms & Conditions Page"><div itemprop="name">Terms & Conditions</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/impressum/" title="Impressum"><div itemprop="name">Impressum</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/forms-downloads/" title="Link to Forms & Downloads Page"><div itemprop="name">Forms & Downloads</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/payroll-calculators/" title="Link to Payroll Calculators Page"><div itemprop="name">Payroll Calculators</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/join-eafs/" title="Link to Join EAFS Page"><div itemprop="name">Join EAFS</div></a></li>
    </ul>
</section>