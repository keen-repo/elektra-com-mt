;(function($){
    $('#00N2X000008sls1').change(function() {
        var $dropdown = $(this);
        $.getJSON("http://www.eafs.eu/jsondata/data.json", function(data) {
            var key = $dropdown.val();
            var vals = [];
            switch(key) {
                case 'Belgium':
                    vals = data.Belgium.split(",");
                    break;
                case 'CzechRepublic':
                    vals = data.CzechRepublic.split(",");
                    break;
                case 'Denmark':
                    vals = data.Denmark.split(",");
                    break;
                case 'France':
                    vals = data.France.split(",");
                    break;
                case 'Germany':
                    vals = data.Germany.split(",");
                    break;
                case 'Italy':
                    vals = data.Italy.split(",");
                    break;
                case 'Latvia':
                    vals = data.Latvia.split(",");
                    break;
                case 'Luxembourg':
                    vals = data.Luxembourg.split(",");
                    break;
                case 'Malta':
                    vals = data.Malta.split(",");
                    break;
                case 'Netherlands':
                    vals = data.Netherlands.split(",");
                    break;
                case 'Norway':
                    vals = data.Norway.split(",");
                    break;
                case 'Poland':
                    vals = data.Poland.split(",");
                    break;
                case 'Spain':
                    vals = data.Spain.split(",");
                    break;
                case 'Sweden':
                    vals = data.Sweden.split(",");
                    break;
                case 'UK':
                    vals = data.UK.split(",");
                    break;
                case 'base':
                    vals = ['Please choose from above'];
            }
            var $jsontwo = $('#00N2X000008sls2');
            $jsontwo.empty();
            $.each(vals, function(index, value) {
                $jsontwo.append('<option value="' + value + '">' + value + '</option>');
                //$jsontwo.append("<option>" + value + "</option>");
            });
        });
    });
})(jQuery);

