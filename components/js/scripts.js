;
(function($) {

    // Page Preloader
    $(window).load(function() {
        $(".loader").delay(300).fadeOut();
        $(".animationload").delay(600).fadeOut("slow");
    });

    // Parallax
    $(window).bind('body', function() {
        parallaxInit();
    });

    function parallaxInit() {
        $('#one-parallax').parallax("30%", 0.1);
        $('#two-parallax').parallax("30%", 0.1);
        $('#three-parallax').parallax("30%", 0.1);
        $('#four-parallax').parallax("30%", 0.4);
        $('#five-parallax').parallax("30%", 0.4);
        $('#six-parallax').parallax("30%", 0.4);
        $('#seven-parallax').parallax("30%", 0.4);
    }

    // DM Top
    $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
            $('.dmtop').css({
                bottom: "25px"
            });
        } else {
            $('.dmtop').css({
                bottom: "-100px"
            });
        }
    });
    $('.dmtop').click(function() {
        $('html, body').animate({
            scrollTop: '0px'
        }, 800);
        return false;
    });

    // TOOLTIP
    $('.social-icons, .bs-example-tooltips, .table-striped, .doc').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });

    // POPOVER
    $('.buddy_tooltip').popover({
        container: '.buddy_carousel, .buddy_members'
    });

    // ACCORDION
    var iconOpen = 'fa fa-minus',
        iconClose = 'fa fa-plus';

    $(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function(e) {
        var $target = $(e.target)
        $target.siblings('.accordion-heading')
            .find('em').toggleClass(iconOpen + ' ' + iconClose);
        if (e.type === 'show')
            $target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
        if (e.type === 'hide')
            $(this).find('.accordion-toggle').not($target).removeClass('active');
    });

    // Validate Belgium
    $('#enquiryFormBelgium').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
       fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });
    // Validate Holland
    $('#enquiryFormDutch').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });
    // Validate Italy
    $('#enquiryFormItaly').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });
    // Validate Luxembourg
    $('#enquiryFormLuxembourg').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });
    // Validate Germany
    $('#enquiryFormGermany').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });
    // Validate Germany
    $('#enquiryFormGeneral').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });

    // Show Div
    $('#00N2X000008sm2S').on('change', function() {
        if (this.value === 'Corporate') {
            // language=JQuery-CSS
            $('#business').show();
        } else if (this.value === 'Agency') {
            // language=JQuery-CSS
            $('#business').show();
        } else {
            // language=JQuery-CSS
            $('#business').hide();
        }
    });

    //
    $(document).ready(function() {
        $('#00N2X000008sm2S').change(function() {
            if (this.value === "Agency") {
                $('#leadtype').val("Business Lead");
            } else if (this.value === "Contractor") {
                $('#leadtype').val("Person Lead");
            } else if (this.value === "Corporate") {
                $('#leadtype').val("Business Lead");
            }
        });
    });

    // WOW
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: true, // default
        live: true // default
    })
    wow.init();


    // Hide Honeypot
    $(".bottrap").hide();

    // Google Maps Plugin Script
    $('#map-expand-button').bind('click', function(event) {
        event.preventDefault();
        var map = $('#map-wrapper');
        var height = parseInt(map.css('height'));
        if (height == 60) map.animate({
            height: '400px'
        }, {
            duration: 500,
            easing: 'easeOutQuint'
        });
        else map.animate({
            height: '60px'
        }, {
            duration: 200,
            easing: 'easeOutQuint'
        });
    });

})(jQuery);