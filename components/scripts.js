/**
* Euro Accountancy Theme v1.0.0 by Aurora Consulting (Scotland)
* Copyright 2018 Aurora Consulting (Scotland)
* Licensed under .
*
* Designed and built with all the love in the world by @auroralabs
*/
;(function($){
    $('#00N2X000008sls1').change(function() {
        var $dropdown = $(this);
        $.getJSON("http://www.eafs.eu/jsondata/data.json", function(data) {
            var key = $dropdown.val();
            var vals = [];
            switch(key) {
                case 'Belgium':
                    vals = data.Belgium.split(",");
                    break;
                case 'Czech Republic':
                    vals = data.CzechRepublic.split(",");
                    break;
                case 'Denmark':
                    vals = data.Denmark.split(",");
                    break;
                case 'France':
                    vals = data.France.split(",");
                    break;
                case 'Germany':
                    vals = data.Germany.split(",");
                    break;
                case 'Italy':
                    vals = data.Italy.split(",");
                    break;
                case 'Latvia':
                    vals = data.Latvia.split(",");
                    break;
                case 'Luxembourg':
                    vals = data.Luxembourg.split(",");
                    break;
                case 'Malta':
                    vals = data.Malta.split(",");
                    break;
                case 'Netherlands':
                    vals = data.Netherlands.split(",");
                    break;
                case 'Norway':
                    vals = data.Norway.split(",");
                    break;
                case 'Poland':
                    vals = data.Poland.split(",");
                    break;
                case 'Spain':
                    vals = data.Spain.split(",");
                    break;
                case 'Sweden':
                    vals = data.Sweden.split(",");
                    break;
                case 'UK':
                    vals = data.UK.split(",");
                    break;
                case 'base':
                    vals = ['Please choose from above'];
            }
            var $jsontwo = $('#00N2X000008sls2');
            $jsontwo.empty();
            $.each(vals, function(index, value) {
                $jsontwo.append('<option value="' + value + '">' + value + '</option>');
                //$jsontwo.append("<option>" + value + "</option>");
            });
        });
    });
})(jQuery);

;(function ($, window, undefined) {
    var $allDropdowns = $();
    $.fn.dropdownHover = function (options) {
        $allDropdowns = $allDropdowns.add(this.parent());
        return this.each(function () {
            var $this = $(this),
                $parent = $this.parent(),
                defaults = {
                    delay: 400,
                    instantlyCloseOthers: true
                },
                data = {
                    delay: $(this).data('delay'),
                    instantlyCloseOthers: $(this).data('close-others')
                },
                showEvent = 'show.bs.dropdown',
                hideEvent = 'hide.bs.dropdown',
            // shownEvent = 'shown.bs.dropdown',
            // hiddenEvent = 'hidden.bs.dropdown',
                settings = $.extend(true, {}, defaults, options, data),
                timeout;

            $parent.hover(function (event) {
                // so a neighbor can't open the dropdown
                if(!$parent.hasClass('open') && !$this.is(event.target)) {
                    // stop this event, stop executing any code
                    // in this callback but continue to propagate
                    return true;
                }

                $allDropdowns.find(':focus').blur();

                if(settings.instantlyCloseOthers === true)
                    $allDropdowns.removeClass('open');

                window.clearTimeout(timeout);
                $parent.addClass('open');
                $this.trigger(showEvent);
            }, function () {
                timeout = window.setTimeout(function () {
                    $parent.removeClass('open');
                    $this.trigger(hideEvent);
                }, settings.delay);
            });

            // this helps with button groups!
            $this.hover(function () {
                $allDropdowns.find(':focus').blur();

                if(settings.instantlyCloseOthers === true)
                    $allDropdowns.removeClass('open');

                window.clearTimeout(timeout);
                $parent.addClass('open');
                $this.trigger(showEvent);
            });

            // handle submenus
            $parent.find('.dropdown-submenu').each(function (){
                var $this = $(this);
                var subTimeout;
                $this.hover(function () {
                    window.clearTimeout(subTimeout);
                    $this.children('.dropdown-menu').show();
                    // always close submenu siblings instantly
                    $this.siblings().children('.dropdown-menu').hide();
                }, function () {
                    var $submenu = $this.children('.dropdown-menu');
                    subTimeout = window.setTimeout(function () {
                        $submenu.hide();
                    }, settings.delay);
                });
            });
        });
    };
    $(document).ready(function () {
        // apply dropdownHover to all elements with the data-hover="dropdown" attribute
        $('[data-hover="dropdown"]').dropdownHover();
    });
    $('.dropdown-toggle').dropdownHover().dropdown();
    $(document).on('click', '.yamm .dropdown-menu', function(e) {
        e.stopPropagation()
    })
})(jQuery, this);
;(function($){

// Page Preloader
    $(window).load(function() {
        $(".loader").delay(300).fadeOut();
        $(".animationload").delay(600).fadeOut("slow");
    });

// Parallax
    $(window).bind('body', function() {
        parallaxInit();
    });
    function parallaxInit() {
        $('#one-parallax').parallax("30%", 0.1);
        $('#two-parallax').parallax("30%", 0.1);
        $('#three-parallax').parallax("30%", 0.1);
        $('#four-parallax').parallax("30%", 0.4);
        $('#five-parallax').parallax("30%", 0.4);
        $('#six-parallax').parallax("30%", 0.4);
        $('#seven-parallax').parallax("30%", 0.4);
    }

// DM Top
    $(window).scroll(function(){
        if ($(this).scrollTop() > 1) {
            $('.dmtop').css({bottom:"25px"});
        } else {
            $('.dmtop').css({bottom:"-100px"});
        }
    });
    $('.dmtop').click(function(){
        $('html, body').animate({scrollTop: '0px'}, 800);
        return false;
    });

// TOOLTIP
    $('.social-icons, .bs-example-tooltips, .table-striped, .doc').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });

// POPOVER
    $('.buddy_tooltip').popover({
        container: '.buddy_carousel, .buddy_members'
    });

// ACCORDION
    var iconOpen = 'fa fa-minus',
        iconClose = 'fa fa-plus';

    $(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function (e) {
        var $target = $(e.target)
        $target.siblings('.accordion-heading')
            .find('em').toggleClass(iconOpen + ' ' + iconClose);
        if(e.type === 'show')
            $target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
        if(e.type === 'hide')
            $(this).find('.accordion-toggle').not($target).removeClass('active');
    });

// Validate Belgium
    $('#enquiryFormBelgium').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
        });
// Validate Holland
    $('#enquiryFormDutch').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });
// Validate Italy
    $('#enquiryFormItaly').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });
// Validate Luxembourg
    $('#enquiryFormLuxembourg').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });
// Validate Germany
    $('#enquiryFormGermany').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });
// Validate Germany
    $('#enquiryFormGeneral').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'The contact number is required and cannot be empty'
                    }
                }
            },
            // country: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The country name is required and cannot be empty'
            //         }
            //     }
            // },
            // city: {
            //     validators: {
            //         notEmpty: {
            //             message: 'The city name is required and cannot be empty'
            //         }
            //     }
            // },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    });

// Show Div
    $('#00N2X000008sm2S').on('change', function() {
        if ( this.value === 'Corporate')
        {
            // language=JQuery-CSS
            $('#business').show();
        }
        else if ( this.value === 'Agency')
        {
            // language=JQuery-CSS
            $('#business').show();
        }
        else
        {
            // language=JQuery-CSS
            $('#business').hide();
        }
    });

    //
    $(document).ready(function(){
        $('#00N2X000008sm2S').change(function() {
            if (this.value === "Agency") {
                $('#leadtype').val("Business Lead");
            } else if (this.value === "Contractor") {
                $('#leadtype').val("Person Lead");
            } else if (this.value === "Corporate") {
                $('#leadtype').val("Business Lead");
            }
        });
    });

// WOW
    wow = new WOW(
        {
            boxClass:     'wow',      // default
            animateClass: 'animated', // default
            offset:       0,          // default
            mobile:       true,       // default
            live:         true        // default
        }
    )
    wow.init();


// Hide Honeypot
    $(".bottrap").hide();

    // Google Maps Plugin Script
    $('#map-expand-button').bind('click',function(event)
    {
        event.preventDefault();
        var  map = $('#map-wrapper');
        var height=parseInt(map.css('height'));
        if(height==60) map.animate({ height:'400px'}, {duration:500, easing:'easeOutQuint'}
        );
        else map.animate({ height:'60px'}, {duration:200,easing:'easeOutQuint'}
        );
    });

})(jQuery);