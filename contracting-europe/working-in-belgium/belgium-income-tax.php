<?php require_once("../../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Belgium Income Tax : Working in Belgium : EAFS Consulting BVBA</title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Euro Accountancy &amp; Finance Services" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="We can assist in helping contractors with information and advice on income tax in Belgium. Visit our Belgium Income Tax page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contracting in Europe : Belgium Income Tax <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Our Belgium Payroll Solution is very straightforward &amp; requires minimal input from you. Visit our Working in Belgium page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-income-tax/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contracting in Europe : Belgium Income Tax <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-income-tax/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="We can assist in helping contractors with information and advice on income tax in Belgium. Visit our Belgium Income Tax page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="pindex,follow" />
    <meta name="googlebot" content="noarchive" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="European Payroll, Working in Europe, Working in Belgium, Belgium Income Tax" />
    <meta name="description" content="We can assist in helping contractors with information and advice on income tax in Belgium. Visit our Belgium Income Tax page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-income-tax/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Belgium Income Tax</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="beadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/" title="European Contractors" alt="European Contractors"><span itemprop="title">European Contractors</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/" title="Working in Belgium" alt="Working in Belgium"><span itemprop="title">Working in Belgium</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title"><strong>Belgium Income Tax</strong></span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Belgium Income Tax" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Belgium Income Tax</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p>You can find some general information on income tax rates, in relation to Belgium on this page. This guide also covers the income tax base for residents and non-residents, allowable tax credits and deductions, the special expatriate tax regime, capital income tax rates and information on double-taxation treaties. </p>

                            <h4>Income Tax Base For Residents and Non-Residents of Belgium</h4>

                            <dl class="dl-horizontal">
                                <dt><strong>Residents of Belgium</strong> </dt>
                                <dd>are subject to personal income tax on their total income, from all sources. Rates vary between 25% and 50%. Residents also pay additional municipal taxes at rates varying between 0% and 9%  of the total payable income tax.</dd>
                                <dt><strong>Non-residents of Belgium</strong> </dt>
                                <dd>are subject to personal income tax on Belgian source income only, notably on Belgian source professional income and on property income located in Belgium and on any Belgian source  investment.</dd>
                            </dl>

                            <p><strong>Individuals will be regarded as tax residents if:</strong></p>
                                <br />
                            <ul>
                                <li>Their main home or center of economic interests is in Belgium.</li>
                                <li>They are registered within the civil register.</li>
                                <li>They are present in Belgium for more than 183 days in any 12-month period.<br>
                                </li>
                            </ul>

                            <p><strong>Allowable Deductions and Tax Credits</strong></p>
                            <p>To know the requirements for expenses that are deductible in personal income tax, consult the<a href="https://www.belgium.be/en/taxes/income_tax/" title="Link to the Portal of the Belgium Government" target="_blank"> portal of the Belgian Government</a>.</p>
                            <p><strong>Special Expatriate Tax Regime</strong><br>
                                <br>
                                Any expatriate employed in Belgium will normally be liable towards Belgian Income Tax. Additionally, gift duty, property tax and succession duty can be relevant also. Residents of Belgium pay personal income tax on their total income from all sources worldwide on a sliding scale. Residents are also liable to pay communal and regional taxes at rates between 0% and 8/5% of the total tax payable from income. Alongside this, the special expatriate tax status also offers two imperative tax benefits to foreign executives: the &ldquo;tax free expatriation allowances&rdquo; and the &ldquo;travel exclusion&rdquo;.</p>
                            <p>
                                <strong>Capital Tax Rate</strong>
                            </p><br>
                            <ul>
                                <li>A 15% withholding tax is imposed on earnings from mutual funds investing more than 40% of their assets in interest-bearing securities.</li>
                                <li>Inheritance tax ranges between 3% and 30% in Wallonia and Brussels regions with 3% to 27% in Flanders.</li>
                                <li>A 16.5% tax is payable on gains on property held for less than 5 years.</li>
                                <li>There is no wealth tax in Belgium.</li>
                            </ul>
                            <h4><strong>Double Taxation Treaties </strong></h4>

                            <dl class="dl-horizontal">
                                <dt><strong>Countries With Whom a Double Taxation Treaty Have Been Signed</strong></dt>
                                <dd><a href="https://fiscus.fgov.be/interfafznl/fr/downloads/cdiListe.pdf" title="Link to the List of the double Taxation Treaties" target="_BLANK">Services Publics Fédéral des Finances (FISCUS)</a>, (<em>List of the double Taxation Treaties</em>)</dd>
                                <dt> <strong>Withholding Taxes                                 Dividends:</strong> </dt>
                                <dd>25% (15% for certain category of  shares), Interest: 25%  (A 15% rate applies to interest on certain  government bonds and  interest from regulated savings deposits in excess  of the tax-exempt  amount), Royalties: 25% (Rate is 15% for income from  author&rsquo;s and  neighboring rights and from legal and compulsory  licenses). </dd>
                                <dt><strong>Bilateral Agreement </strong></dt>
                                <dd>The United Kingdom and Belgium are bound by a <a href="https://www.hmrc.gov.uk/taxtreaties/in-force/belgium-dtc.pdf" title="Link to the HMRC Taxation Treaty" target="_blank">double taxation treaty</a>.</dd>
                            </dl>


                            <h4><strong>Filing Date</strong></h4>
                            <p>Usually by the 30th of June of the assessment year.</p>
                            <h4>Figures Based on Sources </h4>
                            <p><strong>Tax Authorities</strong> </p>
                            <ul>
                                <li><a href="https://www.minfin.fgov.be/portail2/en/index.htm#_blank" title="Link to the Tax Administration" target="_self">Services Publics Fédéral des Finances (FISCUS)</a>, Tax Administration.</li>
                                <li><a href="https://www.minfin.fgov.be/portail2/en/index.htm#_blank" title="Link to the Belgium Ministry of Finance" target="_blank">Belgium Ministry of Finance</a></li>
                            </ul>
                            <br>
                            <!--START WORKING IN Belgium FORM-->
                            <?php require_once("../../forms/enquiry.belgium.inc.php");?>
                            <!--END WORKING IN Belgium FORM-->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complementary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_belgiumimage.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_belgiumlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->

<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->

<!-- START FOOTER ELEMENTS-->
<?php require_once("../../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>