<?php require_once("../../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Belgium Visa : Working in Belgium : EAFS Consulting BVBA</title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="We can assist in helping contractors with information and advice on Visas in Belgium. Visit our Belgium Visas page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Working in Belgium : Belgium Visa : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="We can assist in helping contractors with information and advice on Visas in Belgium. Visit our Belgium Visas page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-visas/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Working in Belgium : Belgium Visa : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-visas/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="We can assist in helping contractors with information and advice on Visas in Belgium. Visit our Belgium Visas page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="noarchive" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="European Payroll, Working in Europe, Working in Belgium, Contracting in Belgium, Belgium Visa, Belgium Visas" />
    <meta name="description" content="We can assist in helping contractors with information and advice on Visas in Belgium. Visit our Belgium Visas page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-visas/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Belgium Visa</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/" title="European Contractors" alt="European Contractors"><span itemprop="title">European Contractors</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/" title="Working in Belgium" alt="Working in Belgium"><span itemprop="title">Working in Belgium</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title"><strong>Belgium Visa</strong></span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Belgium Visa" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Belgium Visa</h2>
                        </header>
                            <div itemprop="articleBody">
                            <h4><strong>Work Permit for Employees</strong></h4>
                            <p>The employer in Belgium must apply to the competent authority, via the public employment services, for an employment permit on behalf of the relevant national. If the employer is granted the employment permit, the worker will be granted a permit for work. The main criteria to obtain a work permit is that a suitable worker can be found on the labour market, to fill the vacancy, within a reasonable time, even after providing the necessary vocational training.</p>
                            <p>There are 3 different types of work permit:</p>
                            <ul type="disc">
                                <li><strong>Work permit type A</strong>, which is valid for all salaried professions and all employers, for an unlimited period;</li>
                                <li><strong>Work permit type B</strong>, which is only valid for one employer for a period of one year;</li>
                                <li><strong>Work permit type C</strong>, which is valid for all salaried professions and all employers, for a limited period (must be renewed each year).</li>
                            </ul>
                            <p>For more information on work permits, see the website of the FPS (in <a href="https://www.emploi.belgique.be/defaultTab.aspx?id=4888#AutoAncher2" title="Link to the site of the FPS Employment, Labour and Social Dialogue" target="_blank">French</a> or <a href="https://www.werk.belgie.be/defaultTab.aspx?id=4888#AutoAncher2" title="Link to the site of the FPS Employment, Labour and Social Dialogue" target="_blank">Dutch</a>).</p>
                            <h4><strong>Professional Cards</strong> </h4>
                            <p>Professional Cards are suitable for the self-employed who want to work in Belgium. Cards are valid for 5 years and are limited to a precise field of practise.</p>
                            <h4>New Member States</h4>
                            <p>Nationals of the EU-Member States, Norway, Iceland, Liechtenstein and Switzerland are able to work without restriction within Belgium. Special rules apply to the new Member States of the European Union. For a certain transitional period, free movement of workers from these countries is subject to some restrictions. However, there are more flexible provisions for workers planning to exercise a profession for which there is a shortage of qualified staff.</p>

                            <p>You can find more information on transitional rules in Belgium on the site of the FPS Employment, Labour and Social Dialogue (in <a href="https://www.emploi.belgique.be/defaultTab.aspx?id=4886" title="Link to the site of the FPS Employment, Labour and Social Dialogue" target="_blank">French</a> or <a href="https://www.werk.belgie.be/defaultTab.aspx?id=4886" title="Link to the site of the FPS Employment, Labour and Social Dialogue" target="_blank">Dutch</a>). Job seekers from the new EU member states are not required to complete any formality in order to obtain a work permit, it is up to the employer to apply to the competent authority. A simplified procedure has been introduced for obtaining a work permit for positions in which there is a labour shortage. In these cases, the competent regional authorities grant work permits within five days. </p>
                            <ul>
                                <li>For all other nationalities, a work permit must be acquired.</li>
                                <li>Travel insurance is required to accompany your visa application. </li>
                            </ul>
                            <h4><strong>Visa Processing Time</strong></h4>
                            <p>It should be noted that, currently, the processing time for Visa applications is a minimum of 10 working days.</p>
                            <h4>Applying for a Work Permit</h4>
                            <p>People applying for work permits must do so using the forms available from the regional employment agencies.</p>

                            <!--START WORKING IN Belgium FORM-->
                            <?php require_once("../../forms/enquiry.belgium.inc.php");?>
                            <!--END WORKING IN Belgium FORM-->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complementary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_belgiumimage.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_belgiumlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->

<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->

<!-- START FOOTER ELEMENTS-->
<?php require_once("../../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>