<?php require_once("../../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>German Income Tax : German Payroll Solutions : EAFS Consulting GmbH</title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="We can assist in helping contractors with information on income tax in Germany. Visit our German Income Tax page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Working in Germany : German Income tax :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="We can assist in helping contractors with information on income tax in Germany. Visit our German Income Tax page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-income-tax/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Working in Germany : German Income tax : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-income-tax/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="We can assist in helping contractors with information on income tax in Germany. Visit our German Income Tax page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="noarchive" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="European Payroll, Working in Europe, Working in Germany, German Income Tax" />
    <meta name="description" content="We can assist in helping contractors with information on income tax in Germany. Visit our German Income Tax page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-income-tax/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>German Income Tax</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/" title="European Contractors" alt="European Contractors"><span itemprop="title">European Contractors</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/" title="Working in Germany" alt="Working in Germany"><span itemprop="title">Working in Germany</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title"><strong>German Income Tax</strong></span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="German Income Tax" itemscope itemtype="https://schema.org/Article" class="doc">
                            <header class="header">
                                <h2 itemprop="headline" class="subheader">German Income Tax</h2>
                            </header>
                            <div itemprop="articleBody">
                            <p>You can find general information on income tax rates here, in relation to Germany. This guide also covers the income tax base for residents and non-residents, allowable tax credits and deductions, the special expatriate tax regime, capital income tax rates and information on double-taxation treaties. </p>
                            <h4>Income Tax Base For Residents and Non-Residents of Germany</h4>

                            <dl class="dl-horizontal">
                                <dt><strong>Residents of Germany</strong> </dt>
                                <dd>are subject to personal income tax on their total income, from all sources worldwide.</dd>
                                <dt><strong>Non-residents of Germany</strong> </dt>
                                <dd>are subject to personal income tax on German sourced income only.</dd>
                            </dl>

                            <strong>Individuals will be regarded as tax residents if:</strong>
                            <ul>
                                <li>Their main home or center of economic interests is in Belgium.</li>
                                <li>They are registered within the civil register.</li>
                            </ul>
                            <p><em>A surcharge must be added, of 5.5% of the amount of income tax, if it concerns a solidarity contribution. Within Bavaria and Baden-Württemberg there is also a 9% Church Tax.</em></p>
                            <p><strong>Allowable Deductions and Tax Credits</strong></p>
                            <p>Insurance bonuses, education and training expenses, retirement expenses, alimony, donations and expenses related to disability are all deductible.</p>
                            <p><strong>Special Expatriate Tax Regime</strong><br>
                                <br>
                                Germany has signed avoidance of double taxation agreements with many countries in the world. For more information refer to the <a href="https://www.bundesfinanzministerium.de/Web/EN/Home/home.html" title="Link to Ministry of Finance" target="_blank">Ministry of Finance</a> (German only).</p>
                            <p><strong>Capital Tax Rate<br>
                            </strong></p>
                            <ul>
                                <li>No special capital gains tax.</li>
                            </ul>
                            <h4><strong>Double Taxation Treaties </strong></h4>

                            <dl class="dl-horizontal">
                                <dt><strong>Countries With Whom a Double Taxation Treaty Have Been Signed</strong></dt>
                                <dd><a href="https://www.cgerli.org/" title="Link to Centre for German Legal Information" target="_blank">Centre for German Legal Information (CGerLI)</a>, Access the list of Double Taxation Treaties signed by Germany</dd>
                                <dt> <strong>Withholding Taxes                                 Dividends:</strong> </dt>
                                <dd>Dividends: 25%, Interest: 0%, Royalties: 15%/30%</dd>
                                <dt><strong>Bilateral Agreement </strong></dt>
                                <dd>The United Kingdom and Belgium are bound by a <a href="https://www.hmrc.gov.uk/taxtreaties/in-force/germany.pdf" title="Link to the HMRC Taxation Treaty" target="_blank">double taxation treaty</a>.</dd>
                            </dl>


                            <h4><strong>Filing Date</strong></h4>
                            <p>Tax returns are due by 31st May the following year.</p>

                            <h4>Figures Based on Sources </h4>
                            <p><strong>Tax Authorities</strong> </p>
                            <ul>
                                <li><a href="https://www.minfin.fgov.be/portail2/en/index.htm#_blank" title="Link to the Tax Administration" target="_self">Services Publics Fédéral des Finances (FISCUS)</a>, Tax Administration.</li>
                                <li><a href="https://www.minfin.fgov.be/portail2/en/index.htm#_blank" title="Link to the Belgium Ministry of Finance" target="_blank">Belgium Ministry of Finance</a></li>
                            </ul>
                            <p>Tax Authorities</p>
                            <ul>
                                <li><a href="https://www.finanzamt.de#_blank" title="Link to Website of the tax department" target="_blank">The tax department</a>, Website of the tax department</li>
                                <li><a href="https://www.bzst.bund.de#_blank" title="Link to Website of the Federal Finance Bureau" target="_blank">The Federal Finance Bureau</a>, Website of the Federal Finance Bureau</li>
                                <li><a href="https://www.bundesfinanzministerium.de#_blank" title="Link to Website of the Federal Ministry of Finance" target="_blank">Bundesministerium der Finanzen</a>, Website of the Federal Ministry of Finance.<br>
                                </li>
                            </ul>
                            <p>Other Domestic Resources</p>
                            <ul>
                                <li><a href="https://www.steuerliches-info-center.de/EN/Home/home_node.html" title="Link to Tax information portal" target="_blank">Tax information portal</a> </li>
                            </ul>
                            <p>Country Guides</p>
                            <ul>
                                <li><a href="https://www.pwc.com" title="Link to Website of PWC" target="_blank">Building relationships, creating value</a></li>
                            </ul>
                            <!--START WORKING IN Belgium FORM-->
                            <?php require_once("../../forms/enquiry.german.inc.php");?>
                            <!--END WORKING IN Belgium FORM-->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_germanimage.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_germanlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>