<?php require_once("../../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Working in Germany : Contracting in Europe : EAFS Consulting GmbH</title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Our German Payroll Solution is very straightforward & requires minimal input from you. Visit our Working in Germany page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Working in Germany : Contracting in Europe : EAFS Consulting GmbH" />
    <meta name="twitter:description" content="Our German Payroll Solution is very straightforward & requires minimal input from you. Visit our Working in Germany page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contracting-europe/working-in-germany/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Working in Germany : Contracting in Europe : EAFS Consulting GmbH" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contracting-europe/working-in-germany/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Our German Payroll Solution is very straightforward & requires minimal input from you. Visit our Working in Germany page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="noarchive" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="European Payroll, Working in Europe, Working in Germany, Contracting in Germany" />
    <meta name="description" content="Our German Payroll Solution is very straightforward & requires minimal input from you. Visit our Working in Germany page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Working in Germany</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/" title="European Contractors" alt="European Contractors"><span itemprop="title">European Contractors</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title"><strong>Working in Germany</strong></span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Working in Germany" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Working in Germany</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>This is our guide to taxation in Germany. You will find everything you need to know, alongside an abundance of information that will be undoubtedly useful when planning to place employees or work within Belgian borders.</strong> </p>
                            <p>EAFS tax guides provide a summary of practiced <strong><a href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-income-tax/" title="Link to German Income Tax Page">taxation rates on income</a></strong> and rules for the tax year, allowable deductions, tax credits and information regarding special expatriate tax regimes. This guide also covers <strong><a href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-social-security/" title="Link to German Social Security Page">social security</a></strong> and <strong><a href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-visas/" title="Link to German Working Visas Page">working visas</a></strong>, highlighting the necessary requirements to work legally in Germany, with key dates highlighted for tax returns. We specialise in helping to reduce tax and administrative overheads for those working in Europe with companies located throughout Europe. Please <strong><a href="<?php echo SITE_URL;?>company-information/contact-eafs/" title="Link to our Contacting EAFS Page">contact us</a></strong> with any enquires you may have about working within German borders.</p>

                            <h4>Working in Germany</h4>
                            <div class="custom-services clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 first">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-bank fa-4x"></i>
                                                    <h3>Income Tax</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Income Tax</h3>
                                                    <p>You can find the income tax rates for 2014 in relation to Germany within this section.</p>
                                                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-income-tax/" title="Link to German Income Tax Page">German Income Tax</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-life-ring fa-4x"></i>
                                                    <h3>Social Security</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Social Security</h3>
                                                    <p>This section contains relevant information on Social Security within Germany.</p>
                                                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-social-security/" title="Link to German Social Security Page">German Social Security</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-plane fa-4x"></i>
                                                    <h3>Working Visas</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Working Visas</h3>
                                                    <p>The employer in Germany must apply to the competent authority for permits on behalf of the employee.</p>
                                                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-visas/" title="Link to German Working Visas Page">German Working Visa</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->
                            </div>
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                        <section itemprop="Management Services in Germany" itemscope itemtype="https://schema.org/Article">
                            <h4>Management Services in Germany</h4>
                            <p>EAFS can take complete control over administration and <strong>payroll</strong>. We can also take full jurisdiction of dealings with the German Tax and National Security Office. Fully specialized in the German labour market and holding an established relationship with the local tax authorities, we can also offer an <strong>Umbrella Service</strong> for contractors within Germany&rsquo;s borders, ensuring legality and helping you to gain the best possible marketplace retention rate. Social Security and Tax are deducted at source, calculated on the tax card basis, submitted for each individual at the beginning of every fiscal year. EAFS is registered with the Tax Authorities and National Social Security Office of Germany. </p>
                            <div class="custom-services clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 first">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-umbrella fa-4x"></i>
                                                    <h3>Payroll</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Payroll</h3>
                                                    <p>Our payroll service can be tailored to your exact requirements, saving your company time and money! </p>
                                                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-payroll/" title="Link to German Payroll Page">German Payroll</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-bank fa-4x"></i>
                                                    <h3>German GmbH</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>German GmbH</h3>
                                                    <p>EAFS Consulting GmbH can take over total responsibility for the administration of your payroll within Germany.</p>
                                                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-gmbh/" title="Link to German GmbH Page">German GmbH</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->
                            </div>
                        </section>

                            <!--START WORKING IN GERMANY FORM-->
                            <?php require_once("../../forms/enquiry.german.inc.php");?>
                            <!--END WORKING IN GERMANY FORM-->




                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_germanimage.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_germanlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->

<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->

<!-- START FOOTER ELEMENTS-->
<?php require_once("../../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->

</body>
</html>