<?php require_once("../../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Working in Italy : Italian Income Tax : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="We can assist in helping contractors with information on income tax in Italy. Visit our Italian Income Tax page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Working in Italy : Italian Income Tax : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="We can assist in helping contractors with information on income tax in Italy. Visit our Italian Income Tax page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-income-tax/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Working in Italy : Italian Income Tax :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-income-tax/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="We can assist in helping contractors with information on income tax in Italy. Visit our Italian Income Tax page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="noarchive" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="European Payroll, Working in Europe, Working in Italy, Italian Income Tax" />
    <meta name="description" content="We can assist in helping contractors with information on income tax in Italy. Visit our Italian Income Tax page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-income-tax/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>European Contractors : Working in Italy : Italian Income Tax</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contracting-europe/" title="European Contractors" alt="European Contractors" itemprop="url"><span itemprop="title">European Contractors</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/" title="Working in Italy" alt="Working in Italy" itemprop="url"><span itemprop="title">Working in Italy</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Italian Income Tax</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Italian Income Tax" itemscope itemtype="https://schema.org/Article" class="doc">
                            <header class="header">
                                <h2 itemprop="headline" class="subheader">Working in Italy : Italian Income Tax</h2>
                            </header>
                            <div itemprop="articleBody">
                            <p>You can find the income tax rates for 2014 here, in relation to Italy. This guide also covers the income tax base for residents and non-residents, allowable tax credits and deductions, the special expatriate tax regime, capital income tax rates and information on double-taxation treaties. </p>

                            <h4>Income Tax Base For Residents and Non-Residents of Italy</h4>

                            <dl class="dl-horizontal">
                                <dt><strong>Residents of Italy</strong> </dt>
                                <dd>are subject to personal income tax on their total income, from all sources worldwide.</dd>
                                <dt><strong>Non-residents of Belgium</strong> </dt>
                                <dd>are subject to personal income tax on Italian sourced income only.</dd>
                            </dl>

                            <strong>Individuals will be regarded as tax residents if:</strong>
                            <ul>
                                <li>Their main home or center of economic interests is in Italy.</li>
                                <li>They are registered within the civil registry for the vast majority of the tax year.</li>
                            </ul>
                            <h4>Income Tax Rates for 2014</h4>

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Amount</th>
                                    <th>%age</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Income tax is withheld at source:</td>
                                    <td>from 23% to 43%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 0 to 15,000</td>
                                    <td>23%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 15,001 to 28,000 </td>
                                    <td>27%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 28,001 to 55,000</td>
                                    <td><p>38%</p></td>
                                </tr>
                                <tr>
                                    <td>From EUR 55,001 to 75,000</td>
                                    <td>41%</td>
                                </tr>
                                <tr>
                                    <td>Over 75,001</td>
                                    <td>43%</td>
                                </tr>
                                </tbody>
                            </table>
                            <p><em>Solidarity Surtax of 3% must be added to all income exceeding EUR 300,000.</em></p>
                            <p><strong>Allowable Deductions and Tax Credits</strong></p>
                            <p>Maintenance allowance; university expenses; medical expenses (if they are over 129 EUR); dependent elderly or disabled persons.</p>
                            <p><strong>Special Expatriate Tax Regime</strong><br>
                                <br>
                                Expatriates who live in Italy will be classified as a Non-resident or a Resident . Non-residents are only taxed on income and gains that arise within Italian borders, while residents are taxed on worldwide income and gains. Expatriates will be classed as a tax payer if, for a period of 183 days, they have their principal place of residence or work in Italy, has their centre of vital interest within Italy or are registered with the registry office of the Population Registry (Anagrafe).</p>
                            <p><strong>Capital Tax Rate<br>
                            </strong></p>
                            <ul>
                                <li>Inheritance tax is paid by heirs or legatees on the net amount inherited by each recipient from the estate of any deceased person who is considered to be a resident of Italy. The amount payable depends on the heir’s relationship to the deceased and ranges between 4% and 8%. In the financial sector, an extra 10% tax is imposed on bonuses and stock options when exceeding three times the employee's fixed salary.</li>
                            </ul>
                            <h4><strong>Double Taxation Treaties </strong></h4>

                            <dl class="dl-horizontal">
                                <dt><strong>Countries With Whom a Double Taxation Treaty Have Been Signed</strong></dt>
                                <dd><a href="https://www.finanze.it/export/finanze/Per_conoscere_il_fisco/fiscalita_Comunitaria_Internazionale/convenzioni_e_accordi/index.htm#_blank" title="Link to the Signed Fiscal Treaties" target="_blank">See the list of the signed fiscal treaties</a>, on the Ministry of Finance website. (in Italian).</dd>
                                <dt> <strong>Withholding Taxes                                 Dividends:</strong> </dt>
                                <dd>Dividends: 27%; Interest: 12.5%/27%; Royalties: 15% if resident and 22.5%/30% if non-resident. </dd>
                                <dt><strong>Bilateral Agreement </strong></dt>
                                <dd>The United Kingdom and Belgium are bound by a <a href="https://www.hmrc.gov.uk/taxtreaties/in-force/italy-dtc.pdf" title="Link to the HMRC Taxation Treaty" target="_blank">double taxation treaty</a>.</dd>
                            </dl>


                            <h4><strong>Filing Date</strong></h4>
                            <p>Taxpayers who derive taxable income in excess of certain limits must file an annual tax return between the  1st of  May and the 31st of  July of the year following the tax year (by 31 October if filing electronically).</p>
                            <h4>Figures Based on Sources </h4>
                            <p><strong>Tax Authorities</strong> </p>
                            <ul>
                                <li><a href="https://www1.agenziaentrate.it/inglese/" title="Link to the Italian Tax Agency">Italian Tax Agency</a></li>
                                <li><a href="https://www.finanze.it/export/finanze/index.htm" title="Link to the Italian Ministry of Finance" target="_blank">Ministry of Finance</a></li>
                            </ul>
                            <p><strong>Other Domestic Resources</strong></p>
                            <ul>
                                <li><a href="https://www.finanzelavoro.it/#_blank">Finanzelavoro.it</a> </li>
                            </ul>
                            <p><strong>Country Guides</strong></p>
                            <ul>
                                <li><a href="https://www2.deloitte.com/content/dam/Deloitte/global/Documents/Tax/dttl-tax-italyguide-2012.pdf"  title="Link to the Deloitte Guide" target="_blank">Deloitte Guide</a></li>
                                <li><a href="https://www.pkf.com/media/387101/italy_2012.pdf#_blank" title="Link to the PKF Tax Guide" target="_blank">2011 PKF Tax Guide</a></li>
                                <li><a href="https://europa.eu/rapid/pressReleasesAction.do?reference=STAT/10/95&format=HTML&aged=0&language=en&guiLanguage=en" title="Link to the Consult Trends Guide" target="_blank">Consult Taxation trends in the European Union.</a>                            </li>
                            </ul>
                            <!--START WORKING IN Italian FORM-->
                            <?php require_once("../../forms/enquiry.italy.inc.php");?>
                            <!--END WORKING IN Italian FORM-->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_italyimage.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_italylinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->

<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->

<!-- START FOOTER ELEMENTS-->
<?php require_once("../../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>