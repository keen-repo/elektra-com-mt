<?php require_once("../../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Working in Luxembourg : Luxembourg Income Tax : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="We can assist in helping contractors with information on income tax in Luxembourg. Visit our Italian Income Tax page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Working in Luxembourg : Luxembourg Income Tax :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="We can assist in helping contractors with information on income tax in Luxembourg. Visit our Italian Income Tax page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-income-tax/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Working in Luxembourg : Luxembourg Income Tax : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-income-tax/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="We can assist in helping contractors with information on income tax in Luxembourg. Visit our Italian Income Tax page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="noarchive" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="European Payroll, Working in Europe, Working in Luxembourg, Luxembourg Income Tax" />
    <meta name="description" content="We can assist in helping contractors with information on income tax in Luxembourg. Visit our Italian Income Tax page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-income-tax/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>European Contractors : Working in Luxembourg : Luxembourg Income Tax</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contracting-europe/" title="European Contractors" alt="European Contractors" itemprop="url"><span itemprop="title">European Contractors</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/" title="Working in Luxembourg" alt="Working in Luxembourg" itemprop="url"><span itemprop="title">Working in Luxembourg</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Luxembourg Income Tax</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Luxembourg Income Tax" itemscope itemtype="https://schema.org/Article" class="doc">
                            <header class="header">
                                <h2 itemprop="headline" class="subheader">Working in Luxembourg : Luxembourg Income Tax</h2>
                            </header>
                            <div itemprop="articleBody">
                            <p>You can find the income tax rates for 2014 here, in relation to Luxembourg. This guide also covers the income tax base for residents and non-residents, allowable tax credits and deductions, the special expatriate tax regime, capital income tax rates and information on double-taxation treaties. </p>

                            <h4>Income Tax Base For Residents and Non-Residents of Luxembourg</h4>

                            <dl class="dl-horizontal">
                                <dt><strong>Residents of Luxembourg</strong> </dt>
                                <dd>are subject to personal income tax on their total income, from all sources worldwide.</dd>
                                <dt><strong>Non-residents of Luxembourg</strong> </dt>
                                <dd>are subject to personal income tax on sourced income from Luxembourg only.</dd>
                            </dl>

                            <strong>Individuals will be regarded as tax residents if:</strong>
                            <ul>
                                <li>Their customary abode is in Luxembourg</li>
                                <li>Their center of economical interests is in Luxembourg </li>
                            </ul>
                            <h4>Income Tax Rates for 2014</h4>

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Ammount</th>
                                    <th>%age</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Income tax is withheld at source:</td>
                                    <td>40%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 0 to 11,265</td>
                                    <td>0%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 11,265 to 13,173</td>
                                    <td>8%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 13,173 to 15,081</td>
                                    <td>10%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 15,081 to 16,989 </td>
                                    <td>12%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 16,989 to 18,897   </td>
                                    <td><p>14%</p></td>
                                </tr>
                                <tr>
                                    <td>From EUR 18,897 to 20,805</td>
                                    <td>16%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 20,805 to 22,713</td>
                                    <td>18%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 22,713 to 24,621</td>
                                    <td>20%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 24,621 to 26,529</td>
                                    <td>22%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 26,529 to 28,437</td>
                                    <td>24%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 28,437 to 30,385</td>
                                    <td>26%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 30,385 to 32,253</td>
                                    <td>28%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 32,253 to 34,161</td>
                                    <td>30%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 34,161 to 36,069</td>
                                    <td>32%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 36, 069 to 37,977</td>
                                    <td>34%</td>
                                </tr>
                                <tr>
                                    <td><p>From EUR 37,977 to 39,885</p></td>
                                    <td>36%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 39,885 to 41,793</td>
                                    <td>38%</td>
                                </tr>
                                <tr>
                                    <td>From EUR 41,793 to 100,000</td>
                                    <td>39%</td>
                                </tr>
                                <tr>
                                    <td>For EUR 100,000 and above</td>
                                    <td>40%</td>
                                </tr>
                                </tbody>
                            </table>
                            <p><em>Solidarity Surtax of 7% of taxes, or 9% for taxpayers earning more than EUR 150,000.</em></p>
                            <p><strong>Allowable Deductions and Tax Credits</strong></p>
                            <p>Subject to limitations, deductions are permitted for insurance premiums for life, accident, sickness; individual pension schemes; alimonies and annuities; childcare and housekeeping cost; charitable contributions; interest on personal and mortgage loans.</p>
                            <p><strong>Special Expatriate Tax Regime</strong><br>
                                <br>
                                Luxembourg does not have a special expatriate tax regime for foreigners.                            </p>
                            <h4><strong>Double Taxation Treaties </strong></h4>

                            <dl class="dl-horizontal">
                                <dt><strong>Countries With Whom a Double Taxation Treaty Have Been Signed</strong></dt>
                                <dd><a href="https://www.mae.lu/fr/Site-MAE/Relations-economiques-internationales/Accords-et-Conventions/Conventions-en-matiere-de-doubles-impositions#_blank" title="Link to Taxation Treaties Page" target="_blank">List of countries having a tax agreement with Luxembourg.</a></dd>
                                <dt> <strong>Withholding Taxes  Dividends:</strong> </dt>
                                <dd>Dividends: 0/15%, Interest and Royalties: 0% </dd>
                                <dt><strong>Bilateral Agreement </strong></dt>
                                <dd>The United Kingdom and Belgium are bound by a <a href="https://www.hmrc.gov.uk/taxtreaties/in-force/uk-luxembourg-inforce.pdf" title="Link to the HMRC Taxation Treaty" target="_blank">double taxation treaty</a>.</dd>
                            </dl>


                            <h4><strong>Filing Date</strong></h4>
                            <p>Tax returns are due by 31st of March of the following year..</p>
                            <h4>Figures Based on Sources </h4>
                            <p><strong>Tax Authorities</strong> </p>
                            <ul>
                                <li><a href="https://www.do.etat.lu/#_blank" title="Link to the Customs &amp; Excise Website of Luxembourg">Customs and Excise Tax Administration</a></li>
                                <li><a href="https://www.mf.public.lu/#_blank" title="Link to the Finance Ministry Luxembourg">Finance Ministry</a><a href="https://www.minfin.fgov.be/portail2/en/index.htm#_blank" title="Link to the Belgium Ministry of Finance" target="_blank"></a></li>
                            </ul>
                            <p>Other Domestic Resources</p>
                            <ul>
                                <li><a href="https://www.impotsdirects.public.lu" title="Link to" target="_blank">Administration of Direct Taxes</a>
                                </li>
                                <li><a href="https://www.aed.public.lu/index.php" title="Link to" target="_blank">Administration de l'enregistrement et des domaines (indirect taxes)</a></li>
                                <li><a href="https://europa.eu/rapid/pressReleasesAction.do?reference=STAT/10/95&format=HTML&aged=0&language=en&guiLanguage=en" title="Link to" target="_blank">Consult Taxation trends in the European Union.</a></li>
                            </ul>

                            <!--START WORKING IN Luxembourg FORM-->
                            <?php require_once("../../forms/enquiry.luxe.inc.php");?>
                            <!--END WORKING IN Luxembourg FORM-->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_luxeimage.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_luxelinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->

<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->

<!-- START FOOTER ELEMENTS-->
<?php require_once("../../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>