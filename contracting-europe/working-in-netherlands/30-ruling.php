<?php require_once("../../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The 30% Ruling : EAFS Consulting BV</title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="The 30% Ruling or 30% regeling allows expatriate employees to The Netherlands to earn up to 30% of their compensation tax free." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Management Services : Dutch 30% Ruling :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="The Dutch 30% Ruling "30% regeling" allows expatriate employees to The Netherlands to earn up to 30% of their compensation tax free." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contracting-europe/working-in-holland/30-ruling/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Dutch 30% Ruling :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contracting-europe/working-in-holland/30-ruling/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="The 30% Ruling or 30% regeling allows expatriate employees to The Netherlands to earn up to 30% of their compensation tax free."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="noarchive" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Dutch Tax Benefit, 30% Ruling, 30% Tax Ruling, 30% Regeling, Dutch 30% Ruling" />
    <meta name="description" content="The 30% Ruling or 30% regeling allows expatriate employees to The Netherlands to earn up to 30% of their compensation tax free." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contracting-europe/working-in-holland/30-ruling/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>The 30% Ruling</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contracting-europe/" title="European Contractors" alt="European Contractors" itemprop="url"><span itemprop="title">European Contractors</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/" title="Working in Netherlands" alt="Working in Netherlands" itemprop="url"><span itemprop="title">Working in Netherlands</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title"><strong>The 30% Ruling</strong></span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Dutch 30% Ruling" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">The 30% Ruling</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>The 30% ruling (or in Dutch &quot;30% regeling&quot;) allows expatriate employees to The Netherlands to earn up to 30% of their compensation tax-free.</strong></p>
                            <p class="has-pullquote pullquote-adelle" data-pullquote="EAFS Consulting BV is specialised in obtaining and implementing the 30% ruling."> In addition, the 30% ruling also allows you to opt for partial non-residency status. With the partial non-residency status you are exempt from Dutch taxation on your investments, except for investments in Dutch real estate. Finally, with the 30% ruling you and your partner are allowed to exchange your foreign driver's license for a Dutch driver's license, without having to take time-consuming tests.</p>
                            <p>Because of these benefits, the 30% ruling is generally considered to be the single most essential part of prudent tax and financial planning for contractors and their employers to the Netherlands. Therefore, the application and the implementation of the ruling should not be taken lightly. </p>
                            <h4>Applying for the 30% Ruling</h4>
                            <p>EAFS Consulting BV is specialised in obtaining and implementing the 30% ruling and assists both employees and employers in the entire process. &nbsp;What we do:</p>
                            <ul type="disc">
                                <li>Review of and planning towards qualifying for the 30% ruling</li>
                                <li>Application of the 30% ruling</li>
                                <li>Implementation of the 30% ruling in compensation packages &nbsp;</li>
                                <li>Personal Service</li>
                                <li>Competitive Fees</li>
                                <li>Application for the 30% ruling</li>
                            </ul>
                            <h4>This Involves:</h4>
                            <ul type="disc">
                                <li>The employer and the employee need to contractually agree on the 30% ruling in the contract of employment;</li>
                                <li>The employer and the employee gather the information and documents needed for the filing of the application for the 30% ruling;</li>
                                <li>The application is filed with the Dutch tax authorities (needs to be done within 4 months of first arrival in the Netherlands);</li>
                                <li>The Dutch tax authorities will make a formal decision on the application within 6-12 weeks;</li>
                                <li>Once granted, the 30% ruling needs to be implemented in the salary administration to effectuate the benefit.</li>
                            </ul>

                            <!--START WORKING IN HOLLAND FORM-->
                            <?php require_once("../../forms/enquiry.dutch.inc.php");?>
                            <!--END WORKING IN HOLLAND FORM-->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_hollandimage.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_hollandlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>