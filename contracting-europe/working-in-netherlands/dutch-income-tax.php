<?php require_once("../../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dutch Income Tax : EAFS Consulting BV</title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="We can assist in helping contractors with information on income tax in Holland. Visit our Dutch Income Tax page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Working in Holland : Dutch Income Tax :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="We can assist in helping contractors with information on income tax in Holland. Visit our Dutch Income Tax page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contracting-europe/working-in-holland/dutch-income-tax/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Working in Holland : Dutch Income Tax :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contracting-europe/working-in-holland/dutch-income-tax/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="We can assist in helping contractors with information on income tax in Holland. Visit our Dutch Income Tax page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="noarchive" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="European Payroll, Working in Europe, Working in Holland, Holland Income Tax" />
    <meta name="description" content="We can assist in helping contractors with information on income tax in Holland. Visit our Dutch Income Tax page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contracting-europe/working-in-holland/dutch-income-tax/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Dutch Income Tax</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contracting-europe/" title="European Contractors" alt="European Contractors" itemprop="url"><span itemprop="title">European Contractors</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/" title="Working in Netherlands" alt="Working in Netherlands" itemprop="url"><span itemprop="title">Working in Netherlands</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title"><strong>Dutch Income Tax</strong></span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Italian Income Tax" itemscope itemtype="https://schema.org/Article" class="doc">
                            <header class="header">
                                <h2 itemprop="headline" class="subheader">Dutch Income Tax</h2>
                            </header>
                            <div itemprop="articleBody">
                            <p>You can find a rough guide to income tax rates in relation to the Netherlands in our guide. This guide also covers the income tax base for residents and non-residents, allowable tax credits and deductions, the special expatriate tax regime, capital income tax rates and information on double taxation treaties. </p>
                            <h4><strong>Income Tax Base For Residents and Non-Residents of Italy</strong></h4>

                            <dl class="dl-horizontal">
                                <dt><strong>Residents of Holland</strong> </dt>
                                <dd>are subject to personal income tax on their total income, from all sources worldwide.</dd>
                                <dt><strong>Non-residents of Holland</strong> </dt>
                                <dd>are subject to personal income tax on Italian sourced income only.</dd>
                            </dl>

                                <h4><strong>Individuals will be regarded as tax residents if:</strong></h4>
                            <ul>
                                <li>Their main home or center of economic interests is in Italy.</li>
                                <li>They are registered within the civil registry for the vast majority of the tax year.</li>
                            </ul>

                            <h4><strong>Allowable Deductions and Tax Credits</strong></h4>
                            <p>Employer reimbursement of (international) school fees. </p>
                            <p><strong>Tax deductions:</strong> interest allowance for mortgage interest related to real property in the Netherlands.</p>
                            <p><strong>Tax exemptions:</strong> an employee subject to the 30% tax ruling (if a resident of the Netherlands) can opt for partial non-resident status, which implies that income (assets/savings) may be tax-exempt in the Netherlands.</p>

                                <h4><strong>Special Expatriate Tax Regime</strong></h4>

                                <p>An employee assigned to the Netherlands, who has specific expertise that is not available or that is scarce in the Dutch labour market, is eligible to apply for a tax-exempt allowance of 30% of their salary. A request has to be made to apply the 30% ruling, and this must be completed within four months of employment starting. Once approved by the authorities, the allowance applies for a ten year maximum term, paired with an interim test in which to determine if the expatriate continues to satisfy the conditions and qualify for the 30% ruling.</p>
                            <p>Employees who do not qualify for the 30% ruling still may receive a tax-free reimbursement, of actual extra-territorial expenses.</p>
                                <h4><strong>Capital Tax Rate</strong></h4>
                            <ul>
                                <li>Municipal authorities levy personal property tax on all immovable property based on the council-rated value of property. The tax base for real estate tax is the fair market value as determined by the municipal tax authorities. The council-rated value of an immovable property applies to certain elements of the personal income tax.</li>
                            </ul>
                            <h4><strong>Double Taxation Treaties </strong></h4>

                            <dl class="dl-horizontal">
                                <dt><strong>Countries With Whom a Double Taxation Treaty Have Been Signed</strong></dt>
                                <dd>See the list of the conventions signed, on the Dutch Finances Ministry website.</dd>
                                <dt> <strong>Withholding TaxesDividends:</strong> </dt>
                                <dd>Withholding taxes are: 15% for dividends. There are no tax rates on interest and royalties.</dd>
                                <dt><strong>Bilateral Agreement </strong></dt>
                                <dd>The United Kingdom and Belgium are bound by a double taxation treaty.</dd>
                            </dl>


                            <h4><strong>Filing Date</strong></h4>
                            <p>Taxpayers who derive taxable income in excess of certain limits must file an annual tax return between the  1st of  May and the 31st of  July of the year following the tax year (by 31 October if filing electronically).</p>

                            <!--START WORKING IN Holland FORM-->
                            <?php require_once("../../forms/enquiry.dutch.inc.php");?>
                            <!--END WORKING IN Holland FORM-->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_hollandimage.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_hollandlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>