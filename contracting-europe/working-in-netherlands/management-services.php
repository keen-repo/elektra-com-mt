<?php require_once("../../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Management Services : PEO Service Holland : EAFS Consulting BV</title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="We can offer various management services in Holland through our BV. Visit our Dutch management services page for more information. " />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Working in Holland : Management Services :   <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="We can offer various management services in Holland through our BV. Visit our Dutch management services page for more information. " />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contracting-europe/working-in-holland/management-services/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Working in Holland : Management Services :   <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contracting-europe/working-in-holland/management-services/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="We can offer various management services in Holland through our BV. Visit our Dutch management services page for more information. "/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="noarchive" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="European Payroll, Working in Europe, Working in Holland, Dutch Management Services" />
    <meta name="description" content="We can offer various management services in Holland through our BV. Visit our Dutch management services page for more information. " />
    <link rel="canonical" href="<?php echo SITE_URL;?>contracting-europe/working-in-holland/management-services/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php // require_once("../../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Management Services</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contracting-europe/" title="European Contractors" alt="European Contractors" itemprop="url"><span itemprop="title">European Contractors</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/" title="Working in Netherlands" alt="Working in Netherlands" itemprop="url"><span itemprop="title">Working in Netherlands</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title"><strong>Management Services</strong></span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Management Services" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Management Services</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>With the tax rules in the Netherlands changing every 12 months, the majority of foreign companies can find it incredibly difficult to keep track of every update and all details. The Dutch Government also holds the employer liable over any mistakes made in the administration of payroll. Many organisations have found that they can reduce costs and improve their competitiveness by outsourcing their payroll services.</strong></p>
                            <h4><a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-payroll/" title="Link to Dutch Payroll Page">Dutch Payroll Service</a></h4>
                            <p>EAFS can take over total responsibility for the administration of your payroll, alongside dealing with the Dutch Tax Office and Social Security Organizations. We are fully specialized in the Dutch labor market, managing numerous contractors for various companies working within Hollands' borders.</p>
                            <h4><a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-bv/" title="Link to Dutch Umbrella Page">Dutch Umbrella</a></h4>
                            <p>EAFS Consulting BV holds an established relationship with the Dutch Tax Authority. This is the only compliant way for contractors and freelancers to work in the Netherlands, ensuring that the local tax authorities are happy with your presence. More importantly, it allows you to gain the best possible retention rate in the market place, in view of your circumstances and requirements.</p>
                            <h4><a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/30-ruling/" title="Link to 30% Ruling Page">The 30% Ruling</a></h4>
                            <p>The 30% ruling (or in Dutch , the &quot;30% regeling&quot;) allows expatriate employees to The Netherlands to earn up to 30% of their compensation tax-free. In addition, the 30% ruling also allows you to opt for partial non-residency status. </p>
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                        <section itemprop="Management Services in Holland" itemscope itemtype="https://schema.org/Article">
                                <div class="custom-services clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 first">
                                        <div class="ch-item">
                                            <div class="ch-info-wrap">
                                                <div class="ch-info">
                                                    <div class="ch-info-front">
                                                        <i class="fa fa-umbrella fa-4x"></i>
                                                        <h3><a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-payroll/" title="Link to Dutch Payroll Page">Dutch Payroll</a></h3>
                                                    </div>
                                                    <div class="ch-info-back">
                                                        <h3>Payroll</h3>
                                                        <p>Our payroll service can be tailored to your exact requirements, saving your company time and money! </p>
                                                        <a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-payroll/" title="Link to Dutch Payroll Page">Dutch Payroll</a>
                                                    </div>
                                                </div><!-- end ch-info -->
                                            </div><!-- end ch-info-wrap -->
                                        </div><!-- end ch-item -->
                                    </div><!-- end col-sm-3 -->
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="ch-item">
                                            <div class="ch-info-wrap">
                                                <div class="ch-info">
                                                    <div class="ch-info-front">
                                                        <i class="fa fa-bank fa-4x"></i>
                                                        <h3><a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-bv/" title="Link to Dutch Umbrella Page">Dutch Umbrella</a></h3>
                                                    </div>
                                                    <div class="ch-info-back">
                                                        <h3>Dutch BV</h3>
                                                        <p>EAFS Consulting BV can take over total responsibility for the administration of your payroll within Holland. </p>
                                                        <a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-bv/" title="Link to Dutch Umbrella Page">Dutch Umbrella</a>
                                                    </div>
                                                </div><!-- end ch-info -->
                                            </div><!-- end ch-info-wrap -->
                                        </div><!-- end ch-item -->
                                    </div><!-- end col-sm-3 -->
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="ch-item">
                                            <div class="ch-info-wrap">
                                                <div class="ch-info">
                                                    <div class="ch-info-front">
                                                        <i class="fa fa-money fa-4x"></i>
                                                        <h3><a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/30-ruling/" title="Link to 30% Ruling Page">30% Ruling</a></h3>
                                                    </div>
                                                    <div class="ch-info-back">
                                                        <h3>30% Ruling</h3>
                                                        <p>The 30% ruling allows expatriate employees to The Netherlands to earn up to 30% of their compensation tax-free. </p>
                                                        <a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/30-ruling/" title="Link to 30% Ruling Page">30% Ruling</a>
                                                    </div>
                                                </div><!-- end ch-info -->
                                            </div><!-- end ch-info-wrap -->
                                        </div><!-- end ch-item -->
                                    </div><!-- end col-sm-3 -->
                                </div>
                        </section>
                            <!--START WORKING IN HOLLAND FORM-->
                            <?php require_once("../../forms/enquiry.dutch.inc.php");?>
                            <!--END WORKING IN HOLLAND FORM-->

                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_hollandimage.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START LINKS -->
                <?php require_once("../../partials/widget_hollandlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>