<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : Agency Workers Regulations :  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="The Agency Workers Regulations (AWR) are a set of regulations intended to protect vulnerable low paid “agency workers”." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : Agency Workers Regulations :   <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="The Agency Workers Regulations (AWR) are a set of regulations intended to protect vulnerable low paid “agency workers”." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/agency-workers-regulations/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : Agency Workers Regulations :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/agency-workers-regulations/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="The Agency Workers Regulations (AWR) are a set of regulations intended to protect vulnerable low paid “agency workers”."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Agency Workers Regulations, Contractor Accountants, Agency Workers" />
    <meta name="description" content="The Agency Workers Regulations (AWR) are a set of regulations intended to protect vulnerable low paid “agency workers”." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/agency-workers-regulations/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants : Agency Workers Regulations</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/" title="Contractor Accountants" alt="Contractor Accountants"><span itemprop="title">Contractor Accountants</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Agency Workers Regulations</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Agency Workers Regulations" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor Accountants : Agency Workers Regulations</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>As an agency worker, you are entitled to rights from day one in your assignment. These include access to collective facilities, such as the staff canteen or toilets and information about job vacancies. If you believe that you are not getting these rights, you should talk to the agency or recruiters. If the matter can&rsquo;t be resolved informally, you can request written information for your hirer about your rights any length of time after the start of your assignment. </strong><br>
                                <strong> </strong></p>
                            <p>The hirer has to provide a written statement with all the relevant information about:</p>
                            <ul type="disc">
                                <li>The rights      of someone doing a similar job.</li>
                                <li>Why agency      workers are treated differently.</li>
                            </ul>
                            <p>The hirer has 28 days to reply to your request.</p>
                            <h4>After 12 Weeks.</h4>
                            <p>After 12 weeks in the same job, if you believe you are not getting your rights, you are entitled to ask for written information from your agency. For example, if you think you are not receiving equal treatment on pay, holidays, etc., you can, under law, approach the agency to ask questions.
                                If you do not get a reply within 30 days then you can ask for the same information from your hirer -who has 28 days to reply.
                                The agency has to provide information relating to:</p>
                            <ul type="disc">
                                <li>Basic      working and employment conditions, e.g. rate of pay and number of weeks'      annual leave.</li>
                                <li>Any      relevant information or factors that were considered when determining      these conditions, for example, is there a pay scale which sets the rate of      pay?</li>
                                <li>The terms      and conditions of a relevant comparable employee (if relevant) and explain      any difference in treatment, e.g. lower rate of pay because you do not      have the same qualifications, skills or experience/expertise.</li>
                            </ul>
                            <h4><strong>What happens if I don&rsquo;t get the Information? </strong></h4>
                            <p>If you make a claim to an Employment Tribunal, the Tribunal may look upon the situation unfavourably if information or a written statement was requested but not provided.
                                So, what should you do if you have a problem with the information provided? You should bring a claim to an Employment Tribunal if the agency (including the umbrella company or other body involved) and your recruiter/hirer:</p>
                            <ul type="disc">
                                <li>Do not      give you equal treatment after 12 weeks.</li>
                                <li>Deny you      access to facilities or information on vacancies from day one.</li>
                            </ul>
                            <p>ACAS (the Advisory Conciliation  and Arbitration Service) may be able to help before, and after, making a claim. </p>
                            <h4><strong>Making a claim to an Employment Tribunal</strong></h4>
                            <p>An Employment Tribunal will not consider a complaint unless it is presented within three months of the actual breach. If it is unclear where liability for your claim might sit, you may claim against the hirer, the agency, the umbrella company or any other parties involved.
                                If your claim is successful then you will be compensated for any loss attributable to the breach including:</p>
                            <ul type="disc">
                                <li>Expenses      reasonably incurred.</li>
                                <li>Loss of      earnings.</li>
                                <li>An      appropriate level of compensation if you have been denied access to a      facility, for example.</li>
                            </ul>
                            <p>There is no maximum award, but there is a minimum amount of two weeks' pay.</p>
                            <h4><strong>Further Information</strong> </h4>
                            <p><cite>This summary of the Agency Workers regulation was obtained from the Direct Gov. website. <br>
                                For further reading and for up to date changes in the legislation please visit the Direct Gov. website pages relating to the Agency Workers Regulations.</cite></p>
                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                                </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_contractorlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>