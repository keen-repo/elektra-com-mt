<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : Benefits of a limited Company :  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="EAFS are a leading UK & Scottish based company formation agent offering all the  benefits of a limited company through our company formation service." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : Benefits of a limited Company : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="EAFS are a leading UK & Scottish based company formation agent offering online formation of limited companies, public limited and limited by guarantee companies " />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/benefits-limited-company/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : Benefits of a limited Company : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/benefits-limited-company/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="EAFS are a leading UK & Scottish based company formation agent offering all the  benefits of a limited company through our company formation service."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Limited Company Advantages, Benefits of a Limited Company " />
    <meta name="description" content="EAFS are a leading UK & Scottish based company formation agent offering all the  benefits of a limited company through our company formation service. " />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/benefits-limited-company/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants : Benefits of a limited Company</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/" title="Contractor Accountants" alt="Contractor Accountants"><span itemprop="title">Contractor Accountants</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Benefits of a Limited Company</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Benefits of a limited Company" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor Accountants : Benefits of a limited Company</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>The principal benefit of trading as a limited company has always been the restricted liability of the company's officers and shareholders.</strong></p>
                            <p> As a sole trader, or other non-limited business, personal assets can be at risk in the event of the failure of a business, but this is not the case for a limited company. As long as the business is operated legally and within the terms of the Companies Act, the personal assets of directors or shareholders are not at risk in the event of receivership or a wind up order.</p>
                            <p class="has-pullquote pullquote-adelle" data-pullquote="EAFS are an accredited specialist accountancy firm for Contractors.">Operating, as a limited company, also gives suppliers and customers a sense of confidence in a business. Larger organisations in particular will prefer not to deal with non-limited businesses. Also, many of the costs associated with managing and operating a limited company are little more than a non-limited business.</p>
                            <h4><strong>Main Benefits of operating your own Limited Company.</strong></h4>
                            <p><strong>Higher &lsquo;Take Home&rsquo; Pay.</strong><br>
                                You'll typically take home around 75% - 80% of your contract wage by working through your own limited company. You can take home as much as £15,000 extra per year on a £350 daily rate compared with using an Umbrella Company.&nbsp;</p>
                            <p><strong>Claim on a Wider Range of Expenses. </strong><br>
                                Anything solely classed as a business cost can be claimed back on expenses. Trading through a Limited company means you can claim on a wider range of expenses, such as; accountancy fees, equipment, software, phones, travel, Internet and a huge range of other incurred expenses.</p>
                            <p><strong>Entitled to the Flat Rate VAT scheme</strong><br>
                                The Flat Rate VAT scheme can generate thousands of pounds of extra profit per annum, with most contractors choosing to apply for this VAT scheme.</p>
                            <p><strong>Personal assets are covered</strong><br>
                                As a non-limited business, personal assets can be at risk if the business fails, but this is not the case for a limited company. As the shareholder you cannot be held personally liable for the debts of a limited company, meaning your personal assets are not at risk.</p>
                            <p><strong>Complete Control of your Business</strong><br>
                                You keep complete control of your financial affairs meaning you do not have to risk your money with any third party administrator or umbrella company.</p>
                            <p><strong>Ease of use</strong><br>
                                Running your own business isn't difficult; submit spreadsheets to your accountant - just like umbrella time-sheets and expenses. Working through your own limited company does require a certain level of commitment but typically most contractors tell us they spend around 15 – 20 minutes per month managing their company.</p>
                            <p><strong>Greater opportunity for tax planning than PAYE Umbrella</strong><br>
                                It goes without saying that working through your own limited company is more financially rewarding, which is only fair bearing in mind the extra responsibilities and loss of full time employment benefit.</p>
                            <p><strong>Company given more credibility</strong><br>
                                Operating as a limited company often gives suppliers and customers a sense of confidence in a business. Quite often, other companies prefer not to deal with non-limited businesses.</p>
                            <p><strong>Protection of your company name</strong><br>
                                Once your proposed company name is registered as a limited company, law protects the name and no one else is allowed to use it. Waiting to register your company could mean you lose the name you had initially wished to trade under.</p>
                            <p>There is no obligation for a limited company to commence trading within any set time period after its incorporation.<br>
                                Running your own business is easy - EAFS will provide you with a dedicated account manager to handle your records and any questions you may have while you can remain in the driving seat - taking control of your finances.</p>
                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_contractorlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>