<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : Business Expenses :  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="There are only two categories of Business Expenses that may be claimed through tax deductible expenses. Visit our Business Expenses page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : Business Expenses :   <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="There are only two categories of Business Expenses that may be claimed through tax deductible expenses. Visit our Business Expenses page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/business-expenses/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : Business Expenses :   <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/business-expenses/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="There are only two categories of Business Expenses that may be claimed through tax deductible expenses. Visit our Business Expenses page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Tax Deductible Expenses, Contractor Accountants, Deductible Expenses, Business Expenses" />
    <meta name="description" content="There are only two categories of Business Expenses that may be claimed through tax deductible expenses. Visit our Business Expenses page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/business-expenses/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants : Business Expenses</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper">
    <div class="container clearfix hidden-xs">
        <div class="col-lg-12 col-md12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/" title="Contractor Accountants" alt="Contractor Accountants"><span itemprop="title">Contractor Accountants</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Business Expenses</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Business Expenses" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor Accountants : Business Expenses</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>The principal benefit of trading as a limited company has always been the restricted liability of the company's officers and shareholders.</strong></p>
                            <p>The purpose of these Guidelines is to show which expenses employees of their Limited Company can claim as allowable, i.e. tax-deductible expenses, and which they cannot.</p>
                            <p>It is the individual contractor’s responsibility to provide substantiation of expenses claim made, and the Guidelines include information on the documentary proof required. The structure of the Guidelines is:</p>
                            <ol>
                                <li><a href="#intro">Introduction</a></li>
                                <li><a href="#travel">Travel</a></li>
                                <li><a href="#subsistence">Subsistence Allowance</a></li>
                                <li><a href="#accommodation">Accommodation</a></li>
                                <li><a href="#meals">Meals</a></li>
                                <li><a href="#entertainment">Business Entertainment</a></li>
                                <li><a href="#clothing">Protective Clothing</a></li>
                                <li><a href="#tools">Tools and Equipment</a></li>
                                <li><a href="#subs">Subscriptions to Professional Societies</a></li>
                                <li><a href="#station">Stationery, Postage, Fax and Telephone Expenses</a></li>
                                <li><a href="#training">Training</a></li>
                                <li><a href="#incidentals">Incidentals</a></li>
                            </ol>
                            <p><a id="intro" name="intro"></a>1: <strong>EXPENSES POLICY INTRODUCTION</strong> <br> There are only two categories of Business Expenses that may be claimed through tax deductible expenses. Visit our Business Expenses page for more information.</p>
                            <p>Other expenses incurred “wholly, exclusively and necessarily in the performance of the duties of the employee”. This means that no expense incurred for a private or other non-business purpose is allowable.</p>
                            <p>This part of the Guidelines sets out which business-related expenses are allowable for employees of their own Limited Company, based on the two principles above and on current tax legislation and specific regulations defining tax-deductible expenses.</p>
                            <p><a id="travel" name="travel"></a>2: <strong>TRAVEL</strong> <br> Only “qualifying travel expenses” are allowable. Theses are travel costs that employees are obliged to incur in performing their duties as a Employee of their own limited Company. Travel costs relating to commuting to and from the normal place of work are not claimable. Neither is private travel.</p>
                            <p>The Inland Revenue specify that travel to and from a site is an allowable expense if the period of time at the site is expected to be a maximum of 24 months, including any time spent on-site prior to the current contract. If, at any time, the contract is extended beyond 24 months, no further travel to and from the site is allowable.</p>
                            <p>One exception to the above principle is where the contractor works on more than one site in the course of a single contract. This exception is called the 40% rule. When a contractor spends 40% or less of his time at his or her normal place of work, then all expenses incurred in the travelling to and from that site are allowed, regardless of the 24 month rule. The claim may continue indefinitely for as long as circumstances continue to satisfy the 40% rule.</p>
                            <div class="indent">
                                <p>2.1 <strong>ROAD TRAVEL &ndash; USE OF PRIVATE VEHICLES</strong> <br> An employee may claim a cost per mile for allowable business journeys in his or her own vehicle on the basis of its engine size and the type of vehicle. A VAT receipt for your fuel to match or exceed the number of miles claimed should be submitted with every form. There is also a distinction between the first 10,000 miles in any tax year and subsequent miles. The rates that may be claimed are as follows:</p>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th><strong>TYPE OF VEHICLE</strong></th>
                                        <th><strong>FIRST 10,000 MILES</strong></th>
                                        <th><strong>10,000+ MILES</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Car &ndash; Up to 1000cc</td>
                                        <td>45p per mile</td>
                                        <td>25p per mile</td>
                                    </tr>
                                    <tr>
                                        <td>Motorcycle &ndash; all</td>
                                        <td>24p per mile</td>
                                        <td>24p per mile</td>
                                    </tr>
                                    <tr>
                                        <td>Bicycle</td>
                                        <td>20p per mile</td>
                                        <td>20p per mile</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <p>2.2 <strong>ROAD TRAVEL &ndash; USE OF HIRE CARS</strong> <br> The cost of a short-term hire car is allowable, provided that the use of the hire car claimed for is necessary to the performance of the Euro Payroll Member’s duties as an employee of his or her company, typically where no privately owned vehicle is available. The long term lease or hire payments made for the acquisition of a vehicle are not an allowable expense, whether or not the vehicle is used for business purposes. The costs of ownership” of a private vehicle used for business travel are already factored into the allowable mileage rates stated above.</p>
                                <p>2.3 <strong>RAIL OR AIR TRAVEL</strong> <br> The cost of train or air fares for business-related journeys is allowable.</p>
                                <p>2.4<strong> OTHER TRAVEL COSTS</strong> <br> Allowable travel costs include bridge, tunnel and road tolls, bus and taxi fares and car parking charges, provided that these expenses have been incurred on a business trip.</p>
                                <p>2.5 <strong>OVERSEAS TRAVEL COSTS</strong> <br> As with travel in the UK, the cost of overseas travel is allowable where employees are obliged to incur the expense in performing their duties of employment.</p>
                            </div>
                            <p><a id="subsistence" name="subsistence"></a>3: <strong>SUBSISTENCE ALLOWANCE</strong> <br> An employee making a business trip may spend money on such items as private telephone calls, laundry or newspapers. However, these are not specifically allowable expenses. Such costs may arise in the course of business travel, but the Inland Revenue regards them as personal rather than business expenditure.</p>
                            <p>However, employees staying overnight while away on business or on allowable work- related training are entitled to claim expenditure of this type by means of a Subsistence Allowance, even though such expenditure may not be allowable in its own right. There are two Subsistence Allowance rates - £5 per night in the UK and £10 per night overseas (including Eire). No receipts need be produced.</p>
                            <p>Subsistence Allowances in relation to site work are allowable if the period of time at the site is expected to be no more than 24 months in total, any time spent on-site prior to the current contract. The “40% rule” (see 1. above) also applies here. Personal costs incurred while away from home on business, for example the cost of childcare, are not allowable.</p>
                            <p><a id="accommodation" name="accommodation"></a>4: <strong>ACCOMMODATION</strong> <br> The cost of hotel accommodation for nights spent away from home on business may be claimed. The cost of maintaining a rental property is also allowable provided that: Use of the property is necessary for business purposes and A permanent residence is being maintained elsewhere within the UK and a regular pattern of commuting back to that residence is evident.</p>
                            <p>Where a rental property is not used exclusively for business purposes, the proportion of costs relating to the period of private usage is not allowable. In such cases it will be necessary to determine the appropriate split of private and business usage and claim only for the business use.</p>
                            <p>The cost of accommodation in relation to site work is allowable if the period of time at the site is expected to be no more than 24 months in total, including any time spent on- site prior to the current contract. The “40% rule” also applies here.</p>
                            <p><a id="meals" name="meals"></a>5: <strong>MEALS</strong> <br>When staying overnight, breakfast and one other meal per day are allowable. The costs claimed in relation to meals must be reasonable. Benchmark costs are: Breakfast or lunch - £15 in London and £10 outside London Dinner - £40 in London and £30 outside London</p>
                            <p>Where the employee has dined with colleagues, only the share of the total cost that pertains to the employee is allowable. No alcoholic beverages may be claimed for. If an employee in rented accommodation prepares his or her own meals, the cost of the food element of one meal per day is an allowable expense. Such a claim should be made supported by supermarket receipts, with the relevant food element clearly marked. Where receipts are submitted in relation to meal overseas, appropriate identification and explanation of the receipts must be provided in English.</p>
                            <p><a id="entertainment" name="entertainment"></a>6: <strong>BUSINESS ENTERTAINMENT</strong> <br> The cost of entertaining clients or anyone else is not allowable.</p>
                            <p><a id="clothing" name="clothing"></a>7: <strong>PROTECTIVE CLOTHING</strong> <br> The cost of the upkeep, repair and replacement of protective clothing and uniforms is allowable where the employee’s duties require such items to be worn.</p>
                            <p><a id="tools" name="tools"></a>8: <strong>TOOLS AND EQUIPMENT (INCLUDING COMPUTER EQUIPMENT)</strong><br> The cost of the upkeep, repair and replacement of tools and equipment is allowable where these items are:</p>
                            <p>Wholly, necessarily and exclusively used in one’s work &ndash; no element of personal use of tools or equipment is allowable &ndash; and; Not fixed assets. For example, items such as an office desk or chair or a portable workbench are classed as fixed assets and therefore would not be allowable. As a rule of thumb, items costing less than £200 are likely to be allowable.</p>
                            <p>Whether or not a particular tool or piece of equipment is classed as work related and, therefore, allowable will depend on the particular circumstances of each assignment.</p>
                            <p><a id="subs" name="subs"></a>9: <strong>SUBSCRIPTIONS TO PROFESSIONAL SOCIETIES</strong> <br> An employee may claim the annual subscriptions paid to certain approved professional bodies or societies, where their activities are relevant to the type of work undertaken by the employee. A claim may also be made for certain statutory fees, where paying them is a pre-condition of being able to do the work. A list of approved institutions is published on the Inland Revenue’s website.</p>
                            <p><a id="station" name="station"></a>10: <strong>STATIONERY, POSTAGE, FAX AND TELEPHONES ETC</strong> <br> The cost of stationery, postage, sending and receiving faxes, printing, photocopying and phone calls etc. is an allowable business expense, provided that these are wholly, exclusively and necessarily incurred in the performance of the duties of employment and provided that documentary proof is made available. Allowable costs may include telephone calls made and faxes sent from home, also mobile phone costs, including the cost of buying the mobile phone. Again, each item of expenditure must be business- related. The cost of submitting weekly hours and expense claims by email, fax or post is not.</p>
                            <p><a id="training" name="training"></a>11: <strong>TRAINING</strong> <br> The cost of an employee’s work-related training courses is an allowable expense.</p>
                            <p><a id="incidentals" name="incidentals"></a>12: <strong>INCIDENTALS</strong> <br> Other expenses not specifically identified above may be allowable, depending on individual circumstances and subject to their being incurred for business purposes.</p>
                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_contractorlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>