<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : Changing Accountants:  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="If you want a dedicated contractor accountant with a fully managed service then EAFS are fully accredited contractor accountants." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : Changing Accountants :   <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="If you want a dedicated contractor accountant with a fully managed service then EAFS are fully accredited contractor accountants." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/changing-accountants/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : Changing Accountants  :   <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/changing-accountants/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="If you want a dedicated contractor accountant with a fully managed service then EAFS are fully accredited contractor accountants."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Changing Accountant, Contractor Accountants" />
    <meta name="description" content="If you want a dedicated contractor accountant with a fully managed service then EAFS are fully accredited contractor accountants." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/changing-accountants/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants : Changing Accountants</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/" title="Contractor Accountants" alt="Contractor Accountants"><span itemprop="title">Contractor Accountants</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Changing Accountants</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Changing Accountants" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor Accountants : Changing Accountants</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>If you require a dedicated contractor accountant with a fully managed service, then EAFS is here to assist.  If you are presently with an Umbrella Company but would rather have the benefits of running your own Limited Company, then EAFS are here to help you make the transition. </strong></p>
                            <p>The process can be relatively simple, just inform your Umbrella Company or current accountant that you no longer require their services. EAFS can then assist in arranging the formation of your new Limited Company, even on the same day. If you already have an existing Limited Company and simply require a change of accountants, we can also help you with the transfer over to using EAFS as your new accountancy service.</p>
                            <p>Working through your own Limited Company is by far the most tax efficient way of working. Alongside having far more control, you will also find that you will keep more of your earnings than you would with any other solution. </p>
                            <h4><strong>How Much More Can I Earn? </strong></h4>
                            <p>The difference in the amount of earnings you can take home between using an Umbrella Company or Limited Company is clearly quite visible.</p>
                            <ul type="disc">
                                <li>With an Umbrella Company you&rsquo;ll typically be taking home 60%-65% of your contract value.</li>
                                <li>With your own Limited Company this figure is more likely to be between 75%-85% of the contract value.</li>
                            </ul>
                            <p>In addition, with the Governments Flat Rate VAT Scheme you will be enabled to take a percentage of the VAT you charge to your clients/agency. This gives you the potential to make even more money!</p>
                            <p>As an example, using the Flat Rate VAT scheme with a £50,000 contract in your first year of using the scheme, you&rsquo;ll take home additional earnings of £1,900. In turn, a contract worth £100,000 will result in additional earnings of £3,800. </p>
                            <h4><strong>Is it not complicated or time consuming to run your own Limited Company?</strong></h4>
                            <p>The simple answer is no, it isn&rsquo;t. Even during a contract, you can switch to being a Limited Company at any point. Setting up your new company only takes a few minutes. Alternatively, you can let EAFS do all this for you, from then on working with you and dealing with your VAT and taxation affairs.</p>
                            <h4><strong>What about EAFS Fees? </strong></h4>
                            <p>Our fees are fixed and will be clearly detailed within our free initial meeting with you. We&rsquo;ll take care of almost every last detail, all you&rsquo;ll need to do it around 15 minutes worth of administration a month, leaving you free to enjoy the rewards from your contract work. Our guarantee is to provide a proactive, prompt and efficient service. </p>
                            <h4><strong>I&rsquo;m inside IR35; can it be worth it?</strong></h4>
                            <p>If your contract and working practices look like you are inside IR35 then you can still claim your traveling and accommodation expenses. You can also claim 5% of your turnover, benefits from the VAT flat rate scheme and receive interest on the funds held within your own company. <br>
                            </p>
                            <p>In summary, yes, from a financial point of view it is well worth it. Also, any other contract work you do could also be put through your existing company. </p>
                            <h4><strong>So what do I need to do?</strong></h4>
                            <p>Your company will be set up the same day, and all documentation will be sent out by first class post within 24 hours. There is a one off charge of just £125 plus VAT, for setting up your Limited Company and includes opening a Company Bank Account, VAT/PAYE registration and advice on share structure.</p>
                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_serviceslinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>