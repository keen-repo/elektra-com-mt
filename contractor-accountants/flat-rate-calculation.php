<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : VAT Flat Rate Scheme Calculation :  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Some limited companies are better off using a flat rate VAT scheme over the standard VAT scheme. Visit our VAT Flat Rate Calculation page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : VAT Flat Rate Scheme Calculation :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Some limited companies are better off using a flat rate VAT scheme over the standard VAT scheme. Visit our VAT Flat Rate Calculation page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/flat-rate-calculation/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : VAT Flat Rate Scheme Calculation :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/flat-rate-calculation/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Although most UK businesses operate via the standard VAT scheme, some limited companies may be better off by using the flat rate VAT scheme. Visit our calculation page to view the calculation process."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="VAT Flat Rate Scheme Calculation, Flat Rate Calculation, Flat Rate, VAT, Contractor Accountants" />
    <meta name="description" content="Some limited companies are better off using a flat rate VAT scheme over the standard VAT scheme. Visit our VAT Flat Rate Calculation page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/flat-rate-calculation/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants : VAT Flat Rate Scheme Calculation</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemscope itemtype="https://schema.org/Breadcrumb">
                        <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url">
                            <span itemprop="title">Home</span></a></li>
                    <li itemscope itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contractor-accountants/" title="Contractor Accountants" alt="Contractor Accountants" itemprop="url">
                            <span itemprop="title">Contractor Accountants</span></a></li>
                    <li itemscope itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contractor-accountants/value-added-tax/" title="VAT Services" alt="VAT Services" itemprop="url">
                            <span itemprop="title">VAT</span></a></li>
                    <li itemscope itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Flat Rate Scheme calculator</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="VAT Flat Rate Scheme Calculation" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">VAT Services : VAT Flat Rate Scheme Calculation</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>To calculate your VAT payable to HMRC, apply your flat rate VAT percentage to your &lsquo;Flat Rate Turnover&rsquo;.  If you are still within your first year of VAT registration then remember to reduce your flat rate percentage by one per cent.</strong></p>
                            <p>Your flat rate turnover is all the supplies your business makes including:</p>
                            <ul type="disc">
                                <li>VAT      inclusive sales for standard rate, zero rate and reduced rate supplies.</li>
                                <li>Sales of      exempt supplies, such as rent or lottery commission - you don't have to      make any partial exemption calculations. </li>
                                <li>Sales of      capital expenditure goods - unless you have previously reclaimed the VAT,      in which case they must be accounted for at the standard rate and not the      flat rate.</li>
                                <li>Sales to      other EU countries.</li>
                                <li>Sales of      second-hand goods - but if you sell a lot of these, you may be better off      leaving the Flat Rate Scheme and using a margin scheme.</li>
                            </ul>
                            <p>Please Don't Include:</p>
                            <ul type="disc">
                                <li>Services      you've purchased from outside the UK that you've had to reverse charge.</li>
                                <li>Disbursements      - costs you pass on to your clients that meet the necessary VAT conditions.</li>
                                <li>Private      income, for example income from shares.</li>
                                <li>Bank      interest received on a business account.</li>
                                <li>The      proceeds from the sale of goods you own but which have not been used in      your business.</li>
                                <li>Any sales      of gold that are covered by the VAT Act, Section 55.</li>
                                <li>Non-business      income and any supplies outside the scope of UK VAT.</li>
                                <li>Sales of      capital expenditure goods on which you have claimed back the VAT you paid.</li>
                            </ul>

                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_contractorlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>