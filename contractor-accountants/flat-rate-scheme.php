<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : VAT Flat Rate Scheme :  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Although most UK businesses operate via the standard VAT scheme, some limited companies may be better off by using the flat rate VAT scheme." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : VAT Flat Rate Scheme :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Although most UK businesses operate via the standard VAT scheme, some limited companies may be better off by using the flat rate VAT scheme." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/flat-rate-scheme/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : VAT Flat Rate Scheme :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/flat-rate-scheme/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Although most UK businesses operate via the standard VAT scheme, some limited companies may be better off by using the flat rate VAT scheme."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="VAT Flat Rate Scheme, Flat Rate, VAT, Contractor Accountants" />
    <meta name="description" content="Although most UK businesses operate via the standard VAT scheme, some limited companies may be better off by using the flat rate VAT scheme." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/flat-rate-scheme/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants : VAT Flat Rate Scheme</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home">
                            <span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/" title="Contractor Accountants" alt="Contractor Accountants">
                            <span itemprop="title">Contractor Accountants</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/value-added-tax/" title="VAT Services" alt="VAT Services">
                            <span itemprop="title">VAT</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Flat Rate Scheme</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="VAT Flat Rate Scheme" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">VAT Services : VAT Flat Rate Scheme</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>Using standard VAT accounting, the VAT that you pay to or claim back from HM Revenue &amp; Customs is the difference between the VAT that you charge your customers and the VAT you pay on your purchases. Using the Flat Rate Scheme, you pay VAT as a fixed percentage of your VAT inclusive turnover. The actual percentage used depends on your type of business. </strong></p>
                            <h4><strong>Who can join the Flat Rate Scheme? </strong></h4>
                            <p class="has-pullquote pullquote-adelle" data-pullquote="Using the Flat Rate Scheme, you pay VAT as a fixed percentage of your VAT inclusive turnover.">You can join the Flat Rate Scheme for VAT if your estimated VAT taxable turnover – excluding VAT – in the next year will be less than £150,000. Your VAT taxable turnover is an accumulation of everything that you sell during the year that is liable for VAT, including standard or reduced rate or zero rate sales. It excludes the actual VAT that you charge, VAT exempt sales and sales of any capital assets. </p>
                            <p>Generally, you don&rsquo;t reclaim any of the VAT that you pay on purchases, although you may be able to claim back the VAT on capital assets worth more than £2,000. Once you have joined, you can stay within the scheme until your total business income is in excess of £230,000. </p>
                            <h4><strong>Who can't join the Flat Rate Scheme? </strong></h4>
                            <p>Unfortunately, you can't join the Flat Rate Scheme if:</p>
                            <ul type="disc">
                                <li>You were      in the scheme and absconded during the previous 12 months. </li>
                                <li>You are or      have been, within the previous 24 months, eligible to join an existing VAT      group or register for VAT as a division of a larger business. </li>
                                <li>You use      one of the margin schemes for second-hand goods, art, antiques and      collectibles, the tour operators margin scheme or the capital goods      scheme. </li>
                                <li>You have      been convicted of a VAT offence or been charged a penalty for VAT evasion      within the last 12 months. </li>
                                <li>You      business is closely associated with another or similar business. <br>
                                </li>
                            </ul>
                            <h4><strong>Working out your Flat Rate Percentage and VAT. </strong> </h4>
                            <p>There is a range of flat rate percentages that correspond to different business sectors. You&rsquo;ll need to choose the sector that best describes your main business activity for the coming year, using only one percentage. If you work in more than one business sector, you must select what represents the greater part of your turnover. You&rsquo;ll then need to apply that percentage to your total turnover. </p>
                            <p>There&rsquo;s a one per cent reduction in the flat rate percentages for your first year of VAT registration. This reduces the flat rate percentage until the day before the first anniversary of your VAT registration, the discount even applying if the flat rate percentage changes for your sector during your first registered year.</p>

                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_contractorlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>