<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants :  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="We are a firm of Contractor Accountants & IR35 specialists. Market leaders in providing accountancy & tax services to Contractors and Freelance Professionals." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="We are a firm of Contractor Accountants & IR35 specialists. Market leaders in providing accountancy & tax services to Contractors and Freelance Professionals." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="We are a firm of Contractor Accountants & IR35 specialists. Market leaders in providing accountancy & tax services to Contractors and Freelance Professionals."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Contractor Accountants, Accountancy for Contractors" />
    <meta name="description" content="We are a firm of Contractor Accountants & IR35 specialists. Market leaders in providing accountancy & tax services to Contractors and Freelance Professionals." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Contractor Accountants</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Contractor Accountants" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor Accountants</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>EAFS are Specialist Contractor Accountants, with extensive expertise in contract reviews for IR35. Providing tailored Limited Company accounting services specifically for contractors, we allow you to concentrate on your area of expertise as we take care of all accountancy, tax and planning compliance. This leaves you free to enjoy your spare time or a well-deserved rest from your employment without the hassle of sorting out and ensuring legality over your tax affairs. </strong></p>
                            <p>We have established ourselves in the contractor payroll sector for over a decade, accumulating a wealth of experience and building a strong team to support your accounting needs.</p>
                            <h4><strong>Accountancy for Contractors</strong></h4>
                            <p>Whether this is the first time that you have considered operating though your own Limited Company or you have experience in operating this way, EAFS can provide all the advice and support you need when setting up and running your Limited Company. </p>
                            <p>All of our fully qualified accountants are selected for their knowledge and expertise in contractor accounting, providing the technical expertise to back up our service. They are also here to answer your accounting queries, which can be accomplished face-to-face or via telephone calls. There is no limit as to the number of times you can call or the amount of advice you can receive. <br>
                                For one of the most competitive monthly fees in the market, EAFS provide:</p>
                            <ul type="disc">
                                <li>Same Day Formation of your Limited Company.</li>
                                <li>A Registered Office Address.</li>
                                <li>Quarterly Bookkeeping.</li>
                                <li>Personal Tax Advice.</li>
                                <li><a href="<?php echo SITE_URL;?>contractor-accountants/ir35-explained/" title="IR35 Review" target="_self">IR35 Review</a></li>
                                <li>Guidance on IR35 &amp; <a href="<?php echo SITE_URL;?>contractor-accountants/s660-settlements/" title="S660 Settlements Reports" target="_self">S.660</a></li>
                                <li>Provision of Mortgage or Tenant Reference.</li>
                                <li>VAT and PAYE Registration for your Limited Company.</li>
                                <li>Free Business Banking with <a href="<?php echo SITE_URL;?>contractor-services/business-bank-account/" title="Cater Allen Private Bank" target="_self">Cater Allen Private Bank</a></li>
                                <li>Preparation and Submission of Quarterly VAT Returns.</li>
                                <li>Submission of Year-end PAYE returns (P35&amp; P14, P60, P11D etc.)</li>
                                <li>Declaration of Dividends.</li>
                                <li>Annual Statutory Accounts.</li>
                                <li>Filing all Statutory Returns including Corporation Tax Returns.</li>
                                <li>A Personal Account Manager.</li>
                                <li>Dealing with all correspondence from HMRC &amp; Companies House.</li>
                                <li>Submission of Companies House Annual Return.</li>
                                <li>Unlimited support.</li>
                            </ul>
                            <p>When it comes down to choosing the right accounting firm, you can&rsquo;t afford to take any chances. You need the knowledge that you&rsquo;ve been given the right advice, that your monetary affairs are in order and that you aren&rsquo;t over or under paying tax. EAFS can help to give you complete peace of mind.</p>
                            <p>By using a specialists limited company accountants like EAFS, you could considerably increase your net profit. As a firm of specialist Contractor Accountants we have the expertise and experience to identify tax savings and tax planning opportunities from which you can benefit from. To discuss further please contact us on +44 (0)131 526 3300 or fill in our <a href="<?php echo SITE_URL;?>company-information/contact-eafs/" title="Contact EAFS Today" target="_self">online enquiry</a> form and one of our consultants will call you back.</p>
                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_contractorlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>