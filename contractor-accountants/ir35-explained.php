<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : IR35 Explained :  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="IR35 was brought about to ensure small companies, contractors and freelancers operate in the proper manner. Visit our IR35 Explained page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : IR35 Explained : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="IR35 was brought about to ensure small companies, contractors and freelancers operate in the proper manner. Visit our IR35 Explained page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/ir35-explained/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : IR35 Explained : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/ir35-explained/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="IR35 was brought about to ensure small companies, contractors and freelancers operate in the proper manner. Visit our IR35 Explained page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="IR35 Explained, IR35, Contractor Accountants" />
    <meta name="description" content="IR35 was brought about to ensure small companies, contractors and freelancers operate in the proper manner. Visit our IR35 Explained page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/ir35-explained/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants : IR35 Explained</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/" title="Contractor Accountants" alt="Contractor Accountants"><span itemprop="title">Contractor Accountants</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">IR35 Explained</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="IR35 Explained" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor Accountants : IR35 Explained</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>IR35 affects all contractors who do not meet the Inland Revenue's definition of 'self employed'. So, are you self-employed? The first and most important point to establish is whether the IRs definition refers to you. Unfortunately, the ambiguities of the &lsquo;employment status&rsquo; guidelines don&rsquo;t really help matters. The Inland Revenue state that they will take an overall view of a contractors position to determine if they are deemed &lsquo;employed&rsquo; under the rules. Therefore, any amended contracts should also reflect your working practices.</strong></p>
                            <h4><strong>Self-Employed Quiz</strong></h4>
                            <p>If the answer is &lsquo;Yes&rsquo; to all of the following questions, it usually means the worker is self-employed.</p>
                            <ul class="check">
                                <li>Can they hire someone to do the work or engage helpers at their own expense?</li>
                                <li>Do they risk their own money?</li>
                                <li>Do they provide the main items of equipment they need to do their job? Not just the small tools that many employees provide for themselves? </li>
                                <li>Do they agree to do a job for a fixed price regardless of how long the job may take?</li>
                                <li>Can they decide what work to do, how and when to do the work and where to provide the services?</li>
                                <li>Do they regularly work for a number of different people?<br>
                                </li>
                            </ul>
                            <h4><strong>IR35 Explained.</strong></h4>
                            <p>If you provide your services to a client or end-user via an intermediary, typically a service company or partnership, and the intermediary doesn&rsquo;t meet the definition of a &lsquo;Managed Service Company&rsquo;, then the IR35 legislation may apply to engagements with that client. </p>
                            <p>Broadly, it applies to those engagements where:</p>
                            <ul type="disc">
                                <li>You      personally perform services for another person (the client).</li>
                                <li>The      services are not directly provided with the client but under arrangements      involving an intermediary.</li>
                                <li>The      circumstances are such that, if you had provided the services directly to      the end user under a contract between you and the client, you would have      been regarded for income tax purposes as an employee of the client and/or,      for NICs purposes, as employed in employed earner&rsquo;s employment by the      client.</li>
                            </ul>
                            <p>In addition, you must receive rights entitling you to obtain a payment or benefit that is not employment income.</p>
                            <h4><strong>Occupations affected by the Legislation.</strong></h4>
                            <p>The legislation is not targeted at any particular occupation or business sector. It can apply to any occupation in any business sector. Examples include medical staff, chief executives of large plc.&rsquo;s, teachers, legal and accountancy staff, construction industry workers, IT contractors, engineering contractors and clerical workers.</p><p>
                                The rules will apply if the intermediary does not meet the &lsquo;Managed Service Company&rsquo; definition and you answer &lsquo;yes&rsquo; to the following:</p>
                            <ul class="check">
                                <li>Would you      be an employee if you worked for your client directly and not through your      company or partnership?</li>
                                <li>Does the      company or partnership you work through meet certain conditions?</li>
                            </ul>
                            <h4><strong>What does it mean for Contractors?</strong></h4>
                            <p>If your contract and working practices give the impression that you are inside, or &lsquo;caught&rsquo;, by IR35 legislation then you will ultimately have to pay full tax and full National Insurance. This is instead of the usual salary and dividends from the profits of your company. Unfortunately, the expenses you can claim will also be reduced. This is due to the HM Revenue and Customs belief that because you aren&rsquo;t taking financial risks or obtaining the same level of control as a director of your own company, you shouldn&rsquo;t be entitled to the same corporate tax structure. <br>
                                If an investigation is launched by HMRC and you are discovered as a &ldquo;disguised employee&rdquo;, inside IR35, you will be liable for any taxes you have evaded. You&rsquo;ll be liable for these due to being afforded a lower tax rate by your Limited Company status, alongside a heavy fine. </p>
                            <p>The only way to be sure if your contract falls inside or outside of IR35, is to have the contract reviewed by a specialist. EAFS can provide our clients with a free IR35 contract review. </p>
                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_contractorlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>