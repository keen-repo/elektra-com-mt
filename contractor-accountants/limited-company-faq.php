<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : Limited Company FAQ :  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Frequently Asked Questions regarding limited company formations & financial solutions for business. Visit our Limited Company FAQ page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : Limited Company FAQ :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Frequently Asked Questions regarding limited company formations & financial solutions for business. Visit our Limited Company FAQ page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : Limited Company FAQ :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Frequently Asked Questions regarding limited company formations & financial solutions for business. Visit our Limited Company FAQ page for more information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Company Formations, Company Registration, Limited Company Formation, Trademark Registration, Company Formation, Setting Up a Limited Company" />
    <meta name="description" content="Frequently Asked Questions regarding limited company formations & financial solutions for business. Visit our Limited Company FAQ page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants : Limited Company FAQ</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home">
                            <span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/" title="Contractor Accountants" alt="Contractor Accountants>
                            <span itemprop="title">Contractor Accountants</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/limited-company-services/" title="Limited Company Services" alt="Limited Company Services">
                            <span itemprop="title">Limited Company Services</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Limited Company FAQ</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
            <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Limited Company FAQ" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Limited Company Services : Limited Company FAQ</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>Whether a contactor completely new to the industry or a seasoned professional, you can still have questions about Limited Companies. We aim to answer all basic enquiries here on this page, from informing you as to wage differences and expenses that can be claimed right through to IR35 and Insurances. If you need to get in touch regarding clarification or if you have a complex situation, don’t hesitate to <a href="#" title="Contact for Contractor Accountants" alt="Contact for Contractor Accountants">Contact Us</a> or call us on; +44 (0) 131 526 3300 . </strong></p>
                            <div class="widget">
                                <h4>Frequently Asked Questions</h4><br>
                                <div class="clearfix" id="accordion-second">
                                    <div id="accordion3" class="accordion">
                                        <div class="accordion-group">
                                            <div class="accordion-heading active">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle1" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle collapsed">
                                                    <em class="icon-fixed-width fa-minus fa"></em>How much of my income can I expect to take home operating my own Limited Company?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle1" style="height: 0px;">
                                                <div class="accordion-inner">
                                                    <p> Operating your own limited company gives you the opportunity to minimise the amount of tax you have to pay through tax planning. Don’t panic though, as our dedicated EAFS accountant will discuss all these opportunities with you. As a general rule of thumb, you could take home between 75-85% of your contract income...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle2" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>Which is better, my own Limited Company or an Umbrella Company?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle2">
                                                <div class="accordion-inner">
                                                    <p>Both options offer their own advantages depending on your circumstances and requirements. If you are uncertain as to which option is best for you please contact one of our specialist business consultants. As EAFS provide both Limited Company Services and an Umbrella Solution, you can be assured that you will be given unbiased advice as to which option is best for you...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle3" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>How much admin time and paperwork is involved?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle3">
                                                <div class="accordion-inner">
                                                    <p> At Euro Accountancy & Finance Services, our services offer you the choice of spending as much or as little time on admin as you favour. By choosing our standard service you will prepare your own sales invoices and spend no more than 15-20 minutes each month completing our simple and easy to use spreadsheets. If paperwork and admin are not for you, then have a look at our additional services that can remove this burden from you...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle4" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>What sort of expenses can I claim for?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle4">
                                                <div class="accordion-inner">
                                                    <p> When operating through your own Limited Company, you will incur expenses that will generally fall into two types; those incurred in performing your contract (e.g. travel) and those incurred to administer your company (e.g. accountancy fees). It is most likely that those incurred in performing your contract will be made from your own finances and will therefore need to be reimbursed from company funds. An expense claim form, detailing each expense, will need to be completed to support this payment to yourself.</p>
                                                    <p> As a client of EAFS, you will be provided with an expenses guide. This outlines the most common expenses that can be incurred by your business, to ensure expenses are claimed correctly and avoid any potential personal tax charges.</p>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle5" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>What if I decide to return to permanent employment?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle5">
                                                <div class="accordion-inner">
                                                    <p> You can simply choose to leave your company dormant, if you plan on going back to contracting, or close your company down. In the event that you choose to close your company, EAFS will be there to assist you in this process at no extra cost...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle6" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>What are dividends and will I have to pay income tax on them?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle6">
                                                <div class="accordion-inner">
                                                    <p>A dividend is a slice of a company’s profit that is given to the shareholders. This means you, if you operate your own Limited Company. Dividend income that you receive will not be subject to National Insurance contributions and will be tax free if you are a basic rate taxpayer. 25% income tax is applied on the amount of dividend income that is received above the basic rate tax threshold...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle7" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>If I don’t trade for a month, do I still have to pay EAFS?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle7">
                                                <div class="accordion-inner">
                                                    <p>Unfortunately, you will still have to pay the full amount for that month. Our accounting fee is based on a fixed annual amount, agreed in advance, for all the work we perform for your company throughout the year. In return for agreeing a fixed fee in advance, we request that payment is broken down into 12 monthly instalments...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle8" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>What is IR35?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle8">
                                                <div class="accordion-inner">
                                                    <p>"IR35" is a tax legislation that affects all contractors and therefore has to be given serious consideration. The legislation allows HMRC to tax the contractor, operating through their own Limited Company, as though they were an employee of the end client, rather than under normal Limited Company rules. The legislation applies to each contract you engage upon rather than to your actual company, so IR35 may apply to one contract but not the next.</p>
                                                    <p>In determining whether or not IR35 applies to your contract, you will need to consider the terms and conditions of the contract, together with your actual working practices. To determine your IR35 status, it would be advisable to seek expert advice and have your contract and working practices assessed by an industry professional.</p>
                                                    ...
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle9" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>If my contract is within IR35, are there still any advantages to operating my own Limited Company?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle9">
                                                <div class="accordion-inner">
                                                    <p>Yes, even if your contract is considered to be inside IR35, there are still some benefits that can make it worthwhile. You can still claim 5% of your turnover to cover company administrative costs, claim the benefit of being on the flat rate vat scheme, claim any HMRC online filing incentives and earn interest on funds held in the business bank account...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle10" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>How do I set up a Limited Company and how long does it take?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle10">
                                                <div class="accordion-inner">
                                                    <p>You can easily form your own Limited Company in an efficient and easy five-step guide, only taking a matter of minutes to complete. Once you have submitted all your details, we can incorporate your company within 24 hours. Alternatively, contact one of our specialist business consultants.</p>
                                                    <p>You can easily form your own Limited Company in a very simple five-step guide, which should only take you a matter of minutes to complete. Once you have submitted all your details, we can have your company incorporated within 24 hours. Alternatively, contact one of our specialist business consultants...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle11" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>How do I operate my own Limited Company?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle11">
                                                <div class="accordion-inner">
                                                    <p>When forming your own Limited Company, you will be appointed as director (and employee) of the company and will be responsible for managing the company affairs. The company will pay you a salary for acting in this capacity. You will also be the owner (shareholder) of the company and will receive dividend income from the company&rsquo;s taxed profits. The frequency and amount of this dividend income is decided upon by the company&rsquo;s director(s). All the cash held in the company bank account legally belongs to the company, the finances of the company are separate to your own personal finances. You can extract funds from the company through a combination of the following:</p>
                                                    <p><strong>Salary</strong> – as an employee, a net salary is paid after income tax and NI deductions.<br>
                                                        <strong>Dividends</strong> – paid to you as a company shareholder.<br>
                                                        <strong>Expenses</strong> – you can claim your allowable business expenses.</p>
                                                    <p>Each year, the company will have to pay corporation tax on its profit, where profit is calculated as the total amount of sales income earned – less any business expenses incurred. As a director of a UK Limited Company, you will be legally obliged to complete a personal tax liability. You may also have to pay additional income tax on your dividend income if you are a higher rate taxpayer. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle12" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>What insurances will I need?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle12">
                                                <div class="accordion-inner">
                                                    <p>When operating your own business, you will need to consider if there is a need to take out any business insurances. This can include professional indemnity, public liability, employer liability or business equipment cover. It is advised that you review your new contract as this may stipulate a minimum level of insurance cover required to fulfil the contract...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle13" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>What fees do EAFS charge?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle13">
                                                <div class="accordion-inner">
                                                    <p>Here at Euro Accountancy & Finance Services, we understand that not every client requires the same exact degree of service. We provide a standard service that can be complemented with additional amenities to suit your needs. All fees are fixed and agreed in advance, starting from as little as £80 plus VAT per month...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/#collapseToggle14" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                                    <em class="fa fa-plus icon-fixed-width"></em>How do I engage EAFS as my accountant?
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseToggle14">
                                                <div class="accordion-inner">
                                                   <p> Quite simply, call us on +44(0)131 220 2820 and speak to one of our specialist business consultants. Alternatively, arrange a no-obligation meeting to discuss your requirements...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- end accordion -->
                                </div><!-- end accordion first -->
                            </div>
                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                    </div><!-- end articleBody -->
                    </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_contractorlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>