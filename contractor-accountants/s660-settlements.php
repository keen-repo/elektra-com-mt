<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : S660 Settlements :  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="The S660 rules on settlements legislation have been around for the last 80 years in relation to family. Visit our S660 Settlements page for more information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : S660 Settlements : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="The S660 rules on settlements legislation have been around for the last 80 years in relation to family. Visit our S660 Settlements page for more information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/s660-settlements/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : S660 Settlements :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/s660-settlements/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="The S660 rules on settlements legislation have been around for the last 80 years in relation to family. Visit our S660 Settlements page for more information.."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="S660 Settlements Explained, Contractor Accountants, S660 Settlements, S660" />
    <meta name="description" content="The S660 rules on settlements legislation have been around for the last 80 years in relation to family. Visit our S660 Settlements page for more information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/s660-settlements/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants : S660 Settlements</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/" title="Contractor Accountants" alt="Contractor Accountants"><span itemprop="title">Contractor Accountants</span></a></li>
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Contractor Accountants : S660 Settlements</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="S660 Settlement" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor Accountants : S660 Settlements</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>The S660 rules, also known as &lsquo;Settlements Legislation&rsquo;, have been present since the 1930&rsquo;s. The original rule stops you from passing income to someone else in the family, or giving income or assets to someone else in an effort to reduce your overall tax bill. This is called a &ldquo;settlement&rdquo;, the aim of the legislation is to also eliminate settling of income with an individual who pays a lower tax rate. </strong></p>
                            <h4><strong>Husband &amp; Wife Companies</strong></h4>
                            <p>In such companies, a husband and wife may – for example – each own an equal number of shares with one being the main fee-earner and the other being responsible for little or no fee income. By paying out profits by the way of dividend, income earned by one of the couple can be partly received and taxed on the other. Resulting in an overall saving on tax, as there will be two lots of personal allowance and basic rate tax band to use up. Currently a very common scenario with thousands of husband and wife companies using, what has always been deemed, an acceptable tax plan.</p>
                            <p>Over the past number of years, there has been uncertainty surrounding the taxation of family businesses. You&rsquo;ll likely be investigated if openly abusing the dividend system, particularly where the main earner provides personal services and does not draw a market value salary, with dividends paid to shareholders on lower tax bands.</p>
                            <p>The legislation falls into difficulty when the company formation and the profit generated from the companies income is shared between spouses usually by not exclusively on an equal basis. In this scenario the primary income-generating spouse takes a small salary to maximise the dividend shared with the other spouse.</p>
                            <p>The settlements legislation normally applies where the individual, in receipt of the income, is the spouse or civil partner of the settlor (unless such a gift is outright and unconditional, and of the capital and income from that share).</p>
                            <p>Broad enough to cover cohabiters&rsquo; arrangements, the settlements legislation would normally apply when:</p>
                            <ul type="disc">
                                <li>The      settling party has retained some &ldquo;reversionary interest&rdquo; over the shares      that have been issued.</li>
                                <li>It has been      issued or given to the cohabitee (for instance a right to acquire the      shares back or the right to enjoy proceeds from, or the income from, the      shares given away).</li>
                            </ul>
                            <p>If, for example, a non-working cohabiting party receives a dividend paid direct into a joint bank account, or pays a share of a joint mortgage, then the cohabite would be considered to have retained an interest. The best way to be sure of avoiding an issue is to make the jointly owned company, or partnership, as commercial an arrangement as possible.</p>
                            <h4><strong>The Inland Revenue does acknowledge…</strong></h4>
                            <p>Where a business has real substance with employees, premises and the profit earning is not just down to one shareholder, s660 should not apply. In other cases there are some simple steps that can be put in place to provide some protection:</p>
                            <ul type="disc">
                                <li>Avoid      having differing classes of shares. Preferably, all shares should have the      same income, capital and voting rights.</li>
                                <li>Where      possible, both parties should contribute to the company&rsquo;s business and      there should be documented evidence of this.</li>
                                <li>Both      parties should subscribe for shares from their own resources and introduce      real value to the business.</li>
                                <li>Dividend      waivers should be avoided.</li>
                            </ul>
                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_contractorlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>