<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : VAT Services :  <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Our qualified accountants can assist you in completing all VAT necessities, providing VAT services, dealing with HMRC investigations & advising on HMRC issues." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : VAT Services :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Our qualified accountants can assist you in completing all VAT necessities, providing VAT services, dealing with HMRC investigations & advising on HMRC issues." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-accountants/value-added-tax/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : VAT Services :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-accountants/value-added-tax/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Our qualified accountants can assist you in completing all VAT necessities, providing VAT services, dealing with HMRC investigations & advising on HMRC issues."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Tax Returns, VAT Returns, Tax Advice" />
    <meta name="description" content="Our qualified accountants can assist you in completing all VAT necessities, providing VAT services, dealing with HMRC investigations & advising on HMRC issues." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-accountants/value-added-tax/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Accountants : VAT Services</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/" title="Contractor Accountants" alt="Contractor Accountants"><span itemprop="title">Contractor Accountants</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">VAT Services</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="VAT Services" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">VAT Services</h2>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>VAT, or Value Added tax, is a sales tax added to the price of most goods and services in the UK. Many goods and services attract VAT, however, many financial services and items such as books and journals are zero-rated. By becoming VAT registered, contractors working through their own limited company have the potential to save a significant amount of money</strong></p>
                            <p class="has-pullquote pullquote-adelle" data-pullquote="EAFS offer specialist VAT services for contractors.">Any limited company that has a gross income of £77,000 per year, known as the VAT threshold, must register for VAT or risk being heavily fined. Any contractor who works for a company turning over less than the threshold can also voluntarily register for VAT. For many contractors, this is well worth considering. </p>
                               <p> As part of our service to contractors, Euro Accountancy &amp; Finance Services will process your VAT returns for you. All that you need to do is send your paperwork in and we will do the legwork – including filling in your on-line return. Simply check the paperwork and pay the VAT via electronic transfer or direct debit when the return is filed. <br>
                            </p>
                            <h4>Contractor Invoices – Output Tax</h4>
                            <p>Output tax relates to the VAT charged on goods &amp; services. As a contractor registered for VAT, when you prepare and issue your invoices you must add VAT at the set rate for that financial year. As an example, when invoicing your client or agency for 5 days work at a daily rate of £500 and tax rate of 20%, the invoice would be set out as follows:<  </p>
                            <div class="alert alert-warning">
                                <p>VAT at 20% the calculation = £2,500*20% = £500<br>
                                    Total including VAT = £2,500 + £500 = £3,000</p>
                            </div>
                            <p>At the end of every quarter, you as the contractor must add up all the output tax that you have charged your clients. Then you must take away any input tax (see below) and pay the balance to HMRC using the VAT on-line returns system. </p>
                            <h4>Contractor Purchases – Input Tax</h4>
                            <p>The opposite is also true. Alongside Output tax is Input Tax. Whenever your contracting limited company buys goods and services, the cost of these purchases will include someone else&rsquo;s VAT; charged at 20%. To you as the contractor, this is called input tax. </p>
                            <div class="alert alert-warning">
                                <p><em>As an example, if you buy an iPad for £600 and the supplier at the rate has charged the VAT set for the financial year, then you as the contractor can offset the VAT on this purchase against any VAT you have charged clients.</em></p>
                            </div>
                            <div class="alert alert-warning">
                                <p><em>As a further example, the VAT element of a laptop is £600/120*20 = £100.00, which means the price of the laptop excluding VAT is £500.00</em></p>
                            </div>
                            <p>Think about how much VAT could be saved when you add up the cost of all your company purchases, including office supplies and equipment. This can amount to a considerable sum and is well worth taking time to gather VAT receipts and returns for, in a professional opinion.</p>

                            <h4>Other VAT Services &amp; Pages</h4>
                            <div class="custom-services clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 first">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-calendar fa-4x"></i>
                                                    <h3>VAT Returns</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>VAT Returns</h3>
                                                    <p>As a contractor, you have to account for VAT every quarter. You can find further information here. </p>
                                                    <a href="<?php echo SITE_URL;?>contractor-accountants/vat-returns/" title="Link to VAT Returns Page">VAT Returns</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-briefcase fa-4x"></i>
                                                    <h3>Flat Rate Scheme</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Flat Rate Scheme</h3>
                                                    <p>Using the Flat Rate Scheme, you pay VAT as a fixed percentage of your VAT inclusive turnover. </p>
                                                    <a href="<?php echo SITE_URL;?>contractor-accountants/flat-rate-scheme/" title="Link to VAT Flat Rate Scheme Page">VAT Flat Rate Scheme</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-bank fa-4x"></i>
                                                    <h3>Calculation</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Calculation</h3>
                                                    <p>To calculate your VAT payable to HMRC, apply your flat rate VAT to your flat rate turnover. </p>
                                                    <a href="<?php echo SITE_URL;?>contractor-accountants/flat-rate-calculation/" titlt="Link to VAT Flat Rate Calculation Page">VAT Flat Rate Calculation</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->
                            </div>

                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_contractorlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>