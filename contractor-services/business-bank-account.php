<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Services : Business Bank Account : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="EAFS has chosen to offer our contractors a Cater Allen business bank account, as part of the EAFS Contractor service." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Services : Business Bank Account :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="EAFS has chosen to offer our contractors a Cater Allen business bank account, as part of the EAFS Contractor service." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-services/business-bank-account/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Services : Business Bank Account :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-services/business-bank-account/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="EAFS has chosen to offer our contractors a Cater Allen business bank account, as part of the EAFS Contractor service."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Business Bank Account, Business Bank Setup" />
    <meta name="description" content="EAFS has chosen to offer our contractors a Cater Allen business bank account, as part of the EAFS Contractor service." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-services/business-bank-account/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Services : Business Bank Account</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemscope itemtype="https://schema.org/Breadcrumb">
                        <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                    <li itemscope itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contractor-services/" title="Contractor Services" alt="Contractor Services" itemprop="url"><span itemprop="title">Contractor Services</span></a></li>
                    <li itemscope itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Business Bank Account</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry">
                    <div class="col-lg-12">
                        <article itemprop="Business Bank Account" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor Services : Business Bank Account</h2>
                        </header>
                            <p class="lead"><strong>Euro Accountancy &amp; Finance Services have chosen to offer our contractors a Cater Allen business bank account, part of the Abbey and Santander Banking group. This is offered as part of the EAFS Contractor Service. </strong></p>
                            <p>As a prestigious private bank, their traditional values of courtesy, convenience and a predominantly personal service are in line with the values of EAFS. Using the facilities of <strong>Cater Allen, </strong>we have been able to negotiate an extremely attractive <strong>FREE</strong> banking package for our visitors.</p>
                            <h4><strong>Key Features and Benefits</strong></h4>
                            <ul class="check">
                                <li>No minimum      balance required (<em>please note the application form is a generic one,      you do not need the £5,000 min balance if introduced by us</em>).</li>
                                <li>Your      account remains free of charges so long as you remain in credit.</li>
                                <li>Funds may      be held in Pound Sterling, Euro or US Dollar.</li>
                                <li>30 free      transactions per month.</li>
                                <li>Monthly      statements.</li>
                                <li>Full chequebook      and banking facilities - bill payments, standing orders and direct debits      + Internet banking.</li>
                                <li>VISA      Business deferred-debit card if required.</li>
                                <li>Competitive      tiered rates of interest, so the larger the balance the greater the return.</li>
                            </ul>
                            <h4><strong>Account Opening</strong></h4>
                            <p>To open an account with Cater Allen, please download the forms from our <a href="<?php echo SITE_URL;?>company-information/forms-downloads/" title="Link to Application Forms & Downloads Page"><strong>downloads </strong></a> section and send them back to us at: EAFS, 23 Blair Street, Edinburgh, EH1 1QR</p>
                            <h4><strong>Business Bank Account: Application Form</strong></h4>
                            <p>To apply for a Cater Allen business bank account please fill out the application form in our <a href="<?php echo SITE_URL;?>company-information/forms-downloads/" title="Link to Application Forms & Downloads Page"><strong>Downloads Section</strong></a>.</p>

                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                        </article>
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_serviceslinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START SOCIAL -->
                <?php require_once("../partials/widget_socialinner.inc.php");?>
                <!-- WIDGET END SOCIAL -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>