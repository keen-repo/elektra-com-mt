<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php // require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Services : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="EAFS can offer an array of other services to benefit contractors and freelancers. Visit our Other Services pages to find out more. " />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Services : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="EAFS can offer an array of other services to benefit contractors and freelancers. Visit our Other Services pages to find out more. " />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-services/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Services : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-services/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="EAFS can offer an array of other services to benefit contractors and freelancers. Visit our Other Services pages to find out more. "/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="EAFS Services UK, EAFS Other Services UK, EAFS Services" />
    <meta name="description" content="EAFS can offer an array of other services to benefit contractors and freelancers. Visit our Other Services pages to find out more. " />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-services/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Services</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemscope itemtype="https://schema.org/Breadcrumb">
                        <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                    <li itemscope itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Contractor Services</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry">
                    <div class="col-lg-12">
                        <article itemprop="Contractor Services" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor Services</h2>
                        </header>
                            <p class="lead"><strong>We have established ourselves in the contractor payroll sector for over a decade, accumulating a wealth of experience and building a strong team to support your accounting needs.</strong></p>
                            <p class="has-pullquote pullquote-adelle" data-pullquote="Specialist accounting services specifically for UK based contractors.">EAFS are Specialist Contractor Accountants, with extensive expertise in contract reviews for IR35. Providing tailored Limited Company accounting services specifically for contractors, we allow you to concentrate on your area of expertise as we take care of all accountancy, tax and planning compliance. This leaves you free to enjoy your spare time or a well-deserved rest from your employment without the hassle of sorting out and ensuring legality over your tax affairs.<strong> </strong></p>
                            <h4><strong>Contractor Accountants - Insurance for Contractors </strong></h4>
                            <p>Business Insurance Cover is usually required by, and included in the contracts of, agencies and clients. Although no longer a statutory requirement, a company that is genuinely in business on its own account is expected to seek protection from an insurance provider to safeguard assets in case of emergency. <br>
                                <br>
                                As a contractor or consultant, there are a number of different insurance products that you could purchase; yet deciding on what cover you need is difficult. With an extensive knowledge of the freelance industry, Kingsbridge Professional Solutions have put together a cost effective package to meet the exposures that you or your business could face.  By carefully selecting cover appropriate for you, we have removed the complication of picking cover and limits to purchase separately. Here you only have to purchase one policy!</p>
                            <h4><strong>Contractor Accountants - Changing Accountants </strong></h4>
                            <p>If you require a dedicated contractor accountant with a fully managed service, then EAFS is here to assist.  If you are presently with an Umbrella Company but would rather have the benefits of running your own Limited Company, then EAFS are here to help you make the transition. </p>
                            <p>The process can be relatively simple, just inform your Umbrella Company or current accountant that you no longer require their services. EAFS can then assist in arranging the formation of your new Limited Company, even on the same day. If you already have an existing Limited Company and simply require a change of accountants, we can also help you with the transfer over to using EAFS as your new accountancy service. </p>
                            <h4><strong>Contractor Accountants - Mortgages for Contractors </strong></h4>
                            <p>EAFS have partnered with Contractor Mortgages Made Easy (CMME). We have chosen to work with CMME due to their superior client service and experience within the contractor marketplace. CMME have heavily invested in technologies that make the contractors life as smooth as possible when applying for a mortgage. </p>
                                <p>CMME is a leading national mortgage brokerage, specialising in bespoke mortgage solutions for contractors. They help contractors to secure a competitive mortgage straightforwardly with none of the hassle that you may have experienced if you have already approached a lender.</p>
                            <p>When it comes down to choosing the right accounting firm, you can&rsquo;t afford to take any chances. You need the knowledge that you&rsquo;ve been given the right advice, that your monetary affairs are in order and that you aren&rsquo;t over or under paying tax. EAFS can help to give you complete peace of mind.</p>
                            <p>By using a specialists limited company accountants like EAFS, you could considerably increase your net profit. As a firm of specialist Contractor Accountants we have the expertise and experience to identify tax savings and tax planning opportunities from which you can benefit from. To discuss further please contact us on +44 (0)131 526 3300 or fill in our <a href="<?php echo SITE_URL;?>company-information/contact-eafs/" title="Contact EAFS Today" target="_self">online enquiry</a> form and one of our consultants will call you back.</p>

                            <h4>Other Services for UK Contractors</h4>
                            <div class="custom-services clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 first">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-support fa-4x"></i>
                                                    <h3>Insurance</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Insurance for Contractors</h3>
                                                    <p>Insurance products to meet the exposures that you or your business could face. </p>
                                                    <a href="<?php echo SITE_URL;?>contractor-services/insurance-for-contractors/" title="Link to Insurance for Contractors Page">Insurance for Contractors</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-bank fa-4x"></i>
                                                    <h3>Bank Account</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Bank Account</h3>
                                                    <p>EAFS can offer our contractors a Cater Allen business bank account, part of the Santander Bank group. </p>
                                                    <a href="<?php echo SITE_URL;?>contractor-services/business-bank-account/" title="Link to Business Bank Account Page">Business Bank Account</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="ch-item">
                                        <div class="ch-info-wrap">
                                            <div class="ch-info">
                                                <div class="ch-info-front">
                                                    <i class="fa fa-bar-chart-o fa-4x"></i>
                                                    <h3>Mortgages</h3>
                                                </div>
                                                <div class="ch-info-back">
                                                    <h3>Contractor Mortgages</h3>
                                                    <p>A straightforward mortgage application from our partners Contractor Mortgages Made Easy. .</p>
                                                    <a href="<?php echo SITE_URL;?>contractor-services/mortgages-for-contractors/" title="Link to Mortgages for Contractors Page">Mortgages for Contractors</a>
                                                </div>
                                            </div><!-- end ch-info -->
                                        </div><!-- end ch-info-wrap -->
                                    </div><!-- end ch-item -->
                                </div><!-- end col-sm-3 -->
                            </div>

                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                        </article>
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_serviceslinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>