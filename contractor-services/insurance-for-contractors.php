<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Services : Insurance for Contractors : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="Business Insurance cover is usually required by & included in the contracts of agencies and clients. Visit our Insurance for Contractors page to find out more." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Services : Insurance for Contractors :  <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="Business Insurance cover is usually required by & included in the contracts of agencies and clients. Visit our Insurance for Contractors page to find out more." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>contractor-services/insurance-for-contractors/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Services : Insurance for Contractors :  <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>contractor-services/insurance-for-contractors/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="Business Insurance cover is usually required by & included in the contracts of agencies and clients. Visit our Insurance for Contractors page to find out more."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Insurance for Contractors, Business Insurance, Contractor Accountants" />
    <meta name="description" content="Business Insurance cover is usually required by & included in the contracts of agencies and clients. Visit our Insurance for Contractors page to find out more." />
    <link rel="canonical" href="<?php echo SITE_URL;?>contractor-services/insurance-for-contractors/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor Services : Insurance for Contractors</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemscope itemtype="https://schema.org/Breadcrumb">
                        <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                    <li itemscope itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a href="<?php echo SITE_URL;?>contractor-services/" title="Contractor Services" alt="Contractor Services" itemprop="url"><span itemprop="title">Contractor Services</span></a></li>
                    <li itemscope itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Insurance for Contractors</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry">
                    <div class="col-lg-12">
                        <article itemprop="Insurance for Contractors" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor Services : Insurance for Contractors</h2>
                        </header>

                            <p class="lead"><strong>Business Insurance Cover is usually required by, and included in the contracts of, agencies and clients. Although no longer a statutory requirement, a company that is genuinely in business on its own account is expected to seek protection from an insurance provider to safeguard assets in case of emergency. </strong></p>
                            <p>As a contractor or consultant, there are a number of different insurance products that you could purchase; yet deciding on what cover you need is difficult. With an extensive knowledge of the freelance industry, Kingsbridge Professional Solutions have put together a cost effective package to meet the exposures that you or your business could face.  By carefully selecting cover appropriate for you, we have removed the complication of picking cover and limits to purchase separately. Here you only have to purchase one policy!</p>
                            <h4>Why Choose Us for Contractor Insurance?</h4>
                            <p>By choosing EAFS&rsquo; insurance partner &lsquo;Kingsbridge&rsquo; as your insurance provider, you&rsquo;ll get:</p>
                            <ul type="disc">
                                <li>The Best      Value Cover for the Lowest Possible Cost with our Price Promise.</li>
                                <li>Instant Cover.</li>
                                <li>Few Exclusions.</li>
                                <li>An A-rated      Insurer.</li>
                                <li>Instant      documents.</li>
                                <li>A Dedicated      UK team.</li>
                                <li>Policies      for On and Offshore Workers.</li>
                                <li>A Partner      with coveted Chartered Insurance Broker Status</li>
                                <li>5 Insurance products in one easy to manage package</li>
                            </ul>
                            <h4>Professional Indemnity Insurance Package for Freelancers and Contractors</h4>
                            <p>At Kingsbridge Professional Solutions, the core product is designed to suit the requirements of freelance professionals. It has been developed to include cover for all of the main exposures that may affect your professional activities. You don&rsquo;t need to choose specific elements of cover as they have been carefully selected to complement your business, you only need to check that the limits provided meet the requirements of your contract. </p>
                            <h4>Why do I need insurance when working as a contractor or freelancer?</h4>
                            <p>For some contractors, there is a legal requirement to hold insurance. The employers&rsquo; liability legislation requires all employers to provide legal liability cover for their employees, as they could suffer injury as a result of their activities. This rule has limited exceptions in law, however a limited company employing only its director, who also owns 50% or more of the share capital, is exempt from employers&rsquo; liability legislation. If this applies to you then the law does not require you to hold insurance, but any prudent individual would normally choose to protect him or herself from liability that might rise as a result of their business activities. </p>
                            <p>In the vast majority of cases, contract conditions will insist that you hold a certain insurance cover, usually employers&rsquo; liability, public liability and professional indemnity. Failure to provide this cover may lead to a breach of contract. The majority of clients and agencies will insist on proof of your cover arrangements. </p>
                            <p>What risks am I exposed to working as a freelance contractor?</p>
                            <ul type="disc">
                                <li>Do I give      professional advice?</li>
                                <li>Will I get      paid if I am injured at work?</li>
                                <li>Do I      employ my spouse?</li>
                                <li>Do I visit      clients' premises?</li>
                                <li>Do my      clients visit me?</li>
                                <li>Do I have      legal duties operating a limited company?</li>
                                <li>Do I need      funds to defend against any actions made against me; even unfounded ones?</li>
                            </ul>
                            <p>Benefits of the Kingsbridge policy</p>
                            <ul class="check">
                                <li>Is quick      and easy to buy online.</li>
                                <li>Has      comprehensive policy limits.</li>
                                <li>Is a key      IR35 compliance indicator?</li>
                                <li>Helps meet      your contractual obligations.</li>
                                <li>Covers an      extensive range of professional occupations.</li>
                                <li>Is a      simple package at a market-leading price.</li>
                            </ul>
                            <p><strong>There are so many policies out there for contractors - how do I choose the right one for me?</strong></p>
                            <p>As a freelance contractor or consultant, there are a number of different insurance products that you can purchase; however deciding upon what you need can be difficult. By carefully selecting cover appropriate for you, we have removed the complication of purchasing cover and choosing limits separately. You simply buy one policy including the following cover:</p>
                            <ul type="disc">
                                <li><strong>Professional      Indemnity</strong> - £1 million per any one claim (and in the aggregate).</li>
                                <li><strong>Public      Liability</strong> - £5 million per original cause (unlimited in the aggregate).</li>
                                <li><strong>Employers'      Liability</strong> - £10 million per original cause (unlimited in the aggregate).</li>
                                <li><strong>Directors'      and Officers' Liability</strong> - £100,000 any one claim (and in the aggregate).</li>
                                <li><strong>Personal      Accident, Weekly Benefit</strong> – up to £500 payable for maximum of 52 weeks.</li>
                                <li><strong>Personal      Accident, Death Benefit</strong> - £100,000.&nbsp;</li>
                            </ul>
                        <h4>Why Choose EAFS?</h4>
                        <ul class="check">
                            <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
                            <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
                            <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
                        </ul>
                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("../partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                        </article>
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_serviceslinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>