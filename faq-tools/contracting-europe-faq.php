<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contracting FAQ + Tools : Contracting in Europe FAQ : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="EAFS offer fully compliant payroll solutions for contractors working in Europe. Please visit our working in Europe FAQ page for further contractor information." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contracting FAQ + Tools : Contracting in Europe FAQ : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="EAFS offer fully compliant payroll solutions for contractors working in Europe. Please visit our working in Europe FAQ page for further contractor information." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contracting FAQ + Tools : Contracting in Europe FAQ : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="EAFS offer fully compliant payroll solutions for contractors working in Europe. Please visit our working in Europe FAQ page for further contractor information."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>faq-tools/contracting-europe-faq/" />
    <!-- Standard meta data -->
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="European Payroll, Working in Europe, Work in the EU, European Contractors" />
    <meta name="description" content="EAFS offer fully compliant payroll solutions for contractors working in Europe. Please visit our working in Europe FAQ page for further contractor information." />
    <link rel="canonical" href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- START SOCIAL TOP BAR-->
<?php require_once("../partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("../partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START PAGE HEADING-->
<section class="headline-top head-shadow clearfix hidden-xs">
    <div class="container">
        <div class="col-lg-12">
            <h1>Contractor FAQ and Tools : Contracting in Europe FAQ</h1>
        </div>
    </div>
</section>
<!-- END PAGE HEADING-->


<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemprop="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb">
                        <a itemprop="url" href="<?php echo SITE_URL;?>" title="Home" alt="Home"><span itemprop="title">Home</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="unavailable">
                        <a itemprop="url" href="<?php echo SITE_URL;?>faq-tools/" title="FAQ and Tools" alt="FAQ and Tools"><span itemprop="title">FAQ and Tools</span></a></li>
                    <li itemprop="child" itemscope="itemscope" itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Contracting in Europe FAQ </span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div role="main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="content">

            <div class="row">
            <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Agency Support" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h2 itemprop="headline" class="subheader">Contractor FAQ and Tools : Contracting in Europe FAQ</h2>
                        </header>
                        <div itemprop="articleBody">
                            <p class="lead"><strong>Whether a contractor completely new to the industry or a seasoned professional, you can still have questions about Contracting when in Europe. We aim to answer all basic enquiries here on this page, from informing you as to the best way to pay overseas social security, how to claim expenses that can be claimed right through to state pensions and 91 & 183 day rules. If you need to get in touch regarding clarification or if you have a complex situation, don’t hesitate to contact us on +44 (0) 131 526 3300. </strong></p>


                        <h4>Contracting in Europe FAQ?</h4>
                        <div class="clearfix" id="accordion-second">
                        <div id="accordion3" class="accordion">
                        <div class="accordion-group">
                            <div class="accordion-heading active">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle1" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle collapsed">
                                    <em class="icon-fixed-width fa-minus fa"></em>What Expenses can I claim for when Working Overseas?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle1" style="height: 0px;">
                                <div class="accordion-inner">
                                    <p>In certain countries, tax breaks and special arrangements exist for expatriate individuals. In addition to such allowances, you can usually claim some expenses incurred in working overseas, free of tax, from most European countries. As an example, you may be able to claim your travel expenses to and from your home alongside double housing costs.</p>
                                    <p>To be eligible for any tax breaks, there are factors that need to be considered. Family Situation, Local Tax Allowances and whether you are domicile or a resident. Each individual country is unique in their particular approach; therefore we recommend that you seek advice on your personal circumstances with regards to the country you are intending to contract in...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle2" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>What Expenses am I able to claim for Locally?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle2">
                                <div class="accordion-inner">
                                    <p>While working overseas, it is normal to expect a percentage of your expenses incurred are going to be claimable as tax-free expenses. However, there is no European or worldwide amount that is allowable - each country has different rules and regulations. For example, in some countries you are able to claim double housing costs, but in others this would be seen as a &lsquo;benefit in kind&rsquo; and is therefore taxable.</p>
                                    <p>It is also worth noting that, if your package includes either a per diem living allowance or monthly allowance to offset cost of living or your company provides free accommodation locally, local tax authorities will not consider tax-free allowances on all aspects.</p>
                                    <p>Whichever country you are moving to, EAFS can provide advice and guidance on the most appropriate structure to suit your circumstances, maximising tax free allowances and retaining the highest percentage of your income... </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle3" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>Where do I pay social security when working overseas?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle3">
                                <div class="accordion-inner">
                                    <p>While working abroad as an independent contractor, it is imperative to ensure that you have appropriate international social security cover. In many cases, social security is hugely important from a compliance perspective, as not paying attention to this aspect of your remuneration can increase the potential amount of liabilities for the individual, agency and end client.</p>
                                    <p>If you work in another EEA (European Economic Area) country for an EEA employer, then you are commonly insured under the social security laws of the country you work in. However, it is possible within the EEA to gain exemption, in some cases, from local social security via an A1 form if you are being seconded by an overseas (EEA) employer. This enables you to continue payments to the social security offices in the country of your residence; assuming your employer is resident in the same country, rather than paying local social security.</p>
                                    <p>Paying social security is a legal obligation in your country of work and the Form A1 provides this legal coverage. A Form A1 is valid for one year and in some countries can be extended annually (with the permission of the working country) for up to 5 years... </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle4" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>Can I be paid into an Offshore Bank Account?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle4">
                                <div class="accordion-inner">
                                    <p>While working overseas, consultants are often offered &lsquo;creative&rsquo; solutions for the management of their contract income. Some of these include offshore trusts or offshore payments. </p>
                                    <p>Given that the EU Savings Directive is still in force, whereby countries are obligated to share information concerning individuals&rsquo; savings accounts, there is now even less scope for these types of arrangements to function effectively. Consultants should consequently exercise a considerable degree of caution.</p>
                                    <p>Offshore companies are often used as a means to shield money from income tax. While some structures allow this under very specific circumstances, agencies now tend to refuse to pay to offshore bank accounts due to these directives from HMRC, as they are often no more than a thin front for tax evasion and the non-declaration of income. If you are so much as considering an offshore company for efficient structure of tax planning under very specific circumstances, governmental advice should be sought before agreeing a structure. It is not an issue for employers to pay into offshore bank accounts but whether correct payroll taxes and social security deductions have been paid before the net salary is paid to the bank account. </p>
                                    <p>In most European countries, trusts are not recognised in the same way as they are in the UK. You will be taxable and liable for social security deductions on all income paid into the trust by your employer. For all intents and purposes, in most countries, income put into a trust is treated in the same way as if it were put into a bank account, and will therefore be liable for the same taxes...</p>

                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle5" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>Why can't I pay my taxes through my Limited Company?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle5">
                                <div class="accordion-inner">
                                    <p>While working overseas, one of the most important issues for contractors and agencies and end clients is the subject of tax compliance. Income tax is due in the country where the income is earned; therefore if you are contracting abroad you will need to ensure that you have a mechanism ensuring tax payment in your country of work.</p>
                                    <p>By continuing to pay tax through your Limited Company, you will be remitting tax in your home country, but not in the country where you are working. Although your tax affairs at home will be in order, under this type of arrangement a tax liability will build up in the country of work. </p>
                                    <p>In many countries, a chain law exists. Meaning that the tax liability can be passed on to the client you are working for. For this reason, many clients will insist that contractors are paid via an in-country payroll. Contractors operating through UK Limited companies outside the UK are consequently running the risk of being liable not only for unpaid personal income tax, but also for corporate tax, having created a permanent establishment in the country where they are working.</p>
                                    <p>It is therefore important to ensure that your tax affairs are in order -  both in your home country and in the country where you are working...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle6" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>What happens to my state pension when working overseas?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle6">
                                <div class="accordion-inner">
                                    <p>While working away from your resident country, it is not always necessary (or in fact possible) to continue paying into your existing pension schemes, or if you do you may not get the same level of investment. </p>
                                    <p>There is also a pronounced difference between the state pension and any private pensions. In general, state pensions will either be covered by local social security or through your A1 form. Dependent on the country of work there may be an element of private pension within the social security system and, therefore, you need to be aware of what your entitlement is.</p>
                                    <p>In every EU/EEA country where you have made social security contributions your record is preserved until retirement age. If you have made contributions for at least one year in any one country, that country will have to pay you a state pension when you reach retirement age. If you have contributed in any one country for a relatively short period, your entitlement to the state pension will be quite small.</p>
                                    <p>In the case that you contributed in a country for less than one year, your contributions will not be lost. They will be taken over by the country where you have made most of your contributions during your working life, or by your final country of employment. As long as you stay within the EU/EEA countries, any time spent contributing to the social security system of an EU/EEA member state will count towards your overall state pension...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle7" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>Do I need to pay voluntary pension contributions when I am working overseas?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle7">
                                <div class="accordion-inner">
                                    <p>If you work overseas within the EU/EEA and you are contributing to the social security system of an EU/EEA country, these contributions will count towards your qualifying years for a state pension. Therefore, if you decide not to make voluntary contributions in your home country, you will not create gaps in your contributions.</p>
                                    <p>You should be aware, however, that your record of contributions is preserved in each country of work until retirement age. Your state pension will then be paid out in part by your home country and in part by the other countries where you have worked, the proportions dependent upon how many years are spent working in each. In some countries you may be able to transfer your contributions. You will need to check the country of work regulations regarding transfer of contributions...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle8" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>When do I have to file a tax return?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle8">
                                <div class="accordion-inner">
                                    <p>As a general rule, a tax return should be filed at the end of the tax year in the country where you are working. Unlike the UK, most other European countries have the tax year correspond to the calendar year...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle9" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>What is a double tax treaty?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle9">
                                <div class="accordion-inner">
                                    <p>This is an agreement between two countries to prevent international double taxation. This occurs when two different states impose a comparable tax on the same potential taxpayer on the same taxable item. Most developed countries have a large number of double tax treaties in place. Double tax treaties are in place between all EU/EEA countries.</p>
                                    <p>When contracted to work abroad, it is important to check that your home country has a double tax treaty with the country where you will be working. If no treaty exists, you should try to ensure that you would be able to acquire a tax certificate that clearly states how much tax you have paid in the country of work. Without this you run the risk of being taxed twice on the same income, leaving you heavily out of pocket...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle10" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>Where should I pay taxes?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle10">
                                <div class="accordion-inner">
                                    <p>Tax is due where income is earned. Any contract revenue should be taxed in the country where you are performing your work. The key factor is where you are physically present. It does not matter which currency you are paid in, where your agency is or where the end client&rsquo;s main office is. If you are performing your work in the Netherlands, for example, the Netherlands is where tax on that income is due.</p>
                                    <p>You do not have to be tax resident in a country to be liable to pay income tax there. Tax residents pay tax on their worldwide income. Tax non-residents pay tax on any part of their income earned in the country.</p>
                                    <p>If you work in one country and remain tax resident in another, you should pay income tax in the country where you are working and declare that income and any tax paid on it, as well as any other worldwide income, in the country where you are tax resident. The country where you are tax resident will take into account any tax paid in another country with double taxation treaties preventing double taxation on the same income.</p>
                                    <p>In order to remain compliant in respect of both the country where you are working and your home country (where you may be tax resident), you must pay income tax in the country where the income is earned...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle11" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>Why is it important to be tax compliant?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle11">
                                <div class="accordion-inner">
                                    <p>When working abroad, it is essential to ensure that your tax affairs comply with the rules and regulations. Not just for your home country, but also the country in which you work. Failures to do so would result in heavy fines or even imprisonment. </p>
                                    <p>There are many &quot;exotic&quot; solutions in the marketplace, promising super-high net retention to consultants and easy business for recruitment agencies, using a variety of imaginative methods. Some are not compliant in any relevant jurisdiction, and others may be compliant in one country but not necessarily in another; if you choose a non-compliant solution the risk is yours and yours alone.</p>
                                    <p>Rules and regulations across nations are constantly changing, EAFS will help you to assess and avoid the risks for you and/or your agency by advising you of the tax legislation in any given country and specifically on what is, and what is not, possible in terms of tax planning...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle12" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>Tax Residency
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle12">
                                <div class="accordion-inner">
                                    <p>Whether you are working on a contract assignment overseas or in your home country, some things never change. Tax will be due where the money is earned. However, ensuring your tax compliance with the relevant international tax legislation is a complex process - especially when multiple countries and tax authorities are involved.</p>
                                    <p>At the centre of this are the rules regarding tax residency. Individuals need to be aware of these to ensure that they pay the correct tax to the correct authorities, thereby remaining tax compliant.</p>
                                    <p>Additional issues that you need to be aware of are: the 183-day rule, the 91-day rule and the regulations regarding double taxation avoidance.</p>
                                    <p>When moving to a new country to work you are generally liable for tax from day one. This status is termed &ldquo;tax non-resident&rdquo; and in most countries means that for the first 183 days of work; you are taxed only on your locally sourced income.</p>
                                    <p>Once you go over the 183 days, and in some cases this can be in one tax year or cumulative over 2 tax years, your status changes to &ldquo;tax resident&rdquo;. This change means that you become potentially liable for tax on your worldwide income.</p>
                                    <p>For most people, this will actually mean nothing will change if the locally-sourced income is the same as the worldwide income, but those with other sources of income will need to be aware of the potential tax implications this creates.</p>
                                    <p>Having the status of tax non-resident does not mean that you can work without paying tax, unless you are working in a country that is zero tax rated...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle13" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>How do I become a tax resident?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle13">
                                <div class="accordion-inner">
                                    <p>Becoming tax-resident may be easier than you think. When you move to a new country you can usually remain tax non-resident for a short stay, but you will almost certainly become a tax-resident for longer stays (after 183 days in many countries).</p>
                                    <p>The point to note is that tax is usually due where income is earned, i.e. from the first day of work in your new country of work; unless you have a pre-arranged exemption. If you simply carry on paying tax in your home country &ldquo;for simplicity&rsquo;s sake&rdquo; or because tax is lower there or because your contract is expected to be short-term, you are potentially creating problems for yourself.</p>
                                    <p>Unless the assignment is short (and not extended), the local authorities will usually tax you from the first day of work and it will be up to you to try to reclaim tax paid elsewhere, which can be a very time-consuming process. In short, it&rsquo;s worth getting your tax affairs clean and correct from the start. In every case, make sure you obtain professional advice before you start a new contract...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle14" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>What is the 183-day rule?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle14">
                                <div class="accordion-inner">
                                    <p>The so-called 183-day rule relating to tax liabilities rarely exists as a clear-cut rule but is used as a guideline in some circumstances.</p>
                                    <p>If you work less than 183 days in many countries you may be considered a tax non-resident if certain other criteria are also met. However, even as a non-resident you should normally still be paying tax on the revenue you generate in that country.</p>
                                    <p>If you work more than 183 days in most countries, then you will become tax-resident and liable for tax on your worldwide income, i.e. revenue from your work, interest on investments, etc.</p>
                                    <p>The &lsquo;183 day rule&rsquo; does NOT automatically mean that you can work for 183 days in a new country without paying tax or becoming a tax-resident. However, in most situations, particularly if a double taxation avoidance treaty exists between your country of work and your home country, you will not have to pay tax on the same income twice...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle15" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>What is the 91-day rule?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle15">
                                <div class="accordion-inner">
                                    <p>The 91-day rule forms part of the test governing tax residency in the UK. It works in conjunction with the 183-day rule.</p>
                                    <p>When deciding if an individual is resident in the UK for tax purposes, days will count if you are in the UK at the end of the day (i.e. at midnight) for residence test purposes.</p>
                                    <p>A person who is currently not resident in the UK will always be treated as resident in the UK if they spend 183 days or more in the country, in any tax year. If they visit the UK on a regular basis and spend, on average, 91 days or more in a UK tax year (taken over a period of four years), they will be treated as resident of the United Kingdom. </p>
                                    <p>If they know that they are going to be a regular visitor or the next three tax years will average 91 or more days within the United Kingdom, then they will be resident from the beginning of the tax year during which they make their first visit. </p>
                                    <p>If they have been resident in the United Kingdom and, having left the UK, continue to visit, they will continue to be treated as a resident if those visits average 91 days or more a tax year; taken over a maximum period of four years. Therefore, if you leave the UK for work overseas, remain out of the UK for one complete tax year and during the period your trips back to the United Kingdom are not more than 91 days (midnight rule applies), averaged over a four year period, you will become a non tax-resident...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle16" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>How do I lose tax residency?
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle16">
                                <div class="accordion-inner">
                                    <p>Becoming a tax non-resident is not straightforward, and varies very much from country to country.</p>
                                    <p>Sometimes it is sufficient for your &ldquo;centre of economic and social interests&rdquo; (your job and/or your home base) to move to another country for you to lose your tax residency in your home country. In other cases you must work outside your home country for one or more tax years in order to lose your tax residency.</p>
                                    <p>In simple terms, it is not because you have become a tax-resident in a new country that you will automatically lose your tax residency in your home country; it is quite possible to be tax-resident in two countries at the same time!</p>
                                    <p>For example, to lose your UK tax residency you must be out of the UK for one complete tax year and, during that year (and averaged over a 4 year period), you must not have returned to the UK for more than 91 full days. The 91 day rule is based on the midnight rule – a day is counted if you are there at midnight.</p>
                                    <p>US nationals will never lose their US tax residency and will always have to pay US taxes. Although in all cases, the double taxation avoidance treaty between the USA and most other countries needs to be applied to ensure that you do not pay full tax twice on the same income...</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/accordion-toggle.html#collapseToggle17" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle">
                                    <em class="fa fa-plus icon-fixed-width"></em>30% Ruling Conditions.
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseToggle17">
                                <div class="accordion-inner">
                                    <p>To be eligible for the 30% tax ruling, the following conditions have to be met:     </p>
                                    <ol start="1" type="1">
                                        <li><strong>The employee has to work for an employer.</strong><br>
                                            In order to take advantage of the 30% ruling you have to be employed. If      you are self-employed, it isn&rsquo;t possible to claim the 30% ruling. However,      if you set up a UK Limited Company or Dutch BV and become an employee of      that company then you are seen as being in employment and consequently      eligible for the 30% ruling.  </li>
                                        <li><strong>The employer and employee have to agree in      writing that the 30% ruling is applicable.</strong><br>
                                            The application for the 30% ruling has to be done by both employer and      employee. If the 30% ruling is fully applicable, the gross salary of the      employee will be reduced by 30%. This can have implications for your      potential unemployment or disability benefits since these benefits are      based on taxable salary. Therefore the tax authorities require that both      employer and employee are aware of these consequences. This agreement in      writing can be done by means of a clause in your employment contract or as      an addendum to the employment contract.</li>
                                        <li><strong>The employee has to transfer or be recruited from      abroad to a Dutch employer</strong><br>
                                            It is only possible to claim the 30% ruling if you are transferred or      recruited from abroad. You have to prove that you were residing in another      country before you began your present employment as a highly skilled      migrant. The employer has to state by means of a letter of recommendation      to the tax authorities the reason why they have hired the employee from      abroad and what skills make the employee so special for the company. The      employer may also be asked to prove that they were not successful in      finding an employee with comparable expertise in the Netherlands.      Furthermore, the employee must not have lived within 150 km of the Dutch      border for 16 or more months out of the last 24 months prior to the start      of their employment in the Netherlands.</li>
                                        <li><strong>The      employee has to have specific experience or expertise that is not or is      rarely available in the Netherlands</strong><br>
                                            The employee has to have specific skills that are scarce in the Dutch      labour market. These skills are determined by several facets such as      salary, age, employment history, education and level of employment. None      of these are conclusive but the combination of all aspects determines your      specific skills.</li>
                                        <li><strong>The gross annual salary has to surpass a minimum      limit (adjusted annually)</strong></li>
                                    </ol>
                                    <p><em>Source: https://www.iamsterdam.com/en-GB/living/official-matters/thirty-percent-ruling</em></p>
                                </div>
                            </div>
                        </div>
                        </div><!-- end accordion -->
                        </div><!-- end accordion first -->
                        <!-- CONTACT ME BUTTON  -->
                        <?php require_once("../partials/widget_contactbutton.inc.php");?>
                        <!-- CONTACT ME BUTTON  -->
                        </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->

        </div><!-- end content -->
        <aside role="complimentary" itemscope itemtype="https://schema.org/WPSideBar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
                <!-- WIDGET START LINKS -->
                <?php require_once("../partials/widget_faqlinks.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START CONTACT -->
                <?php require_once("../partials/widget_contact.inc.php");?>
                <!-- WIDGET END CONTACT -->
        </aside><!-- end aside -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("../partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("../partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("../partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("../inc/scripts.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>