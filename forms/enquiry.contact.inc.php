<h4>Contact EAFS Today</h4>

<!--START GENERAL ENQUIRY FORM-->
<div class="tableInputs clearfix">
    <form role="form" id="enquiryFormGeneral" method="post" action="https://info.bradfordjacobs.com/l/540262/2018-07-26/32lv4cn">
        <input type="hidden" name="00N2X000008sqLT" value="Web Form EAFS">
        <input type="hidden" id="leadtype" name="012w0000000APkv" value="">
        <input type=hidden name="oid" value="00D20000000CdoA">
        <input type=hidden name="retURL" value="https://go.eafs.eu/eafs-thank-you">
        <input type="hidden" id="7012X000000fwmx" name="Campaign_ID" value="7012X000000fwmx">
        <input type="hidden" id="member_status" name="member_status" value="Submitted webform">
        <input type="hidden" id="GCLID_c" name="00N2X000008sxda" value="">
        <input type="hidden" id="GA_Client_ID" name="00N2X000003sUyq" value="">
        <input type="hidden" id="lc_channel" name="00N2X000008sxds" value="">
        <input type="hidden" id="lc_source" name="00N2X000008sxdx" value="">
        <input type="hidden" id="lc_medium" name="00N2X000008sxdv" value="">
        <input type="hidden" id="lc_referrer" name="00N2X000008sxdw" value="">
        <input type="hidden" id="lc_term" name="00N2X000008sxdy" value="">
        <input type="hidden" id="lc_content" name="00N2X000008sxdt" value="">
        <input type="hidden" id="lc_campaign" name="00N2X000008sxdr" value="">  
        <input type="hidden" id="lc_landing" name="00N2X000008sxdu" value="">
        <input type="hidden" id="fc_channel" name="00N2X000008sxdh" value="">
        <input type="hidden" id="fc_source" name="00N2X000008sxdm" value=""> 
        <input type="hidden" id="fc_medium" name="00N2X000008sxdk" value="">
        <input type="hidden" id="fc_referrer" name="00N2X000008sxdl" value="">
        <input type="hidden" id="fc_term" name="00N2X000008sxdn" value="">         
        <input type="hidden" id="fc_content" name="00N2X000008sxdi" value="">
        <input type="hidden" id="fc_campaign" name="00N2X000008sxdg" value="">
        <input type="hidden" id="fc_landing" name="00N2X000008sxdj" value=""> 

        <input type="hidden" id="all_traffic_sources" name="00N2X000008sxda" value="">
        <input type="hidden" id="browser" name="00N2X000008sxdd" value="">
        <input type="hidden" id="city" name="00N2X000008sxde" value="">
        <input type="hidden" id="country" name="00N2X000008sxdf" value="">
        <input type="hidden" id="device" name="00N2X000008sxdb" value="">
        <input type="hidden" id="ip_address" name="00N2X000008sxdq" value="">
        <input type="hidden" id="region" name="00N2X000008sxdt" value=""> 
        <input type="hidden" id="time_passed" name="00N2X000008sxe5" value="">
        <input type="hidden" id="OS" name="00N2X000008sxe3" value="">
        <input type="hidden" id="page_visits" name="00N2X000008sxe2" value=""> 
        <input type="hidden" id="pages_visited_list" name="00N2X000008sxe4" value="">
        <input type="hidden" id="time_zone" name="00N2X000008sxe7" value="">
        <input type="hidden" id="latitude" name="00N2X000008sxdz" value="">         
        <input type="hidden" id="longitude" name="00N2X000008sxe1" value="">
        <input type="hidden" id="region" name="00N2X000008sxdt" title="Tracked Location" value="">
 
        <div class="form-group clearfix">
            <label for="00N2X000008sm2S" class="col-md-3 control-label">Type of Enquirer <span class="require">*</span></label>
            <div class="col-md-9">
                <select name="00N2X000008sm2S" id="00N2X000008sm2S" class="form-control inner selectpicker" required>
                    <option value="">--Make your Selection--</option>
                    <option value="Agency">Agency</option>
                    <option value="Contractor">Contractor</option>
                    <option value="Corporate">Corporate</option>
                </select>
            </div>
        </div>

        <div style='display:none;' id='business'>
            <fieldset>
                <legend>Company Details</legend>
                <div class="form-group clearfix"><label for="company" class="col-md-3 control-label">Company: <span class="require">*</span></label>
                    <div class="col-md-9">
                        <div class="input-icon right"><i class="fa fa-location-arrow"></i>
                            <input type="text" id="company" name="company" placeholder="Company" class="form-control" required></div>
                    </div>
                </div>
                <div class="form-group clearfix"><label for="industry" class="col-md-3 control-label">Industry
                        Sector:</label>
                    <div class="col-md-9">
                        <select name="industry" id="industry" class="form-control inner selectpicker">
                            <option value="--Make your Selection--" selected>--Make your Selection--</option>
                            <option value="Banking">Banking</option>
                            <option value="Biotechnology">Biotechnology</option>
                            <option value="Chemicals">Chemicals</option>
                            <option value="Corporate">Corporate</option>
                            <option value="Construction">Construction</option>
                            <option value="Consulting">Consulting</option>
                            <option value="Electronics">Electronics</option>
                            <option value="Energy">Energy</option>
                            <option value="Engineering">Engineering</option>
                            <option value="Finance">Finance</option>
                            <option value="Government">Government</option>
                            <option value="Healthcare">Healthcare</option>
                            <option value="Hospitality">Hospitality</option>
                            <option value="Insurance">Insurance</option>
                            <option value="Recruitment">Recruitment</option>
                            <option value="Manufacturing">Manufacturing</option>
                            <option value="Media">Media</option>
                            <option value="Other">Other</option>
                            <option value="Shipping">Shipping</option>
                            <option value="Technology">Technology</option>
                            <option value="Telecommunications">Telecommunications</option>
                            <option value="Transportation">Transportation</option>
                            <option value="Utilities">Utilities</option>
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>

        <fieldset>
            <legend>Personal Details</legend>
            <div class="form-group clearfix"><label for="first_name" class="col-md-3 control-label">First Name:
                    <span class="require">*</span></label>
                <div class="col-md-9">
                    <div class="input-icon"><i class="fa fa-user"></i><input type="text" id="first_name" name="first_name" placeholder="First Name" class="form-control" required></div>
                </div>
            </div>
            <div class="form-group clearfix"><label for="last_name" class="col-md-3 control-label">Last Name:
                    <span class="require">*</span></label>
                <div class="col-md-9">
                    <div class="input-icon"><i class="fa fa-user"></i><input type="text" id="last_name" name="last_name" placeholder="Last Name" class="form-control" required></div>
                </div>
            </div>
            <div class="form-group clearfix"><label for="mobile" class="col-md-3 control-label">Contact Number:
                    <span class="require">*</span></label>
                <div class="col-md-9">
                    <div class="input-icon"><i class="fa fa-phone"></i><input type="tel" id="mobile" name="mobile" placeholder="Contact Number" class="form-control" required></div>
                </div>
            </div>
            <div class="form-group clearfix"><label for="email" class="col-md-3 control-label">Email Address:
                    <span class="require">*</span></label>
                <div class="col-md-9">
                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="email" id="email" name="email" placeholder="Email Address" class="form-control" required></div>
                </div>
            </div>
            <div class="form-group clearfix"><label for="cemail" class="col-md-3 control-label">Confirm Email Address:
                    <span class="require">*</span></label>
                <div class="col-md-9">
                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="email" id="cemail" name="cemail" placeholder="Confirm Email Address" class="form-control" required></div>
                </div>
            </div>
            <div class="form-group clearfix"><label for="country" class="col-md-3 control-label">Country:
                    </label>
                <div class="col-md-9">
                    <div class="input-icon right"><i class="fa fa-location-arrow"></i>
                        <input type="text" id="country" name="country" placeholder="Country" class="form-control"></div>
                </div>
            </div>
            <div class="form-group clearfix"><label for="city" class="col-md-3 control-label">City:</label>
                <div class="col-md-9">
                    <div class="input-icon right"><i class="fa fa-location-arrow"></i>
                        <input type="text" id="city" name="city" placeholder="City" class="form-control"></div>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend>Enquiry Details</legend>
            <div class="form-group clearfix bottrap"><label for="honeypot" class="col-md-3 control-label">
                    <span class="require"></span></label>
                <div class="col-md-9">
                    <div class="input-icon right"><i class="fa fa-location-arrow"></i>
                        <input type="text" id="honeypot" name="honeypot" value="" class="form-control" required></div>
                </div>
            </div>
            <div class="form-group clearfix"><label for="00N2X000008sls1" class="col-md-3 control-label">Country of Enquiry:
                    <span class="require">*</span></label>
                <div class="col-md-9">
                    <select name="00N2X000008sls1" id="00N2X000008sls1" class="form-control inner selectpicker" required>
                        <option value="">--Select Country--</option>
                        <option value="Belgium">Belgium</option>
                        <option value="CzechRepublic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="France">France</option>
                        <option value="Germany">Germany</option>
                        <option value="Italy">Italy</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Malta">Malta</option>
                        <option value="Netherlands">Netherlands</option>
                        <option value="Norway">Norway</option>
                        <option value="Poland">Poland</option>
                        <option value="Spain">Spain</option>
                        <option value="Sweden">Sweden</option>
                        <option value="UK">UK</option>
                    </select>
                </div>
            </div>
            <div class="form-group clearfix"><label for="00N2X000008sls2" class="col-md-3 control-label">Type of Enquiry:
                    <span class="require">*</span></label>
                <div class="col-md-9">
                    <select data-selected-text-format="count" name="00N2X000008sls2" id="00N2X000008sls2" class="form-control inner selectpicker" required>
                        <option selected>--Please choose a country first--</option>
                    </select>
                </div>
            </div>
            <div class="form-group clearfix"><label for="00N2X0000099QGg" class="col-md-3 control-label">Found us
                    from:</label>
                <div class="col-md-9">
                    <select name="00N2X0000099QGg" id="00N2X0000099QGg" class="form-control inner selectpicker">
                        <option value="">--Make your Selection--</option>
                        <option value="Article">Article</option>
                        <option value="Bing">Bing</option>
                        <option value="Blog">Blog</option>
                        <option value="Facebook">Facebook</option>
                        <option value="Google">Google</option>
                        <option value="Linkedin">Linkedin</option>
                        <option value="Other">Other</option>
                        <option value="Previous Contractor Returning">Previous Contractor Returning</option>
                        <option value="Referral">Referral</option>
                        <option value="Word of mouth">Word of mouth</option>
                    </select>
                </div>
            </div>
        </fieldset>

        <div class="form-group clearfix"><label for="description" class="col-md-3 control-label">Comments:
                <span class="require">*</span></label>
            <div class="col-md-9">
                <textarea id="description" name="description" placeholder="Your Message" class="form-control" required></textarea>
            </div>
        </div>
        <div class="col-md-offset-8 col-md-4 clearfix">
            <input type="submit" value="Click to Send your Enquiry" class="btn btn-primary btn-outlined">
        </div>
    </form>
</div>
<script> 
  function readCookie(name) { 
  var n = name + "="; 
  var cookie = document.cookie.split(';'); 
  for(var i=0;i < cookie.length;i++) {      
      var c = cookie[i];      
      while (c.charAt(0)==' '){c = c.substring(1,c.length);}      
      if (c.indexOf(n) == 0){return c.substring(n.length,c.length);} 
  } 
  return null; 
  } 

  window.onload = function() {      
      document.getElementById('gclid__c').value = 
  readCookie('gclid'); 
  } 
  </script>
<br>
<!--END GENERAL ENQUIRY FORM-->

