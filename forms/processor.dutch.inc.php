<?php
$errors = '';
$myemail = 'web.enq@eafs.eu';
// $myemail = 'r.warner@ecrm-euro.com';

// Check form for Errors and Return Error
if(empty($_POST['first_name'])  ||
    empty($_POST['last_name']) ||
    empty($_POST['phone']) ||
    empty($_POST['email']) ||
    empty($_POST['country']) ||
    empty($_POST['city']) ||
    empty($_POST['description']))
{
    $errors .= "\n Error: all fields are required";
}

// Load the Form Fields
$first_name = strip_tags($_POST['first_name']);
$last_name = strip_tags($_POST['last_name']);
$phone_number = strip_tags($_POST['phone']);
$email_address = strip_tags($_POST['email']);
$home_country = strip_tags($_POST['country']);
$home_city = strip_tags($_POST['city']);
$message = strip_tags($_POST['description']);
$selected_country = $_POST['00Nw0000003jevA'];
$selected_service = $_POST['00Nw0000003joCt'];

// Check form email Field for Formatting Error
if (!preg_match(
    "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i",
    $email_address))
{
    $errors .= "\n Error: Invalid email address";
}

// Spanm Prevention Honeypot
if($_POST['honeypot'] != '')
{
    $errors .= "\n Error: You are a Bot Go Away";
}

// Prepare the Body of the E-Mail Message
$email_body = '<html><body>';
$email_body .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
$email_body .= '<tr><td>';
$email_body .= '<table width="650" border="0" cellspacing="0" cellpadding="0" align="center">';
$email_body .= "<tr><td style='padding: 10px; font-family: sans-serif; font-size: 14px; text-align: left;'>" . '<img src="http://www.eafs.eu/images/assets/eafs-logo.png" width="400" height="116" alt="Euro Accountancy &amp; Finance Services" />' . "</td></tr>" . "\n";
$email_body .= "<tr><td style='padding: 10px; font-family: sans-serif; text-align: left; font-weight: bold;'>First Name: </td><td style='padding:10px; font-family: sans-serif; font-size: 14px; text-align: left;'>" . $first_name . "</td></tr>" . "\n";
$email_body .= "<tr><td style='padding: 10px; font-family: sans-serif; text-align: left; font-weight: bold;'>Last Name: </td><td style='padding:10px; font-family: sans-serif; font-size: 14px; text-align: left;'>" . $last_name . "</td></tr>" . "\n";
$email_body .= "<tr><td style='padding: 10px; font-family: sans-serif; text-align: left; font-weight: bold;'>Contact Number: </td><td style='padding:10px; font-family: sans-serif; font-size: 14px; text-align: left;'>" . $phone_number . "</td></tr>" . "\n";
$email_body .= "<tr><td style='padding: 10px; font-family: sans-serif; text-align: left; font-weight: bold;'>E-Mail Address: </td><td style='padding:10px; font-family: sans-serif; font-size: 14px; text-align: left;'>" . $email_address . "</td></tr>" . "\n";
$email_body .= "<tr><td style='padding: 10px; font-family: sans-serif; text-align: left; font-weight: bold;'>Country: </td><td style='padding:10px; font-family: sans-serif; font-size: 14px; text-align: left;'>" . $home_country . "</td></tr>" . "\n";
$email_body .= "<tr><td style='padding: 10px; font-family: sans-serif; text-align: left; font-weight: bold;'>City: </td><td style='padding:10px; font-family: sans-serif; font-size: 14px; text-align: left;'>" . $home_city . "</td></tr>" . "\n";
$email_body .= "<tr><td style='padding: 10px; font-family: sans-serif; text-align: left; font-weight: bold;'>Country Enquiry: </td><td style='padding:10px; font-family: sans-serif; font-size: 14px; text-align: left;'>" . $selected_country . "</td></tr>" . "\n";
$email_body .= "<tr><td style='padding: 10px; font-family: sans-serif; text-align: left; font-weight: bold;'>I'm Interested in: </td><td style='padding:10px; font-family: sans-serif; font-size: 14px; text-align: left;'>" . $selected_service . "</td></tr>" . "\n";
$email_body .= "<tr><td style='padding: 10px; font-family: sans-serif; text-align: left; font-weight: bold;'>You Message: </td><td style='padding:10px; font-family: sans-serif; font-size: 14px; text-align: left;'>" . $message . "</td></tr>" . "\n";
$email_body .= "</table>";
$email_body .= "</td></tr>";
$email_body .= "</table>";
$email_body .= "</body></html>";

// Format the E-Mail Template
if( empty($errors))
{
    $to = $myemail;
    $email_subject = 'Euro Accountancy & Finance Services Website Enquiry: Netherlands';
    $headers = "From: $myemail\n";
    $headers .= "Reply-To: $email_address";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=utf-8\r\n";

    if (mail($to, $email_subject, $email_body, $headers)) {
        echo 'Your message has been sent.';
    } else {
        echo 'There was a problem sending the email.';
    }

// SALESFORCE cURL REQUEST
// cURL Initialize the $query_string variable for later use

    $query_string = "";

// cURL If there are POST variables
    if ($_POST) {

// cURL Initialize the $kv array for later use
        $kv = array();

// cURL For each POST variable as $name_of_input_field => $value_of_input_field
        foreach ($_POST as $key => $value) {

// cURL Set array element for each POST variable (ie. last_name=Yazeedi)
            $kv[] = stripslashes($key)."=".stripslashes($value);
        }
// cURL Create a query string with join function separted by &
        $query_string = join("&", $kv);
    }
// cURL URL for cURL to post to
    $url = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';

// cURLOpen cURL connection
    $ch = curl_init();

// cURL Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($kv));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);

// cURL Set some settings that make it all work
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

// cURL Execute SalesForce web to lead PHP cURL
    $result = curl_exec($ch);

// cURL close cURL connection
    curl_close($ch);

}
?>
<!DOCTYPE html PUBLIC"-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title></title>
</head>

<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- This page is displayed only if there is some error -->
<?php
echo nl2br($errors);
?>

<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>