<?php
define('DEVELOPMENT_ENVIRONMENT',true);
define("DB_NAME",'notset');
define("DB_HOST",'localhost');
define("DB_USERNAME",'notset');
define("DB_PASSWORD",'notset');

error_reporting(E_ERROR | E_WARNING | E_PARSE);

define(SITE_URL,"https://www.eafs.eu/");
/* define(SITE_URL,"http://www.eafs.eu.local/");/*
/* define(SITE_URL,"http://v2.eafs.eu/"); */
define(SITE_TITLE,"Contractor Accountants");
define(SITE_ADMIN_TITLE,"Contractor Accountants");