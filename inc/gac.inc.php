<script>
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function getCID() {
    var _ga = getCookie('_ga');
    var gaObj = window[window.GoogleAnalyticsObject];
    if (typeof gaObj !== 'undefined' && typeof gaObj['getAll'] !== 'undefined') {
        var tracker = gaObj.getAll()[0];
        return tracker.get("clientId");
    } else if (typeof _ga === 'string' && _ga.length > 0) {
        var cidArr = _ga.split('.');
        return cidArr.slice(cidArr.length - 2, cidArr.length).join('.');
    }
}

function hasClass(element, klass) {
    return (' ' + element.className + ' ').indexOf(' ' + klass+ ' ') > -1;
}

function getPardotIframes(callback) {
    var iframes = document.getElementsByTagName('iframe');
    for (var i=0; i<iframes.length; i++) {
        callback(iframes[i]);
        try {
            var innerIframes = iframes[i].contentDocument.getElementsByTagName('iframe');
            for (var j=0; j<innerIframes.length; j++) {
                callback(innerIframes[j]);
            }
        } catch (e) {}
    }
}

function sendCidToIframe() {
    try {
        var cid = getCID();
        if (typeof cid === 'undefined') return;

        getPardotIframes(function(pardotIframe) {
            pardotIframe.contentWindow.postMessage(cid, "*");
        });
    } catch (error) {
        console.log("Pardot CID script error:", error);
    }
}
  
setInterval(sendCidToIframe, 1000); 
</script>