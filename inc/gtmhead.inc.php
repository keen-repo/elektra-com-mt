<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N2ZDZ8D');</script>
<!-- End Google Tag Manager -->
<script data-cfasync="false" type="text/javascript" src="https://tracker.gaconnector.com/gaconnector.js"></script>
<script>
function setGaconnectorHiddenFields() {
    var gaFields = gaconnector.getCookieValues();
    for (var fieldName in gaFields) {
        var selectors = 'form input[name="' + fieldName + '"], form input#' + fieldName + ', form input#field_' + fieldName + ', form input[name="' + fieldName.toLowerCase() + '"], form input#' + fieldName.toLowerCase() + ', form input#field_' + fieldName.toLowerCase();
        selectors += 'form textarea[name="'+fieldName+'"], form textarea#'+fieldName+', form textarea#field_'+fieldName + ', form textarea[name="'+fieldName.toLowerCase()+'"], form textarea#'+fieldName.toLowerCase()+', form textarea#field_'+fieldName.toLowerCase()+', form textarea.'+fieldName+', form textarea[name="param['+fieldName+']"]'+", form textarea[id^='field_"+fieldName+"']";
        var inputs = document.querySelectorAll(selectors);
        if (inputs === null) {
            continue;
        } else if (typeof inputs.length === 'undefined') {
            inputs.value = gaFields[fieldName];
        } else {
            for (var i = 0; i < inputs.length; i++) {
                inputs[i].value = gaFields[fieldName];
            }
        }
    }
}

gaconnector.setCallback(setGaconnectorHiddenFields);
setInterval(setGaconnectorHiddenFields, 1000);
</script>
<script>
    function setGaconnectorHiddenFields() {
        var gaFields = gaconnector.getCookieValues();
        setInterval(function(){
          var pardotForm = document.querySelector("iframe.pardotform");
          if (pardotForm !== null) {
              pardotForm.contentWindow.postMessage(gaFields, "*");
          }
      }, 1000);   
    }
 
    gaconnector.setCallback(setGaconnectorHiddenFields);
</script>
