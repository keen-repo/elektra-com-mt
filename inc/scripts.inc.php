<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo SITE_URL;?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery Easing Functions -->
<script src="<?php echo SITE_URL;?>bower_components/jquery-easing/jquery.easing.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo SITE_URL;?>js/bootstrap.min.js"></script>
<!-- Bootstrap's Form Validation Functions -->
<script src="<?php echo SITE_URL;?>js/bootstrap.validator.min.js"></script>
<!-- WoW Effects plugin  -->
<script src="<?php echo SITE_URL;?>js/wow.min.js"></script>
<!-- Website  compiled plugins  -->
<script src="<?php echo SITE_URL;?>js/scripts.min.js"></script>