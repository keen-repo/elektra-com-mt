<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
<!-- Basic Page Needs
	================================================== -->
<meta charset="utf-8">
    <title>Dutch Payroll | Dutch Payroll Services | Euro Accountancy &amp; Finance Services</title>
    <meta name="description" content="EAFS Consulting BV offers Dutch Payroll solutions specifically for contractors working within Netherlands.">
    <meta name="keywords" content"Dutch Payroll, Dutch Payroll Services">
<!-- Mobile Specific Metas
	================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Fav and touch icons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
<!-- Custom styles 
	================================================== -->
<link href="<?php echo SITE_URL;?>landing/css/style.css" rel="stylesheet" media="screen">
<link href="<?php echo SITE_URL;?>landing/css/color/blue.css" rel="stylesheet" media="screen">
<!--[if IE 8 ]><link href="<?php echo SITE_URL;?>landing/css/ie8.css" rel="stylesheet" media="screen"><![endif]-->
<!-- Scripts Libs 
	================================================== -->
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/ScriptLibrary/jquery-latest.pack.js"></script>
<!-- 1.9.1 -->
<!-- HTML5 shim, for IE6-8 support of HTML5 elements 
	================================================== -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL;?>landing/bootstrap/css/bootstrap.css" />
<style type="text/css">
.span31 {    width: 270px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL;?>landing/bootstrap/css/bootstrap-responsive.css" />
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/bootstrap/js/bootstrap.js"></script>
</head>

<body>
<?php require_once("../inc/gtmbody.inc.php");?>
	<!-- Header-->
    <header>
    		<!-- Top Bar -->
            <div class="top">
                <div class="container">
                    <div class="row-fluid">   
                    	<p class="animated fadeInRight"> +44 131 526 3300</p>
                    </div>
                    
                </div>
            </div>
            <!-- End Top Bar -->
            
            <!-- Nav-->
            <nav>
            	<div class="container">
                	<div class="row-fluid">
                        	
                            <!-- Logo-->
                        	<div class="span3"><span class="span31"><img src="<?php echo SITE_URL;?>landing/img/blue/logo.png" alt="Euro Accountancy &amp; Finance Services" class="logo animated delay1 fadeInDown" title="Euro Accountancy &amp; Finance Services"></span></div>
                            <!-- End Logo-->
                            
                            <!-- Intro Text-->
                            <div class="span9">
                            	<h1 class="animated delay2 fadeInDown">Euro Consulting BV<br>
                           	    Dutch Payroll Specialists</h1>
                            </div>
                            <!-- End Intro Text-->
                            
                	</div>
            	</div>            
            </nav>
            <!-- End Nav-->
            
            <!-- Slider -->
            <div class="slider">
            	<div class="container">
                	<div class="row-fluid">
                    	<div class="span6 offset6">
                        	<!-- Form -->
                        	<div class="form">
                            	<h2 class="animated delay3 bounceIn">Contact Euro Consulting BV Today ....</h2>
                                
                                <div  id="loading" style="display: none" class='alert'>
					  				<a class='close' data-dismiss='alert'>×</a>
					  				Loading
								</div>
								<div id="response"></div>

                                <form action="http://www.eafs.eu/landing/processor.php" method="POST" name="frmContact" id="frmContact">
                                    <input type=hidden name="oid" value="00D20000000CdoA">
                                    <input type=hidden name="lead_source" value="Email2DB">
                                    <input type=hidden name="retURL" value="http://www.eafs.eu/landing/thank-you/">

                                    <div class="row-fluid">
                                        <div class="span6">
                                            <input type="text" required placeholder="First Name" name="first_name" id="first_name"  />
                                        </div>
                                        <div class="span6">
                                            <input type="text" required placeholder="Last Name" name="last_name" id="last_name" />
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <input type="email" required placeholder=" * Email" name="email" id="email" />
                                        </div>
                                        <div class="span6">
                                            <input type="text" required placeholder="Phone" name="phone" id="phone" />
                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span6">
                                            <select  name="00Nw0000003jevA" id="00Nw0000003jevA" title="Select Country" class="input-xlarge">
                                                <option value="--Make your Selection--" selected>--Make your Selection--</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Italy">Italy</option>
                                                <option value="Luxembourg">Luxembourg</option>
                                                <option value="Netherlands">Netherlands</option>
                                                <option value="Sweden">Sweden</option>
                                                <option value="UK">UK</option>
                                            </select>
                                        </div>
                                        <div class="span6">
                                            <select  name="00Nw0000003joCt" id="00Nw0000003joCt" title="Enquiry Type" class="input-xlarge">
                                                <option selected>--Please choose a country first--</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row4">
                                        <input type="submit" data-loading-text="Loading..." class="btn button animated bounceIn" value="request information">
                                    </div>

                                </form>
                                <div class="clear"></div>
                            </div>
                            <!-- End Form -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Slider -->
    
    </header>
    <!-- End Header-->
    
    
     <!--Content Info-->
	<section class="container info">
    	<div class="row-fluid">
        	
            <div class="span5">
            	<h3 class="animated delay1 fadeInDown">Dutch Payroll</h3>
                <p class="animated delay1 fadeInUp">The correct choice of <strong>Dutch Payroll</strong> provider for contractors is vital to ensure you are paid legally, accurately and on time while contracting in The Netherlands.  </p>

                <p class="animated delay1 fadeInUp">Dutch Payroll and Tax Rules change every 12 months, the majority of contractors finding it difficult to keep track of all Dutch Payroll changes and all relevant updates.  Many contractors find they can reduce costs and save time by outsourcing their Dutch Payroll needs.</p>
                <p class="animated delay1 fadeInUp">EAFS Consulting BV offers Dutch Payroll solutions specifically for contractors working within the Netherlands. Our Dutch Payroll can be tailored to your exact requirements, saving you time and money! Dutch Payroll can be as little as one person on a set monthly salary or a high number of employees with various payments, deductions and administrations. </p>
            </div>
            
            <div class="span7">
            	<div class="top-item">
                    <h3 class="iconset iconbestcity animated delay2 fadeInDown">Outsourcing Dutch Payroll</h3>
                    <p class="animated delay2 fadeInUp">Dutch Payroll administration can be complex with numerous layers and language barriers. EAFS Consulting BV are specialists in administering Dutch Payroll with valuable experience and vast knowledge on the intricate procedures relating to Dutch Payroll and the Dutch tax system.  As a Dutch Payroll provider for Contractors, we can assist with registrations and applications in relation to Dutch Payroll with social security and wage tax. We can also advise on the most effective ways to administer your Dutch Payroll to ensure legal compliance. </p>
                </div>
                
                <div class="bottom-item">
                  <h3 class="iconset iconworldwide animated delay3 fadeInDown">Choose the Best Dutch Payroll Provider</h3>
                    <p class="animated delay3 fadeInUp">The way in which you choose to manage your Dutch Payroll is an important decision. Any mistakes paired with penalties for overdue Dutch Payroll payments related to wage tax could be seriously expensive. It is therefore extremely important to locate a solution tailored to your business needs. EAFS Consulting BV can provide that solution for your Dutch payroll. </p>
                </div>
                
            </div>
            

            
        </div>
    </section>
	<!--End Content Info-->
    
    <!--courses-->

	<!--End Courses-->
    
    <!--Tables-->
    <section class="container pricing-tables"></section>
    <!--End Tables-->
    
    <!-- Extra Info-->
    <section class="extra"></section>
    <!-- End Extra Info-->
    
    <!--Newsletter-->
    <section class="newsletter">
    	<div class="container">
        	<div class="row-fluid">
            	<h1>Call Euro Accountancy &amp; Finance Services today<br>
+44 131 526 3300</h1>
        	</div>
        </div>
    </section>
    <!--End Newsletter-->
    
    <!--Footer-->
    <footer>
    	<div class="container">
        
        	<div class="row-fluid">
            	<div class="span6">
                	<h6>© 2014 EAFS. All rights reserved.   /  <a href="#">Official Website</a><br>
                	</h6>
                </div>
                
                <div class="span6">
                	<ul class="social">
                    </ul>
                </div>
             </div>
        </div>
    </footer>
    <!--End Footer-->
<!-- ======================= JQuery libs =========================== -->
<!-- Bootstrap.js-->
<!-- carrousell -->
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/js/validator.js"></script>

<!-- custom -->
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/js/json.js"></script>

<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
<!-- ======================= End JQuery libs =========================== -->

  </body>
</html>