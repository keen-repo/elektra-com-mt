//<![CDATA[
jQuery(document).ready(function($) {
	
	if( $.fn.tooltip() ) {
		$('[class="tooltip_hover"]').tooltip();
	}
	
	$(".carousel").jCarouselLite({
		btnNext : ".next",
		btnPrev : ".prev",
		easing : "swing", //efecto debe cargarse la libreria easing
		vertical : true,
		auto : 4000,
		speed : 500,
		visible : 3, //cantidad de items visibles
		scroll : 1,
		mouseWheel : true //se mueve con la rueda del mouse, debe cargarse la libreria mouseweel
	});
	
	$('input, textarea').placeholder();

});
//]]>
