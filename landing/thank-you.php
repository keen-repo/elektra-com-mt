<?php require_once("../inc/config.inc.php");?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<head>
<?php require_once("../inc/gtmhead.inc.php");?>
    <!-- Basic Page Needs
        ================================================== -->
    <meta charset="utf-8">
    <title>Euro Accountancy &amp; Finance Services</title>
    <meta name="description" content="description">
    <meta name="author" content="mustachethemes">
    <!-- Mobile Specific Metas
        ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Fav and touch icons
        ================================================== -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Custom styles
        ================================================== -->
    <link href="<?php echo SITE_URL;?>landing/css/style.css" rel="stylesheet" media="screen">
    <link href="<?php echo SITE_URL;?>landing/css/color/blue.css" rel="stylesheet" media="screen">
    <!--[if IE 8 ]><link href="<?php echo SITE_URL;?>landing/css/ie8.css" rel="stylesheet" media="screen"><![endif]-->
    <!-- Scripts Libs
        ================================================== -->
    <script type="text/javascript" src="<?php echo SITE_URL;?>landing/ScriptLibrary/jquery-latest.pack.js"></script>
    <!-- 1.9.1 -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements
        ================================================== -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo SITE_URL;?>landing/bootstrap/css/bootstrap.css" />
    <style type="text/css">
        .span31 {    width: 270px !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<?php echo SITE_URL;?>landing/bootstrap/css/bootstrap-responsive.css" />
    <script type="text/javascript" src="<?php echo SITE_URL;?>landing/bootstrap/js/bootstrap.js"></script>
</head>

<body>
<?php require_once("../inc/gtmbody.inc.php");?>
<!-- Header-->
<header>
    <!-- Top Bar -->
    <div class="top">
        <div class="container">
            <div class="row-fluid">
                <p class="animated fadeInRight"><strong>CALL: +44 131 526 3300</strong></p>
            </div>

        </div>
    </div>
    <!-- End Top Bar -->

    <!-- Nav-->
    <nav>
        <div class="container">
            <div class="row-fluid">

                <!-- Logo-->
                <div class="span3"><span class="span31"><img src="<?php echo SITE_URL;?>landing/img/blue/logo.png" alt="Euro Accountancy &amp; Finance Services" class="logo animated delay1 fadeInDown" title="Euro Accountancy &amp; Finance Services"></span></div>
                <!-- End Logo-->

                <!-- Intro Text-->
                <div class="span9">
                    <h1 class="animated delay2 fadeInDown">Euro Consulting BV<br>
                        Dutch Payroll Specialists</h1>
                </div>
                <!-- End Intro Text-->

            </div>
        </div>
    </nav>
    <!-- End Nav-->

    <!-- Slider -->
    <div class="slider">
        <div class="container">
            <div class="row-fluid">
                <div class="span6 offset6">
                    <!-- Form -->
                    <div class="form">
                        <h2 class="animated delay3 bounceIn">FORM SUCCESS....</h2>

                        <div  id="loading" style="display: none" class='alert'>
                            <a class='close' data-dismiss='alert'>×</a>
                            Loading
                        </div>
                        <div id="response"></div>
                        <h4>Your form has been sucessfully submitted and one of our team will be in touch very soon. <br><br>For more general information on EAFS please visit our website by following one of the links listed below.</h4><br><br>
                        <div class="clear"></div>
                    </div>
                    <!-- End Form -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Slider -->

</header>
<!-- End Header-->




<!--courses-->
<section class="container courses">
    <h2 class="animated delay3 fadeInDown">Best client retention rates</h2>

    <div class="row-fluid ">

        <div class="span3">
            <div class="item">
                <div class="ch-item ch-img-1 thumb">
                    <div class="ch-info">
                    </div>
                </div>
                <h4><a title="Payroll Services Belgium" href="http://www.eafs.eu/contracting-europe/working-in-belgium/?utm_source=website&utm_medium=landingpage&utm_term=payroll_belgium&utm_campaign=payroll_services"> Belgium Payroll</a><br>
                    <span style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #666666; line-height: 24px; font-weight: bold;">Typical Retention 60-80%</span>            		</h4>
                <p>Working through our Belgian subsidiary EAFS Consulting BVBA offers both self-employed and employed solution. In Belgium there are special tax allowances for expatriates, which EAFS utilise. <br>
                    <br>
                </p>
            </div>
        </div>
        <div class="span3 fadeInDown delay4">
            <div class="item">
                <div class="ch-item ch-img-2 thumb">
                    <div class="ch-info">
                    </div>
                </div>
                <h4><a title="Payroll Services Germany" href="http://www.eafs.eu/contracting-europe/working-in-germany/?utm_source=website&utm_medium=landingpage&utm_term=payroll_germany&utm_campaign=payroll_services"> Germany Payroll</a><br>
                    <span style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #666666; line-height: 24px; font-weight: bold;">Typical Retention 60-80%</span></h4>
                <p>Working through our Germany subsidiary EAFS Consulting GmbH, which has the necessary AUG licence to carry out labour leasing. We offer both self-employed and employed solutions, depending on your circumstances.</p>


            </div>
        </div>
        <div class="span3 fadeInDown delay5">
            <div class="item">
                <div class="ch-item ch-img-3 thumb">
                    <div class="ch-info">
                    </div>
                </div>
                <h4><a title="Dutch Payroll Services" href="http://www.eafs.eu/contracting-europe/working-in-netherlands/?utm_source=website&utm_medium=landingpage&utm_term=payroll_netherlands&utm_campaign=payroll_services"> Dutch Payroll</a><br>
                    <span style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #666666; line-height: 24px; font-weight: bold;">Typical Retention 65-75%</span>            		</h4>
                <p>Working as an employee of our Dutch subsidiary EAFS Consulting BV, which can utilise the special 30% ruling for highly skilled migrant. <br>
                    <br>
                    <br>
                    <br>
                </p>
            </div>
        </div>
        <div class="span3 fadeInDown delay5">
            <div class="item">
                <div class="ch-item ch-img-4 thumb">
                    <div class="ch-info">
                    </div>
                </div>
                <h4><a title="European Payroll Services" href="http://www.eafs.eu/contracting-europe/?utm_source=website&utm_medium=landingpage&utm_term=payroll_europe&utm_campaign=payroll_services"> European Payroll</a><br>
                    <span style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #666666; line-height: 24px; font-weight: bold;"><br>
           		    Typical Retention 65-75%</span><br>
                </h4>
                <p>EAFS specalise in operating weekly payroll for contractors &amp;  agencies in the UK . This is not  out-sourced to another accounting firm, but is carried out by our  qualified internal accounts team many who specialise in payroll  services.<br></p></div>
        </div>

    </div>
</section>
<!--End Courses-->

<!--Tables-->
<section class="container pricing-tables"></section>
<!--End Tables-->

<!-- Extra Info-->
<section class="extra"></section>
<!-- End Extra Info-->

<!--Newsletter-->
<section class="newsletter">
    <div class="container">
        <div class="row-fluid">
            <h1>Call Euro Accountancy &amp; Finance Services today<br>
                +44 131 526 3300</h1>
        </div>
    </div>
</section>
<!--End Newsletter-->

<!--Footer-->
<footer>
    <div class="container">

        <div class="row-fluid">
            <div class="span6">
                <h6>© 2014 EAFS. All rights reserved.   /  <a href="http://www.eafs.eu/?utm_source=website&utm_medium=landingpage&utm_term=eafs_payroll&utm_campaign=payroll_services">Official Website</a><br>
                </h6>
            </div>

            <div class="span6">
                <ul class="social">
                </ul>
            </div>
        </div>
    </div>
</footer>
<!--End Footer-->
<!-- ======================= JQuery libs =========================== -->
<!-- Bootstrap.js-->
<!-- carrousell -->
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/js/jcarousellite_1.0.1.pack.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/js/validator.js"></script>

<!-- custom -->
<script type="text/javascript" src="<?php echo SITE_URL;?>landing/js/scripts.js"></script>

<!-- START SEO SCRIPTS ELEMENTS-->
<?php // require_once("../inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
<!-- ======================= End JQuery libs =========================== -->

</body>
</html>