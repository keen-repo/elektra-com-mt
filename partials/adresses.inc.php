<h4>EAFS Around Europe</h4>
<div class="row">
    <div class="col-sm-6">
        <div class="shadow-wrap footer-wrap" itemprop="sourceOrganization" itemscope="itemscope" itemtype="https://schema.org/LocalBusiness">
            <h5><strong><span itemprop="name">Euro Accountancy & Finance Services</span></strong></h5>
            <div itemprop="address" itemscope="itemscope" itemtype="https://schema.org/PostalAddress">
                <em>
                    <strong><span>Head Office</span></strong><br>
                    <span itemprop="streetAddress">23 Blair Street</span>
                    <span itemprop="addressLocality">Edinburgh</span><br>
                    <span itemprop="postalCode">EH1 1QR</span><br>
                    <span itemprop="addressCountry">Scotland</span><br>
                </em>
            </div>
        </div>
    </div>
<div class="col-sm-6">
    <div class="shadow-wrap footer-wrap belgium" itemprop="sourceOrganization" itemscope="itemscope" itemtype="https://schema.org/LocalBusiness">
        <h5><strong><span itemprop="name">EAFS Consulting BVBA</span></strong></h5>
        <div itemprop="address" itemscope="itemscope" itemtype="https://schema.org/PostalAddress">
            <em>
                <strong><span>Head Office</span></strong><br>
                <span itemprop="streetAddress">Square be Meeus 37,</span>
                <span itemprop="addressLocality">Brussles</span><br>
                <span itemprop="postalCode">1000</span><br>
                <span itemprop="addressCountry">Belgium</span><br>
            </em>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="shadow-wrap footer-wrap holland" itemprop="sourceOrganization" itemscope="itemscope" itemtype="https://schema.org/LocalBusiness">
        <h5><strong><span itemprop="name">EAFS Consulting BV</span></strong></h5>
        <div itemprop="address" itemscope="itemscope" itemtype="https://schema.org/PostalAddress">
            <em>
                <strong><span>Head Office</span></strong><br>
                <span itemprop="streetAddress">Evert V/D Beekstraat 310,</span>
                <span itemprop="addressLocality">Schiphol</span><br>
                <span itemprop="postalCode">1118CX</span><br>
                <span itemprop="addressCountry">Netherlands</span><br>
            </em>
        </div>
    </div>
</div><div class="col-sm-6">
    <div class="shadow-wrap footer-wrap germany" itemprop="sourceOrganization" itemscope="itemscope" itemtype="https://schema.org/LocalBusiness">
        <h5><strong><span itemprop="name">EAFS Consulting GmbH</span></strong></h5>
        <div itemprop="address" itemscope="itemscope" itemtype="https://schema.org/PostalAddress">
            <em>
                <strong><span>Head Office</span></strong><br>
                <span itemprop="streetAddress">Altrottstr. 31,</span>
                <span itemprop="addressLocality">Walldorf</span><br>
                <span itemprop="postalCode">D-69190</span><br>
                <span itemprop="addressCountry">Germany</span><br>
            </em>
        </div>
    </div>
</div>
</div>