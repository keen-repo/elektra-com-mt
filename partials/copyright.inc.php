<section id="copyrights">
    <div class="container">
        <div class="col-lg-5 col-md-6 col-sm-12">
            <div class="copyright-text">
                <p>Copyright &copy; 2019 - Euro Accountancy and Finance Services</p>
            </div><!-- end copyright-text -->
        </div><!-- end widget -->
        <div class="col-lg-7 col-md-6 col-sm-12 clearfix">
            <div class="footer-menu">
                <ul class="menu" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
                    <li class="active"><a itemprop="url" href="<?php echo SITE_URL;?>"  title="Link to our Home Page"><div itemprop="name">Home</div></a></li>
                    <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/" title="Link to our Contracting in Europe Page"><div itemprop="name">Contracting in Europe</div></a></li>
                    <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/payroll-calculators/" title="Link to our Payroll Calculators Page"><div itemprop="name">Payroll Calculators</div></a></li>
                    <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/" title="Link to our Contractor Accountants Page"><div itemprop="name">Contractor Accountants</div></a></li>
                    <li><a itemprop="url" href="<?php echo SITE_URL;?>company-formations/" title="Link to our Company Formations Page"><div itemprop="name">Company Formations</div></a></li>
                </ul>
            </div>
        </div><!-- end large-7 -->
    </div><!-- end container -->
</section>