<footer id="footer-style-1" role="contentinfo" itemscope itemtype="https://schema.org/WPFooter">
    <div class="container">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="widget">
                <img class="padding-top" src="<?php echo SITE_URL;?>assets/logo.png" alt="Euro Accountancy & Finance Services">
                <p>Euro Accountancy & Finance Services provide accountancy services and fully compliant payroll solutions designed specifically for contractors and freelancers working within the UK and throughout Europe.</p>
                <div class="social-icons">
                    <span><a href="https://www.eafs.eu/social/networks/twitter/" data-original-title="Link to our Twitter Page" title="Link to our Twitter Page" data-placement="bottom" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></span>
                    <span><a href="https://www.eafs.eu/social/networks/facebook/" data-original-title="Link to our Facebook Page" title="Link to our Facebook Page" data-placement="bottom" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></span>
                    <span><a href="https://www.eafs.eu/social/networks/googleplus/" data-original-title="Link to our Google+ Page" title="Google+" data-placement="bottom" data-toggle="tooltip" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></span>
                    <span><a href="https://www.eafs.eu/social/networks/linkedin/" data-original-title="Link to our Linkedin Page" title="Link to our Linkedin Page" data-placement="bottom" data-toggle="tooltip" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></span>
                    <span><a href="https://www.eafs.eu/social/networks/youtube/" data-original-title="Link to our YouTube Channel" title="Link to our YouTube Channel" data-placement="bottom" data-toggle="tooltip" data-original-title="YouTube"><i class="fa fa-youtube"></i></a></span>
                </div><!-- end social icons -->
            </div><!-- end widget -->
        </div><!-- end columns -->
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="widget">
                <div class="title">
                    <h3>Twitter Feeds</h3>
                </div><!-- end title -->
                <ul class="twitter_feed">
                    <li><span></span><p>EAFS open new Dutch BV for our contractors working in Holland! <br><a href="#">about 2 days ago</a></p></li>
                    <li><span></span><p>All you need to know about retention rates in EU countries!  <br><a href="#">about 9 days ago</a></p></li>
                </ul><!-- end twiteer_feed -->
            </div><!-- end widget -->
        </div><!-- end columns -->
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="widget">
                <div class="title">
                    <h3>Company Info.</h3>
                </div><!-- end title -->
                <ul class="nav nav-tabs-foot nav-stacked-foot" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
                    <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/about-eafs/" title="Link to our Company Information Page"><div itemprop="name">About EAFS</div></a></li>
                    <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/contact-eafs/" title="Link to our Contacting EAFS Page"><div itemprop="name">Contacting Us</div></a></li>
                    <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/privacy-policy/" title="Link to our Privacy Policy Page"><div itemprop="name">Privacy Policy</div></a></li>
                    <li><a itemprop="url" href="<?php echo SITE_URL;?>company-information/terms-conditions/" title="Link to our Terms Conditions Page"><div itemprop="name">Terms Conditions</div></a></li>
                </ul>
            </div><!-- end widget -->
        </div><!-- end columns -->
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="widget">
                <div class="title">
                    <h3>NewsLetter</h3>
                </div><!-- end title -->
                <div class="newsletter_widget">
                    <p>Subscribe to our newsletter to receive news, updates, free stuff and new releases by email. We don't do spam.</p>
                    <form action="https://auroralabs.createsend.com/t/r/s/tkkiyhi/" method="post" class="newsletter_form">
                        <input type="text" name="cm-tkkiyhi-tkkiyhi" class="form-control" placeholder="Enter your email address">
                        <button type="submit" class="btn btn-primary pull-right">Subscribe</button>
                    </form><!-- end newsletter form -->
                </div>
            </div><!-- end widget -->
        </div><!-- end columns -->
    </div><!-- end container -->
</footer>