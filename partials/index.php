<?php require_once("inc/config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contractor Accountants : European Payroll : <?php echo SITE_TITLE;?></title>
    <!-- Start MetaData -->
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Euro Accountancy &amp; Finance Services" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="<?php echo SITE_TITLE;?>" />
    <meta itemprop="copyright" content="<?php echo SITE_TITLE;?> 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe.  We also provide a full accountancy service for small to medium owner managed UK businesses." />
    <meta itemprop="image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eafseu">
    <meta name="twitter:title" content="Contractor Accountants : European Payroll : <?php echo SITE_TITLE;?>" />
    <meta name="twitter:description" content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe.  We also provide a full accountancy service for small to medium owner managed UK businesses." />
    <meta name="twitter:creator" content="@eafseu" />
    <meta name="twitter:image:src" content="<?php echo SITE_URL;?>social/src/logo280x150.png" />
    <meta name="twitter:domain" content="<?php echo SITE_URL;?>" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Contractor Accountants : European Payroll : <?php echo SITE_TITLE;?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo SITE_URL;?>" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="<?php echo SITE_URL;?>social/src/logo250x250.png" />
    <meta property="og:description" content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe.  We also provide a full accountancy service for small to medium owner managed UK businesses."/>
    <meta property="og:site_name" content="<?php echo SITE_TITLE;?>" />
    <!-- Standard meta data -->
    <meta name="robots" content="Index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Accountancy Services, European Payroll, Contractor Accountants" />
    <meta name="description" content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe.  We also provide a full accountancy service for small to medium owner managed UK businesses." />
    <link rel="canonical" href="<?php echo SITE_URL;?>" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>social/src/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL;?>social/favicon/apple-touch-icon-114x114.png">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL;?>css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="animationload">
    <div class="loader">Loading...</div>
</div>

<!-- START SOCIAL TOP BAR-->
<?php require_once("partials/topbar.inc.php");?>
<!-- END SOCIAL TOP BAR-->

<!-- START NAVIGATION ELEMENTS-->
<?php require_once("partials/navigation.inc.php");?>
<!-- END NAVIGATION ELEMENTS-->

<!-- START VIDEO HEADING-->
<div class="slide-wrapper">
    <section class="headline-top head-shadow clearfix">
        <?php require_once("partials/widget_slider.inc.php");?>
    </section>
</div>
<!-- END VIDEO HEADING-->

<!-- START BREADCRUMB ELEMENTS-->
<div class="breadcrumb-wrapper hidden-xs">
    <div class="container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrumb-container clearfix">
                <ul class="breadcrumbs clearfix">
                    <li itemscope itemtype="https://schema.org/Breadcrumb">
                        <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                    <li itemscope itemtype="https://schema.org/Breadcrumb" class="current">
                        <span itemprop="title">Euro Accountancy & Finance Services</span></li>
                </ul><!-- .breadcrumb end -->
            </div>
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END BREADCRUMB ELEMENTS-->

<!-- START MAIN CONTENT ELEMENTS-->
<div class="blog-wrapper">
    <div class="container">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="content">

            <div class="row">
                <div class="blog-masonry" itemprop="mainContentOfPage">
                    <div class="col-lg-12">
                        <article itemprop="Contractor Services" itemscope itemtype="https://schema.org/Article" class="doc">
                        <header class="header">
                            <h1 itemprop="headline" class="subheader">Contractor Services</h1>
                        </header>
                            <div itemprop="articleBody">
                            <p class="lead"><strong>Welcome to the Euro Accountancy & Finance Services website. We have established ourselves in the contractor payroll sector for over a decade, accumulating a wealth of experience and building a strong team to support your accounting needs.</strong></p>
                            <p class="has-pullquote pullquote-adelle" data-pullquote="Specialists in the contractor payroll sector for over a decade.">EAFS provide accountancy and tax solutions designed specifically for contractors and freelancers working within the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses. Whether you choose to contract through your own <strong><a itemprop="url" title="Company Formations (UK)" href="<?php echo SITE_URL;?>company-formations/">Limited Company</a></strong>, require a <strong><a itemprop="url" title="European Payroll" href="<?php echo SITE_URL;?>contracting-europe/">European Payroll Solution</a></strong> or wish to take advantage of our <strong><a itemprop="url" title="Specialist Accountancy Services" href="<?php echo SITE_URL;?>contractor-accountants/">Specialist Accountancy Services</a></strong>, EAFS provide a bespoke service tailored to your individual circumstances.</p>
                            <p>Unlike other payroll providers, EAFS provide both <strong><a itemprop="url" title="European Payroll" href="<?php echo SITE_URL;?>contracting-europe/">European Payroll</a></strong> and <strong><a itemprop="url" title="Company Formations (UK)" href="<?php echo SITE_URL;?>company-formations/">Limited Company</a></strong> services, making us uniquely placed to give full and qualified advice on the best solution for your individual requirements and needs. We are happy to discuss your financial &amp; payroll requirements on a one to one basis and can meet you at your premises for a free initial, no obligation meeting. </p>

                            <h4>Recruitment Agency Support</h4>
                            <p>EAFS work with recruitment agencies from across the globe in order to place contractors and freelancers compliantly in Europe, giving both the agency and the contractor piece of mind that they are fully compliant and their tax affairs are in safe hands. </p>
                            <p><strong>EAFS Consulting BV</strong>, Our Dutch Company, is registered at the Dutch Chamber of Commerce and is fully compliant with all Dutch labour leasing regulations, allowing you to work within Holland&rsquo;s borders. </p>
                            <p><strong>EAFS Consulting BVBA</strong>, Our Belgian Company, is registered at the Belgian Chamber of Commerce and is fully complaint with all Belgian labour leasing regulations. </p>
                            <p><strong>EAFS Consulting GmbH</strong> is established in Germany and holds the required AüG License to operate within German borders. </p>


                            <!-- CONTACT ME BUTTON  -->
                            <?php require_once("partials/widget_contactbutton.inc.php");?>
                            <!-- CONTACT ME BUTTON  -->
                            </div><!-- end articleBody -->
                        </article><!-- end article -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->
            </div><!-- end row -->


            <!-- START WIDGET LATEST NEWS -->
            <?php require_once("partials/widget_carousel_nov.inc.php");?>
            <!-- END WIDGET LATEST NEWS -->


        </div><!-- end content -->
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="sidebar">
            <aside>
                <!-- WIDGET START CONTACT -->
                <?php require_once("partials/widget_adviser.inc.php");?>
                <!-- WIDGET END CONTACT -->
                <!-- WIDGET START LINKS -->
                <?php require_once("partials/widget_social.inc.php");?>
                <!-- WIDGET END LINKS -->
                <!-- WIDGET START DOWNLOADS -->
                <?php require_once("partials/widget_downloads.inc.php");?>
                <!-- WIDGET END DOWNLOADS -->
                <!-- WIDGET START CALCULATOR -->
                <?php require_once("partials/widget_calculators.inc.php");?>
                <!-- WIDGET END CALCULATOR -->

            </aside><!-- end aside -->
        </div><!-- end sidebar -->
    </div><!-- end container -->
</div><!-- end wrapper -->
<!-- END MAIN CONTENT ELEMENTS-->


<!-- START TRANSPARENT BACKGROUND -->
<?php require_once("partials/parallax.inc.php");?>
<!-- END TRANSPARENT BACKGROUND -->


<!-- START FOOTER ELEMENTS-->
<?php require_once("partials/footer.inc.php");?>
<!-- END FOOTER ELEMENTS-->

<!-- START COPYRIGHT ELEMENTS-->
<?php require_once("partials/copyright.inc.php");?>
<!-- END COPYRIGHT ELEMENTS-->

<div class="dmtop" style="bottom: 25px;">Scroll to Top</div>

<!-- START SCRIPTS ELEMENTS-->
<?php require_once("inc/scripts_front.inc.php");?>
<!-- END SCRIPTS ELEMENTS-->
<!-- START SEO SCRIPTS ELEMENTS-->
<?php require_once("inc/ga.inc.php");?>
<!-- END SEO SCRIPTS ELEMENTS-->
</body>
</html>