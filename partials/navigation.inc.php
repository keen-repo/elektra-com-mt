<header id="header-style-1" data-spy="affix" data-offset-top="100" class="affix-top" role="banner" itemscope itemtype="https://schema.org/WPHeader">
    <div class="container">
        <nav class="navbar yamm navbar-default">
            <div class="navbar-header">
                <button class="navbar-toggle" data-target="#navbar-collapse-1" data-toggle="collapse" type="button">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo SITE_URL;?>"></a>
            </div><!-- end navbar-header -->

            <div class="navbar-collapse collapse navbar-right" id="navbar-collapse-1">
                <ul role="menu" class="nav navbar-nav" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
                    <!-- standard drop down -->
                    <li class="dropdown"><a itemprop="url" class="dropdown-toggle disabled" data-toggle="dropdown" title="Contracting in Europe" href="<?php echo SITE_URL;?>contracting-europe/"><div itemprop="name">Contracting in Europe</div><div class="arrow-up"></div></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                <a itemprop="url" title="Working in Belgium" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/"><div itemprop="name">Working in Belgium</div><i class="fa fa-angle-right faright"></i></a>
                                <ul class="dropdown-menu  yamm-fw">
                                    <li><a itemprop="url" title="Belgium Income Tax" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-income-tax/"><div itemprop="name">Belgium Income Tax</div></a></li>
                                    <li><a itemprop="url" title="Belgium Social Security" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-social-security/"><div itemprop="name">Belgium Social Security</div></a></li>
                                    <li><a itemprop="url" title="Belgium Visas" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-visas/"><div itemprop="name">Belgium Visas</div></a></li>
                                    <li><a itemprop="url" title="Management Services" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/management-services/"><div itemprop="name">Management Services</div></a></li>
                                    <li><a itemprop="url" title="Belgium BVBA" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-bvba/"><div itemprop="name">Belgium BVBA</div></a></li>
                                    <li><a itemprop="url" title="Belgium Payroll" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-payroll/"><div itemprop="name">Belgium Payroll</div></a></li>
                                </ul><!-- end dropdown-menu -->
                            </li><!-- end dropdown-submenu -->
                            <li class="dropdown-submenu">
                                <a itemprop="url" title="Working in Germany" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/"><div itemprop="name">Working in Germany</div><i class="fa fa-angle-right faright"></i></a>
                                <ul class="dropdown-menu  yamm-fw">
                                    <li><a itemprop="url" title="German Income Tax" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-income-tax/"><div itemprop="name">German Income Tax</div></a></li>
                                    <li><a itemprop="url" title="German Social Security" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-social-security/"><div itemprop="name">German Social Security</div></a></li>
                                    <li><a itemprop="url" title="German Visas" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-visas/"><div itemprop="name">German Visas</div></a></li>
                                    <li><a itemprop="url" title="Management Services" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/management-services/"><div itemprop="name">Management Services</div></a></li>
                                    <li><a itemprop="url" title="German GmbH" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-gmbh/"><div itemprop="name">German GmbH</div></a></li>
                                    <li><a itemprop="url" title="German Payroll" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-payroll/"><div itemprop="name">German Payroll</div></a></li>
                                </ul><!-- end dropdown-menu -->
                            </li><!-- end dropdown-submenu -->
                            <li class="dropdown-submenu">
                                <a itemprop="url" title="Working in Netherlands" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/"><div itemprop="name">Working in Netherlands</div><i class="fa fa-angle-right faright"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a itemprop="url" title="Dutch Income Tax" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-income-tax/"><div itemprop="name">Dutch Income Tax</div></a></li>
                                    <li><a itemprop="url" title="Dutch Social Security" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-social-security/"><div itemprop="name">Dutch Social Security</div></a></li>
                                    <li><a itemprop="url" title="Dutch Visas" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-visas/"><div itemprop="name">Dutch Visas</div></a></li>
                                    <li><a itemprop="url" title="Management Services" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/management-services/"><div itemprop="name">Management Services</div></a></li>
                                    <li><a itemprop="url" title="Dutch BV" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-bv/"><div itemprop="name">Dutch BV</div></a></li>
                                    <li><a itemprop="url" title="Dutch Payroll" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-payroll/"><div itemprop="name">Dutch Payroll</div></a></li>
                                    <li><a itemprop="url" title="30% Ruling" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/30-ruling/"><div itemprop="name">30% Ruling</div></a></li>
                                </ul><!-- end dropdown-menu --><!-- end dropdown-menu -->
                            </li><!-- end dropdown-submenu -->
                            <li class="dropdown-submenu">
                                <a itemprop="url"  title="Working in Italy" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/"><div itemprop="name">Working in Italy</div><i class="fa fa-angle-right faright"></i></a>
                                <ul class="dropdown-menu  yamm-fw">
                                    <li><a itemprop="url" title="Italian Income Tax" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-income-tax/"><div itemprop="name">Italian Income Tax</div></a></li>
                                    <li><a itemprop="url" title="Italian Social Security" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-social-security/"><div itemprop="name">Italian Social Security</div></a></li>
                                    <li><a itemprop="url" title="Italian Visas" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-visas/"><div itemprop="name">Italian Visas</div></a></li>
                                    <li><a itemprop="url" title="Management Services" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/management-services/"><div itemprop="name">Management Services</div></a></li>
                                    <li><a itemprop="url" title="Italian Umbrella" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-umbrella/"><div itemprop="name">Italian Umbrella</div></a></li>
                                    <li><a itemprop="url" title="Italian Payroll" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-payroll/"><div itemprop="name">Italian Payroll</div></a></li>
                                </ul><!-- end dropdown-menu -->
                            </li><!-- end dropdown-submenu -->
                            <li class="dropdown-submenu">
                                <a itemprop="url" title="Working in Luxembourg" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/"><div itemprop="name">Working in Luxembourg</div><i class="fa fa-angle-right faright"></i></a>
                                <ul class="dropdown-menu  yamm-fw">
                                    <li><a itemprop="url" title="Luxembourg Income Tax" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-income-tax/"><div itemprop="name">Luxembourg Income Tax</div></a></li>
                                    <li><a itemprop="url" title="Luxembourg Social Security" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-social-security/"><div itemprop="name">Luxembourg Social Security</div></a></li>
                                    <li><a itemprop="url" title="Luxembourg Visas" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-visas/"><div itemprop="name">Luxembourg Visas</div></a></li>
                                    <li><a itemprop="url" title="Management Services" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/management-services/"><div itemprop="name">Management Services</div></a></li>
                                    <li><a itemprop="url" title="Luxembourg Umbrella" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-umbrella/"><div itemprop="name">Luxembourg Umbrella</div></a></li>
                                    <li><a itemprop="url" title="Luxembourg Payroll" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-payroll/"><div itemprop="name">Luxembourg Payroll</div></a></li>
                                </ul><!-- end dropdown-menu -->
                            </li><!-- end dropdown-submenu -->
                        </ul><!-- end dropdown-menu -->
                    </li><!-- end drop down -->
                    <!-- standard drop down -->
                    <li class="dropdown"><a itemprop="url" class="dropdown-toggle disabled" data-toggle="dropdown" title="FAQ &amp; Tools" href="<?php echo SITE_URL;?>faq-tools/"><div itemprop="name">FAQ &amp; Tools</div><div class="arrow-up"></div></a>
                        <ul class="dropdown-menu">
                            <li><a itemprop="url" title="Agency Support" href="<?php echo SITE_URL;?>faq-tools/agency-support/"><div itemprop="name">Agency Support</div></a></li>
                            <li><a itemprop="url" title="Contracting in Europe FAQ" href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/"><div itemprop="name">Contracting in Europe FAQ</div></a></li>
                            <!--<li><a href="<?php echo SITE_URL;?>faq-tools/european-payroll-faq/">European Payroll FAQ</a></li>-->
                        </ul><!-- end dropdown-menu -->
                    </li><!-- end standard drop down -->
                    <!-- standard drop down -->
                    <li class="dropdown"><a itemprop="url" class="dropdown-toggle disabled" data-toggle="dropdown" title="Contractor Accountants (UK)" href="<?php echo SITE_URL;?>contractor-accountants/"><div itemprop="name">Contractor Accountants (UK)</div><div class="arrow-up"></div></a>
                        <ul class="dropdown-menu">
                            <li><a itemprop="url" title="Benefits of a Limited Company" href="<?php echo SITE_URL;?>contractor-accountants/benefits-limited-company/"><div itemprop="name">Benefits of a Limited Company</div></a></li>
                            <li><a itemprop="url" title="Business Expenses" href="<?php echo SITE_URL;?>contractor-accountants/business-expenses/"><div itemprop="name">Business Expenses</div></a></li>
                            <li><a itemprop="url" title="Corporation Tax" href="<?php echo SITE_URL;?>contractor-accountants/corporation-tax/"><div itemprop="name">Corporation Tax</div></a></li>
                            <li><a itemprop="url" title="Changing Accountants" href="<?php echo SITE_URL;?>contractor-accountants/changing-accountants/"><div itemprop="name">Changing Accountants</div></a></li>
                            <li><a itemprop="url" title="IR35 Explained" href="<?php echo SITE_URL;?>contractor-accountants/ir35-explained/"><div itemprop="name">IR35 Explained</div></a></li>
                            <li class="dropdown-submenu">
                                <a itemprop="url" title="Limited Company Services" href="<?php echo SITE_URL;?>contractor-accountants/limited-company-services/"><div itemprop="name">Limited Company Services</div><i class="fa fa-angle-right faright"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a itemprop="url" title="Limited Company Contractors" href="<?php echo SITE_URL;?>contractor-accountants/limited-company-contractors/"><div itemprop="name">Limited Company Contractors</div></a></li>
                                    <li><a itemprop="url" title="Limited Company FAQ" href="<?php echo SITE_URL;?>contractor-accountants/limited-company-faq/"><div itemprop="name">Limited Company FAQ</div></a></li>
                                    <li><a itemprop="url" title="Limited versus Umbrella" href="<?php echo SITE_URL;?>contractor-accountants/limited-versus-umbrella/"><div itemprop="name">Limited versus Umbrella</div></a></li>
                                </ul><!-- end dropdown-menu --><!-- end dropdown-menu -->
                            </li><!-- end dropdown-submenu -->
                            <li><a itemprop="url" title="PAYE (Pay As You Earn)" href="<?php echo SITE_URL;?>contractor-accountants/pay-as-you-earn/"><div itemprop="name">PAYE (Pay As You Earn)</div></a></li>
                            <li><a itemprop="url" title="S660 Settlements" href="<?php echo SITE_URL;?>contractor-accountants/s660-settlements/"><div itemprop="name">S660 Settlements</div></a></li>
                            <li class="dropdown-submenu">
                                <a itemprop="url" title="VAT Services" href="<?php echo SITE_URL;?>contractor-accountants/value-added-tax/"><div itemprop="name">VAT Services</div><i class="fa fa-angle-right faright"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a itemprop="url" title="VAT Returns" href="<?php echo SITE_URL;?>contractor-accountants/vat-returns/"><div itemprop="name">VAT Returns</div></a></li>
                                    <li><a itemprop="url" title="VAT Flat Rate Scheme" href="<?php echo SITE_URL;?>contractor-accountants/flat-rate-scheme/"><div itemprop="name">VAT Flat Rate Scheme</div></a></li>
                                    <li><a itemprop="url" title="VAT Flat Rate Calculation" href="<?php echo SITE_URL;?>contractor-accountants/flat-rate-calculation/"><div itemprop="name">VAT Flat Rate Calculation</div></a></li>
                                </ul><!-- end dropdown-menu --><!-- end dropdown-menu -->
                            </li><!-- end dropdown-submenu -->
                            <li><a itemprop="url" title="Why Choose EAFS?" href="<?php echo SITE_URL;?>contractor-accountants/why-choose-eafs/"><div itemprop="name">Why Choose EAFS?</div></a></li>
                            <li><a itemprop="url" title="Agency Workers Regulations" href="<?php echo SITE_URL;?>contractor-accountants/agency-workers-regulations/"><div itemprop="name">Agency Workers Regulations</div></a></li>
                        </ul><!-- end dropdown-menu -->
                    </li><!-- end standard drop down -->
                    <!-- standard drop down -->
                    <li class="dropdown"><a itemprop="url" class="dropdown-toggle disabled" data-toggle="dropdown" title="Other Services (UK)" href="<?php echo SITE_URL;?>contractor-services/"><div itemprop="name">Other Services (UK)</div><div class="arrow-up"></div></a>
                        <ul class="dropdown-menu">
                            <li><a itemprop="url" title="Business Bank Account" href="<?php echo SITE_URL;?>contractor-services/business-bank-account/"><div itemprop="name">Business Bank Account</div></a></li>
                            <li><a itemprop="url" title="Insurance for Contractors" href="<?php echo SITE_URL;?>contractor-services/insurance-for-contractors/"><div itemprop="name">Insurance for Contractors</div></a></li>
                            <li><a itemprop="url" title="Mortgages for Contractors" href="<?php echo SITE_URL;?>contractor-services/mortgages-for-contractors/"><div itemprop="name">Mortgages for Contractors</div></a></li>
                        </ul><!-- end drop down menu -->
                    </li><!-- end drop down -->
                    <!-- standard drop down -->
                    <li class="dropdown"><a itemprop="url" class="dropdown-toggle disabled" data-toggle="dropdown" title="Company Formations (UK)" href="<?php echo SITE_URL;?>company-formations/"><div itemprop="name">Company Formations (UK)</div><div class="arrow-up"></div></a>
                        <ul class="dropdown-menu">
                            <!--<li><a href="<?php echo SITE_URL;?>company-formations/sole-trader/">Sole Trader</a></li>-->
                            <!--<li><a href="<?php echo SITE_URL;?>company-formations/partnership/">Partnership</a></li> -->
                            <li><a itemprop="url" title="Limited Company" href="<?php echo SITE_URL;?>company-formations/limited-company/"><div itemprop="name">Limited Company</div></a></li>
                        </ul><!-- end drop down menu -->
                    </li><!-- end drop down -->
                    <!-- standard drop down -->
                </ul><!-- end navbar-nav -->
            </div><!-- #navbar-collapse-1 -->
        </nav><!-- end navbar yamm navbar-default -->
    </div><!-- end container -->
</header>