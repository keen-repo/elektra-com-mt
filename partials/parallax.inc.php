<section id="one-parallax" class="parallax" style="background-image: url('<?php echo SITE_URL;?>assets/parallax_01.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
    <div class="overlay">
        <div class="container">
            <div class="testimonial-widget">
                <div class="row padding-top margin-top">
                    <div class="contact_details">
                        <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                            <div class="text-center">
                                <div class="wow swing">
                                    <div class="contact-icon">
                                        <a href="<?php echo SITE_URL;?>company-information/contact-eafs/" title="Contacting EAFS" class=""> <i class="fa fa-map-marker fa-3x"></i> </a>
                                    </div><!-- end dm-icon-effect-1 -->
                                    <p><strong>SCOTLAND:</strong> 23 Blair Street, Edinburgh, Scotland<br>
                                        <strong>BELGIUM:</strong> Square de Meeus 37, Brussels, Belgium</p>
                                </div><!-- end service-icon -->
                            </div><!-- end miniboxes -->
                        </div><!-- end col-lg-4 -->

                        <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                            <div class="text-center">
                                <div class="wow swing">
                                    <div class="contact-icon">
                                        <a href="<?php echo SITE_URL;?>company-information/contact-eafs/" title="Contacting EAFS" class=""> <i class="fa fa-phone fa-3x"></i> </a>
                                    </div><!-- end dm-icon-effect-1 -->
                                    <p><strong>Phone (UK):</strong> +44 131 526 3300<br>
                                        <strong>Phone (Netherlands):</strong> +31 20 808 45 44</p>
                                </div><!-- end service-icon -->
                            </div><!-- end miniboxes -->
                        </div><!-- end col-lg-4 -->

                        <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                            <div class="text-center">
                                <div class="wow swing">
                                    <div class="contact-icon">
                                        <a href="<?php echo SITE_URL;?>company-information/contact-eafs/" title="Contacting EAFS" class=""> <i class="fa fa-map-marker fa-3x"></i> </a>
                                    </div><!-- end dm-icon-effect-1 -->
                                    <p><strong>NETHERLANDS:</strong>Beekstraat 310, Schiphol, Netherlands<br>
                                        <strong>GERMANY:</strong> Altrottstr. 31, Walldorf, Germany</p>
                                </div><!-- end service-icon -->
                            </div><!-- end miniboxes -->
                        </div><!-- end col-lg-4 -->
                    </div><!-- end contact_details -->
                </div><!-- end margin-top -->
            </div><!-- end testimonial widget -->
        </div><!-- end container -->
    </div><!-- end overlay -->
</section>