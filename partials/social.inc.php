<section class="col-sm-12">
    <div class="social-icons2">
        <ul>
            <li><a href="<?php echo SITE_URL;?>social/networks/twitter/" data-original-title="Link to our Twitter Page" title="Link to our Twitter Page"><i class="fa fa-twitter"></i></a></li>
            <li><a href="<?php echo SITE_URL;?>social/networks/facebook/" data-original-title="Link to our Facebook Page" title="Link to our Facebook Page"><i class="fa fa-facebook"></i></a></li>
            <li><a href="<?php echo SITE_URL;?>social/networks/googleplus/" data-original-title="Link to our Google+ Page" title="Google+"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="<?php echo SITE_URL;?>social/networks/linkedin/" data-original-title="Link to our Linkedin Page" title="Link to our Linkedin Page"><i class="fa fa-linkedin"></i></a></li>
        </ul>
    </div>
</section>