<div class="clearfix" id="topbar">
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="social-icons">
                <span><a href="https://www.eafs.eu/social/networks/facebook/" title="Link to our Facebook Page"
                         target="_blank" data-placement="bottom" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></span>
                <span><a href="https://www.eafs.eu/social/networks/googleplus/" title="Link to our GooglePlus Page"
                         target="_blank" data-placement="bottom" data-toggle="tooltip" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a></span>
                <span><a href="https://www.eafs.eu/social/networks/twitter/" title="Link to our Twitter Page"
                         target="_blank" data-placement="bottom" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></span>
                <span><a href="https://www.eafs.eu/social/networks/youtube/" title="Link to our You Tube Page"
                         target="_blank" data-placement="bottom" data-toggle="tooltip" data-original-title="Youtube"><i class="fa fa-youtube"></i></a></span>
                <span><a href="https://www.eafs.eu/social/networks/linkedin/" title="Link to our Linkedin Page"
                         target="_blank" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-linkedin"></i></a></span>
                <span class="last"><a href="https://www.eafs.eu/social/networks/skype/" title="Skype EAFS"
                                      target="_blank" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-skype"></i></a></span>
            </div><!-- end social icons -->
        </div><!-- end columns -->
        <div class="col-lg-8 col-md-8 col-sm-6 hidden-xs">
            <div class="topmenu">
                <span class="topbar-cart"><i class="fa fa-user"></i> <a href="<?php echo SITE_URL;?>company-information/impressum/" title="Impressum">Impressum</a></span>
                <span class="topbar-cart"><i class="fa fa-user"></i> <a href="<?php echo SITE_URL;
                ?>company-information/contact-eafs/" title="Link to Contacting EAFS Page">Contact EAFS</a></span>
            </div><!-- end top menu -->
            <div class="callus">
                <span class="topbar-phone"><i class="fa fa-phone"></i> +31 20 808 45 44</span>
                <span class="topbar-phone"><i class="fa fa-phone"></i> +44 131 526 3300</span>
            </div><!-- end callus -->
        </div><!-- end columns -->
    </div><!-- end container -->
</div>


