<section class="download">
    <span><i class="glyphicon glyphicon-info-sign"></i></span>
    <h3>Contact an adviser</h3>
    <p>FREE One to One meeting to assess your needs available when you call us direct...</p>
    <p><a href="<?php echo SITE_URL;?>company-information/contact-eafs/" title="Contact an Adviser" class="btn btn-primary btn-small">Contact an Adviser</a></p>
</section>