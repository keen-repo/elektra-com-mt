<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Working in Belgium</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-income-tax/" title="Link to Belgium Income Tax Page"><div itemprop="name">Belgium Income Tax</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-social-security/" title="Link to Belgium Social Security Page"><div itemprop="name">Belgium Social Security</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-visas/" title="Link to Belgium Visas Page"><div itemprop="name">Belgium Visas</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-bvba/" title="Link to Belgium BVBA Page"><div itemprop="name">Belgium BVBA</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/belgium-payroll/" title="Link to Belgium Payroll Page"><div itemprop="name">Belgium Payroll</div></a></li>
    </ul>
</section>