<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Latest News</h3>
    </div>
    <ul class="recent_posts_widget">
        <li>
            <a href="blog-two-sidebar.html#"><img alt="Latest blog post from EAFS." src="<?php echo SITE_URL;?>assets/tabbed_widget_01.gif">Latest blog post from EAFS.</a>
            <a href="blog-two-sidebar.html#" class="readmore">June 16, 2014</a>
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o"></i>
            </div><!-- rating -->
        </li>
        <li>
            <a href="blog-two-sidebar.html#"><img alt="Latest blog post from EAFS." src="<?php echo SITE_URL;?>assets/tabbed_widget_01.gif">Latest blog post from EAFS.</a>
            <a href="blog-two-sidebar.html#" class="readmore">June 16, 2014</a>
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div><!-- rating -->
        </li>
        <li>
            <a href="blog-two-sidebar.html#"><img alt="Latest blog post from EAFS." src="<?php echo SITE_URL;?>assets/tabbed_widget_01.gif">Latest blog post from EAFS.</a>
            <a href="blog-two-sidebar.html#" class="readmore">June 16, 2014</a>
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o"></i>
            </div><!-- rating -->
        </li>
        <li>
            <a href="blog-two-sidebar.html#"><img alt="Latest blog post from EAFS." src="<?php echo SITE_URL;?>assets/tabbed_widget_01.gif">Latest blog post from EAFS.</a>
            <a href="blog-two-sidebar.html#" class="readmore">June 16, 2014</a>
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div><!-- rating -->
        </li>
    </ul><!-- recent posts -->
</section>