<section class="download">
    <span><i class="glyphicon glyphicon-plus"></i></span>
    <h3>Payroll Calculators</h3>
    <p>Our payroll calculators demonstrates how much you can save under our European Payroll solutions. The calculation provides a projection of your weekly take home pay based on the information that you have provided regarding income...</p>
    <p><a href="<?php echo SITE_URL;?>company-information/payroll-calculators/" title="Payroll & Financial Calculators" class="btn btn-primary btn-small">Payroll Calculators</a></p>
    <div class="pdf"><img alt="Payroll & Financial Calculators" src="<?php echo SITE_URL;?>assets/calculator.png"></div>
</section>