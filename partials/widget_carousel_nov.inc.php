<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Untitled Document</title>
</head>

<body>

<section class="latest_news doc">
    <div class="caption">
        <h4 class="featured_article_title">Latest News from Euro Accountancy</h4>
    </div>

    <div class="row">
        <div class="blog-masonry clearfix">

            <div class="col-lg-6 first wow fadeInLeft" data-wow-duration="2s" data-wow-delay="5s">
                <div class="blog-carousel">
                    <div class="entry">
                        <img class="img-responsive" alt="" src="<?php echo SITE_URL;?>assets/it-professionals.jpg" alt="IT Professionals in Netherlands">
                        <div class="magnifier">
                            <div class="buttons">
                                <a href="https://blog.eafs.eu/it-professionals-in-netherlands/" title="Link to IT Professionals in Netherlands Blog" rel="bookmark" class="st"><i class="fa fa-link"></i></a>
                            </div><!-- end buttons -->
                        </div><!-- end magnifier -->
                        <div class="post-type">
                            <i class="fa fa-picture-o"></i>
                        </div><!-- end pull-right -->
                    </div><!-- end entry -->
                    <div class="blog-carousel-header">
                        <h3><a href="https://blog.eafs.eu/it-professionals-in-netherlands/" title="Link to IT Professionals in Netherlands Blog" title="">IT Professionals in Netherlands</a></h3>
                        <div class="blog-carousel-meta">
                            <span><i class="fa fa-calendar"></i> November 11, 2014</span>
                        </div><!-- end blog-carousel-meta -->
                    </div><!-- end blog-carousel-header -->
                    <div class="blog-carousel-desc">
                        <p>The number of IT professional vacancies within the Netherlands is steadily continuing to grow, according to industry analysts, continuing the demand for IT professionals in ...</p>
                    </div><!-- end blog-carousel-desc -->
                </div><!-- end blog-carousel -->
            </div><!-- end col-lg-4 -->

            <div class="col-lg-6 last wow fadeInRight" data-wow-duration="2s" data-wow-delay="5s">
                <div class="blog-carousel">
                    <div class="entry">
                        <img class="img-responsive" alt="" src="<?php echo SITE_URL;?>assets/mortgages-contractors.jpg" alt="Mortgages for Contractors">
                        <div class="magnifier">
                            <div class="buttons">
                                <a href="<?php echo SITE_URL;?>contractor-services/mortgages-for-contractors/" title="Link to Contractor Mortgages Page" rel="bookmark" class="st"><i class="fa fa-link"></i></a>
                            </div><!-- end buttons -->
                        </div><!-- end magnifier -->
                        <div class="post-type">
                            <i class="fa fa-picture-o"></i>
                        </div><!-- end pull-right -->
                    </div><!-- end entry -->
                    <div class="blog-carousel-header">
                        <h3><a href="<?php echo SITE_URL;?>contractor-services/mortgages-for-contractors/" title="Link to Contractor Mortgages Page" title="">Contractor Mortgages</a></h3>
                        <div class="blog-carousel-meta">
                            <span><i class="fa fa-calendar"></i> July 01, 2014</span>
                        </div><!-- end blog-carousel-meta -->
                    </div><!-- end blog-carousel-header -->
                    <div class="blog-carousel-desc">
                        <p>EAFS have partnered with Contractor Mortgages Made Easy (CMME) to offer contractors a competitive mortgage with none of the hassle that you may have experienced with other lenders. </p>
                    </div><!-- end blog-carousel-desc -->
                </div><!-- end blog-carousel -->
            </div><!-- end col-lg-4 -->

            <div class="col-lg-6 first wow fadeInLeft" data-wow-duration="2s" data-wow-delay="5s">
                <div class="blog-carousel">
                    <div class="entry">
                        <img class="img-responsive" alt="" src="<?php echo SITE_URL;?>assets/business-banking.jpg" alt="Business Banking">
                        <div class="magnifier">
                            <div class="buttons">
                                <a href="<?php echo SITE_URL;?>contractor-services/business-bank-account/" title="Link to Business Banking Page" rel="bookmark" class="st"><i class="fa fa-link"></i></a>
                            </div><!-- end buttons -->
                        </div><!-- end magnifier -->
                        <div class="post-type">
                            <i class="fa fa-picture-o"></i>
                        </div><!-- end pull-right -->
                    </div><!-- end entry -->
                    <div class="blog-carousel-header">
                        <h3><a href="<?php echo SITE_URL;?>contractor-services/business-bank-account/" title="Link to Business Banking Page" title="">Business Banking</a></h3>
                        <div class="blog-carousel-meta">
                            <span><i class="fa fa-calendar"></i> July 01, 2014</span>
                        </div><!-- end blog-carousel-meta -->
                    </div><!-- end blog-carousel-header -->
                    <div class="blog-carousel-desc">
                        <p>EAFS have been able to negotiate an extremely attractive banking package for contractors from cater Allen a prestigious private bank, part of the Abbey and Santander Banking group. </p>
                    </div><!-- end blog-carousel-desc -->
                </div><!-- end blog-carousel -->
            </div><!-- end col-lg-4 -->

            <div class="col-lg-6 last wow fadeInRight" data-wow-duration="2s" data-wow-delay="5s">
                <div class="blog-carousel">
                    <div class="entry">
                        <img class="img-responsive" alt="" src="<?php echo SITE_URL;?>assets/dutch-economy.jpg" alt="Expanding Dutch Economy">
                        <div class="magnifier">
                            <div class="buttons">
                                <a href="https://blog.eafs.eu/expanding-dutch-economy/" title="Link to Expanding Dutch Economy Blog" rel="bookmark" class="st"><i class="fa fa-link"></i></a>
                            </div><!-- end buttons -->
                        </div><!-- end magnifier -->
                        <div class="post-type">
                            <i class="fa fa-picture-o"></i>
                        </div><!-- end pull-right -->
                    </div><!-- end entry -->
                    <div class="blog-carousel-header">
                        <h3><a href="https://blog.eafs.eu/expanding-dutch-economy/" title="Link to Expanding Dutch Economy Blog" title="">Expanding Dutch Economy</a></h3>
                        <div class="blog-carousel-meta">
                            <span><i class="fa fa-calendar"></i> November 11, 2014</span>
                        </div><!-- end blog-carousel-meta -->
                    </div><!-- end blog-carousel-header -->
                    <div class="blog-carousel-desc">
                        <p>The GDP in the Netherlands expanded 0.50 percent in the second quarter of 2014, leaving the expanding Dutch economy as the fifth largest within the EU …</p>
                    </div><!-- end blog-carousel-desc -->
                </div><!-- end blog-carousel -->
            </div><!-- end col-lg-4 -->

        </div><!-- end blog-masonry -->
    </div><!-- end row -->

    <h4>Why Choose EAFS?</h4>
    <ul class="check">
        <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
        <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
        <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
    </ul>
    <div class="btn btn-primary btn-lg btn-block butlink hidden-xs" type="button">
        <a href="<?php echo SITE_URL;?>company-information/about-eafs/" target="_self" title="Contact Euro Accountancy &amp; Finance Services TODAY!!!" >Ready to Talk? Contact Euro Accountancy &amp; Finance Services TODAY!!! </a>
    </div>
    <div class="btn btn-primary btn-lg btn-block butlink visible-xs" type="button">
        <a href="<?php echo SITE_URL;?>company-information/about-eafs/" target="_self" title="Contact Euro Accountancy &amp; Finance Services TODAY!!!" >Contact EAFS TODAY!!! </a>
    </div>
</section><!-- end section latest news -->
</body>
</html>
