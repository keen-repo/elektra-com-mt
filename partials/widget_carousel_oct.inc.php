<section class="latest_news doc">
    <div class="caption">
    <h4 class="featured_article_title">Latest News from Euro Accountancy</h4>
</div>

<div class="row">
    <div class="blog-masonry clearfix">

        <div class="col-lg-6 first">
            <div class="blog-carousel">
                <div class="entry">
                    <img class="img-responsive" alt="" src="<?php echo SITE_URL;?>assets/recruitmentagencyexpo.gif" alt="Recruitment Agency Expo Birmingham 2014">
                    <div class="magnifier">
                        <div class="buttons">
                            <a href="<?php echo SITE_URL;?>landing/recruitment_expo/" title="Link to Recruitment Agency Expo Page" rel="bookmark" class="st"><i class="fa fa-link"></i></a>
                        </div><!-- end buttons -->
                    </div><!-- end magnifier -->
                    <div class="post-type">
                        <i class="fa fa-picture-o"></i>
                    </div><!-- end pull-right -->
                </div><!-- end entry -->
                <div class="blog-carousel-header">
                    <h3><a href="<?php echo SITE_URL;?>landing/recruitment_expo/" title="Link to Recruitment Agency Expo Page" title="">Recruitment Agency Expo</a></h3>
                    <div class="blog-carousel-meta">
                        <span><i class="fa fa-calendar"></i> Oct 01-02, 2014</span>
                    </div><!-- end blog-carousel-meta -->
                </div><!-- end blog-carousel-header -->
                <div class="blog-carousel-desc">
                    <p>We are exhibiting at the NEC on the 1st and 2nd of October for the Recruitment Agency Expo. We will be there from 09.30 until 17.30 both days. Come visit us on Stand E30! </p>
                </div><!-- end blog-carousel-desc -->
            </div><!-- end blog-carousel -->
        </div><!-- end col-lg-4 -->


        <div class="col-lg-6 last">
            <div class="blog-carousel">
                <div class="entry">
                    <img class="img-responsive" alt="" src="<?php echo SITE_URL;?>assets/mortgages-contractors.jpg" alt="Mortgages for Contractors">
                    <div class="magnifier">
                        <div class="buttons">
                            <a href="<?php echo SITE_URL;?>contractor-services/mortgages-for-contractors/" title="Link to Contractor Mortgages Page" rel="bookmark" class="st"><i class="fa fa-link"></i></a>
                        </div><!-- end buttons -->
                    </div><!-- end magnifier -->
                    <div class="post-type">
                        <i class="fa fa-picture-o"></i>
                    </div><!-- end pull-right -->
                </div><!-- end entry -->
                <div class="blog-carousel-header">
                    <h3><a href="<?php echo SITE_URL;?>contractor-services/mortgages-for-contractors/" title="Link to Contractor Mortgages Page" title="">Contractor Mortgages</a></h3>
                    <div class="blog-carousel-meta">
                        <span><i class="fa fa-calendar"></i> July 01, 2014</span>
                    </div><!-- end blog-carousel-meta -->
                </div><!-- end blog-carousel-header -->
                <div class="blog-carousel-desc">
                    <p>EAFS have partnered with Contractor Mortgages Made Easy (CMME) to offer contractors a competitive mortgage with none of the hassle that you may have experienced with other lenders. </p>
                </div><!-- end blog-carousel-desc -->
            </div><!-- end blog-carousel -->
        </div><!-- end col-lg-4 -->


        <div class="col-lg-6 first">
            <div class="blog-carousel">
                <div class="entry">
                    <img class="img-responsive" alt="" src="<?php echo SITE_URL;?>assets/business-banking.jpg" alt="Business Banking">
                    <div class="magnifier">
                        <div class="buttons">
                            <a href="<?php echo SITE_URL;?>contractor-services/business-bank-account/" title="Link to Business Banking Page" rel="bookmark" class="st"><i class="fa fa-link"></i></a>
                        </div><!-- end buttons -->
                    </div><!-- end magnifier -->
                    <div class="post-type">
                        <i class="fa fa-picture-o"></i>
                    </div><!-- end pull-right -->
                </div><!-- end entry -->
                <div class="blog-carousel-header">
                    <h3><a href="<?php echo SITE_URL;?>contractor-services/business-bank-account/" title="Link to Business Banking Page" title="">Business Banking</a></h3>
                    <div class="blog-carousel-meta">
                        <span><i class="fa fa-calendar"></i> July 01, 2014</span>
                    </div><!-- end blog-carousel-meta -->
                </div><!-- end blog-carousel-header -->
                <div class="blog-carousel-desc">
                    <p>EAFS have been able to negotiate an extremely attractive banking package for contractors from cater Allen a prestigious private bank, part of the Abbey and Santander Banking group. </p>
                </div><!-- end blog-carousel-desc -->
            </div><!-- end blog-carousel -->
        </div><!-- end col-lg-4 -->



        <div class="col-lg-6 last">
            <div class="blog-carousel">
                <div class="entry">
                    <div class="quote-post">
                        <blockquote>
                            <p class="lead">Figures from Her Majesties Revenue and Customs have shed light on claims that IR35 is wasting taxpayer’s money.</p>
                            <small>
                                Marketing Team
                                <cite title="Source Title">EAFS</cite>
                            </small>
                        </blockquote>
                    </div><!-- end quote-post -->
                    <div class="post-type">
                        <i class="fa fa-quote-right"></i>
                    </div><!-- end pull-right -->
                </div>
                <div class="blog-carousel-header">
                    <h3><a href="https://blog.eafs.eu/2014/07/hmrc-figures-confirm-ir35-wasting-taxpayers-money/" title="HMRC Figures Confirm IR35 is Wasting Taxpayers Money.">IR35 is Wasting Taxpayers Money.</a></h3>
                    <div class="blog-carousel-meta">
                        <span><i class="fa fa-calendar"></i> July 01, 2014</span>
                    </div><!-- end blog-carousel-meta -->
                </div><!-- end blog-carousel-header -->
                <div class="blog-carousel-desc">
                    <p>IR35 data obtained by industry professionals, covering the first tax year, is the first gathered since the introduction of HMRC’s new framework for administration.  </p>
                </div><!-- end blog-carousel-desc -->
            </div><!-- end blog-carousel -->
        </div><!-- end col-lg-4 -->


    </div><!-- end blog-masonry -->
</div><!-- end row -->

<h4>Why Choose EAFS?</h4>
<ul class="check">
    <li><strong>We are Payroll Experts:</strong> We have major experience in Europe and hold all necessary licences for distributing payroll across the EU. </li>
    <li><strong>Value for Money:</strong> Our fees are highly competitive and very reasonable, especially compared to other firms. </li>
    <li><strong>We are on top of the Latest EU Developments:</strong> The legislation for the European Union is constantly changing, we stay up to date with all developments to provide the most legally compliant service at a competitive price. </li>
</ul>
    <div class="btn btn-primary btn-lg btn-block butlink hidden-xs" type="button">
        <a href="<?php echo SITE_URL;?>company-information/about-eafs/" target="_self" title="Contact Euro Accountancy &amp; Finance Services TODAY!!!" >Ready to Talk? Contact Euro Accountancy &amp; Finance Services TODAY!!! </a>
    </div>
    <div class="btn btn-primary btn-lg btn-block butlink visible-xs" type="button">
        <a href="<?php echo SITE_URL;?>company-information/about-eafs/" target="_self" title="Contact Euro Accountancy &amp; Finance Services TODAY!!!" >Contact EAFS TODAY!!! </a>
    </div>
</section><!-- end section latest news -->