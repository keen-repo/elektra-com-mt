<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Company Formations</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>company-formations/limited-company/"><div itemprop="name">Limited Company</div></a></li>
    </ul>
</section>