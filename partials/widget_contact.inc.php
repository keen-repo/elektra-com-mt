<section class="widget_contact">
    <div class="caption" >
        <h3 class="featured_article_title">Contact Info</h3>
    </div>
    <div class="space margin-b20"></div>
        <div class="addr"><i class="fa fa-home"></i> &nbsp;&nbsp;23 Blair Street</div>
        <div class="addr"><i class="fa fa-phone"></i> &nbsp;&nbsp;+44 131 526 3300</div>
        <div class="addr"><i class="fa fa-phone"></i> &nbsp;&nbsp;+31 20 808 45 44</div>
</section>