<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Contractor Accountants</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/benefits-limited-company/" title="Link to Benefits Limited Company Page"><div itemprop="name">Benefits Limited Company</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/business-expenses/" title="Link to Business Expenses Page"><div itemprop="name">Business Expenses</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/changing-accountants/" title="Link to Change Accountants Page"><div itemprop="name">Change Accountants</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/corporation-tax/" title="Link to Corporation Tax Page"><div itemprop="name">Corporation Tax</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/ir35-explained/" title="Link to IR35 Explained Page"><div itemprop="name">IR35 Explained</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/limited-company-services/" title="Link to Limited Company Services Page"><div itemprop="name">Limited Company Services</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/pay-as-you-earn/" title="Link to PAYE Page"><div itemprop="name">PAYE</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/s660-settlements/" title="Link to s660 Settlements Page"><div itemprop="name">s660 Settlements</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/value-added-tax/" title="Link to VAT Page"><div itemprop="name">VAT</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/why-choose-eafs/" title="Link to Why choose EAFS? Page"><div itemprop="name">Why choose EAFS?</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-accountants/agency-workers-regulations/" title="Link to Agency Workers Regulations Page"><div itemprop="name">Agency Workers Regulations</div></a></li>
    </ul>
</section>