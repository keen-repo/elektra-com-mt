<section class="countries">
    <div class="general-title">
        <h2>Working in Europe?</h2>
        <hr>
        <p class="lead">We provide the best solutions for contractors in Europe.</p>
    </div><!-- end general title -->

    <div class="col-lg-4 first">
        <div class="service-with-image">
            <div class="entry">
                <img src="<?php echo SITE_URL;?>assets/country_belgium.png" class="img-responsive center-block" alt="Working in Belgium">
                <span class="service-title">
                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/"><i class="fa fa-user"></i>Belgium</a>
                </span>
            </div><!-- end entry -->
            <div class="service-desc">
                <p>In this Belgian section you can find everything that you need to know about working as a contractor or placing an employee within Belgium. Build your expertise with us.</p>
                <a class="readmore" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/">Read More...</a>
            </div><!-- end service-desc -->
        </div><!-- end service-with-image -->
    </div><!-- end col-lg-4 -->
    <div class="col-lg-4">
        <div class="service-with-image">
            <div class="entry">
                <img src="<?php echo SITE_URL;?>assets/country_germany.png" class="img-responsive center-block" alt="Working in Germany">
                <span class="service-title">
                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/"><i class="fa fa-user"></i>Germany</a>
                </span>
            </div><!-- end entry -->
            <div class="service-desc">
                <p>With EAFS you will find all the information you require to compliantly place an employee or work in Germany. Accumulate your knowledge with Euro Accountancy & Finance Services. </p>
                <a class="readmore" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/">Read More...</a>
            </div><!-- end service-desc -->
        </div><!-- end service-with-image -->
    </div><!-- end col-lg-4 -->
    <div class="col-lg-4 last">
        <div class="service-with-image">
            <div class="entry">
                <img src="<?php echo SITE_URL;?>assets/country_holland.png" class="img-responsive center-block" alt="Working in Holland">
                <span class="service-title">
                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/"><i class="fa fa-user"></i>Holland</a>
                </span>
            </div><!-- end entry -->
            <div class="service-desc">
                <p>Within our Netherlands section you will find all the information required to understand the regulations and rules for placing Contractors and working within Dutch borders. </p>
                <a class="readmore" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/">Read More...</a>
            </div><!-- end service-desc -->
        </div><!-- end service-with-image -->
    </div><!-- end col-lg-4 -->
    <div class="col-lg-4 first">
        <div class="service-with-image">
            <div class="entry">
                <img src="<?php echo SITE_URL;?>assets/country_italy.png" class="img-responsive center-block" alt="Working in Italy">
                <span class="service-title">
                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/"><i class="fa fa-user"></i>Italy</a>
                </span>
            </div><!-- end entry -->
            <div class="service-desc">
                <p>Our Italian section will provide you with all the required information on legislation to place contractors and work compliantly within Italy. Let EAFS help to build your knowledge and expertise. </p>
                <a class="readmore" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/">Read More...</a>
            </div><!-- end service-desc -->
        </div><!-- end service-with-image -->
    </div><!-- end col-lg-4 -->
    <div class="col-lg-4">
        <div class="service-with-image">
            <div class="entry">
                <img src="<?php echo SITE_URL;?>assets/country_luxembourg.png" class="img-responsive center-block" alt="Working in Luxembourg">
                <span class="service-title">
                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/"><i class="fa fa-user"></i>Luxembourg</a>
                </span>
            </div><!-- end entry -->
            <div class="service-desc">
                <p>This section on Luxembourg contains the required information on working or place contractors within Luxembourg’s borders. Build your knowledge and expertise with EAFS.</p>
                <a class="readmore" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/">Read More...</a>
            </div><!-- end service-desc -->
        </div><!-- end service-with-image -->
    </div><!-- end col-lg-4 -->
    <div class="col-lg-4 last">
        <div class="service-with-image">
            <div class="entry">
                <img src="<?php echo SITE_URL;?>assets/country_sweden.png" class="img-responsive center-block" alt="Working in Sweden">
                <span class="service-title">
                    <a href="<?php echo SITE_URL;?>contracting-europe/working-in-sweden/"><i class="fa fa-user"></i>Sweden</a>
                </span>
            </div><!-- end entry -->
            <div class="service-desc">
                <p>In this section you can find everything that you need to know about working as a contractor or placing an employee within Sweden. Build your expertise here.</p>
                <a class="readmore" href="<?php echo SITE_URL;?>contracting-europe/working-in-sweden/">Read More...</a>
            </div><!-- end service-desc -->
        </div><!-- end service-with-image -->
    </div><!-- end col-lg-4 -->
</section>