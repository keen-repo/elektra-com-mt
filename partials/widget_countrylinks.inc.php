<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Working in Europe</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-belgium/" title="Link to Working in Belgium Page"><div itemprop="name">Working in Belgium</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/" title="Link to Working in Germany Page"><div itemprop="name">Working in Germany</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/" title="Link to Working in Holland Page"><div itemprop="name">Working in Holland</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/" title="Link to Working in Italy Page"><div itemprop="name">Working in Italy</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/" title="Link to Working in Luxembourg Page"><div itemprop="name">Working in Luxembourg</div></a></li>
    </ul>
</section>