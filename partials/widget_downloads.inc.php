<section class="download">
    <span><i class="glyphicon glyphicon-pencil"></i></span>
    <h3>Guides & Form</h3>
    <p>Visit our download area for all downloads including country guides and application forms available throughout our website...</p>
    <p><a href="<?php echo SITE_URL;?>company-information/forms-downloads/" title="Link to Contractor Guides Page" class="btn btn-primary btn-small">Contractor Guides</a></p>
    <div class="pdf"><img alt="Contractor Guides & Application Forms" src="<?php echo SITE_URL;?>assets/adobe.png"></div>
</section>