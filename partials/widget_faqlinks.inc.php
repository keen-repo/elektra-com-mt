<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Contractor FAQ's & Tools</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>faq-tools/agency-support/" title="Link to Agency Support Page"><div itemprop="name">Agency Support</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>faq-tools/contracting-europe-faq/" title="Link to Contracting Europe FAQ Page"><div itemprop="name">Contracting Europe FAQ</div></a></li>
    </ul>
</section>