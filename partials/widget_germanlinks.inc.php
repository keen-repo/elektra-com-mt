<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Working in Germany</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-income-tax/" title="Link to German Income Tax Page"><div itemprop="name">German Income Tax</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-social-security/" title="Link to German Social Security Page"><div itemprop="name">German Social Security</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-visas/" title="Link to German Visas Page"><div itemprop="name">German Visas</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-gmbh/" title="Link to German GmbH Page"><div itemprop="name">German GmbH</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-germany/german-payroll/" title="Link to German Payroll Page"><div itemprop="name">German Payroll</div></a></li>
    </ul>
</section>