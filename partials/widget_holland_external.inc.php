<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Related Articles</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" target="_blank" href="https://eafspayroll.hubpages.com/hub/dutch-payroll" title="Link to Dutch Payroll Article on Hubpages"><div itemprop="name">Dutch Payroll Hubpages</div></a></li>
        <li><a itemprop="url" target="_blank" href="https://goarticles.com/article/Understanding-Dutch-Payroll/9288511/" title="Link to Dutch Payroll Article on GoArticles"><div itemprop="name">Dutch Payroll GoArticles</div></a></li>
    </ul>
</section>