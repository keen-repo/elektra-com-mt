<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Working in Netherlands</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-income-tax/" title="Link to Dutch Income Tax Page"><div itemprop="name">Dutch Income Tax</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-social-security/" title="Link to Dutch Social Security Page"><div itemprop="name">Dutch Social Security</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-visas/" title="Link to Dutch Working Visas Page"><div itemprop="name">Dutch Working Visas</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-bv/" title="Link to Dutch Umbrella Page"><div itemprop="name">Dutch Umbrella</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/dutch-payroll/" title="Link to Dutch Payroll Services Page"><div itemprop="name">Dutch Payroll Services</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-netherlands/30-ruling/" title="Link to The 30% Ruling Page"><div itemprop="name">The 30% Ruling</div></a></li>
    </ul>
</section>