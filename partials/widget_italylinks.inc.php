<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Working in Italy</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-income-tax/" title="Link to Italian Income Tax Page"><div itemprop="name">Italian Income Tax</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-social-security/" title="Link to Italian Social Security Page"><div itemprop="name">Italian Social Security</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-visas/" title="Link to Italian Visas Page"><div itemprop="name">Italian Visas</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-umbrella/" title="Link to Italian Umbrella Page"><div itemprop="name">Italian Umbrella</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-italy/italian-payroll/" title="Link to Italian Payroll Page"><div itemprop="name">Italian Payroll</div></a></li>
    </ul>
</section>