<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Working in Luxembourg</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-income-tax/" title="Link to Luxembourg Income Tax Page"><div itemprop="name">Luxembourg Income Tax</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-social-security/" title="Link to Luxembourg Social Security Page"><div itemprop="name">Luxembourg Social Security</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-visas/" title="Link to Luxembourg Visas Page"><div itemprop="name">Luxembourg Visas</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-umbrella/" title="Link to Luxembourg Umbrella Page"><div itemprop="name">Luxembourg Umbrella</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contracting-europe/working-in-luxembourg/luxembourg-payroll/" title="Link to Luxembourg Payroll Page"><div itemprop="name">Luxembourg Payroll</div></a></li>
    </ul>
</section>