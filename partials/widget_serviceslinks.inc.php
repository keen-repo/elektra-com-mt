<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Contractor Services</h3>
    </div>
    <div class="space margin-b20"></div>
    <ul class="nav nav-tabs nav-stacked" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-services/business-bank-account/" title="Link to Business Bank Account Page"><div itemprop="name">Business Bank Account</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-services/insurance-for-contractors/" title="Link to Insurance for Contractors Page"><div itemprop="name">Insurance for Contractors</div></a></li>
        <li><a itemprop="url" href="<?php echo SITE_URL;?>contractor-services/mortgages-for-contractors/" title="Link to Mortgages for Contractors Page"><div itemprop="name">Mortgages for Contractors</div></a></li>
    </ul>
</section>