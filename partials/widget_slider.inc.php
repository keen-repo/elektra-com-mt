<div class="tp-banner-container">
    <div class="tp-banner" >
        <ul>
            <!-- SLIDE  -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" data-delay="5000">
                <!-- MAIN IMAGE -->
                <img src="<?php echo SITE_URL;?>assets/video_bg.jpg"  alt="Euro Accountancy & Finance Services" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-fade fadeout fullscreenvideo"
                     data-x="0"
                     data-y="0"
                     data-speed="1000"
                     data-start="800"
                     data-easing="Power4.easeOut"
                     data-endspeed="1500"
                     data-endeasing="Power4.easeIn"
                     data-autoplay="true"
                     data-autoplayonlyfirsttime="false"
                     data-nextslideatend="true"
                     data-forceCover="1" data-aspectratio="16:9" data-forcerewind="on"
                     style="z-index: 2">
                    <video id="revvideo" class="video-js vjs-default-skin" preload="none" poster='<?php echo SITE_URL;?>assets/video_bg.jpg' data-setup="{}">
                        <source src="<?php echo SITE_URL;?>video/business_edit.webm" type="video/webm">
                        <source src="<?php echo SITE_URL;?>video/business_edit.mp4" type="video/mp4">
                        <source src="<?php echo SITE_URL;?>video/business_edit.ogv" type="video/ogg">
                    </video>

                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption big_title_onepage skewfromleft customout"
                     data-x="center" data-hoffset="0"
                     data-y="top" data-voffset="130"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1300"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 6"><p>Fully Compliant Payroll Solutions <span>for Contractors</span></p>
                </div>
                <!-- LAYER NR. 7 -->
                <div class="tp-caption small_thin_grey1 customin customout"
                     data-x="245"
                     data-y="320"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="2400"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8"><span><i class="fa fa-bullseye"></i></span> BELGIUM
                </div>
                <!-- LAYER NR. 7 -->
                <div class="tp-caption small_thin_grey1 customin customout"
                     data-x="415"
                     data-y="320"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="2600"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8"><span><i class="fa fa-bullseye"></i></span> GERMANY
                </div>
                <!-- LAYER NR. 7 -->
                <div class="tp-caption small_thin_grey1 customin customout"
                     data-x="570"
                     data-y="320"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="2800"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8"><span><i class="fa fa-bullseye"></i></span> ITALY
                </div>
                <!-- LAYER NR. 7 -->
                <div class="tp-caption small_thin_grey1 customin customout"
                     data-x="730"
                     data-y="320"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="3000"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8"><span><i class="fa fa-bullseye"></i></span> NETHERLANDS
                </div>
                <!-- LAYER NR. 7 -->
                <div class="tp-caption small_thin_grey1 customin customout"
                     data-x="920"
                     data-y="320"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="3200"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8"><span><i class="fa fa-bullseye"></i></span> UK
                </div>
            </li>
        </ul>
        <div class="tp-bannertimer"></div>
    </div>
</div>