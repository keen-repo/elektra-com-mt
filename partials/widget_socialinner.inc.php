<section class="widget_contact">
    <div class="caption">
        <h3 class="featured_article_title">Keep in Touch</h3>
    </div>

    <div class="social_widget">
        <div class="social_like clearfix">
        <div class="icon-container pull-left">
            <i class="fa fa-twitter"></i>
        </div>
        <div class="social_count">
            <h4><a href="<?php echo SITE_URL;?>social/networks/twitter/" title="Link to our Twitter Page" target="_blank">Follow Us on Twitter</a></h4>
            <small>37 Followers</small>
        </div>
    </div><!-- end social like -->
        <div class="social_like clearfix">
        <div class="icon-container pull-left">
            <i class="fa fa-facebook"></i>
        </div>
        <div class="social_count">
            <h4><a href="<?php echo SITE_URL;?>social/networks/facebook/" title="Link to our Facebook Page" target="_blank">Like on Facebook</a></h4>
            <small>221 Fans</small>
        </div>
    </div><!-- end social like -->
        <div class="social_like clearfix">
        <div class="icon-container pull-left">
            <i class="fa fa-google-plus"></i>
        </div>
        <div class="social_count">
            <h4><a href="<?php echo SITE_URL;?>social/networks/googleplus/" title="Link to our Google Plus Page" target="_blank">Follow Us on Google+</a></h4>
            <small>6169 Subscribers</small>
        </div>
    </div><!-- end social like -->
        <div class="social_like clearfix">
        <div class="icon-container pull-left">
            <i class="fa fa-linkedin"></i>
        </div>
        <div class="social_count">
            <h4><a href="<?php echo SITE_URL;?>social/networks/linkedin/" title="Link to our Linkedin Page" target="_blank">Like on Linkedin</a></h4>
            <small>887 Followers</small>
        </div>
    </div><!-- end social like -->
</div>
</section>