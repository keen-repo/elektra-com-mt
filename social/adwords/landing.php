<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Euro Accountancy &amp; Finance Services</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="js/validator.js" type="text/javascript"></script>



</head>

<body>





<div class="holder">
  <div class="bghead1">
  <div class="best">
    <h2>The UK's  <span class="special">PREMIERE</span> Accountants for  <a href="http://www.eafs.eu/contractor-accountants" title="Contractor Accountants" target="_blank">Contractors &amp; Freelancers</a></h2>
  </div>
</div>
  <div class="bghead"><a href="http://www.eafs.eu" target="_self"><img src="images/bghead.png" alt="EAFS" width="960" height="100" /></a></div>
  <div class="holderleft">
    <h2>Get <span class="special">a FREE</span> initial consultancy when you enquire about our services online <span class="special">TODAY!</span></h2>
        <h4>EAFS provide tailored limited company accounting services specifically for contractors allowing you to concentrate on your area of expertise, whilst we take care of all accountancy compliance, tax compliance and planning.</h4>
    <h4>EAFS are proud to have received PCG Accreditation as specialist Contractor Accountants and for our expertise in contract review for IR35.</h4>
    <h2>£99 (+VAT)<br />
    The All-inclusive EAFS<br />
Limited Company Service</h2>
        <p>WHY CHOOSE EAFS?</p>
        <ul>
          <li>No charge when not in contract</li>
          <li>No set-up or joining fee</li>
          <li>Free Business Banking with Cater Allen private Bank</li>
          <li>Free IR35 Contract Review</li>
          <li>Competitive Contractor Insurance through Kingsbridge  </li>
        </ul>
        <p>ONE SET FEE, NO HIDDEN CHARGES.<br />
        FULLY COMPLIANT SOLUTIONS. </p>
  </div>
  <div class="holderright">
    <div class="formholder">
      <div class="formheading">
        <h3>Contact EAFS Today!!</h3>
      </div>
      <div class="formcontent">
       <form action="handler.php" name="contactform" id="contactform" method="post">
                    
                        <ul>				
                            
                            <li class="firstname">
                                <label>FIRST NAME: <span class="required">required</span></label>
                                <input type="text" name="firstname" id="firstname" value="" class="long" />
                            </li>
                                                
                            <li class="lastname">
                                <label>LAST NAME: <span class="required">required</span></label>					
                                <input type="text" name="lastname" id="lastname" value="" class="long" />
                            </li>
                                            
                            <li class="email">
                                <label>EMAIL: <span class="required">required</span></label>					
                                <input type="text" name="email" id="email" value="" class="long" />
                            </li>
                            
                            <li class="contact">
                                <label>CONTACT N0: <span class="required">required</span></label>
                                <input type="text" name="phone" id="phone" value="" class="long" />
                            </li>
                            <li class="enquiry">
                                <label>YOUR ENQUIRY:</label>
                                <textarea name="message" cols="60" rows="3" class="long" id="message"></textarea>
                            </li>                            
                        
                             <li class="button">
                             
								<input id="submitted" class="input-submit" type="submit" value="" name="submit" />                            </li>
                        </ul>
                        
        </form>
        
      
      </div>
    </div>
        <div class="formholder1">
          <h2><span class="special">visit:</span> <a href="http://www.eafs.eu" title="Euro Accountancy &amp; Finance Services" target="_self">www.eafs.eu</a> <span class="special">today </span></h2>
          <h2>SPECIAL OFFER ENDING IN JULY 2012            </h2>
          <h3 class="orange">* Free Company Formation * 20% discount on our fees for one year * 15% discount on PCG membership </h3>
          <p>ONE SET FEE, NO HIDDEN CHARGES. FULLY COMPLIANT SOLUTIONS. </p>
        </div>

  </div>
  <div class="float"></div>
  <div class="bgbottom"></div>
  <div class="bghead1">
  <div class="best">
    <h2><span class="special">Visit:</span> Euro Accountancy &amp; Finance Services<span class="special"></span> Today<br />
      <a href="http://www.eafs.eu" title="Euro Accountancy &amp; Finance Services" target="_self">www.eafs.eu</a> <span class="special">or call</span> +44 (0)131 626 3300
    </h2>
   <h2>The No1 Supplier of Accountancy Services for <br />
          <a href="http://www.eafs.eu/contractor-accountants" title="Contractor Accountants" target="_self">Contractors &amp; Freelancers</a></h2>
  </div>
</div>
</div>
<script language="JavaScript">
var frmvalidator  = new Validator("contactform");
frmvalidator.addValidation("firstname","req","Please provide your firstname"); 
frmvalidator.addValidation("lastname","req","Please provide your lastname"); 
frmvalidator.addValidation("phone","req","Please provide your phone number"); 
frmvalidator.addValidation("email","req","Please provide your email"); 
frmvalidator.addValidation("email","email","Please enter a valid email address"); 
</script>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-24524584-1']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<script type="text/javascript"> if (!window.mstag) mstag = {loadTag : function(){},time : (new Date()).getTime()};</script> <script id="mstag_tops" type="text/javascript" src="//flex.atdmt.com/mstag/site/f73f0a77-2d98-4d73-a0e9-4984842ac647/mstag.js"></script> <script type="text/javascript"> mstag.loadTag("analytics", {dedup:"1",domainId:"1709870",type:"1",actionid:"62939"})</script> <noscript> <iframe src="//flex.atdmt.com/mstag/tag/f73f0a77-2d98-4d73-a0e9-4984842ac647/analytics.html?dedup=1&domainId=1709870&type=1&actionid=62939" frameborder="0" scrolling="no" width="1" height="1" style="visibility:hidden;display:none"> </iframe> </noscript>
</body>
</html>
