<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <title>EAFS Consulting : European Payroll : Contractor Accountants</title>
    <link href="https://plus.google.com/112046644106149130619/posts" rel="author">
    <link href="https://plus.google.com/112046644106149130619" rel="publisher">
    <meta content="Euro Accountancy & Finance Services" itemprop="name">
    <meta content="https://plus.google.com/112046644106149130619/posts" itemprop="author">
    <meta content="https://plus.google.com/112046644106149130619" itemprop="creator">
    <meta content="Contractor Accountants" itemprop="provider">
    <meta content="Contractor Accountants 2014" itemprop="copyright">
    <meta content="General" itemprop="contentRating">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." itemprop="description">
    <meta content="http://www.eafs.eu/social/src/logo250x250.png" itemprop="image">
    <meta content="summary_large_image" name="twitter:card">
    <meta content="@eafseu" name="twitter:site">
    <meta content="Contractor Accountants : European Payroll : Contractor Accountants" name="twitter:title">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." name="twitter:description">
    <meta content="@eafseu" name="twitter:creator">
    <meta content="http://www.eafs.eu/social/src/logo280x150.png" name="twitter:image:src">
    <meta content="http://www.eafs.eu/" name="twitter:domain">
    <meta content="en_GB" property="og:locale">
    <meta content="Contractor Accountants : European Payroll : Contractor Accountants" property="og:title">
    <meta content="article" property="og:type">
    <meta content="http://www.eafs.eu/" property="og:url">
    <meta content="54.3934" property="og:latitude">
    <meta content="-3.95508" property="og:longitude">
    <meta content="23 Blair Street" property="og:street-address">
    <meta content="Edinburgh" property="og:locality">
    <meta content="EH11QR" property="og:postal-code">
    <meta content="GB" property="og:country-name">
    <meta content="+44 (0)131 526 3300" property="og:phone_number">
    <meta content="+44 (0)131 526 3300" property="og:fax_number">
    <meta content="http://www.eafs.eu/social/src/logo250x250.png" property="og:image">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." property="og:description">
    <meta content="Contractor Accountants" property="og:site_name">
    <meta content="Index,follow" name="robots">
    <meta content="7 days" name="revisit-after">
    <meta content="Accountancy Services, European Payroll, Contractor Accountants" name="keywords">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." name="description">
    <!-- Bootstrap -->
    <link href="http://www.eafs.eu/social/css/style.min.css" rel="stylesheet">
    <script src="http://www.eafs.eu//social/js/vendor/modenizr/modernizr-2.8.3.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body itemscope itemtype="http://schema.org/WebPage">
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-4">
      <!-- START HEADER-->
      <header class="site-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
        <h1 class="site-title" itemprop="headline"><span class="feature-icon"><img src="http://www.eafs.eu/social/assets/646x220.png" alt="EAFS Consulting BV" class="img-responsive" title="EAFS Consulting BV"></span></h1>
      </header>
      <!-- END HEADER ELEMENT-->
    </div>
    <div class="col-xs-12 col-sm-8"></div>
  </div>
</div>
<!-- START BORDER ELEMENTS-->
<section class="statistics yellow">
  <div class="container">
    <div class="row center">
      <div class="col-xs-12 col-md-12">
        <h2 class="white">Workforce Management & Payroll Solutions Throughout Europe!!</h2>
        <div class="center-line"></div>
        <p class="padd-both white">BELGIUM <span class="yellowdot">+</span> GERMANY <span class="yellowdot">+</span> ITALY <span class="yellowdot">+</span> LUXEMBOURG <span class="yellowdot">+</span> MALTA <span class="yellowdot">+</span> NETHERLANDS <span class="yellowdot">+</span> UK</p>
      </div>
    </div>
  </div>
</section>
<!-- END BORDER ELEMENTS-->


<!-- START BODY-->
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-8">
			<!-- START MAIN CONTENT ELEMENTS-->
            <main class="site-content" role="main" itemprop="mainContentOfPage">
            <article itemscope itemtype="http://schema.org/Article" class="doc">
                <hgroup class="header">
                    <h1 class="site-headline" itemprop="headline">Introducing EAFS ELS</h1>
                </hgroup>
                <section itemprop="articleSection">
                    <div itemprop="text">
                        <p class="lead">EAFS are proud to announce the immediate availability of our new product ELS. ELS allows contractors to retain up to 72% of take home pay compliantly, in most european jurisdictions. - </p>
                        <p>By using ELS you can be safe in the knowledge that no offshore mechanism is employed and that we are 100% EU/UK compliant. Our clear and transparent solution is hassle free with no need for complex company setup and is fully managed on your behalf by our dedicated specialist accountancy team !!! #els </p>
                    </div>
                </section>
                <section itemprop="articleSection">
                    <div itemprop="text">
                      <ul class="fa-ul">
                        <li class="yellowtxt"><i class="fa-li fa fa-check"></i>up to 72% Take Home Pay</li>
                        <li class="yellowtxt"><i class="fa-li fa fa-check"></i>No Offshore Mechanism</li>
                        <li class="yellowtxt"><i class="fa-li fa fa-check"></i>100% EU/UK Compliant</li>
                        <li class="yellowtxt"><i class="fa-li fa fa-check"></i>Dedicated Accountancy Team</li>
                      </ul>
                    </div>
                </section>
                <section itemprop="articleSection">
                    <h2 class="site-section" itemprop="headline">Contact EAFS Today</h2>
                    <div itemprop="text">
                        <p>With changes in EU payroll and tax legislation, EAFS have been working hard to to find a more competitive way for contractors to work in european countries COMPLIANTLY without having to pay unnecessary high tax to foreign governments. Our solution allows contractors to take advantage of the EU taxation agreements and directives making us the only partner in Europe able to offer such a high yield take home pay.  To find out more please fill in our contact us page or call us directly and speak to one of our European Business Managers on 0131 526 3300. </p>
                    </div>
                </section>
            </article>
            </main>
            <!-- END MAIN CONTENT ELEMENTS-->
		</div>
		<div class="col-xs-12 col-sm-4">
            <!-- START SIDEBAR + WIDGET START -->
            <aside class="site-sidebar sidebar-primary widget-area" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">
                <div class="form-bg">
                <h3>Contact EAFS Today!!</h3>
                  <!-- Form -->
                    <form action="http://www.eafs.eu/social/forms/processor.php" method="POST" name="frmContact" id="frmContact">
                        <input type=hidden name="oid" value="00D20000000CdoA">
                        <input type=hidden name="lead_source" value="Mautic">
                        <input type=hidden name="retURL" value="http://www.eafs.eu/social/campaigns/gatc001/campaign_thankyou.php">
                    <div class="form-group">
                      <input type="text" placeholder="First Name" class="form-control f-input input-field" id="first_name" name="first_name">
                    </div>
                    <div class="form-group">
                      <input type="text" placeholder="Last Name" class="form-control f-input input-field" id="last_name" name="last_name">
                    </div>
                    <div class="form-group">
                      <input type="text" placeholder="Email" class="form-control f-input input-field" name="email">
                    </div>
                    <div class="form-group">
                      <input type="text" placeholder="Phone" class="form-control f-input input-field" maxlength="15" name="phone">
                    </div>

                    <div class="form-group">
                        <select class="form-control f-input input-field" name="00Nw0000003jevA" id="00Nw0000003jevA" title="Select Country">
                            <option value="--Make your Selection--" selected>--Make your Selection--</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Germany">Germany</option>
                            <option value="Italy">Italy</option>
                            <option value="Luxembourg">Luxembourg</option>
                            <option value="Netherlands">Netherlands</option>
                            <option value="Sweden">Sweden</option>
                            <option value="UK">UK</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <select  name="00Nw0000003joCt" id="00Nw0000003joCt" title="Enquiry Type" class="form-control f-input input-field">
                            <option selected>--Please choose a country first--</option>
                        </select>
                    </div>
                    
                    <button id="submit_btn" class="submit-btn" type="submit">Submit</button>
                    </form>
                </div>
            </aside><!-- .site-sidebar -->
            <!-- END SIDEBAR + WIDGET START -->
		</div>
	</div>
</div>
<!-- END BODY LAYOUT-->

<!-- START SUBSCRIBE ELEMENTS-->
<section class="statistics">
  <div class="container">
    <div class="row center"> 
    	<div class="col-xs-12 col-md-12">
			    <h2 class="white">No 1 European Payroll Company !!</h2>
			    <div class="center-line"></div>
			    <p class="padd-both white">Already working in the EU and want to move over the the <strong>No 1 European Payroll Company</strong>? We are ready to listen!</p>
		</div>
  </div>
</section>
<!-- END SUBSCRIBE ELEMENTS-->

<!-- START SUGGESTIONS ELEMENTS-->
<section class="suggestions gray">
  <div class="container">
    <div class="center">
      <h2>FREE Company Health Check</h2>
      <div class="center-line"></div>
      <p>For the month of September EAFS are offering a free tax and payroll health check to understand how we can efficiently move you to one of our platinum payroll packages with no setup fee and if your convinced your first month is on us. PLUS FREE PRIORITY PASS for usage at all worldwide airports (Terms and Conditions apply) </p>
    </div>
    <div class="row">

    </div>
  </div>
</section>
<!-- END SUGGESTIONS ELEMENTS-->

<!-- START SUBSCRIBE ELEMENTS-->
<section class="statistics">
  <div class="container">
    <div class="row center">
      <div class="col-xs-12 col-md-12">
        <h2 class="white">Fully Compliant Payroll Solutions throughout Europe!!</h2>
        <div class="center-line"></div>
        <p class="padd-both white">Accountancy and Tax Solutions designed specifically for contractors and freelancers working within the UK and throughout Europe. </p>
        <h2 class="white">www.eafs.eu</h2>
      </div>
    </div>
  </div>
</section>
<!-- END SUBSCRIBE ELEMENTS-->


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('http://www.eafs.eu/social/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script type="text/javascript" src="http://www.eafs.eu/social/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="http://www.eafs.eu/social/js/json.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-24524584-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
</body>
</html>
