<?php
$errors = '';
$myemail = 'unsubscribe@eafs-info.com';

// Check form for Errors and Return Error
if(empty($_POST['email']))
{
    $errors .= "\n Error: all fields are required";
}

// Load the Form Fields
$email_address = strip_tags($_POST['email']);


//  MAKE SURE THE "FROM" EMAIL ADDRESS DOESN'T HAVE ANY NASTY STUFF IN IT
$pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i";
if (!preg_match($pattern, $email_address))
{
    $errors .= "\n Error: Invalid email address";
}

// PREPARE THE BODY OF THE MESSAGE

$message = '<html><body>';
$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
$message .= "<tr><td><strong>Email:</strong> </td><td>" . $email_address . "</td></tr>";
$message .= "</table>";
$message .= "</body></html>";


// Format the E-Mail Template
if( empty($errors))
{
    $to = $myemail;
    $subject = "EAFS Consulting - Unsubscribe Request";

    $headers = "From: $myemail\n";
    $headers .= "Reply-To: $email_address";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    if (mail($to, $subject, $message, $headers)) {
        //echo 'Your message has been sent.';
        header('Location: http://www.eafs.eu/social/newsletters/email_success_unsubscribe.php');
    } else {
        echo 'There was a problem sending the email.';
    }

}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
</head>

<body>
<!-- This page is displayed only if there is some error -->
<?php
    echo nl2br($errors);
?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-24524584-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
</body>
</html>