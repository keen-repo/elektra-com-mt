$(function() {
    $("#00Nw0000003jevA").change(function() {
        var $dropdown = $(this);
        $.getJSON("http://www.eafs.eu/social/jsondata/data.json", function(data) {
            var key = $dropdown.val();
            var vals = [];
            switch(key) {
                case 'Belgium':
                    vals = data.Belgium.split(",");
                    break;
                case 'Germany':
                    vals = data.Germany.split(",");
                    break;
                case 'Italy':
                    vals = data.Italy.split(",");
                    break;
                case 'Luxembourg':
                    vals = data.Luxembourg.split(",");
                    break;
                case 'Netherlands':
                    vals = data.Netherlands.split(",");
                    break;
                case 'Sweden':
                    vals = data.Sweden.split(",");
                    break;
                case 'UK':
                    vals = data.UK.split(",");
                    break;
                case 'base':
                    vals = ['Please choose from above'];
            }

            var $jsontwo = $("#00Nw0000003joCt");
            $jsontwo.empty();
            $.each(vals, function(index, value) {
                $jsontwo.append('<option value="' + value + '">' + value + '</option>');
                //$jsontwo.append("<option>" + value + "</option>");
            });
        });
    });
});


