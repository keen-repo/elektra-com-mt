
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <title>EAFS Consulting : European Payroll : Contractor Accountants</title>
    <link href="https://plus.google.com/112046644106149130619/posts" rel="author">
    <link href="https://plus.google.com/112046644106149130619" rel="publisher">
    <meta content="Euro Accountancy & Finance Services" itemprop="name">
    <meta content="https://plus.google.com/112046644106149130619/posts" itemprop="author">
    <meta content="https://plus.google.com/112046644106149130619" itemprop="creator">
    <meta content="Contractor Accountants" itemprop="provider">
    <meta content="Contractor Accountants 2014" itemprop="copyright">
    <meta content="General" itemprop="contentRating">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." itemprop="description">
    <meta content="http://www.eafs.eu/social/src/logo250x250.png" itemprop="image">
    <meta content="summary_large_image" name="twitter:card">
    <meta content="@eafseu" name="twitter:site">
    <meta content="Contractor Accountants : European Payroll : Contractor Accountants" name="twitter:title">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." name="twitter:description">
    <meta content="@eafseu" name="twitter:creator">
    <meta content="http://www.eafs.eu/social/src/logo280x150.png" name="twitter:image:src">
    <meta content="http://www.eafs.eu/" name="twitter:domain">
    <meta content="en_GB" property="og:locale">
    <meta content="Contractor Accountants : European Payroll : Contractor Accountants" property="og:title">
    <meta content="article" property="og:type">
    <meta content="http://www.eafs.eu/" property="og:url">
    <meta content="54.3934" property="og:latitude">
    <meta content="-3.95508" property="og:longitude">
    <meta content="23 Blair Street" property="og:street-address">
    <meta content="Edinburgh" property="og:locality">
    <meta content="EH11QR" property="og:postal-code">
    <meta content="GB" property="og:country-name">
    <meta content="+44 (0)131 526 3300" property="og:phone_number">
    <meta content="+44 (0)131 526 3300" property="og:fax_number">
    <meta content="http://www.eafs.eu/social/src/logo250x250.png" property="og:image">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." property="og:description">
    <meta content="Contractor Accountants" property="og:site_name">
    <meta content="Index,follow" name="robots">
    <meta content="7 days" name="revisit-after">
    <meta content="Accountancy Services, European Payroll, Contractor Accountants" name="keywords">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." name="description">
    <!-- Bootstrap -->
    <link href="http://www.eafs.eu/social/css/style.min.css" rel="stylesheet">
    <script src="http://www.eafs.eu//social/js/vendor/modenizr/modernizr-2.8.3.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body itemscope itemtype="http://schema.org/WebPage">

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-4">
            <!-- START HEADER-->
            <header class="site-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
                <h1 class="site-title" itemprop="headline"><span class="feature-icon"><img src="http://www.eafs.eu/social/assets/646x220.png" alt="EAFS Consulting BV" class="img-responsive" title="EAFS Consulting BV"></span></h1>
            </header>
            <!-- END HEADER ELEMENT-->
    	</div>
        <div class="col-xs-12 col-sm-8">
        </div>
    </div>
</div>

<!-- START SUBSCRIBE ELEMENTS-->
<section class="statistics yellow">
  <div class="container">
    <div class="row center"> 
    	<div class="col-xs-12 col-md-12">
			    <h2 class="white">Workforce Management & Payroll Solutions Throughout Europe!!</h2>
			    <div class="center-line"></div>
			    <p class="padd-both white">BELGIUM <span class="yellowdot">+</span> GERMANY <span class="yellowdot">+</span> ITALY <span class="yellowdot">+</span> LUXEMBOURG <span class="yellowdot">+</span> MALTA <span class="yellowdot">+</span> NETHERLANDS <span class="yellowdot">+</span> UK</p>
		</div>
    </div>
  </div>
</section>
<!-- END SUBSCRIBE ELEMENTS-->

<!-- START BODY-->
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-8">
			<!-- START MAIN CONTENT ELEMENTS-->
            <main class="site-content" role="main" itemprop="mainContentOfPage">
            <article itemscope itemtype="http://schema.org/Article" class="doc">
                <hgroup class="header">
                    <h1 class="site-headline" itemprop="headline">European Contractor News </h1>
                </hgroup>
                <section itemprop="articleSection"> </section>
                <section itemprop="articleSection">
                  <h2 class="site-section" itemprop="headline">Sorry to see you leave!!</h2>
                    <div itemprop="text">
                        <p><span id="message" name="message" contenteditable="">Please enter your email address in the field below and will will take you off our list....</span></p>
                    </div>
                </section>
                <section itemprop="articleSection">
                    <div id="mc_embed_signup">
                        <form action="http://www.eafs.eu/social/forms/unsubscribe.php" method="POST" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate subscribe-form" novalidate="">
                            <input type=hidden name="retURL" value="http://www.eafs.eu/social/newsletters/email_success_unsubscribe.php">

                            <div id="mc_embed_signup_scroll">
                                <input type="email" class="subscribe-input-inner email" name="email" id="email" placeholder="email address" required="">
                                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;">
                                    <input type="text" name="b_cfe258a0cf370d5efaa793bc7_fa81ce5caf" tabindex="-1">
                                </div>
                                <div class="clear">
                                    <input type="submit" class="subscribe-button" value="UNSUBSCRIBE" name="unsubscribe" id="mc-embedded-subscribe">
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
                
            </article>
            </main>
            <!-- END MAIN CONTENT ELEMENTS-->
		</div>
		<div class="col-xs-12 col-sm-4">
            <!-- START SIDEBAR + WIDGET START -->
            <aside class="site-sidebar sidebar-primary widget-area" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">
			  <h2>Connect with us</h2>
                <p>Why not stay in touch with EAFS Consulting on your favorite social network.</p>
                     <ul class="social">
                      <li class="facebook2"><a href="http://facebook.com/COMPANY"><i class="fa fa-facebook fa-3x"></i></a></li>
                      <li class="twitter2"><a href="http://twitter.com/COMPANY"><i class="fa fa-twitter fa-3x"></i></a></li>
                      <li class="behance3"><a href="https://www.linkedin.net/COMPANY"><i class="fa fa-linkedin fa-3x"></i></a></li>
                    </ul>
            </aside><!-- .site-sidebar -->
            <!-- END SIDEBAR + WIDGET START -->
		</div>
	</div>
</div>
<!-- END BODY LAYOUT-->

<!-- START SUBSCRIBE ELEMENTS-->
<section class="statistics">
  <div class="container">
    <div class="row center"> 
    	<div class="col-xs-12 col-md-12">
			    <h2 class="white">Fully Compliant Payroll Solutions throughout Europe!!</h2>
			    <div class="center-line"></div>
			    <p class="padd-both white">Accountancy and Tax Solutions designed specifically for contractors and freelancers working within the UK and throughout Europe. </p>
		</div>
    </div>
  </div>
</section>
<!-- END SUBSCRIBE ELEMENTS-->

<!-- START DIFFERENT ELEMENTS-->
<section class="different">
  <div class="container">
    <div class="center">
        <h2>What makes us different ?</h2>
        <div class="center-line"></div>
        <p class="padd-both">EAFS Consulting have established ourselves in the contractor payroll sector for over a decade, accumulating a wealth of experience and building a strong team to support your accounting needs. EAFS Consulting have many established and fully compliant local payroll solutions in place for contractors deciding to work overseas. Our payroll solutions ensure you get paid on time, every time no matter which country you are working in.</p>
        <p class="padd-both">&nbsp;</p>
    </div>
    <div class="row"> 
      <!-- FEATURE -->
      <div class="col-sm-4 feature">
        <div class="feature-icon"><img src="http://www.eafs.eu/social/assets/eafs_bv.png" alt="EAFS Consulting BV" class="img-responsive" title="EAFS Consulting BV"></div>
        <h4>EAFS Consulting BV</h4>
        <p>EAFS Consulting BV holds an established relationship with the Dutch Tax Authority, allowing contractors and freelancers to work in the Netherlands, whilst also ensuring that the local tax authorities are happy with your presence.</p>
      </div>
      <!-- /END FEATURE--> 
      <!-- FEATURE -->
      <div class="col-sm-4 feature">
        <div class="feature-icon"><img src="http://www.eafs.eu/social/assets/eafs_bvba.png" alt="EAFS Consulting BVBA" class="img-responsive" title="EAFS Consulting BVBA"></div>
        <h4>EAFS Consulting GmbH</h4>
        <p>EAFS Consulting GmbH holds an established relationship with the German Tax Authority, allowing contractors and freelancers to work throughout Germany, whilst also ensuring that the local tax authorities are happy with your presence.</p>
      </div>
      <!-- /END FEATURE--> 
      <!-- FEATURE -->
      <div class="col-sm-4 feature">
        <div class="feature-icon"><img src="http://www.eafs.eu/social/assets/eafs_gmbh.png" alt="EAFS Consulting GmbH" class="img-responsive" title="EAFS Consulting GmbH"></div>
        <h4>EAFS Consulting BVBA</h4>
        <p>EAFS Consulting BVBA holds an established relationship with the Belgian Tax Authority, allowing contractors and freelancers to work throughout Belgium, whilst also ensuring that the local tax authorities are happy with your presence.</p>
      </div>
      <!-- /END FEATURE--> 
    </div>
  </div>
</section>
<!-- END DIFFERENT ELEMENTS-->


<!-- START SUBSCRIBE ELEMENTS-->
<section class="statistics">
  <div class="container">
    <div class="row center"> 
      <div class="col-xs-12 col-md-12">
          <h2 class="white">Visit us on the Web: <span class="yellowdot">www.eafs.eu </span>!!</h2>
    </div>
    </div>
  </div>
</section>
<!-- END SUBSCRIBE ELEMENTS-->


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-24524584-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>

</body>
</html>
