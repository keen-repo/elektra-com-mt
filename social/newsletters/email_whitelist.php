<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <title>EAFS Consulting : European Payroll : Contractor Accountants</title>
    <link href="https://plus.google.com/112046644106149130619/posts" rel="author">
    <link href="https://plus.google.com/112046644106149130619" rel="publisher">
    <meta content="Euro Accountancy & Finance Services" itemprop="name">
    <meta content="https://plus.google.com/112046644106149130619/posts" itemprop="author">
    <meta content="https://plus.google.com/112046644106149130619" itemprop="creator">
    <meta content="Contractor Accountants" itemprop="provider">
    <meta content="Contractor Accountants 2014" itemprop="copyright">
    <meta content="General" itemprop="contentRating">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." itemprop="description">
    <meta content="http://www.eafs.eu/social/src/logo250x250.png" itemprop="image">
    <meta content="summary_large_image" name="twitter:card">
    <meta content="@eafseu" name="twitter:site">
    <meta content="Contractor Accountants : European Payroll : Contractor Accountants" name="twitter:title">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." name="twitter:description">
    <meta content="@eafseu" name="twitter:creator">
    <meta content="http://www.eafs.eu/social/src/logo280x150.png" name="twitter:image:src">
    <meta content="http://www.eafs.eu/" name="twitter:domain">
    <meta content="en_GB" property="og:locale">
    <meta content="Contractor Accountants : European Payroll : Contractor Accountants" property="og:title">
    <meta content="article" property="og:type">
    <meta content="http://www.eafs.eu/" property="og:url">
    <meta content="54.3934" property="og:latitude">
    <meta content="-3.95508" property="og:longitude">
    <meta content="23 Blair Street" property="og:street-address">
    <meta content="Edinburgh" property="og:locality">
    <meta content="EH11QR" property="og:postal-code">
    <meta content="GB" property="og:country-name">
    <meta content="+44 (0)131 526 3300" property="og:phone_number">
    <meta content="+44 (0)131 526 3300" property="og:fax_number">
    <meta content="http://www.eafs.eu/social/src/logo250x250.png" property="og:image">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." property="og:description">
    <meta content="Contractor Accountants" property="og:site_name">
    <meta content="Index,follow" name="robots">
    <meta content="7 days" name="revisit-after">
    <meta content="Accountancy Services, European Payroll, Contractor Accountants" name="keywords">
    <meta content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe. We also provide a full accountancy service for small to medium owner managed UK businesses." name="description">
    <!-- Bootstrap -->
    <link href="http://www.eafs.eu/social/css/style.min.css" rel="stylesheet">
    <script src="http://www.eafs.eu//social/js/vendor/modenizr/modernizr-2.8.3.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body itemscope itemtype="http://schema.org/WebPage">
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-4">
      <!-- START HEADER-->
      <header class="site-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
        <h1 class="site-title" itemprop="headline"><span class="feature-icon"><img src="http://www.eafs.eu/social/assets/646x220.png" alt="EAFS Consulting BV" class="img-responsive" title="EAFS Consulting BV"></span></h1>
      </header>
      <!-- END HEADER ELEMENT-->
    </div>
    <div class="col-xs-12 col-sm-8"></div>
  </div>
</div>
<!-- START BORDER ELEMENTS-->
<section class="statistics">
  <div class="container">
    <div class="row center">
      <div class="col-xs-12 col-md-12">
        <h2 class="white">Workforce Management & Payroll Solutions Throughout Europe!!</h2>
        <div class="center-line"></div>
        <p class="padd-both white">BELGIUM <span class="yellowdot">+</span> GERMANY <span class="yellowdot">+</span> ITALY <span class="yellowdot">+</span> LUXEMBOURG <span class="yellowdot">+</span> MALTA <span class="yellowdot">+</span> NETHERLANDS <span class="yellowdot">+</span> UK</p>
      </div>
    </div>
  </div>
</section>
<!-- END BORDER ELEMENTS-->


<!-- START BODY-->
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-8">
			<!-- START MAIN CONTENT ELEMENTS-->
            <main class="site-content" role="main" itemprop="mainContentOfPage">
            <article itemscope itemtype="http://schema.org/Article" class="doc">
                <hgroup class="header">
                    <h1 class="site-headline" itemprop="headline"><span id="lblTitle">How to Whitelist our Email Address</span></h1>
                    <span itemprop="name">How to Whitelist our email address</span></hgroup>
                <section itemprop="articleSection">
                    <div itemprop="text">
                        <p>To ensure you continue to receive emails from us add the <strong>From/</strong><strong>On Behalf Of</strong> email address(es) to your address book today.  Here's How...</p>
                        <div><strong><a href="#aol">AOL</a><br>
                          <a href="#bellsouth">Bellsouth</a><br>
                          <a href="#comcast">Comcast</a><br>
                          <a href="#earthlink">EarthLink</a><br>
                          <a href="#gmail">Google Mail/Gmail</a><br>
                          <a href="#msn">MSN/Hotmail</a><br>
                          <a href="#outlook">Outlook</a><br>
                          <a href="#outlook2007">Outlook 2003/2007</a><br>
                      <a href="#shaw">ShawWebmail</a></strong></div>
                        <div><strong><a href="#yahoo">Yahoo</a></strong></div>
                        <p><br>
                        </p>
                        <p>If your email provider is not on the list , please contact them directly for assistance.  If you encounter any problems following these instructions, please contact your email provider for assistance.</p>
                    </div>
                </section>
                <section itemprop="articleSection">
                	<a name="aol"></a>
                    <h2 class="site-section" itemprop="headline"><strong>AOL</strong></h2>
                    <div itemprop="text">
                        <ul>
                          <li>Open your email message.</li>
                          <li>Click on 'More Details' at the top of your email message.</li>
                          <li>Hover mouse over the From address.</li>
                          <li>The From email address is automatically placed in the email field in the "Add Contact" pop-up box.</li>
                          <li>Click on 'Add Contact'.</li>
                          <li>Add additional contact information.</li>
                          <li>Click the 'Add Contact' button.</li>
                          <li>The sender's information is now saved to your address book.</li>
                        </ul>
                    </div>
                </section>
                <section itemprop="articleSection">
                <a name="bellsouth"></a>
                    <h2 class="site-section" itemprop="headline"><strong>BellSouth</strong></h2>
                    <div itemprop="text">
                        <ul>
                          <li>Open email address and click Address Book.</li>
                          <li>Click Add New Contact.</li>
                          <li>In the display name field, type the name to display for the sender.</li>
                          <li>In the email field, type in sender email address (the email address in the FROM field).</li>
                          <li>Click the Save button when done.</li>
                          <li>The sender's information is now saved to your address book.</li>
                        </ul>
                    </div>
                </section>
                <section itemprop="articleSection">
                <a name="comcast"></a>
                    <h2 class="site-section" itemprop="headline"><strong>Comcast</strong></h2>
                    <div itemprop="text">
                        <ul><li> Open your email message and click the link titled Add to Address Book.</li>
                          <li>The Email Address field will be pre-formatted with the address displayed in the sender's message.</li>
                          <li>You may enter additional information in the general, Phone, Address, Internet and Personal pages.</li>
                          <li>Click save when you are finished entering all contact information.</li>
                          <li>The sender's information is now saved to your address book.</li>
                        </ul>
                    </div>
                </section>
                <section itemprop="articleSection">
                <a name="earthlink"></a>
                    <h2 class="site-section" itemprop="headline"><strong>EarthLink</strong></h2>
                    <div itemprop="text">
                        <ul>
                          <li>Open  your email message, look for the Add to Address Book link (should  appear next to the sender's address). Click the Add to Address Book  link.</li>
                          <li>The sender's email address should automatically appear in the  first line of the Internet Information box in the new window. Enter the  email address if it does not appear automatically.</li>
                          <li>Make sure the Set as Default bubble next to this line is clicked.</li>
                          <li>Add any additional information that you would like in the provided spaces.</li>
                          <li>Click the Save button located at the top of the window.</li>
                          <li>The sender's information is now saved to your address book.</li>
                        </ul>
                    </div>
                </section>                
                <section itemprop="articleSection">
                <a name="gmail"></a>
                    <h2 class="site-section" itemprop="headline"><strong>Gmail/Google Mail</strong></h2>
                    <div itemprop="text">
                        <ul>
                          <li>Open your email message, look for the Add to Address Book link (should  appear under the drop down arrow located to the right of the reply  option).</li>
                          <li>Click the Add _____ to Contacts list option.</li>
                          <li>The sender has been added to your contacts list, will be displayed on the screen.</li>
                          <li>The sender&rsquo;s information is now saved to your address book.</li>
                        </ul>
                    </div>
                </section>   
                
                <section itemprop="articleSection">
                <a name="msn"></a>
                    <h2 class="site-section" itemprop="headline"><strong>MSN/Hotmail</strong></h2>
                    <div itemprop="text">
                        <ul><li> Open your email message, look for the Add to Contacts button.</li>
                          <li>Click the Add to Contacts button.</li>
                          <li>The sender's information is now saved to your address book.</li>
                        </ul>
                    </div>
                </section>                   
                
                <section itemprop="articleSection">
                <a name="outlook"></a>
                    <h2 class="site-section" itemprop="headline"><strong>Outlook</strong></h2>
                    <div itemprop="text">
                        <ul>
                          <li>Open your email message, click under the subject line, Click and Copy the email address.</li>
                          <li>Click back to email inbox. Click Tools tab. Then click address book.</li>
                          <li>Click on File / New Entry.</li>
                          <li>Click Add New Contact and then click Okay.</li>
                          <li>Add any additional information that you would like in the provided spaces.</li>
                          <li>Paste the email address you want to add in the email line.</li>
                          <li>Click &ldquo;Save and Close&rdquo; button.</li>
                          <li>The sender's information is now saved to your address book.</li>
                        </ul>
                    </div>
                </section>   
                
                <section itemprop="articleSection">
                <a name="outlook2007"></a>
                    <h2 class="site-section" itemprop="headline"><strong>Outlook 2003/2007</strong></h2>
                    <div itemprop="text">
                        <ul><li>Open your email message.</li>
                          <li>Click on 'Actions' from the menu bar.</li>
                          <li>Click on 'Junk E-mail' from drop down menu.</li>
                          <li>Click on 'Add Sender to Safe Senders List'.</li>
                          <li>Our email address will be automatically entered into your Safe senders list.</li>
                          <li>The sender's information is now saved to your address book.</li>
                        </ul>
                    </div>
                </section>                   
 
                 <section itemprop="articleSection">
                 <a name="yahoo"></a>
                    <h2 class="site-section" itemprop="headline"><strong>Yahoo!</strong></h2>
                    <div itemprop="text">
                        <ul><li>Open your email message, look for the Add to Contacts link (should appear next to the sender's address).</li>
                          <li>Click the Add to Contacts link.</li>
                          <li>The sender's information will automatically appear. Enter the email address if it does not appear automatically.</li>
                          <li>Add any additional information that you would like in the provided spaces.</li>
                          <li>Click the Save button.</li>
                          <li>The sender's information is now saved to your address book.</li>
                        </ul>
                    </div>
                </section>   
                
                <section itemprop="articleSection">
                <a name="shaw"></a>
                    <h2 class="site-section" itemprop="headline"><strong>ShawWebmail</strong></h2>
                    <div itemprop="text">
                        <ul>
                        	<li>Look at the email received and write down or copy the name and address found in the from field.</li>
                            <li>On the main screen of your email, select the "Address Book"  tab seen at the top. It lists all your contacts within your address  book.</li>
                            <li>On this page, enter the name and email address you wrote down or copied. </li>
                            <li>Select 'Add Contact' when complete.</li>
                        </ul>
                    </div>
                </section>                                  
                
            </article>
            </main>
            <!-- END MAIN CONTENT ELEMENTS-->
		</div>
    <div class="col-xs-12 col-sm-4">
            <!-- START SIDEBAR + WIDGET START -->
            <aside class="site-sidebar sidebar-primary widget-area" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">
                <div class="form-bg">
                <h3>Get your free<br>something hurry!!</h3>
                  <!-- Form -->
                    <div class="form-group">
                      <input type="text" placeholder="First Name" class="form-control f-input input-field" id="first_name" name="first_name">
                    </div>
                    <div class="form-group">
                      <input type="text" placeholder="Last Name" class="form-control f-input input-field" id="last_name" name="last_name">
                    </div>
                    <div class="form-group">
                      <input type="text" placeholder="Email" class="form-control f-input input-field" name="email">
                    </div>
                    <div class="form-group">
                      <input type="text" placeholder="Phone" class="form-control f-input input-field" maxlength="15" name="phone">
                    </div>
                    
                    <button id="submit_btn" class="submit-btn" type="submit">Submit</button>
                    
                </div>
            </aside><!-- .site-sidebar -->
            <!-- END SIDEBAR + WIDGET START -->
    </div>
	</div>
</div>
<!-- END BODY LAYOUT-->

<!-- START SUBSCRIBE ELEMENTS-->
<section class="statistics">
  <div class="container">
    <div class="row center">
      <div class="col-xs-12 col-md-12">
        <h2 class="white">Workforce Management & Payroll Solutions Throughout Europe!!</h2>
        <div class="center-line"></div>
        <p class="padd-both white">BELGIUM <span class="yellowdot">+</span> GERMANY <span class="yellowdot">+</span> ITALY <span class="yellowdot">+</span> LUXEMBOURG <span class="yellowdot">+</span> MALTA <span class="yellowdot">+</span> NETHERLANDS <span class="yellowdot">+</span> UK</p>
      </div>
    </div>
  </div>
</section>
<!-- END SUBSCRIBE ELEMENTS-->

<!-- START DIFFERENT ELEMENTS-->
<section class="different">
  <div class="container">
    <div class="center">
        <h2>What makes us different ?</h2>
        <div class="center-line"></div>
        <p class="padd-both">EAFS Consulting have established ourselves in the contractor payroll sector for over a decade, accumulating a wealth of experience and building a strong team to support your accounting needs. EAFS Consulting have many established and fully compliant local payroll solutions in place for contractors deciding to work overseas. Our payroll solutions ensure you get paid on time, every time no matter which country you are working in.</p>
        <p class="padd-both">&nbsp;</p>
    </div>
    <div class="row"> 
      <!-- FEATURE -->
      <div class="col-sm-4 feature">
        <div class="feature-icon"><img src="http://www.eafs.eu/social/assets/eafs_bv.png" alt="EAFS Consulting BV" class="img-responsive" title="EAFS Consulting BV"></div>
        <h4>EAFS Consulting BV</h4>
        <p>EAFS Consulting BV holds an established relationship with the Dutch Tax Authority, allowing contractors and freelancers to work in the Netherlands, whilst also ensuring that the local tax authorities are happy with your presence.</p>
      </div>
      <!-- /END FEATURE--> 
      <!-- FEATURE -->
      <div class="col-sm-4 feature">
        <div class="feature-icon"><img src="http://www.eafs.eu/social/assets/eafs_bvba.png" alt="EAFS Consulting BVBA" class="img-responsive" title="EAFS Consulting BVBA"></div>
        <h4>EAFS Consulting GmbH</h4>
        <p>EAFS Consulting GmbH holds an established relationship with the German Tax Authority, allowing contractors and freelancers to work throughout Germany, whilst also ensuring that the local tax authorities are happy with your presence.</p>
      </div>
      <!-- /END FEATURE--> 
      <!-- FEATURE -->
      <div class="col-sm-4 feature">
        <div class="feature-icon"><img src="http://www.eafs.eu/social/assets/eafs_gmbh.png" alt="EAFS Consulting GmbH" class="img-responsive" title="EAFS Consulting GmbH"></div>
        <h4>EAFS Consulting BVBA</h4>
        <p>EAFS Consulting BVBA holds an established relationship with the Belgian Tax Authority, allowing contractors and freelancers to work throughout Belgium, whilst also ensuring that the local tax authorities are happy with your presence.</p>
      </div>
      <!-- /END FEATURE--> 
    </div>
  </div>
</section>
<!-- END DIFFERENT ELEMENTS-->

<!-- START SUBSCRIBE ELEMENTS-->
<section class="statistics">
  <div class="container">
    <div class="row center"> 
      <div class="col-xs-12 col-md-12">
          <h2 class="white">Visit us on the Web: <span class="yellowdot">www.eafs.eu </span>!!</h2>
    </div>
    </div>
  </div>
</section>
<!-- END SUBSCRIBE ELEMENTS-->


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-24524584-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>

</body>
</html>