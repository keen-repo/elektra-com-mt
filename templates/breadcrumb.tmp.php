

<!-- Contractor Accountants-->

<section>
    <div class="row lightbg">
        <div class="large-12 columns">
            <div class="breadcrumb-container">
                <span>You are here: </span>
                    <ul class="breadcrumbs">
                        <li itemscope itemtype="https://schema.org/Breadcrumb">
                            <a href="<?php echo SITE_URL;?>" title="Home" alt="Home" itemprop="url"><span itemprop="title">Home</span></a></li>
                        <li itemscope itemtype="https://schema.org/Breadcrumb" class="unavailable">
                            <a href="<?php echo SITE_URL;?>contractor-accountants" title="Contractor Accountants" alt="Contractor Accountants" itemprop="url"><span itemprop="title">Contractor Accountants</span></a></li>
                        <li itemscope itemtype="https://schema.org/Breadcrumb" class="current">
                            <span itemprop="title">Limited Company Contractors</span></li>
                    </ul><!-- .breadcrumb end -->
            </div> <!-- .breadcrumb-container end -->
        </div>
    </div>
</section>

<div class="breadcrumb-container">
    <span>You are here: </span>
    <ul class="breadcrumb">
        <li><a href="#">Contractor Accountants</a></li>
        <li><a href="#">Limited Company Contractors</a></li>
    </ul><!-- .breadcrumb end -->
</div>