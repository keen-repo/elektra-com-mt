<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/112046644106149130619/posts" />
    <link rel="publisher" href="https://plus.google.com/112046644106149130619" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Euro Accountancy &amp; Finance Services" />
    <meta itemprop="author" content="https://plus.google.com/112046644106149130619/posts" />
    <meta itemprop="creator" content="https://plus.google.com/112046644106149130619" />
    <meta itemprop="provider" content="Euro Accountancy &amp; Finance Services" />
    <meta itemprop="copyright" content="Euro Accountancy &amp; Finance Services 2014" />
    <meta itemprop="contentRating" content="General" />
    <meta itemprop="description" content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe.  We also provide a full accountancy service for small to medium owner managed UK businesses." />
    <meta itemprop="image" content="https://www.eafs.eu/social/src/logo250x550.png" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="xxxxxxx" />
    <meta name="twitter:site" content="@twitter">
    <meta name="twitter:title" content="xxxxxxx" />
    <meta name="twitter:description" content="xxxxxxx" />
    <meta name="twitter:creator" content="@twitter" />
    <meta name="twitter:image:src" content="https://www.eafs.eu/social/src/logo280x150.png" />
    <meta name="twitter:domain" content="www.eafs.eu" />
    <!-- Open Graph data -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="European Payroll : Accountancy Services : Euro Accountancy &amp; Finance Services" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://www.eafs.eu/" />
    <meta property="og:latitude" content="54.3934" />
    <meta property="og:longitude" content="-3.95508" />
    <meta property="og:street-address" content="23 Blair Street" />
    <meta property="og:locality" content="Edinburgh" />
    <meta property="og:postal-code" content="EH11QR" />
    <meta property="og:country-name" content="GB" />
    <meta property="og:phone_number" content="+44 (0)131 526 3300" />
    <meta property="og:fax_number" content="+44 (0)131 526 3300" />
    <meta property="og:image" content="https://wwww.eafs.eu/social/src/logo250x550.png" />
    <meta property="og:description" content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe.  We also provide a full accountancy service for small to medium owner managed UK businesses."/>
    <meta property="og:site_name" content="Euro Accountancy &amp; Finance Services" />
    <meta property="fb:admins" content="345577402164972" />
    <!-- Standard meta data -->
    <meta name="robots" content="Index,follow" />
    <meta name="revisit-after" content="7 days" />
    <meta name="keywords" content="Accountancy Services, European Payroll, Contractor Accountants" />
    <meta name="description" content="EAFS provide accountancy and tax solutions designed specifically for contractors working in the UK and throughout Europe.  We also provide a full accountancy service for small to medium owner managed UK businesses.." />
    <link rel="canonical" href="https://www.eafs.eu/" />
    <!-- <link rel="shortlink" href="https://www.eafs.eu/" /> -->
    <!-- Favicons -->
    <link rel="shortcut icon" href="https://wwww.eafs.eu/social/src/favicon.ico">
    <link rel="apple-touch-icon" href="https://wwww.eafs.eu/social/src/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://wwww.eafs.eu/social/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://wwww.eafs.eu/social/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://wwww.eafs.eu/social/favicon/apple-touch-icon-114x114.png">
</head>
<body>
</body>
</html>