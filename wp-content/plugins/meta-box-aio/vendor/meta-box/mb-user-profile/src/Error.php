<?php
namespace MBUP;

class Error {
	private $key;

	public function __construct( $form ) {
		$this->key = md5( md5( serialize( $form->config ) ) );
	}

	public function set( $error = false ) {
		if ( false === $error ) {
			$error = __( 'There are some errors submitting the form. Please correct and try again.', 'mb-user-profile' );
		}
		$_SESSION[ $this->key ] = $error;
	}

	public function has() {
		return ! empty( $_SESSION[ $this->key ] );
	}

	public function clear() {
		unset( $_SESSION[ $this->key ] );
	}

	public function show() {
		?>
		<div class="rwmb-error"><?= wp_kses_post( $_SESSION[ $this->key ] ); ?></div>
		<?php
	}
}