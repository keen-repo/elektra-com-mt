<?php
namespace MBUP\Forms;

class Info extends Base {
	protected $type = 'info';

	protected function has_privilege() {
		if ( is_user_logged_in() ) {
			return true;
		}
		$request = rwmb_request();
		if ( 'error' !== $request->get( 'rwmb-form-submitted' ) && ! $request->get( 'lost-password' ) && ! $request->get( 'reset-password' ) ) {
			echo '<div class="rwmb-notice">';
			esc_html_e( 'Please login to continue.', 'mb-user-profile' );
			echo '</div>';
		}
		$url = remove_query_arg( 'rwmb-form-submitted' ); // Do not show success message after logging in.
		echo do_shortcode( "[mb_user_profile_login redirect='$url' ]" );
		return false;
	}

	public function render() {
		if ( ! $this->has_privilege() ) {
			return;
		}

		// Get correct values for user fields.
		$user_fields = [
			'user_login',
			'user_email',
			'user_nicename',
			'user_url',
			'display_name',
		];
		foreach ( $user_fields as $user_field ) {
			add_filter( "rwmb_{$user_field}_field_meta", function() use ( $user_field ) {
				$user_id = $this->config['user_id'] ?: get_current_user_id();
				$user = get_userdata( $user_id );
				return $user->{$user_field};
			} );
		}

		parent::render();
	}

	protected function submit_button() {
		?>
		<div class="rwmb-field rwmb-button-wrapper rwmb-form-submit">
			<button class="rwmb-button" id="<?= esc_attr( $this->config['id_submit'] ) ?>" name="rwmb_profile_submit_info" value="1"><?= esc_html( $this->config['label_submit'] ) ?></button>
		</div>
		<?php
	}
}
