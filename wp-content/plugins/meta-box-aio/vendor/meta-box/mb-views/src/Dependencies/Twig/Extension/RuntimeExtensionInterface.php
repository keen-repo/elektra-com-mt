<?php

/*
 * This file is part of Twig.
 *
 * (c) Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MBViews\Dependencies\Twig\Extension;

/**
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 */
interface RuntimeExtensionInterface
{
}
