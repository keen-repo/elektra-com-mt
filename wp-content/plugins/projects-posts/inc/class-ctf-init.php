<?php
/**
 * Main Init Class
 *
 * @package     CTF
 * @subpackage  CTF/includes
 * @copyright   Copyright (c) 2014, Jason Witt
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 * @author      Jason Witt <contact@jawittdesigns.com>
 */
class CTF_Init {
	/**
	 * Initialize the class
	 */
	public function __construct() {
		$register_post_types     = new CFT_Register_Post_Types();
	}
}
