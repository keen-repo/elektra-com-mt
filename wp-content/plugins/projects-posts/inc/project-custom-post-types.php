<?php
/**
 * Register custom post types
 *
 * @package     CTF
 * @subpackage  CTF/includes
 * @copyright   Copyright (c) 2014, Jason Witt
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */
class CFT_Register_Post_Types {
    /**
     * Initialize the class
     */
    public function __construct() {
       add_action( 'init', array( $this, 'register_projects_post_type' ) );
    }
    /**
     * Register Projects Post Type
     *
     * @since  1.0.0
     * @access public
     * @return void
     */
    public function register_projects_post_type() {
        register_post_type( 'projects',
            array(
                'labels'        => array(
                'name'          => __( 'Projects' ),
                'singular_name' => __( 'Project' ),
                'add_new'       => __( 'Add Project' ),
                'add_new_item'  => __( 'Add New Projects' ),
                'edit_item'     => __( 'Edit Projects' ),
            ),
            'public'      => true,
			'has_archive' => false,
			'supports' => array('title')
            )
        );
    }
}
