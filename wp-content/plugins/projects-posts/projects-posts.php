<?php
/*
Plugin Name: Elektra Custom Plugin
Plugin URI:
Description: Elektra Custom Plugin for Projects
Version: 1.0.0
Author: Keen
Author URI:http://www.keen.com.mt
*/


// If this file is called directly, abort.
if ( !defined( 'WPINC' ) ) {
	die;
}
if( !class_exists( 'CTF' ) ) {
	class CTF {
		/**
		 * Instance of the class
		 *
		 * @since 1.0.0
		 * @var Instance of CTF class
		 */
		private static $instance;
		/**
		 * Instance of the plugin
		 *
		 * @since 1.0.0
		 * @static
		 * @staticvar array $instance
		 * @return Instance
		 */
		public static function instance() {
			if ( !isset( self::$instance ) && ! ( self::$instance instanceof CTF ) ) {
				self::$instance = new CTF;
				self::$instance->define_constants();
				//add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
				self::$instance->includes();
				self::$instance->init = new CTF_Init();
			}
		return self::$instance;
		}
		/**
		 * Define the plugin constants
		 *
		 * @since  1.0.0
		 * @access private
		 * @return void
		 */
		private function define_constants() {
			// Plugin Version
			if ( ! defined( 'CTF_VERSION' ) ) {
				define( 'CTF_VERSION', '1.0.0' );
			}
			// Prefix
			if ( ! defined( 'CTF_PREFIX' ) ) {
				define( 'CTF_PREFIX', 'ctf_' );
			}
			// Textdomain
			if ( ! defined( 'CTF_TEXTDOMAIN' ) ) {
				define( 'CTF_TEXTDOMAIN', 'ctf' );
			}
			// Plugin Options
			if ( ! defined( 'CTF_OPTIONS' ) ) {
				define( 'CTF_OPTIONS', 'ctf-options' );
			}
			// Plugin Directory
			if ( ! defined( 'CTF_PLUGIN_DIR' ) ) {
				define( 'CTF_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
			}
			// Plugin URL
			if ( ! defined( 'CTF_PLUGIN_URL' ) ) {
				define( 'CTF_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
			}
			// Plugin Root File
			if ( ! defined( 'CTF_PLUGIN_FILE' ) ) {
				define( 'CTF_PLUGIN_FILE', __FILE__ );
			}
		}
		/**
		 * Load the required files
		 *
		 * @since  1.0.0
		 * @access private
		 * @return void
		 */
		private function includes() {
			$includes_path = plugin_dir_path( __FILE__ ) . 'inc/';
			require_once CTF_PLUGIN_DIR . 'inc/project-custom-post-types.php';

			require_once CTF_PLUGIN_DIR . 'inc/class-ctf-init.php';
		}

		/**
		 * Throw error on object clone
		 *
		 * @since  1.0.0
		 * @access public
		 * @return void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', CTF_TEXTDOMAIN ), '1.6' );
		}
		/**
		 * Disable unserializing of the class
		 *
		 * @since  1.0.0
		 * @access public
		 * @return void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', CTF_TEXTDOMAIN ), '1.6' );
		}
	}
}
/**
 * Return the instance
 *
 * @since 1.0.0
 * @return object The Safety Links instance
 */
function CTF_Run() {
	return CTF::instance();
}
CTF_Run();
