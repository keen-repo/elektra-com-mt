<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');

$projects_arr = rwmb_meta('projects-group', '', 96);

// reverse to get the ids in correct order
//$projects = array_reverse($projects_arr, true);
$projects = $projects_arr;

//var_dump($projects);
//die();
foreach($projects as $project){

	$content = $project['_kmeta_project_long_description'].'<!--more-->'.$project['_kmeta_project_long_description_hidden'];

	var_dump($project);

	$id = wp_insert_post(array(
		'post_type' => 'projects',
		'post_title' => $project['_kmeta_project_title'],
		'post_name' => $project['_kmeta_project_title'],
		'post_content' => $content,
		'post_status' => 'publish'
	));

	// custom meta fields
	update_post_meta($id, '_kmeta_project_title', $project['_kmeta_project_title']);
	update_post_meta($id, '_kmeta_project_location', $project['_kmeta_project_location']);
	update_post_meta($id,'_kmeta_project_show_homepage' , $project['_kmeta_project_show_homepage']);

	update_post_meta($id, '_kmeta_project_description', $project['_kmeta_project_description']);
	update_post_meta($id, '_kmeta_project_long_description', $project['_kmeta_project_long_description']);
	update_post_meta($id, '_kmeta_project_long_description_hidden', $project['_kmeta_project_long_description_hidden']);

	update_post_meta($id, 'project-videos', $project['project-videos']);
	update_post_meta($id, 'project-slider', $project['project-slider']);

	update_post_meta( $post_id,'_wporg_meta_key', $_POST['wporg_field'] );


	//break;
}
