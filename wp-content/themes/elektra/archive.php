<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php get_template_part( 'template-parts/components/inner-top-banner', 'none' ); ?>
			<?php
			$counter = 0;
			if ( have_posts() ) :

				$counter = $counter + 1;
				?>

				<section class="blog-content">
				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					if($counter == 1) {
						get_template_part( 'template-parts/components/newest-posts', "none" );
					} else if($counter == 2) { ?>
						<div class="container">
        					<div class="row-l blog-post-row blog-post-row-first">
							<?php }

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content-archive', get_post_format() );

						endwhile;

					else :

						get_template_part( 'template-parts/content', 'none' ); ?>
							</div>
						</div>
				</section>
			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
