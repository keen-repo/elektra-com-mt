jQuery(document).ready(function($){
    let faqQuestionsNumber = 1;
    let faqQuestionsContent = ".faq-questions-content-1";
    let faqQuestionsAttribute = ".faq-sections-nav-section-1";

    $(".faq-sections-nav").click(function(event) {
        event.preventDefault();

        $(faqQuestionsAttribute).removeClass("faq-questions-active");
        $(faqQuestionsContent).removeClass("faq-content-active"); 
        
        faqQuestionsAttribute = event.target;
        faqQuestionsNumber = $(faqQuestionsAttribute).attr("class").slice(-1);
        faqQuestionsContent = ".faq-questions-content-" + faqQuestionsNumber;

        $(faqQuestionsAttribute).addClass("faq-questions-active");
        $(faqQuestionsContent).addClass("faq-content-active");  
    });

    // FAQ questions read more
    let faqQuestionsRowAttribute;
    let faqQuestionsRowNumber;
    let faqQuestionsRowContent;
    let faqQuestionsRowContentIcon;
    $(".faq-questions-row").click(function(event) {
        faqQuestionsRowAttribute = event.currentTarget;
        faqQuestionsRowNumber = $(faqQuestionsRowAttribute).attr("class").match(/\d+/);
        faqQuestionsRowContent = faqQuestionsContent + " .faq-questions-content-addition-" + faqQuestionsRowNumber;
        faqQuestionsRowContentIcon = ".faq-questions-row-" + faqQuestionsRowNumber;
        
        // Toggles additional content
        $(faqQuestionsRowContent).toggle("faq-questions-content-addition-active");  
        
        // Toggles button
        $(faqQuestionsRowContentIcon + " .faq-image-plus").toggleClass("faq-questions-image-show");
        $(faqQuestionsRowContentIcon + " .faq-image-minus").toggleClass("faq-questions-image-show");
    });
});