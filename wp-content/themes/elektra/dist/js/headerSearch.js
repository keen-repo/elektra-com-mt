jQuery(document).ready(function(){
    $(".search-mobile").click(function(event) {
        event.preventDefault();
        $(".search-input").toggleClass("search-input-show");
        $(".social-icons-column").toggleClass("social-icons-column-margin");
    });
});
