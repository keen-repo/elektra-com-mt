WebFont.load({
	google: {
		families: ['Ubuntu:300,400,500,500i,700']
	},
});

jQuery(document).ready(function($) {
	$('.wp-block-advgb-tabs li a').on('click', function(e) {
		e.off();
		e.preventDefault();
		e.stopPropagation();
		console.log('def prop')
	});

	jQuery('.gut-tabbed_accordions .tabcontents .tabcontent').not('.is-active').hide();
	jQuery('.gut-tabbed_accordions .tabs .tab').click(function(){
		jQuery('.is-active').removeClass('is-active');
		jQuery(this).addClass('is-active');
		var id = $(this).find('a').attr("id");
		jQuery('.gut-tabbed_accordions .tabcontents .tabcontent').hide();

		jQuery('#tabcontent-'+id).show();
		jQuery('#tabcontent-'+id).addClass('is-active');
	});

	jQuery('.gut-tabbed_accordions .tabcontents .tabcontent ul li').click(function(){
		jQuery(this).find('ul').slideToggle();
		jQuery(this).find('.cross').toggleClass('active');
	});

	jQuery('.gut-tabbed_accordions .tabcontents .tabcontent ul li').has('ul').append('<span class="cross"></span>').css('cursor', 'pointer');

	jQuery('.menu .sub-menu').hide();
	jQuery('.menu .menu-item-has-children').click(function(){
		jQuery(this).find('.sub-menu').slideToggle();
	});

	jQuery('.scroll-up').click(function(){
		window.scrollTo({ top: 0, behavior: 'smooth' });
	});

});
