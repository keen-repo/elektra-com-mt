jQuery(document).ready(function($) {
    $('.sgallery').slick({
        prevArrow: '<img src="wp-content/themes/elektra/dist/images/arrows/arrow-left-black.svg" class="slick-prev">',
        nextArrow: '<img src="wp-content/themes/elektra/dist/images/arrows/arrow-right-black.svg" class="slick-next">',
        centerMode: true,
        infinite: true,
        autoplay: true,
        centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 1,
        variableWidth: true,
        responsive: [{
            breakpoint: 1300,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true
            }
        }, {
            breakpoint: 1030,
            settings: {
                centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }, {
            breakpoint: 768,
            settings: {
                centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }, {
            breakpoint: 500,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });
    jQuery('.sgallery').css('visibility', 'visible');

});