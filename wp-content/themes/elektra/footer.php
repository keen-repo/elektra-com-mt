<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package generic
 */

$prefix = K_MB_PREFIX;
$logos = rwmb_meta("{$prefix}header_logo_image", array( 'limit' => 1, 'size'=>'full' ), 6 /*id of homepage*/ );
$logo = reset($logos);
$facebook = rwmb_meta("{$prefix}header_facebook", '', 6 /*id of homepage*/ );
$linkedin = rwmb_meta("{$prefix}header_linkedin", '', 6 /*id of homepage*/ );
$twitter = rwmb_meta("{$prefix}header_twitter", '', 6 /*id of homepage*/ );
$instagram = rwmb_meta("{$prefix}header_instagram", '', 6 /*id of homepage*/ );
$street = rwmb_meta("{$prefix}contacts_street","",105 /*id of contacts page*/);
$town = rwmb_meta("{$prefix}contacts_town","",105 /*id of contacts page*/);
$postcode = rwmb_meta("{$prefix}contacts_postcode","",105 /*id of contacts page*/);
$country = rwmb_meta("{$prefix}contacts_country","",105 /*id of contacts page*/);
$telephone = rwmb_meta("{$prefix}contacts_phone","",105 /*id of contacts page*/);
$email = rwmb_meta("{$prefix}contacts_email","",105 /*id of contacts page*/);
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container-fluid footer-newsletter">
			<?= do_shortcode('[formidable id=2]'); ?>
		</div>

		<div class="container-fluid footer-main">
			<div class="container">
				<div class="row-m">
					<div class="col-12-m footer-main-logo">
						<a href="/">
							<img src="<?= $logo['url']; ?>" alt="Elektra Logo">
						</a>
					</div>
				</div>

				<div class="row-m">
					<div class="col-12-m social-icons-column">
						<div class="social-icons">
							<?php if($facebook) { ?>
							<a href="<?= $facebook; ?>">
								<img src="<?= get_template_directory_uri() ?>/dist/images/footer/icons/facebook-white.svg" alt="Facebook">
							</a>
							<?php } ?>
							<?php if($linkedin) { ?>
							<a href="<?= $linkedin; ?>">
								<img src="<?= get_template_directory_uri() ?>/dist/images/footer/icons/linkedin-white.svg" alt="Linkedin">
							</a>
							<?php } ?>
							<?php if($twitter) { ?>
							<a href="<?= $twitter; ?>">
								<img src="<?= get_template_directory_uri() ?>/dist/images/footer/icons/twitter.svg" alt="Twitter">
							</a>
							<?php } ?>
							<?php if($instagram) { ?>
							<a href="<?= $instagram; ?>">
								<img src="<?= get_template_directory_uri() ?>/dist/images/footer/icons/instagram-white.svg" alt="Instagram">
							</a>
							<?php } ?>
						</div>
					</div>
				</div>

				<div class="row-m footer-main-contact">
					<div class="col-4-m">
						<div><?= $street; ?></div>
						<div><?= $town; ?> <?= $postcode; ?> <?= $country; ?></div>
					</div>
					<div class="col-4-m mobile-number">
						<div><?= $telephone; ?></div>
					</div>
					<div class="col-4-m email">
						<div><a href="mailto:<?= $email; ?>"><?= $email; ?></a></div>
					</div>
				</div>

				<div class="row-m horizontal-line__wrapper">
					<div class="col-12-m horizontal-line"></div>
				</div>

				<div class="row-m footer-nav">
					<div class="col-11-m">
						<nav>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-2',
								'menu_id'        => 'main-footer-menu',
							) );
							?>
						</nav>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid footer-copyright__wrapper">
			<div class="footer-copyright">
				<div class="container">
					<div class="row-m">
						<div class="col-12-m">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'menu-3',
									'menu_id'        => 'lower-footer-menu',
								) );
								?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid footer-design">
			<div class="row-m">
				<div class="col-12-m">
					<p><a href="https://www.keen.com.mt" target="_blank">Website designed & developed by Keen Ltd Malta</a></p>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->


<?php wp_footer(); ?>

<?php
//<script src="wp-content/themes/elektra/dist/js/headerSearch.js"></script>
$curatorCode = rwmb_meta( $prefix . 'curator_code' );
if ( $curatorCode ) : ?>
<!-- The Javascript can be moved to the end of the html page before the </body> tag -->
<script type="text/javascript">
/* curator-feed-default-layout */
(function(){
var i, e, d = document, s = "script";i = d.createElement("script");i.async = 1;
i.src = "https://cdn.curator.io/published/<?php echo $curatorCode; ?>.js";
e = d.getElementsByTagName(s)[0];e.parentNode.insertBefore(i, e);
})();
</script>
<?php
endif; ?>
<script>AOS.init();</script>
</body>
</html>
