<?php
/**
 * _s functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package _s
 */

if ( ! function_exists( '_s_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function _s_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on _s, use a find and replace
	 * to change '_s' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( '_s', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', '_s' ),
		'menu-2' => esc_html__( 'Main Footer', '_s' ),
		'menu-3' => esc_html__( 'Lower Footer', '_s' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', '_s_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function _s_content_width() {
	$GLOBALS['content_width'] = apply_filters( '_s_content_width', 640 );
}
add_action( 'after_setup_theme', '_s_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
/*
function _s_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', '_s' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', '_s' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', '_s_widgets_init' );
*/

/**
 * Custom Image Size for Banner.
 */

function wpse_setup_theme() {
   add_theme_support( 'post-thumbnails' );
	add_image_size( 'inner-top-banner-size', 9999, 400, true );
	add_image_size( 'inner-bottom-banner-size', 9999, 336, true );
	add_image_size( 'project-large', 2000, 800, false );
	add_image_size( 'front-page_latest_blogp', 582, 446, true );
	add_image_size( 'blog_post_slider_img', 930, 600, true );
}

add_action( 'after_setup_theme', 'wpse_setup_theme' );

function render_scroll_up_button() { ?>
	<div class="row-m scroll-up__wrapper">
		<div class="col-12-m">
			<a href="#top-nav" class="scroll-up">
				<img src="<?= get_template_directory_uri(); ?>/dist/images/work-with-us/icons/arrow.svg">
			</a>
		</div>
	</div>
	<script>
		jQuery(document).ready(function($) {

			function checkIfScrolledTo500() {
				var top_of_screen = $(window).scrollTop();

				if(top_of_screen > 500){
					$('.scroll-up').addClass('show-scroll-up');
				} else {
					$('.scroll-up').removeClass('show-scroll-up');
				}
			}

			// Show affix on scroll.
			window.addEventListener('scroll', function () {
				checkIfScrolledTo500();
			});
		});
	</script>
<?php }

/**
 * Enqueue scripts and styles.
 */
function _s_scripts() {

	$tpl_dir = get_template_directory_uri();

	wp_enqueue_style( '_s-style',			$tpl_dir . '/dist/css/style.css' );
	wp_enqueue_style( '_s-fancyBox-style',	$tpl_dir . '/dist/css/jquery.fancybox.min.css' );
	wp_enqueue_style( '_s-aos-style',		$tpl_dir . '/dist/css/aos.css' );

	$path = '/src/js/';
	$path = '/dist/js/';

	wp_enqueue_script( '_s-all_min',		$tpl_dir . "{$path}all.min.js", array('jquery'), '20190822', true );

	// AOS doesn't like going through Babel so we'll leave it alone
	wp_enqueue_script( '_s-aos',			$tpl_dir . "{$path}aos.js", array(), '20151215', true );


	//wp_enqueue_script( '_s-fancybox',				$tpl_dir . $path . 'fancyBox/jquery.fancybox.min.js', array('jquery'), '20151215', true );
	//wp_enqueue_script( '_s-fancybox-home',		$tpl_dir . $path . 'fancyBox.js', array('jquery'), '20151215', true );
	//wp_enqueue_script( '_s-faq',					$tpl_dir . $path . 'faq.js', array('jquery'), '20151215', true );
	//wp_enqueue_script( '_s-hamburger',			$tpl_dir . $path . 'hamburger.js', array('jquery'), '20151215', true );
	//wp_enqueue_script( '_s-mobilenav',			$tpl_dir . $path . 'mobileNav.js', array('jquery'), '20151215', true );
	//wp_enqueue_script( '_s-skip-link-focus-fix',	$tpl_dir . $path . 'skip-link-focus-fix.js', array(), '20151215', true );
	//wp_enqueue_script( '_s-slick',				$tpl_dir . $path . 'slick.min.js', array('jquery'), '20151215', true );
	//wp_enqueue_script( '_s-slider',				$tpl_dir . $path . 'slider.js', array('jquery'), '20151215', true );
	//wp_enqueue_script( '_s-webfont',				$tpl_dir . $path . 'webfont.js', array(), '20151215', true );
	//wp_enqueue_script( '_s-scripts',				$tpl_dir . $path . 'scripts.js', array('jquery'), '20151215', true );
	//wp_enqueue_script( '_s-workwithus',			$tpl_dir . $path . 'workWithUS.js', array('jquery'), '20151215', true );
	//wp_enqueue_script( '_s-headersearch',			$tpl_dir . $path . 'headerSearch.js', array('jquery'), '20151215', true );

	wp_enqueue_script('_s-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', null, null, true);

	// Additional feature: gallery
	wp_enqueue_style( '_s-style-gallery',			$tpl_dir . '/fix/gallery.css' );
	wp_enqueue_script( '_s-gallery-slick',			$tpl_dir . '/fix/slick.min.js', array('_s-jquery'), '20151215', true );
	wp_enqueue_script( '_s-gallery',			$tpl_dir . '/fix/gallery.js', array('_s-jquery','_s-gallery-slick'), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', '_s_scripts' );

//add_filter( 'cpsh_load_styles', '__return_false' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

function destroy_everything() {
	require_once ('/vendor/autoload.php');
	\Carbon_Fields\Carbon_Fields::boot();
	require_once get_template_directory() . '/inc/carbon-blocks.php';
} //add_action( 'after_theme_setup', 'destroy_everything' );



function add_allowed_origins( $origins ) {
	$origins[] = 'http://localhost:3000';
	$origins[] = 'http://localhost';
	return $origins;
} add_filter( 'allowed_http_origins', 'add_allowed_origins' );

//require_once 'inc/carbon-blocks.php';

function crb_load() {
    include_once 'vendor/autoload.php';
    \Carbon_Fields\Carbon_Fields::boot();
	require_once get_template_directory() . '/inc/carbon-blocks.php';
} add_action( 'after_setup_theme', 'crb_load' );


/**
 * Add Theme Support for wide and full-width images.
 *
 * Add this to your theme's functions.php, or wherever else 
 * you are adding your add_theme_support() functions.
 * 
 * @action after_setup_theme
 */
function jr3_theme_setup() {
	add_theme_support( 'align-wide' );
}
add_action( 'after_setup_theme', 'jr3_theme_setup' );