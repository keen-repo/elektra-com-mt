// Base Gulp File
var gulp = require('gulp'),
		watch = require('gulp-watch'),
		sass = require('gulp-sass'),
		sourcemaps = require('gulp-sourcemaps'),
		cssBase64 = require('gulp-css-base64'),
		cleanCSS = require('gulp-clean-css'),
		path = require('path'),
		notify = require('gulp-notify'),
		browserSync = require('browser-sync'),
		imagemin = require('gulp-imagemin'),
		del = require('del'),
		cache = require('gulp-cache'),
		autoprefixer = require('gulp-autoprefixer'),
		runSequence = require('run-sequence'),
		uglify = require('gulp-uglify'),
		concat = require('gulp-concat'),
		babel = require('gulp-babel'),
		config = require('./package.json');

// Task to compile SCSS
gulp.task('sass', function () {
	return gulp.src('./src/sass/style.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({
			errLogToConsole: false,
			paths: [ path.join(__dirname, 'scss', 'includes') ]
		})
		.on("error", notify.onError(function(error) {
			return "Failed to Compile SCSS: " + error.message;
		})))
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(cssBase64())
		.pipe(autoprefixer())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./src/css/'))
		.pipe(gulp.dest('./dist/css/'))
		.pipe(browserSync.reload({
			stream: true
		}))
		.pipe(notify("SCSS Compiled Successfully :)"));
});

// Task to Copy JS
gulp.task('jscopy', function() {
	return gulp.src('./src/js/**/*.js')
		.pipe(gulp.dest('./dist/js/'));
});

//Task to move slick fonts
gulp.task('addSlickFonts', function() {
	return gulp.src(['./src/slick/**/*']).pipe(gulp.dest('./dist/css/'));
});

//Task to move fancyBox css
gulp.task('addFancyBoxCss', function() {
	return gulp.src(['./src/fancyBox/**/*.css']).pipe(gulp.dest('./dist/css/'));
});

//Task to move AOS css
gulp.task('addAOSCss', function() {
	return gulp.src(['./src/css/aos.css']).pipe(gulp.dest('./dist/css/'));
});

// Minify Images -- NEEDS FIXING!
gulp.task('imagemin', function (){
	return gulp.src('./src/img/**/*.+(png|jpg|jpeg|gif|svg)')
		// Caching images that ran through imagemin
		.pipe(cache(imagemin({
			interlaced: true
		})))
		.pipe(gulp.dest('./dist/images'))
		.pipe(browserSync.reload({
			stream: true
		}));
		//.pipe(notify("Images compressed :)"));
});

// BrowserSync Task (Live reload)
gulp.task('browserSync', function() {
	browserSync({
		proxy: config.homepage
	})
});

// Gulp Watch Task
gulp.task('watch', ['browserSync'], function () {
	gulp.watch('src/sass/**/**/*', {cwd: './'}, ['sass']);
	//gulp.watch('src/js/*.js', {cwd: './'}, ['jscopy']).on('change', browserSync.reload);
	gulp.watch('src/js/*.js', {cwd: './'}, ['babel_it']).on('change', browserSync.reload);
	gulp.watch(['*.php', '**/*.php'], {cwd: './'}).on('change', browserSync.reload);
	gulp.watch('src/images/*', {cwd: './'}, ['imagemin']);
});

// Minify
gulp.task('minify-css', function() {
	return gulp.src('./src/sass/style.scss')
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gulp.dest('./dist/css/'))
		.pipe(notify("CSS Minified Successfully :)"));
});

// Concatenate, Babel & Uglify JS
gulp.task('babel_it', function() {
	var jsScripts = [
		'./src/js/fancyBox/jquery.fancybox.min.js',
		'./src/js/fancyBox.js',
		'./src/js/faq.js',
		'./src/js/hamburger.js',
		'./src/js/mobileNav.js',
		'./src/js/skip-link-focus-fix.js',
		'./src/js/slick.min.js',
		'./src/js/slider.js',
		'./src/js/webfont.js',
		'./src/js/scripts.js',
		'./src/js/headerSearch.js',
		'./src/js/workWithUS.js',
	];
	//return gulp.src( ['./src/js/*.js', '!./src/js/aos.js'] )
	return gulp.src( jsScripts )
		.pipe(concat('all.min.js'))
		// .pipe(babel({presets: ['@babel/env']}))
		// .pipe(uglify())
		.pipe(gulp.dest('./dist/js/'));
});

// Gulp Clean Up Task
gulp.task('clean', function() {
	del('dist');
});

// Gulp Default Task
gulp.task('default', ['watch']);

// Gulp Build Task
gulp.task('build', function() {
	runSequence('clean', 'sass', 'imagemin', 'addSlickFonts', 'addFancyBoxCss', 'addAOSCss', 'minify-css', 'babel_it');
});
