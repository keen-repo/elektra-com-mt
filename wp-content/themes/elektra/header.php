<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package generic
 */

$prefix = K_MB_PREFIX;
$logos = rwmb_meta("{$prefix}header_logo_image", array( 'limit' => 1, 'size'=>'full' ), 6 /*id of homepage*/ );
$logo = reset($logos);
$facebook = rwmb_meta("{$prefix}header_facebook", '', 6 /*id of homepage*/ );
$linkedin = rwmb_meta("{$prefix}header_linkedin", '', 6 /*id of homepage*/ );
$twitter = rwmb_meta("{$prefix}header_twitter", '', 6 /*id of homepage*/ );
$instagram = rwmb_meta("{$prefix}header_instagram", '', 6 /*id of homepage*/ );

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css" media="screen" />
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">
		
		<div class="container">
			<div class="row top-nav">
				<div class="col-4 mobile-nav-hamburger">
					<button class="hamburger hamburger--elastic" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="col-4 top-nav-logo">
					<a href="/">
						<img 
							src="<?php if($logo) { 
									//echo wp_get_attachment_url($logo[0]); 
									echo $logo['url'];
								} else { ?><?= get_template_directory_uri() ?>/dist/images/header/elektra-logo.png <?php } ?>" 
							alt="Elektra Logo">
					</a>
				</div>
				<div class="col-4 search-column">
					<a href="" class="search-mobile">
						<img src="<?= get_template_directory_uri() ?>/dist/images/header/icons/search.svg" alt="Search">
					</a>
					<div class="search-input">
						<?php get_search_form() ?>
					</div>
				</div>
				<div class="col-12-m col-4-l social-icons-column">
					<h4>
						<a href="<?php echo site_url(); ?>/contact-us">Contact us</a>
					</h4>
					<div class="social-icons">
						<?php if($facebook) { ?>
						<a href="<?= $facebook; ?>">
							<img src="<?= get_template_directory_uri() ?>/dist/images/header/icons/facebook.svg" alt="Facebook">
						</a>
						<?php } ?>
						<?php if($linkedin) { ?>
						<a href="<?= $linkedin; ?>">
							<img src="<?= get_template_directory_uri() ?>/dist/images/header/icons/linkedin.svg" alt="Linkedin">
						</a>
						<?php } ?>
						<?php if($twitter) { ?>
						<a href="<?= $twitter; ?>">
							<img src="<?= get_template_directory_uri() ?>/dist/images/header/icons/twitter.svg" alt="Twitter">
						</a>
						<?php } ?>
						<?php if($instagram) { ?>
						<a href="<?= $instagram; ?>">
							<img src="<?= get_template_directory_uri() ?>/dist/images/header/icons/instagram.svg" alt="Instagram">
						</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid horizontal-line"></div>

		<div class="container header-navigation">
			<div class="row-m">
				<div class="col-12-m">
					<nav>
						<div class="nav-desktop">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'menu-1',
									'menu_id'        => 'primary-menu',
								) );
							?>
						</div>

						<div class="nav-mobile">
							<div>
								<div class="nav-mobile-navigation">
									<?php
										wp_nav_menu( array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',
										) );
									?>
								</div>
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
