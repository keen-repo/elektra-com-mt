<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Carbon_Fields\Block;
use Carbon_Fields\Field\Complex_Field;

/** Multiple image with caption and slick slider */
function blog_post_slider_gallery_block() {
	Block::make('post_meta', __( 'Blog Post Slider Gallery' ) )
    ->add_fields( array(
        Field::make( 'media_gallery', 'crb_media_gallery', __( 'Blog Slider Gallery' ) )
    	->set_type( array( 'image', 'video' ) ),
    ) )
    ->set_render_callback( function ( $fields, $attributes, $inner_blocks ) {

		$gallery = $fields['crb_media_gallery'];
        ?>
		<div class="gut-blog-post-sliders">
		<?php foreach ($gallery as $image ) { ?>

			<?php 
				$imgUrl = wp_get_attachment_image_src( $image, 'full' );
				$imgCap =  wp_get_attachment_caption( $image );
				$title = get_the_title($image);
			?>
			<div class="slider-image-wrap">
				<img class="slider-image" src="<?php echo $imgUrl[0]; ?>">
				<?php if ( $imgCap ) : ?>
				<p class="image-caption"><?php echo empty($imgCap) ? $title : $imgCap; ?></p>
				<?php endif; ?>
			</div>

		<?php } ?>
        </div><!-- /.gut-blog-post-sliders -->

        <?php
    } );

} add_action( 'carbon_fields_register_fields', 'blog_post_slider_gallery_block' );

// Old Blog post slider
function blog_post_slider_images_block() {
	Block::make( 'post_meta', 'Blog Post Slider Images' )
		->add_fields(
			[
				Field::make( 'complex', 'blog_post_slider_images', __( 'Blog Post Slider Images' ) )
					->add_fields(
						[
							Field::make( 'image', 'slider_image' ),
							Field::make( 'text', 'image_caption' )
						]
					)
			]
		)
		->set_render_callback( function( $img ) { ?>
			<div class="gut-blog-post-sliders">
				<?php
				$images = $img['blog_post_slider_images'];
				foreach ( $images as $image ) {
					$imgID = $image['slider_image'];
					$imgCap = $image['image_caption'];
					$imgSrc = wp_get_attachment_image_src( $imgID, 'full' );
					$imgURL = $imgSrc[0]; ?>
					<div class="slider-image-wrap">
						<img class="slider-image" src="<?php echo $imgURL; ?>">
						<?php if ( $imgCap ) : ?>
						<p class="image-caption"><?php echo $imgCap; ?></p>
						<?php endif; ?>
					</div>
					<?php
				} ?>
			</div>
			<?php
		});
} add_action( 'carbon_fields_register_fields', 'blog_post_slider_images_block' );


//accordion within tabs
function tabbed_accordions() {
	Block::make( 'block', 'Tabbed Accordions' )
	->add_fields(
		[
			Field::make( 'complex', 'custom_tabs', __( 'Tabs' ) )
				->add_fields(
					[
						Field::make( 'text', 'tab_title' ),
						Field::make( 'rich_text', 'tab_text' ),
					]
				)
				->set_layout( "tabbed-horizontal" )
		]
	)
	->set_render_callback( function( $tabs ) { ?>
		<div class="gut-tabbed_accordions wp-block-advgb-tabs">
			<ul class="tabs">
			<?php foreach($tabs['custom_tabs'] as $key=>$tab) { ?>
				<li class="tab <?=($key==0 ? "is-active" : "") ?>">
					<a id="<?=$key ?>" class="ui-tabs-anchor " href="javascript:void(0)"><?php echo $tab["tab_title"] ?></a>
				</li>
			<?php }	?>
			</ul>

			<div class="tabcontents">
				<?php foreach($tabs['custom_tabs'] as $key=>$tab) { ?>
					<div class="tabcontent <?=($key==0 ? "is-active" : "") ?>" id="tabcontent-<?=$key ?>">
						<p><?= $tab["tab_text"] ?></p>
					</div>
				<?php }	?>
			</div>
		</div>
		<?php
	});
} add_action( 'carbon_fields_register_fields', 'tabbed_accordions' );

