<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package _s
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function _s_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', '_s_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function _s_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', '_s_pingback_header' );

function _s_define_theme_image_sizes(){
	add_image_size( 'full-bg', 2000, 500, true );
}

add_action( 'after_setup_theme', '_s_define_theme_image_sizes' );

/**
* Set default prefix (mainly for meta boxes) and include other helper functionality 
*
* @package _s
*/

define('K_MB_PREFIX', '_kmeta_');

function _s_get_metabox_image_src($field, $ID, $size){
	$image = rwmb_meta( $field, null, $ID );
	if(count($image) > 0 && $image){
		$image = reset($image);
		return wp_get_attachment_image_src( $image['ID'], $size )[0];
	} else return '';
}

function _s_get_featured_image_src($ID, $size){

	$thumb_id = get_post_thumbnail_id( $ID );
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, $size, true);
	$thumb_url = $thumb_url_array[0];

	if(!empty($thumb_url)){
		return $thumb_url;
	} else {
		return '';
	}
}

/**
 * Hide editor for specific pages.
 *
 */

/*
add_action( 'admin_init', '_s_hide_editor' );

function _s_hide_editor() {

	// Get the Post ID.
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	if( !isset( $post_id ) ) return;

	// Get the name of the Page Template file.
	//$template_file = get_post_meta($post_id, '_wp_page_template', true);

    $onPages = array();
    if(in_array($post_id,$onPages)) { 
    	remove_post_type_support('page', 'editor');
    }

}
*/


/**
 * Modify Main Query
 *
 */

/*
add_action( 'pre_get_posts', '_s_modify_main_query' );

function _s_modify_main_query( $query ) {

 	if ( is_post_type_archive( 'post_type_id' ) && $query->is_main_query() && !is_admin() ) { 
		
		$query->query_vars['order'] = 'ASC';
		$query->query_vars['orderby'] = 'menu_order';

 	}

}
*/


/* Custom Theme Helper Functions */

// Change post galleries to slideshows
add_filter('post_gallery', 'ek_post_gallery', 10, 2);

function ek_post_gallery($html, $attr) {

	$order 		= 'ASC';
	$orderby 	= $attr['orderby'];

	$args = array(
		'include' => $attr['include'], 
		'post_status' => 'inherit', 
		'post_type' => 'attachment', 
		'post_mime_type' => 'image', 
		'order' => $order, 
		'orderby' => $orderby
	);

	$attachments = get_posts($args);

	if (empty($attachments)) return '';

	$html = '<div class="pad-top-30 pad-bottom-30 article-gallery-wrap">';
		$html .= '<div class="post-gallery pad-30-m grey-border">';

		foreach ( $attachments as $attachment ) {
			$html .= '<div>'
				.wp_get_attachment_image( $attachment->ID, 'large', false, array('class' => 'center') );

				$caption = wp_get_attachment_caption( $attachment->ID ); 
				if(!empty($caption))
					$html .= '<small class="caption inline-block pad-top-5"><i>' . $caption . '</i></small>';

			$html .= '</div>';
		}

		$html .= '</div>';
	$html .= '</div>';

	return $html;

}


/* Includes */

require get_template_directory() . '/inc/metaboxes.php';
require get_template_directory() . '/inc/ajax.php';
require get_template_directory() . '/inc/shortcodes.php';


?>