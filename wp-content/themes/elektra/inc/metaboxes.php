<?php


add_filter( 'rwmb_meta_boxes', '_s_register_meta_boxes' );

function _s_register_meta_boxes( $meta_boxes ) {

    $prefix = K_MB_PREFIX;

    /**
     * Add gallery metabox
     */

    $meta_boxes[] = array(
        'id' => 'common_gallery',
        'title' => esc_html__( 'Gallery', '_s' ),
        'post_types' => array('post', 'page' ),
        'context' => 'advanced',
        'priority' => 'default',
        'autosave' => 'false',
        'fields' => array(
            // array(
            //     'id' => '{$prefix}gallery_photos',
            //     'type' => 'image_advanced',
            //     'name' => esc_html__( 'Upload Photos', '_s' ),
            //     'desc' => esc_html__( 'Image size: 500px(w) X 333px(h)', '_s' ),
            // ),

            array(
                // 'name'   => __( 'Banner Images:', '' ),
                'id'     => 'banner_images_group',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
    
                // sub-fields
                'fields' => array(
                    array(
                        'name' => __( 'Banner Image:', '' ),
                        'id'   => "{$prefix}banner_images_g",
                        'desc' => 'Image size: 500px(w) X 333px(h)',
                        'type' => 'image_advanced',
                        'max_file_uploads' => '1',
                    ),
                    array(
                        'id' => "{$prefix}banner_images_title",
                        'type' => 'text',
                        'name' => esc_html__( 'Title', '_s'),
                    ),
                    array(
                        'id' => "{$prefix}banner_images_text",
                        'type' => 'text',
                        'name' => esc_html__( 'Subtitle', '_s'),
                    ),
                ),
            ),
        ), 
        'include' => array(
            'template' => array( 'template-residential-commercial.php' ),
        )
       
    );


    /*Start: Header, Footer and Homepage */

    $meta_boxes[] = array(
        'id'         => 'header-fields',
        'title'      => __( 'Header', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Logo',
                'id' => "{$prefix}header_logo_image",
                'type' => 'image',
                'max_file_uploads' => 1
            ),

            array(
                'name' => 'Facebook Link:',
                'id' => "{$prefix}header_facebook",
                'type' => 'text',
            ),
            array(
                'name' => 'LinkedIn Link:',
                'id' => "{$prefix}header_linkedin",
                'type' => 'text',
            ),
            array(
                'name' => 'Twitter Link:',
                'id' => "{$prefix}header_twitter",
                'type' => 'text',
            ),
            array(
                'name' => 'Instagram Link:',
                'id' => "{$prefix}header_instagram",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array( 'template-home.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'page-banners-meta',
        'title'      => __( 'Page Banners', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id' => 'page-banners',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Banner Image:',
                        'id' => "{$prefix}page_banner_image",
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1
                    ),

                    array(
                        'name' => 'Banner Text:',
                        'id' => "{$prefix}page_banner_text",
                        'type' => 'textarea',
                    ),
                    array(
                        'name' => 'Banner Button Text:',
                        'id' => "{$prefix}page_banner_button_text",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Banner Button URL:',
                        'id' => "{$prefix}page_banner_button_url",
                        'type' => 'text',
                    ),
                ),
            ),
        ),
        'include' => array(
            'template' => array( 'template-home.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'homepage-about-us',
        'title'      => __( 'About Us ( homepage )', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Title:',
                'id' => "{$prefix}about_us_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Content:',
                'id' => "{$prefix}about_us_content",
                'type' => 'wysiwyg',
                'raw'     => false,
                'options' => array(
                    'textarea_rows' => 4,
                    'teeny'         => true,
                ),
            ),
            array(
                'name' => 'Button Title:',
                'id' => "{$prefix}about_us_button_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Button URL:',
                'id' => "{$prefix}about_us_button_url",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array( 'template-home.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'our-brands',
        'title'      => __( 'Our Brands', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Title:',
                'id' => "{$prefix}our_brands_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Brand Image:',
                'id' => "{$prefix}our_brands_image",
                'type' => 'image_advanced',
                'max_file_uploads' => 25
            ),
        ),
        'include' => array(
            'template' => array( 'template-brands.php' ),
        )
    );

    /*End: Header, Footer and Homepage */

    /*Start: Inner Page */

    $meta_boxes[] = array(
        'id'         => 'inner-banner',
        'title'      => __( 'Inner Banner - Top', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Inner Banner Title:',
                'id' => "{$prefix}inner_banner_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Inner Banner Image:',
                'id' => "{$prefix}inner_banner_image",
                'type' => 'image_advanced',
                'max_file_uploads' => 1
            ),
        ),
        'exclude' => array(
            'template' => array('template-home.php', ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'inner-bottom-banner',
        'title'      => __( 'Inner Banner - Bottom', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Inner Bottom Banner Title:',
                'id' => "{$prefix}inner_bottom_banner_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Inner Bottom Banner Image:',
                'id' => "{$prefix}inner_bottom_banner_image",
                'type' => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'name' => 'Inner Bottom Banner Image Position:',
                'id' => "{$prefix}inner_bottom_banner_image_position",
                'type' => 'text'
            ),
            array(
                'name' => 'Inner Bottom Banner Content:',
                'id' => "{$prefix}inner_bottom_banner_content",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Inner Bottom Banner Button Title:',
                'id' => "{$prefix}inner_bottom_banner_button_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Inner Bottom Banner Button URL:',
                'id' => "{$prefix}inner_bottom_banner_button_url",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array('template-residential-commercial.php' ),
        )
    );

    /*End: Inner Page */

    /*Start: Services */

    $meta_boxes[] = array(
        'id'         => 'services-list',
        'title'      => __( 'Services List', '_s' ),
        'post_types' => array(  'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id' => 'services-list-items',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Icon:',
                        'id' => "{$prefix}services_list_icon_url",
                        'type' => 'text',
                    ),

                    array(
                        'name' => 'Title:',
                        'id' => "{$prefix}services_list_title",
                        'type' => 'text',
                    ),
                    array(
                        'id' => 'services-list-item',
                        'type' => 'group',
                        'clone'  => true,
                        'sort_clone' => true,
                        'fields' => array(
                            array(
                                'name' => 'List Item:',
                                'id' => "{$prefix}services_list_item",
                                'type' => 'text',
                            ),
                        ),
                    ),
                ),
            ),
            array(
                'name' => 'T&C URL:',
                'id' => "{$prefix}services_list_tc_url",
                'type' => 'text',
            ),
            array(
                'name' => 'Button Title:',
                'id' => "{$prefix}services_list_button_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Button URL:',
                'id' => "{$prefix}services_list_button_url",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array( 'template-services.php' ),
        )
    );

    /*End: Services */

    /*Start: About us */

    $meta_boxes[] = array(
        'id'         => 'about-us',
        'title'      => __( 'About us', '_s' ),
        'post_types' => array(  'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Our vision:',
                'id' => "{$prefix}about_vision",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Our mission:',
                'id' => "{$prefix}about_mission",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Founded:',
                'id' => "{$prefix}about_founded",
                'type' => 'text',
            ),
            array(
                'name' => 'CEO:',
                'id' => "{$prefix}about_ceo",
                'type' => 'text',
            ),
            array(
                'name' => 'Employees:',
                'id' => "{$prefix}about_employees",
                'type' => 'text',
            ),
            array(
                'id' => 'about-timeline',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Date:',
                        'id' => "{$prefix}about_timeline_date",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Content:',
                        'id' => "{$prefix}about_timeline_content",
                        'type' => 'textarea',
                    ),
                ),
            ),
            array(
                'name' => 'Our Services Image:',
                'id' => "{$prefix}about_our_services_image",
                'type' => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'name' => 'Our Services Title:',
                'id' => "{$prefix}about_our_services_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Our Services Paragraph:',
                'id' => "{$prefix}about_our_services",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Our Services Button Text:',
                'id' => "{$prefix}about_our_services_button_text",
                'type' => 'text',
            ),
            array(
                'name' => 'Our Services URL:',
                'id' => "{$prefix}about_our_services_url",
                'type' => 'text',
            ),
            array(
                'name' => 'Our Projects Image:',
                'id' => "{$prefix}about_our_projects_image",
                'type' => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'name' => 'Our Projects Title:',
                'id' => "{$prefix}about_our_projects_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Our Projects Paragraph:',
                'id' => "{$prefix}about_our_projects",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Our Projects Button Text:',
                'id' => "{$prefix}about_our_projects_button_text",
                'type' => 'text',
            ),
            array(
                'name' => 'Our Projects URL:',
                'id' => "{$prefix}about_our_projects_url",
                'type' => 'text',
            ),
            array(
                'name' => 'Shop Image:',
                'id' => "{$prefix}about_shop_image",
                'type' => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'name' => 'Online Shop Title:',
                'id' => "{$prefix}about_shop_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Online Shop Paragraph:',
                'id' => "{$prefix}about_shop",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Online Shop Button Text:',
                'id' => "{$prefix}about_shop_button_text",
                'type' => 'text',
            ),
            array(
                'name' => 'Online Shop URL:',
                'id' => "{$prefix}about_shop_url",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array( 'template-about.php' ),
        )
    );

    /*End: About us */

    /*Start: FAQ */

    $meta_boxes[] = array(
        'id'         => 'lighting-design-faq',
        'title'      => __( 'Lighting and Design FAQ', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id' => 'faq-lighting-design',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Question:',
                        'id' => "{$prefix}faq_lighting_design_question",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Answer:',
                        'id' => "{$prefix}faq_lighting_design_answer",
                        'type' => 'textarea',
                    ),
                ),
            ),
        ),
        'include' => array(
            'template' => array( 'template-faq.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'general-questions-faq',
        'title'      => __( 'General Questions on LEDs FAQ', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id' => 'faq-general',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Question:',
                        'id' => "{$prefix}faq_general_question",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Answer:',
                        'id' => "{$prefix}faq_general_answer",
                        'type' => 'textarea',
                    ),
                ),
            ),
        ),
        'include' => array(
            'template' => array( 'template-faq.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'wiring-accessories-faq',
        'title'      => __( 'Wiring Accessories FAQ', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id' => 'faq-wiring-accessories',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Question:',
                        'id' => "{$prefix}faq_wiring_accessories_question",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Answer:',
                        'id' => "{$prefix}faq_wiring_accessories_answer",
                        'type' => 'textarea',
                    ),
                ),
            ),
        ),
        'include' => array(
            'template' => array( 'template-faq.php' ),
        )
    );

    /*End: FAQ */

    /*Start: Blog Post */

    $meta_boxes[] = array(
        'id'         => 'blog-post',
        'title'      => __( 'Blog Post', '_s' ),
        'post_types' => array( 'post' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Highlight Title:',
                'id' => "{$prefix}blog_post_highlight_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Highlight Content:',
                'id' => "{$prefix}blog_post_highlight_content",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Summary Content:',
                'id' => "{$prefix}blog_post_summary_content",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Additional Image:',
                'id' => "{$prefix}blog_post_images",
                'type' => 'image_advanced',
                'max_file_uploads' => 2
            ),
        ),
    );

    /*End: Blog Post */

    /*Start: Work With Us */

    $meta_boxes[] = array(
        'id'         => 'work-with-us-culture',
        'title'      => __( 'Our Culture', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Title:',
                'id' => "{$prefix}work_with_us_culture_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Content - First Paragraph:',
                'id' => "{$prefix}work_with_us_culture_content_first",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Content - Remaining:',
                'id' => "{$prefix}work_with_us_culture_content_remaining",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Button Text:',
                'id' => "{$prefix}work_with_us_culture_button_text",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array( 'template-work_with_us.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'work-with-us-philosophy',
        'title'      => __( 'Philosophy', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Title:',
                'id' => "{$prefix}work_with_us_philosophy_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Content:',
                'id' => "{$prefix}work_with_us_philosophy_content",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Card 1 Title:',
                'id' => "{$prefix}work_with_us_philosophy_card_1_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Card 1 Content - First Paragraph:',
                'id' => "{$prefix}work_with_us_philosophy_card_1_content_first",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Card 1 Content - Remaining:',
                'id' => "{$prefix}work_with_us_philosophy_card_1_content_remaining",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Card 2 Title:',
                'id' => "{$prefix}work_with_us_philosophy_card_2_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Card 2 Content - First Paragraph:',
                'id' => "{$prefix}work_with_us_philosophy_card_2_content_first",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Card 2 Content - Remaining:',
                'id' => "{$prefix}work_with_us_philosophy_card_2_content_remaining",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Card 3 Title:',
                'id' => "{$prefix}work_with_us_philosophy_card_3_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Card 3 Content - First Paragraph:',
                'id' => "{$prefix}work_with_us_philosophy_card_3_content_first",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Card 3 Content - Remaining:',
                'id' => "{$prefix}work_with_us_philosophy_card_3_content_remaining",
                'type' => 'textarea',
            ),
        ),
        'include' => array(
            'template' => array( 'template-work_with_us.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'work-with-us-principles',
        'title'      => __( 'Principles', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Title:',
                'id' => "{$prefix}work_with_us_principles_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Subtext:',
                'id' => "{$prefix}work_with_us_principles_subtext",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Card 1 Title:',
                'id' => "{$prefix}work_with_us_principles_card_1_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Card 1 Content:',
                'id' => "{$prefix}work_with_us_principles_card_1_content",
                'type' => 'wysiwyg',
            ),
            array(
                'name' => 'Card 2 Title:',
                'id' => "{$prefix}work_with_us_principles_card_2_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Card 2 Content:',
                'id' => "{$prefix}work_with_us_principles_card_2_content",
                'type' => 'wysiwyg',
            ),
        ),
        'include' => array(
            'template' => array( 'template-work_with_us.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'work-with-us-our-story',
        'title'      => __( 'Our Story', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id' => 'work-with-us-story',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Tab Title:',
                        'id' => "{$prefix}work_with_us_story_title",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Tab Content:',
                        'id' => "{$prefix}work_with_us_story_content",
                        'type' => 'textarea',
                    ),
                ),
            ),
        ),
        'include' => array(
            'template' => array( 'template-work_with_us.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'work-with-us-join',
        'title'      => __( 'Join Us', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Title:',
                'id' => "{$prefix}work_with_us_join_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Subtext:',
                'id' => "{$prefix}work_with_us_join_subtext",
                'type' => 'text',
            ),
            array(
                'name' => 'Content:',
                'id' => "{$prefix}work_with_us_join_content",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Banner Button Text:',
                'id' => "{$prefix}work_with_us_join_button_text",
                'type' => 'text',
            ),
            array(
                'name' => 'Banner Button URL:',
                'id' => "{$prefix}work_with_us_join_button_url",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array( 'template-work_with_us.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'work-with-us-activities',
        'title'      => __( 'Our Activities', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Title:',
                'id' => "{$prefix}work_with_us_activities_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Content:',
                'id' => "{$prefix}work_with_us_activities_content",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Gallery:',
                'id' => "{$prefix}work_with_us_activities_image",
                'type' => 'image_advanced',
                'max_file_uploads' => 25
            ),
        ),
        'include' => array(
            'template' => array( 'template-work_with_us.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'work-with-us-our-people',
        'title'      => __( 'Our People', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Title:',
                'id' => "{$prefix}work_with_us_people_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Content:',
                'id' => "{$prefix}work_with_us_people_content",
                'type' => 'textarea',
            ),
            array(
                'id' => 'work-with-us-people',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Image:',
                        'id' => "{$prefix}work_with_us_people_image",
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1
                    ),
                    array(
                        'name' => 'Full Name:',
                        'id' => "{$prefix}work_with_us_people_name",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Position:',
                        'id' => "{$prefix}work_with_us_people_position",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Bio:',
                        'id' => "{$prefix}work_with_us_people_bio",
                        'type' => 'wysiwyg',
                    ),
                    array(
                        'name' => 'LinkedIn URL:',
                        'id' => "{$prefix}work_with_us_people_linkedin",
                        'type' => 'text',
                    ),
                ),
            ),
        ),
        'include' => array(
            'template' => array( 'template-work_with_us.php' ),
        )
    );

    /*End: Work With Us */

    /*Start: Projects */

    /*$meta_boxes[] = array(
        'id'         => 'projects',
        'title'      => __( 'Projects', '_s' ),
        'post_types' => array( 'post', 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Content:',
                'id' => "{$prefix}projects_content",
                'type' => 'textarea',
            ),
            array(
                'name' => 'Button Title:',
                'id' => "{$prefix}projects_button_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Button URL:',
                'id' => "{$prefix}projects_button_url",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array( 'template-projects.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'project-group',
        'title'      => __( 'Projects Slider', '_s' ),
        'post_types' => array( 'post', 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id' => 'projects-group',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Title:',
                        'id' => "{$prefix}project_title",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Show On Homepage:',
                        'id' => "{$prefix}project_show_homepage",
                        'type' => 'checkbox',
                    ),
                    array(
                        'name' => 'Location (Homepage):',
                        'id' => "{$prefix}project_location",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Short Description (Homepage):',
                        'id' => "{$prefix}project_description",
                        'type' => 'textarea',
                        'rows' => 15
                    ),
                    array(
                        'name' => 'Long Description Shown:',
                        'id' => "{$prefix}project_long_description",
                        'type' => 'textarea',
                    ),
                    array(
                        'name' => 'Long Description Hidden:',
                        'id' => "{$prefix}project_long_description_hidden",
                        'type' => 'textarea',
                    ),
                    array(
                        'id' => 'project-videos',
                        'type' => 'group',
                        'clone'  => true,
                        'sort_clone' => true,
                        'fields' => array(
                              array(
                                    'name' => 'Video URL:',
                                    'id' => "{$prefix}project_video_url",
                                    'type' => 'text',
                                    'max_file_uploads' => 1
                                    ),
                              array(
                                    'name' => 'Video Title:',
                                    'id' => "{$prefix}project_video_title",
                                    'type' => 'text',
                                    ),
                              ),
                        ),
                    array(
                        'id' => 'project-slider',
                        'type' => 'group',
                        'clone'  => true,
                        'sort_clone' => true,
                        'fields' => array(
                            array(
                                'name' => 'Image:',
                                'id' => "{$prefix}project_slider_image",
                                'type' => 'image_advanced',
                                'max_file_uploads' => 1
                            ),
                            array(
                                'name' => 'Image Title:',
                                'id' => "{$prefix}project_slider_title",
                                'type' => 'text',
                            ),
                        ),
                    ),
                ),
            ),
        ),
        'include' => array(
            'template' => array( 'template-projects.php' ),
        )
    );*/
    /* ******************************************************************************************************************************** */
    /* ******************************************************************************************************************************** */

    $meta_boxes[] = array(
        'id'         => 'project-group',
        'title'      => __( 'Projects Slider', '_s' ),
        'post_types' => 'projects',
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Title:',
                'id' => "{$prefix}project_title",
                'type' => 'text',
            ),
            array(
                'name' => 'Show On Homepage:',
                'id' => "{$prefix}project_show_homepage",
                'type' => 'checkbox',
            ),
            array(
                'name' => 'Location (Homepage):',
                'id' => "{$prefix}project_location",
                'type' => 'text',
            ),
            array(
                'name' => 'Short Description (Homepage):',
                'id' => "{$prefix}project_description",
                'type' => 'textarea',
                'rows' => 15
            ),
            array(
                'name' => 'Long Description Shown:',
                'id' => "{$prefix}project_long_description",
                'type' => 'wysiwyg',
            ),
            array(
                'name' => 'Long Description Hidden:',
                'id' => "{$prefix}project_long_description_hidden",
                'type' => 'wysiwyg',
            ),
            array(
                'id' => 'project-videos',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                      array(
                            'name' => 'Video URL:',
                            'id' => "{$prefix}project_video_url",
                            'type' => 'text',
                            'max_file_uploads' => 1
                            ),
                      array(
                            'name' => 'Video Title:',
                            'id' => "{$prefix}project_video_title",
                            'type' => 'text',
                            ),
                      ),
                ),
            array(
                'id' => 'project-slider',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Image:',
                        'id' => "{$prefix}project_slider_image",
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1
                    ),
                    array(
                        'name' => 'Image Title:',
                        'id' => "{$prefix}project_slider_title",
                        'type' => 'text',
                    ),
                ),
            ),
        ),
        /*'include' => array(
            'template' => array( 'template-projects.php' ),
        )*/
    );

    /*End: Projects */

    /*Start: Contacts */

    $meta_boxes[] = array(
        'id'         => 'contacts-address',
        'title'      => __( 'Address', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Street:',
                'id' => "{$prefix}contacts_street",
                'type' => 'text',
            ),
            array(
                'name' => 'Town:',
                'id' => "{$prefix}contacts_town",
                'type' => 'text',
            ),
            array(
                'name' => 'Postcode:',
                'id' => "{$prefix}contacts_postcode",
                'type' => 'text',
            ),
            array(
                'name' => 'Country:',
                'id' => "{$prefix}contacts_country",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array( 'template-contacts.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'contacts-details',
        'title'      => __( 'Contact details', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Telephone:',
                'id' => "{$prefix}contacts_phone",
                'type' => 'text',
            ),
            array(
                'name' => 'Email:',
                'id' => "{$prefix}contacts_email",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array( 'template-contacts.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'contacts-opening',
        'title'      => __( 'Opening Hours', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Monday to Friday:',
                'id' => "{$prefix}contacts_monday-friday",
                'type' => 'text',
            ),
            array(
                'name' => 'Saturday:',
                'id' => "{$prefix}contacts_saturday",
                'type' => 'text',
            ),
            array(
                'name' => 'Sunday:',
                'id' => "{$prefix}contacts_sunday",
                'type' => 'text',
            ),
        ),
        'include' => array(
            'template' => array( 'template-contacts.php' ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'contacts-directions',
        'title'      => __( 'Directions', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'By Car Title:',
                'id' => "{$prefix}contacts_by_car_title",
                'type' => 'text',
            ),
            array(
                'name' => 'By Car Content:',
                'id' => "{$prefix}contacts_by_car_content",
                'type' => 'textarea',
            ),
            array(
                'name' => 'By Bus Title:',
                'id' => "{$prefix}contacts_by_bus_title",
                'type' => 'text',
            ),
            array(
                'name' => 'By Bus Content:',
                'id' => "{$prefix}contacts_by_bus_content",
                'type' => 'textarea',
            ),
        ),
        'include' => array(
            'template' => array( 'template-contacts.php' ),
        )
    );

    /*End: Contacts */

    /*Start: Brands */
    $meta_boxes[] = array(
        'id'         => 'brands',
        'title'      => __( 'Brands', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name' => 'Brand Logo:',
                'id' => "{$prefix}brand_logo_image",
                'type' => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'name' => 'Brand Content:',
                'id' => "{$prefix}brand_content",
                'type' => 'textarea',
            ),
        ),
        'include' => array(
            'template' => array( 'template-brands.php' ),
        )
    );

    /*Brand Projects*/
    $meta_boxes[] = array(
        'id'         => 'brands-products',
        'title'      => __( 'Products', '_s' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id' => 'brand-product',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Product Title:',
                        'id' => "{$prefix}brand_project_title",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Project Content:',
                        'id' => "{$prefix}brand_product_content",
                        'type' => 'textarea',
                    ),
                    array(
                        'name' => 'Colour:',
                        'id' => "{$prefix}brand_product_colour",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Amperage:',
                        'id' => "{$prefix}brand_product_amp",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Voltage:',
                        'id' => "{$prefix}brand_product_voltage",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Width:',
                        'id' => "{$prefix}brand_product_w",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Height:',
                        'id' => "{$prefix}brand_product_h",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Price:',
                        'id' => "{$prefix}brand_product_price",
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Project Image:',
                        'id' => "{$prefix}brand_product_image",
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1
                    ),
                ),
            ),
        ),
        'include' => array(
            'template' => array( 'template-brands.php' ),
        )
    ); /* End Brand Projects*/

    /*End: Brands */

    // Curator Feed Code Input
    $meta_boxes[] = array(
        'id'            => 'curator_io',
        'title'         => esc_html__( 'Curator.io', '_s' ),
        'post_types'    => array( 'page' ),
        'context'       => 'normal',
        'priority'      => 'default',
        'autosave'      => 'false',
        'fields'        => array(
            array(
                'id'            => $prefix . 'curator_code',
                'type'          => 'text',
                'name'          => esc_html__( 'Curator Feed ID', '_s' ),
                'desc'          => esc_html__( 'Please insert the Curator Feed ID here. Curator Settings: FEED: GRID, STYLE: SYDNEY. Rows: 1; Min post width: 325 (for 5 images).', '_s' ),
                'placeholder'   => esc_html__( 'e.g. 98e53c40-bcbc-aa94-43b8-ac2dedb411cf', '_s' ),
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'                =>  'blog_post_slider',
        'title'             =>  esc_html__( 'Blog Post Slider', '_s' ),
        'type'              =>  'block',
        'category'          =>  'layout',
        'context'           =>  'side',
        'render_template'   =>  get_template_directory() . '/g-blocks/blog-post-slider.php',
        'autosave'          =>  'true',
        'supports'          =>  array(
                                    'align' => array( 'wide', 'full' ),
                                ),

        'fields'            => array(
            //'id'              => 'slider_images',
            //'type'                => 'group',
            //'clone'               => true,
            //'sort_clone'      => true,
            //'fields'          => array(
                array(
                    'id'                    => $prefix . 'slider_image',
                    'type'                  => 'image_advanced',
                    'name'                  => esc_html__( 'Slider Image', '_s' ),
                    'max_file_uploads'      => '1',
                ),
                array(
                    'id'                    => $prefix . 'image_desc',
                    'type'                  => 'text',
                    'name'                  => esc_html__( 'Image Description', '_s' ),
                ),
            //),
        ),
    );

    return $meta_boxes;

}



?>
