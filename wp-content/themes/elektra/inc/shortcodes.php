<?php 

add_shortcode( 'gallery', 's_gallery' );

function s_gallery() {
    ob_start();
    get_template_part( 'template-parts/components/gallery' );
    return ob_get_clean();   
} 
