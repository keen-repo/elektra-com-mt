<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package generic
 */

get_header();

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php get_template_part( 'template-parts/components/inner-top-banner', 'none' ); ?>

			<?php
			while ( have_posts() ) : the_post();

				if(is_singular('projects')) {
					get_template_part( 'template-parts/content-projects', get_post_format() );
				} else {
					 get_template_part( 'template-parts/content-single', get_post_format() );
				}

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
