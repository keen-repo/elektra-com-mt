jQuery(document).ready(function($){
    // Home slider
    $('.home-slider').slick({
        prevArrow: '<img src="wp-content/themes/elektra/dist/images/arrows/arrow-left-white.svg" class="slick-prev">',
        nextArrow: '<img src="wp-content/themes/elektra/dist/images/arrows/arrow-right-white.svg" class="slick-next">',
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    // Our Projects Slider
    $('.our-projects-slider-slick').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });
    // Our Projects Image Slider
    let background = $('#our-projects-slider').find('.our-projects-slide-slick.slick-active').attr('data-background');
    $('.our-projects__wrapper').css('background', 'url("'+background+'") no-repeat center center');
    $('.our-projects-slider-slick').on('afterChange', function(){
        let background = $(this).find('.our-projects-slide-slick.slick-active').attr('data-background');
        $('.our-projects__wrapper').css('background', 'url("'+background+'") no-repeat center center');
    });

    // Our Brands Slider
    $('.our-brands-slider').slick({
        prevArrow: '<img src="wp-content/themes/elektra/dist/images/arrows/arrow-left-black.svg" class="slick-prev">',
        nextArrow: '<img src="wp-content/themes/elektra/dist/images/arrows/arrow-right-black.svg" class="slick-next">',
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 1030,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            }
        ]
    });

    // All Projects Slider
    $('.all-projects-slider').slick({
        prevArrow: '<img src="wp-content/themes/elektra/dist/images/arrows/arrow-left-white.svg" class="slick-prev">',
        nextArrow: '<img src="wp-content/themes/elektra/dist/images/arrows/arrow-right-white.svg" class="slick-next">',
        // autoplay: true,
        autoplaySpeed: 5000,
        variableWidth: true,
        centerMode: true,
        arrows: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    // Our Activities Slider
    $('.our-activities-slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 770,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            }
        ]
	});

	 // Brand Slider
	$('.brand-product-slider').slick({
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true,
		infinite: true,
		autoplay: false,
		autoplaySpeed: 2000,
		arrows: true,
		prevArrow: '<img src="wp-content/themes/elektra/dist/images/arrows/arrow-left-gray.svg" class="slick-prev">',
		nextArrow: '<img src="wp-content/themes/elektra/dist/images/arrows/arrow-right-grey.svg" class="slick-next">',
		responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 770,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 1,
                settings: "unslick"
            }
        ]
	  });

	// Slider for Custom Gutenberg Block
	$('.gut-blog-post-sliders').slick({
		//autoplay: true,
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		centerMode: true,
		adaptiveHeight: true
	});

	$('.blog-post-sliders-123').slick({
		autoplay: true,
	});

});







