jQuery(document).ready(function($) {
    // Navigation
    let workWithUsNavNumber = 1;
    let workWithUsNavAttribute = ".nav-list-inner-1";

    $(".work-with-us-nav-list").click(function(event) {
        $(workWithUsNavAttribute).removeClass("work-with-us-nav-active");

        workWithUsNavAttribute = event.target;
        workWithUsNavNumber = $(workWithUsNavAttribute).attr("class").slice(-1);

        $(workWithUsNavAttribute).addClass("work-with-us-nav-active");
    });

    // Our Culture Read More button
    $("#work-culture-btn").click(function() {
        // Adds additional Our Culture text
        $(".work-with-us-culture-addition").toggle();

        // Toggles arrow button
        $("#work-culture-btn").toggleClass("arrow-btn");
    });

    let mobileViewPhilosophy = false;

    if ($(window).width() <= 979) {
        mobileViewPhilosophy = true;
    } else {
        mobileViewPhilosophy = false;
    }

    $(window).resize(function() {
        if ($(window).width() <= 979) {
            mobileViewPhilosophy = true;
        } else {
            mobileViewPhilosophy = false;
        }
    });

    // Our Philosophy Columns

    // Evolve
    $("#find-out-more-evolve").click(function(event) {
        // Adds additional Evolve text
        $(".work-with-us-philosophy-evolve .work-with-us-philosophy-addition").toggle();

        $("#find-out-more-evolve").toggleClass("arrow-btn");

        if (!mobileViewPhilosophy) {
            $(".work-with-us-philosophy-excite").toggleClass("hide-column");
            $(".work-with-us-philosophy-energise").toggleClass("hide-column");
        }

        $(".work-with-us-philosophy-evolve").toggleClass("wide-column");
    });

    // Excite
    $("#find-out-more-excite").click(function(event) {
        // Adds additional Excite text
        $(".work-with-us-philosophy-excite .work-with-us-philosophy-addition").toggle();

        $("#find-out-more-excite").toggleClass("arrow-btn");

        if (!mobileViewPhilosophy) {
            $(".work-with-us-philosophy-evolve").toggleClass("hide-column");
            $(".work-with-us-philosophy-energise").toggleClass("hide-column");
        }

        $(".work-with-us-philosophy-excite").toggleClass("wide-column");
    });

    // Energise
    $("#find-out-more-energise").click(function(event) {
        // Adds additional Energise text
        $(".work-with-us-philosophy-energise .work-with-us-philosophy-addition").toggle();

        $("#find-out-more-energise").toggleClass("arrow-btn");

        if (!mobileViewPhilosophy) {
            $(".work-with-us-philosophy-evolve").toggleClass("hide-column");
            $(".work-with-us-philosophy-excite").toggleClass("hide-column");
        }

        $(".work-with-us-philosophy-energise").toggleClass("wide-column");
    });

    // Our Story Columns
    let mobileView = false;

    if ($(window).width() <= 1199) {
        mobileView = true;
    } else {
        mobileView = false;
    }

    $(window).resize(function() {
        if ($(window).width() <= 1199) {
            mobileView = true;
        } else {
            mobileView = false;
        }
    });

    let ourStoryNumber = 1;
    let ourStoryContent = ".our-story-content-1";
    let ourStoryAttribute = ".tab_1";
    let ourStoryAttributeHref = "#our-story-content-id-1";

    $(ourStoryAttribute).addClass("our-story-clicked");
    $(ourStoryContent).addClass("active-content");

    $(".our-story-list").click(function(event) {
        if (!mobileView) {
            event.preventDefault();
        }

        $(ourStoryAttribute).removeClass("our-story-clicked");
        $(ourStoryContent).removeClass("active-content");

        ourStoryAttribute = event.target;
        ourStoryNumber = $(ourStoryAttribute).attr("class").slice(-1);
        ourStoryContent = ".our-story-content-" + ourStoryNumber;

        if (mobileView) {
            ourStoryAttributeHref = "#our-story-content-id-" + ourStoryNumber;
            $(ourStoryAttribute).attr("href", ourStoryAttributeHref );
        }

        $(ourStoryAttribute).addClass("our-story-clicked");
        $(ourStoryContent).addClass("active-content");
    });

    // Navigation hover
    // $(".work-with-us-nav a div").hover(function() {
    //     $(this).addClass("work-with-us-nav-active");
    // },
    // function() {
    //     $(this).removeClass("work-with-us-nav-active");
    // });

    // Smooth scroll
    $('a[href*=\\#]:not([href=\\#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
            || location.hostname == this.hostname) {

            let target = $(this.hash);
            target = target.length ? target : $('[id=' + this.hash.slice(1) +']');
            if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - 100
            }, 1000);
            return false;
            }
        }
    });

    //nav deco
    function checkIfInViewport(element, item) {
        var top_of_element = $(element).offset().top - 120;
        var top_of_screen = $(window).scrollTop();

        if(top_of_element < top_of_screen){
            $('.work-with-us-nav-list div').removeClass('work-with-us-nav-active');
            $(item).addClass('work-with-us-nav-active');
        }
    }

    function checkIfScrolledTo500() {
        var top_of_screen = $(window).scrollTop();

        if(top_of_screen > 500){
            $('.scroll-up').addClass('show-scroll-up');
        } else {
            $('.scroll-up').removeClass('show-scroll-up');
        }
    }

    // Show affix on scroll.
    let affixElement = document.getElementById('workWithUsNav')
    if (affixElement !== null) {
      let position = $('#workWithUsNav').position()
      window.addEventListener('scroll', function () {
        var height = $(window).scrollTop()
        if (height > position.top) {
          $('#workWithUsNav').addClass('affix');
        } else {
          $('#workWithUsNav').removeClass('affix');
        }

        checkIfInViewport('#our-culture', '.nav-list-inner-1');
        checkIfInViewport('#our-philosophy', '.nav-list-inner-2');
        checkIfInViewport('#our-values-principles', '.nav-list-inner-3');
        checkIfInViewport('#our-story', '.nav-list-inner-4');
        checkIfInViewport('#join-us', '.nav-list-inner-5');
        checkIfInViewport('#our-activities', '.nav-list-inner-6');
        checkIfInViewport('#our-people', '.nav-list-inner-7');

        checkIfScrolledTo500();

      })
    }

});
