<?php /* Template Name: Brands*/

get_header();

$prefix = K_MB_PREFIX;

$products = rwmb_meta( 'brand-product');

$brand_logo_image = rwmb_meta("{$prefix}brand_logo_image", array( 'limit' => 1, 'size'=>'full' ));
$brand_logo_image = reset($brand_logo_image);


$brand_content = rwmb_meta("{$prefix}brand_content");



?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php get_template_part( 'template-parts/components/inner-top-banner-brand', 'none' ); ?>

			<section class="brand-short-description">
				<div class="container" >
					<div class="row-m">
						<div class="col-12-m">
							<div class="brand-logo" style="background: url(<?= $brand_logo_image['url']; ?>) no-repeat center center;" ></div>
						</div>
					</div>
					<div class="row-m">
						<div class="col-12-m">
							<p><?php echo $brand_content; ?></p>
						</div>
					</div>
				</div>
			</section>

			<section class="brands-content">
				<div class="container" >
					<?php the_content(get_the_ID()); ?>
				</div>
			</section>

			<section class="brands-products">
				<div class="container" >
					<h3>Products</h3>
				</div>
			</section>


			<div class="brands-products brand-product-slider">
				<?php


					if($products) {
						foreach ($products as $product) {

							if(empty($product["{$prefix}brand_project_title"])){
								$title = '';
							} else {
								$title 	= $product["{$prefix}brand_project_title"];
							}

							if(empty($product["{$prefix}brand_product_content"])){
								$description = '';
							} else {
								$description = $product["{$prefix}brand_product_content"];
							}

							if(empty($product["{$prefix}brand_product_colour"])){
								$colour = '';
							} else {
								$colour = $product["{$prefix}brand_product_colour"];
							}

							if(empty($product["{$prefix}brand_product_amp"])){
								$amp = '';
							} else {
								$amp = $product["{$prefix}brand_product_amp"];
							}

							if(empty($product["{$prefix}brand_product_voltage"])){
								$volt = '';
							} else {
								$volt = $product["{$prefix}brand_product_voltage"];
							}

							if(empty($product["{$prefix}brand_product_w"])){
								$width = '';
							} else {
								$width = $product["{$prefix}brand_product_w"];
							}

							if(empty($product["{$prefix}brand_product_h"])){
								$height = '';
							} else {
								$height = $product["{$prefix}brand_product_h"];
							}

							if(empty($product["{$prefix}brand_product_price"])){
								$price = '';
							} else {
								$price 	= $product["{$prefix}brand_product_price"];
							}

							if(empty($product["{$prefix}brand_product_image"])){
								$image_id = '';
								$image_url = '';
							} else {
								$image_id = $product["{$prefix}brand_product_image"];
								$image_url = wp_get_attachment_url($image_id[0]);
							}


				?>

				<div class="slide-box">
					<div>
						<img src="<?= $image_url; ?>" width="433" height="433">
						<div class="right-text">
							<?php
							if ( !$title ) { ?>
							<h5><?= empty( $title) ? "" : "" ?></h5>
							<?php } ?>
							<?= $description; ?>
							<?php
							if ( $colour or $amp or $volt or $width or $width ) { ?>
							<ul>
								<li><strong><?php echo empty($colour) ? "" : "Colour:" ?></strong> <?= $colour; ?></li>
								<li><strong><?php echo empty($amp) ? "" : "Amperage:" ?></strong> <?= $amp; ?></li>
								<li><strong><?php echo empty($volt) ? "" : "Voltage:" ?></strong> <?= $volt; ?></li>
								<li><strong><?php echo empty($width) ? "" : "Width:" ?></strong> <?= $width; ?></li>
								<li><strong><?php echo empty($height) ? "" : "Height:" ?></strong> <?= $height; ?></li>
							</ul>
							<?php
							}
							if ( $price ) { ?>
							<div class="price"><?= $price; ?></div>
							<?php } ?>
						</div>
					</div>
				</div>

				<?php
						}
					}
				?>

			</div>

			<section>
				<div class="container" style="padding-top:50px; padding-bottom:50px; ">
					<div style="display: flex; justify-content: center;">
						<button class="button-main brand-button" onclick="location.href='/contact-us';"><i class="icon-brand"></i>REQUEST QUOTE</button>
					</div>
				</div>
			</section>


		</main><!-- #main -->
	</div><!-- #primary -->
<?php
	get_footer();
?>
