<?php /* Template Name: Homepage */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php get_template_part( 'template-parts/content-home', 'none' ); ?>

		<?php
		// WP_Query arguments
		$args = array(
			'post_type'			=> 'post',
			'posts_per_page'	=> 1
		);

		// The Query
		$query = new WP_Query( $args ); ?>

		<div class="latest-blog-post">
		<?php
		// The Loop
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post(); ?>
				<?php
				$exc = get_the_excerpt();
				$exc = wp_trim_words( $exc, 50, null );
				?>
				<h3 class="el-lp-blog-title">BLOG</h3>
				<div class="el-lp-wrapper">
					<div class="el-lp-thumb"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'front-page_latest_blogp' ); ?></a></div>
					<div class="el-lp-content">
						<div class="el-lp-title"><?php the_title(); ?></div>
						<div class="el-lp-date"><?php echo get_the_date( 'd.m.y' ); ?></div>
						<div class="el-lp-excerpt"><?php echo $exc; ?></div>
						<a href="<?php the_permalink(); ?>" class="el-lp-readmore">read more</a>
					</div>
				</div>
			<?php
			} ?>
		</div>
		<?php
		} else {
			// no posts found
		}

		// Restore original Post Data
		wp_reset_postdata(); ?>


		<?php
		$prefix = K_MB_PREFIX;
		$curatorCode = rwmb_meta( $prefix . 'curator_code' );

		if ( $curatorCode ) : ?>
			<!-- Place <div> tag where you want the feed to appear -->
			<div id="curator-feed-default-layout"><a href="https://curator.io" target="_blank" class="crt-logo crt-tag">Powered by Curator.io</a></div>
		<?php
		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

?>
