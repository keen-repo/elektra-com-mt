<div class="row-m send-message-row">
    <div class="col-12-m">
        <h3>Send us a message below</h3>

        <?= do_shortcode('[formidable id=4]'); ?>

    </div>
</div>