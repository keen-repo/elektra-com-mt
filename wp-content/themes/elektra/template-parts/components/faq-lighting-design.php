<?php 

$prefix = K_MB_PREFIX;

$faq = rwmb_meta('faq-lighting-design');
$counter = 0;

?>


<div class="faq-questions-content faq-questions-content-1 faq-content-active">

	<?php 

	if ( ! empty( $faq ) ) {
    foreach ( $faq as $group_value ) {
    $question = $group_value["{$prefix}faq_lighting_design_question"];
    $answer = $group_value["{$prefix}faq_lighting_design_answer"];
    $counter = $counter + 1;
	?>
	<div class="row-m faq-questions-row faq-questions-row-<?= $counter; ?>">
		<div class="col-12-m faq-questions__wrapper">
			<div class="faq-questions-icon faq-image-plus faq-questions-image-show">
				<img src="<?= get_template_directory_uri() ?>/dist/images/faq/icons/plus-icon.svg">
			</div>
			<div class="faq-questions-icon faq-image-minus">
				<img src="<?= get_template_directory_uri() ?>/dist/images/faq/icons/minus-icon.svg">
			</div>

			<div>
				<h3><?= $question; ?></h3>
				
				<div class="faq-questions-content-addition faq-questions-content-addition-<?= $counter; ?>">
					<?= $answer; ?>
				</div>
			</div>
		</div>
	</div>
	<?php }} ?>

	
</div>