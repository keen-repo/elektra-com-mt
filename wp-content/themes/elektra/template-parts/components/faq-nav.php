<section class="faq-sections">
    <div class="container">
        <div class="row-m faq-sections-row">
            <div class="col-12-m">
                <ul class="faq-sections-nav">
                    <li><a href="#" class="faq-sections-nav-section faq-sections-nav-section-1 faq-questions-active">Lighting and Design</a></li>
                    <li></li>
                    <li><a href="#" class="faq-sections-nav-section faq-sections-nav-section-2">General Questions on LEDs</a></li>
                    <li></li>
                    <li><a href="#" class="faq-sections-nav-section faq-sections-nav-section-3">Wiring Accessories</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>