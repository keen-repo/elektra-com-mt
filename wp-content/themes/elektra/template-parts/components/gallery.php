<?php 
$prefix = K_MB_PREFIX;
// $images = rwmb_meta( '{$prefix}gallery_photos', array( 'size' => 'full' ) );
global $post;
$images = get_post_meta($post->ID, 'banner_images_group', true);


?>
<section class="box-gallery">
	<div class="inner">
		<div class="sgallery">
			<?php foreach ($images as $key => $image): ?>
				<?php 
					$id = $image['_kmeta_banner_images_g'][0];
				    $src = wp_get_attachment_image_src( $id, 'full' )[0];
				    $srcset = wp_get_attachment_image_srcset( $id, 'full' );
				    $sizes = wp_get_attachment_image_sizes( $id, 'full' );
				    $alt = get_post_meta( $id, '_wp_attachment_image_alt', true); 

				    $caption = $image['_kmeta_banner_images_text'];
				    $title = $image['_kmeta_banner_images_title'];

				?>
				<div class="sgallery-item first image-<?php echo $id; ?>" >
					<?php if ( !empty( $title ) ): ?>
						<h2 class="title"><?php echo $title; ?></h2>
					<?php endif; ?>
					<img src="<?php echo esc_attr( $src );?>"
			        srcset="<?php echo esc_attr( $srcset ); ?>"
			        sizes="<?php echo esc_attr( $sizes );?>"
			        alt="<?php echo esc_attr( $alt );?>" />
			        
					<?php if ( !empty( $caption ) ): ?>
						<h3 class="caption"><?php echo $caption; ?></h3>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

