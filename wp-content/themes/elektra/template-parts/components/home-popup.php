<section class="home-popup">
	<div id="home-popup">
		<div class="container">
			<div class="row-xl">
				<div class="col-6-xl">
					<img src="<?= get_template_directory_uri(); ?>/dist/images/popup/elektra-logo.png" alt="Elektra logo">
					
					<div class="popup-ribbon">
						<h2>Join our newsletter</h2>
					</div>
					
					<p>It only takes a second to be the first to find out about our latest news and promotions…</p>

					<?= do_shortcode("[formidable id=6]"); ?>
				</div>
			</div>
		</div>
	</div>
</section>