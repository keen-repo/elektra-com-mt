<?php
$prefix = K_MB_PREFIX;
//_kmeta_project_show_homepage
$projects = rwmb_meta ( 'projects-group', '', 96 /*post_id of projects page*/ );
$counter = 1;
?>

<section class="our-projects__wrapper">
	<div class="container">
		<div class="row-m">
			<div class="our-projects">
				<div class="our-projects-overlay"></div>
				<div class="our-projects-slider">

					<div class="our-projects-slide">
						<h3>Our projects</h3>
						<div id="our-projects-slider" class="our-projects-slider-slick">
						<?php
						if ( $projects ) {
							foreach ( $projects as $project ) {
								if ( isset( $project[ "{$prefix}project_show_homepage" ] ) && $counter <= 3 ) {
									$counter = $counter + 1;
									$title = $project[ "{$prefix}project_title" ];
									$description = $project[ "{$prefix}project_description" ];
									$slider = $project[ 'project-slider' ];
									$image_id = $slider[ 0 ][ "{$prefix}project_slider_image" ];
									$image_url = wp_get_attachment_url ( $image_id[ 0 ] );
									?>
									<div class="our-projects-slide-slick" data-background="<?= $image_url; ?>">
										<div>
											<h4><a href="<?php echo get_permalink ( 96 ); ?>#<?= sanitize_title ( $title ); ?>"><?= $title; ?></a></h4>
											<p><?= $description; ?></p>
										</div>
									</div>
									<?php
								}
							}
						} ?>
						</div>
					</div>
					<button onclick="location.href='/projects'" class="button-main read-more">View Projects</button>
				</div>
			</div>
		</div>
	</div>
</section>
