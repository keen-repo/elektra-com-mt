<?php 

$prefix = K_MB_PREFIX;

$backgrounds = rwmb_meta("{$prefix}inner_bottom_banner_image", array( 'limit' => 1, 'size'=>'full' ));
$background = reset($backgrounds);
$background_position = rwmb_meta("{$prefix}inner_bottom_banner_image_position");
$title = rwmb_meta("{$prefix}inner_bottom_banner_title");
$content = rwmb_meta("{$prefix}inner_bottom_banner_content");
$button_title = rwmb_meta("{$prefix}inner_bottom_banner_button_title");
$button_url = rwmb_meta("{$prefix}inner_bottom_banner_button_url");

if(!(empty($content) && empty($button_url))){
?>

<section class="residential-online-shop" style="background: url(<?= $background['url']; ?>) no-repeat <?= $background_position; ?>;">                
    <div class="container">
        <div class="row-m">
            <div class="col-6-m residential-online-shop-left">
                
                <?php if($title) { ?>
                    <h2><?= $title; ?></h2>
                <?php } ?>

                <?php if($content) { ?>
                    <p><?= $content; ?></p>
                <?php } ?>

                <?php if($button_url) { ?>
                    <button class="button-main commercial" onclick="location.href='<?= $button_url ?>';"><?= $button_title; ?></button>
                <?php } ?>

            </div>
            <div class="col-6-m residential-online-shop-right"></div>
        </div>
    </div>
</section>

<?php } ?>