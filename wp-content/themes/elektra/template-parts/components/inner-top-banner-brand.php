<?php

$prefix = K_MB_PREFIX;

if(is_singular('projects') || is_archive('projects')) {
	$background["url"] = "/wp-content/uploads/2018/10/projects-main-banner-image-2000x400.jpg";
	$title = "Projects";
	$htag = 'h2';
} else {

	$htag = 'h1';

	$backgrounds = rwmb_meta("{$prefix}inner_banner_image", array( 'limit' => 1, 'size'=>'inner-top-banner-size' ));
	$background = reset($backgrounds);
	$title = rwmb_meta("{$prefix}inner_banner_title");

}
?>

<section class="brands-banner" style="background: url(<?= $background['url']; ?>) no-repeat center center;">
    <div class="container-fluid">
        <div class="row-m overlay-half">
            <div class="col-12-m brands-banner-title">
				<!-- change the h1 to h2 on single pages -->
				<<?=$htag?>><?= $title; ?></<?=$htag?>>
			</div>
			<div class="col-12-m">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
				?>
			</div>
        </div>
    </div>
</section>
