<?php
$prefix = K_MB_PREFIX;
$highlight_title = rwmb_meta("{$prefix}blog_post_highlight_title");
$highlight_content = rwmb_meta("{$prefix}blog_post_highlight_content");

?>
<div class="container-fluid">
	<div class="container">
		<div class="row-l newest-blog-post">
			<div class="col-6-l">
				<div class="blog-image">
					<img src="<?= esc_url(get_the_post_thumbnail_url(get_the_ID(),'full')); ?>" alt="Light bulb">
				</div>
			</div>
			<div class="col-6-l blog-text">
				<h2><?php the_title(); ?></h2>
				<p><?= get_the_date("d.m.Y"); ?></p>
				<?php if ( $highlight_title ) { ?>
				<h4><?= $highlight_title; ?></h4>
				<?php } ?>
				<p>
				<?php if ( $highlight_content ) {
					echo wp_trim_words( $highlight_content, 55, null );
				} else {
					$output = wp_trim_words(get_the_content(), 55, '...');
					$output = wp_strip_all_tags( $output );
					echo $output; //str_replace( '(more&hellip;)', '', wp_trim_words(get_the_content(), 55, '...'));
				} ?>
				</p>
				<a href="<?= get_permalink(); ?>" class="button-main read-more-blog">Read more</a>
			</div>
		</div>
	</div>
</div>
