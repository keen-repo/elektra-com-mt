<?php
$prefix = K_MB_PREFIX;

$project = get_post_meta($post->ID);

$slider = unserialize( $project['project-slider'][0] );
$image_id = $slider[0]["{$prefix}project_slider_image"];
$image_url = wp_get_attachment_url($image_id[0]);

?>

<div class="container-fluid">
    <div class="container">
        <div class="row-l newest-blog-post">
            <div class="col-6-l">
                <div class="blog-image">
					<img src="<?= $image_url; ?>" alt="<?= $slider[0]['_kmeta_project_slider_image'][0]; ?>">
                </div>
            </div>
            <div class="col-6-l blog-text">
					<h2> <?php the_title(); ?> Main </h2>
					<p></p>
					<p><?php echo $project['_kmeta_project_description'][0]; ?></p>

					<a href="<?= get_permalink(); ?>" class="button-main read-more-blog">Read more</a>
            </div>
        </div>
    </div>
</div>
