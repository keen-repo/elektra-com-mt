<?php
$prefix = K_MB_PREFIX;

$slug = $post->post_type;
$button_url = get_site_url().'/'.$slug;

$project = get_post_meta($post->ID);

$title = $project["{$prefix}project_title"][0];
$long_description = $project["{$prefix}project_long_description"][0];
$long_description_hidden = $project["{$prefix}project_long_description_hidden"][0];

$videos =  unserialize($project["project-videos"][0]);

$sliders = unserialize( $project['project-slider'][0] );
$image_id = $slider[0]["{$prefix}project_slider_image"];
$image_url = wp_get_attachment_url($image_id[0]);

$counter = 0;

/*echo '<pre>';
var_dump($sliders);
echo '/<pre>';*/
?>

<section class="projects-projects">
    <div class="container container-fluid">


		<div class="row-m" id="<?= sanitize_title($title); ?>" name="<?= sanitize_title($title); ?>">
			<div class="col-12-m">
				<div class="">
					<h1 class="text-right"><?= $title; ?></h1>
				</div>
			</div>
		</div>

		<?php if(!empty($long_description)){ ?>
			<div>
				<div class="text-center">
					<div class="row-m project-description">
						<div class="col-12-m">
							<?= apply_filters('the_content', $long_description); ?>

							<?php if(!empty($long_description_hidden)){ ?>
								 
								<div id="toggle_tst" class="project-addition">
									<?= apply_filters('the_content', $long_description_hidden); ?>
								</div>
								<button id="show_hide" class="button-main project-addition-btn read-more-project"><?php if( $button_text ) { echo $button_text; } else { echo "Read More"; } ?></button>
								<?php /* <button class="button-main project-addition-btn read-more-project"><?php if( $button_text ) { echo $button_text; } else { echo "Read More"; } ?></button> */ ?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>


		<div class="row-m projects-projects-row" style="padding-top:15px">
			<div class="col-12-m">
				<div class="all-projects-slider">
						<?php
						if($videos) {
							foreach($videos as $video) {
								$video_url = $video["{$prefix}project_video_url"];
								$video_title = $video["{$prefix}project_video_title"];
						?>
						<div class="projects-slider-slide keen-video">
							<iframe src="<?= $video_url; ?>" width="1120" height="768" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							<?php if ($video_title) : ?>
							<div class="slide-text">
								<p><?= $video_title; ?></p>
							</div>
							<?php endif; ?>
							<style>
								.een-video iframe {
									width: 1120px !important;
									height: 768px !important;
								}
								.projects-projects .all-projects-slider .projects-slider-slide {
									max-width: 1120px !important;
								}
								.slick-next:before {
									right: 70px;
									position: relative;
								}
							</style>
						</div>
						<?php
							}
						}
						if($sliders) {
							foreach($sliders as $slider) {

							/*echo '<pre>';
							var_dump($slider);
							echo '/<pre>';*/

							$image_id = $slider["{$prefix}project_slider_image"];
							$image_url = wp_get_attachment_image((int)$image_id[0], 'project-large');
							$slider_title = $slider["{$prefix}project_slider_title"];

							//var_dump($image_url);
							//var_dump($slider_title);
						?>
						<div class="projects-slider-slide">
							<?php echo $image_url ?>
							<!--img src="<?php //echo $image_url; ?>" alt="<?php //echo $slider_title ?>"-->
							<?php if ($slider_title) : ?>
								<div class="slide-text">
									<p><?php echo $slider_title; ?></p>
								</div>
							<?php endif; ?>

							<div class="projects-slide-overlay"></div>
						</div>
					<?php
						}
					}
					?>
				</div>
			</div>
		</div>

		<section class="blog-post-content">
			<div class="container">
				<div class="row-l blog-post-row blog-post-row-first">

					<?php

					$args = array(
						'post_type' 	 => 'projects',
						'posts_per_page' => 3,
						'post__not_in'   => array(get_the_ID())
					);
					$loop = new WP_Query( $args );

					while ( $loop->have_posts() ) : $loop->the_post();

						$project = get_post_meta($post->ID);

						$slider = unserialize( $project['project-slider'][0] );
						$image_id = $slider[0]["{$prefix}project_slider_image"];
						$image_url = wp_get_attachment_url($image_id[0]);

					?>
					<div class="col-4-l">
						<div class="blog-image">
							<img src="<?= $image_url; ?>" alt="<?= $slider[0]['_kmeta_project_slider_title']; ?>">
						</div>
						<h4><?= the_title(); ?></h4>
						<button onclick="location.href='<?= get_permalink($post->ID) ?>';" class="button-main read-more-blog">Read more</button>
					</div>
					<?php endwhile; ?>

				</div>
			</div>
		</section>


		<div class="row-m">
			<div class="col-12-m projects-button">
				<button onclick="location.href='<?= $button_url ?>';" class="button-main projects">View All Projects</button>
			</div>
		</div>
    </div>
</section>


<script>
	// This script is here just to load on this project pages it should move in to scripts 
	// but for time sake to make it work it is here.
	jQuery(document).ready(function() {
		var par = $('#toggle_tst');
		jQuery(par).hide();
		jQuery("#show_hide").click(function () {
			jQuery("#toggle_tst").toggle();

			if(jQuery('#toggle_tst').is(':visible')){
				jQuery('#show_hide').html('READ LESS');
			}else {
				jQuery('#show_hide').html('READ MORE');
			}
  		});
  });
</script>
