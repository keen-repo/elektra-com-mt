<?php 

$prefix = K_MB_PREFIX;

$services_list = rwmb_meta("services-list-items");
$tc_url = rwmb_meta("{$prefix}services_list_tc_url");
$button_title = rwmb_meta("{$prefix}services_list_button_title");
$button_url = rwmb_meta("{$prefix}services_list_button_url");

?>

<section class="services-list">
    <div class="container">
        <div class="row-m services-list-row">
            <?php
            if ( ! empty( $services_list ) ) {
            foreach ( $services_list as $group_value ) {
                $icon = isset( $group_value["{$prefix}services_list_icon_url"] ) ? $group_value["{$prefix}services_list_icon_url"] : '';
                $title = isset( $group_value["{$prefix}services_list_title"] ) ? $group_value["{$prefix}services_list_title"] : '';
                $list = $group_value["services-list-item"];
            ?>
                <div class="col-6-m">
                    <div class="services-list-img">
                        <img src="<?= $icon; ?>">
                    </div>
                    <h2><?= $title; ?></h2>
                    <ul>
                        <?php if ( ! empty( $list ) ) {
                        foreach ( $list as $value ) {
                            $list_value = isset( $value["{$prefix}services_list_item"] ) ? $value["{$prefix}services_list_item"] : ''; ?>
                            <li><?= $list_value; ?></li>
                        <?php }} ?>
                    </ul>
                </div>
            <?php }} ?>
        </div>

        <div class="row-m">
            <div class="col-12-m apply">
                <a href="<?= $tc_url; ?>">T&C’s Apply</a>
            </div>
        </div>

        <?php if($button_url) { ?>
        <div class="row-m">
            <div class="col-12-m services-list-button">
                <button onclick="location.href='<?= $button_url ?>';" class="button-main"><?= $button_title; ?></button>
            </div>
        </div>
        <?php } ?>
    </div>
</section>