<?php 

$prefix = K_MB_PREFIX;
$timeline = rwmb_meta("about-timeline");

?>

<section class="about-timeline">
    <div class="container">
        <div class="row-m">
            <div class="col-12-m">
                <h2>Historic timeline</h2>
            </div>
        </div>
        <div class="about-timeline-events">
        	<?php

        	if ( ! empty( $timeline ) ) {
            	
            foreach ( $timeline as $group_value ) {

            $date = $group_value["{$prefix}about_timeline_date"];
            $content = $group_value["{$prefix}about_timeline_content"];

        	?>
            <div class="single-event__wrapper single-event__wrapper-first">
                <div class="single-event-year"><?= $date; ?></div>
                <div class="single-event single-event-left" data-aos="fade-up">
                    <p><?= $content; ?></p>
                </div>
            </div>
        	<?php }} ?>
        </div>
    </div>
</section>