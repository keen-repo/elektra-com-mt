<?php

$prefix = K_MB_PREFIX;

$title = rwmb_meta("{$prefix}work_with_us_activities_title");
$content = rwmb_meta("{$prefix}work_with_us_activities_content");
$images = rwmb_meta("{$prefix}work_with_us_activities_image",array( 'size'=>'full' ));

?>

<section id="our-activities" class="work-with-us-our-activities">
	<div class="container">
		<div class="row-m">
			<div class="col-12-m">
				<h2><?= $title; ?></h2>
			</div>
		</div>
		<div class="row-m">
			<div class="col-12-m col-count-2">
				<p><?= $content; ?></p>
			</div>
		</div>

		<div class="our-activities-slider">

			<?php
			foreach ( $images as $image ) { 
			?>
			<div class="our-activities-slide">
				<img src="<?= $image['url']; ?>">
			</div>
			<?php } ?>
		</div>
	</div>
</section>