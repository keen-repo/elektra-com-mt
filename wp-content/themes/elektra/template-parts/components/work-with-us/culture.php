<?php 

$prefix = K_MB_PREFIX;

$title = rwmb_meta("{$prefix}work_with_us_culture_title");
$first_p = rwmb_meta("{$prefix}work_with_us_culture_content_first");
$remaining_p = rwmb_meta("{$prefix}work_with_us_culture_content_remaining");
$button_text = rwmb_meta("{$prefix}work_with_us_culture_button_text");

?>

<section id="our-culture" class="work-with-us-culture">
    <div class="container">
        <div class="row">
            <div class="col-12-m">
                <h2><?= $title; ?></h2>
                <p class="work-with-us-initial"><?= $first_p; ?></p>
                <p class="work-with-us-culture-addition"><?= $remaining_p; ?></p>

                <button id="work-culture-btn" class="button-main read-more-work-culture"><?php if( $button_text ) { echo $button_text; } else { echo "Read More"; } ?></button>

            </div>
        </div>
    </div>
</section>