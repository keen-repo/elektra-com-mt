<?php

$prefix = K_MB_PREFIX;

$title = rwmb_meta("{$prefix}work_with_us_join_title");
$subtext = rwmb_meta("{$prefix}work_with_us_join_subtext");
$content = rwmb_meta("{$prefix}work_with_us_join_content");
$button_text = rwmb_meta("{$prefix}work_with_us_join_button_text");
$button_url = rwmb_meta("{$prefix}work_with_us_join_button_url");

?>

<section id="join-us" class="work-with-us-join-us">
	<div class="container">
		<div class="row-m">
			<div class="join-us">
				<div class="join-us-overlay"></div>
				<div class="join-us-text">
					<h2><?= $title; ?></h2>
					<h3><?= $subtext; ?></h3>
					<p><?= $content; ?></p>

					<?php if($button_url) { ?>
					<button onclick="location.href='<?= $button_url ?>';" class="button-main join-us-btn"><?php if($button_text) { echo $button_text; } else { echo "Join our team"; } ?></button>
					<?php } ?>

				</div>
			</div>
		</div>
	</div>
</section>