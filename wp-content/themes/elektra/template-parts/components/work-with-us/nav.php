<section id="top-nav" class="work-with-us-nav">
	<nav id="workWithUsNav">
		<div class="container">
			<div class="row-m">
				<div class="col-12-m">
					<ul class="work-with-us-nav-list">
						<li>
							<a href="#our-culture">
								<div class="nav-list-inner-1 work-with-us-nav-active">
									Our Culture
								</div>
							</a>
						</li>
						<li>
							<a href="#our-philosophy">
								<div class="nav-list-inner-2">
									Our Philosophy
								</div>
							</a>
						</li>
						<li>
							<a href="#our-values-principles">
								<div class="nav-list-inner-3">
									Our Values & Principles
								</div>
							</a>
						</li>
						<li>
							<a href="#our-story">
								<div class="nav-list-inner-4">
									Our Story
								</div>
							</a>
						</li>
						<li>
							<a href="#join-us">
								<div class="nav-list-inner-5">
									Join Us
								</div>
							</a>
						</li>
						<li>
							<a href="#our-activities">
								<div class="nav-list-inner-6">
									Our Activities
								</div>
							</a>
						</li>
						<li>
							<a href="#our-people">
								<div class="nav-list-inner-7">
									Meet Our People
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
</section>