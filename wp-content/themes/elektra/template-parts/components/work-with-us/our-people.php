<?php 

$prefix = K_MB_PREFIX;

$title = rwmb_meta("{$prefix}work_with_us_people_title");
$content = rwmb_meta("{$prefix}work_with_us_people_content");
$people = rwmb_meta("work-with-us-people");

?>

<section id="our-people" class="work-with-us-meet-our-people">
	<div class="container">
		<div class="row-m">
			<div class="col-12-m">
				<h2><?= $title; ?></h2>
				<p class="meet-our-people-description"><?= $content; ?></p>
			</div>
		</div>

		<div class="row-m meet-our-people-board-row">

			<?php 

			if($people) {
			foreach($people as $group_value) {
			$image_id = $group_value["{$prefix}work_with_us_people_image"];
			$image_url = wp_get_attachment_url($image_id[0]);
			$name = $group_value["{$prefix}work_with_us_people_name"];
			$position = $group_value["{$prefix}work_with_us_people_position"];
			$bio = $group_value["{$prefix}work_with_us_people_bio"];
			$linkedin = $group_value["{$prefix}work_with_us_people_linkedin"];
			?>
			<div class="col-1-2-m col-1-3-l col-1-5-xl">
				<div class="meet-our-people-image">
					<img src="<?= $image_url; ?>" alt="<?= $name; ?>">
					
					<div class="meet-our-people-image-overlay" data-fancybox data-src="#meet-our-people-<?= str_replace(".", "", str_replace(" ", "-", $name)); ?>" href="javascript:;">
						<p>
							More<br> 
							info
						</p>
					</div>
				</div>
				<p class="meet-our-people-name"><?= $name; ?></p>
				<p class="meet-our-people-position"><?= $position; ?></p>
				<div id="meet-our-people-<?= str_replace(".", "", str_replace(" ", "-", $name)); ?>" class="meet-our-people-modal-wrapper">
					<div class="meet-our-people-image-modal">
						<img src="<?= $image_url; ?>" alt="<?= $name; ?>">
					</div>
					<h2><?= $name; ?></h2>
					<p class="work-position-modal"><?= $position; ?></p>
					<div class="meet-our-people-content-modal">
						<?= wpautop($bio); ?>
					</div>
					<div class="connect-modal">
						<div class="connect-modal-inner">

							<?php if($linkedin) { ?>
							<a href="<?= $linkedin ?>">
								<div class="connect-link">
									<div class="connect">Connect</div>
									<div class="linkedin">
										<img src="<?= get_template_directory_uri(); ?>/dist/images/work-with-us/icons/red-linkedin.svg" alt="Linkedin">
									</div>
								</div>
							</a>
							<?php } ?>

						</div>
					</div>
				</div>
			</div>
			<?php }} ?>
				
		</div>
		
		<?php render_scroll_up_button(); ?>
	</div>
</section>