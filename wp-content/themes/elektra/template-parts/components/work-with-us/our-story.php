<?php

$prefix = K_MB_PREFIX;

$tabs = rwmb_meta('work-with-us-story');

$title_counter = 0;
$content_counter = 0;

?>

<section id="our-story" class="work-with-us-our-story">
	<div class="container">
		<div class="row-m">
			<div class="col-12-m">
				<h2>Our story</h2>
			</div>
		</div>

		<div class="row-m">
			<div class="col-12-m">
				<ul class="our-story-list">
					<?php 
					if ( ! empty( $tabs ) ) {
				    foreach ( $tabs as $group_value ) {
				    	$title_counter = $title_counter + 1;
				        $title = isset( $group_value["{$prefix}work_with_us_story_title"] ) ? $group_value["{$prefix}work_with_us_story_title"] : '';
					?>
					<li>
						<a href="#" class="tab_<?= $title_counter; ?>">
							<?= $title; ?>
						</a>
					</li>
					<?php }} ?>
				</ul>
			</div>
		</div>

		<div class="row-m">
			<div class="col-12-m">
				<?php 
				if ( ! empty( $tabs ) ) {
			    foreach ( $tabs as $group_value ) {
			    	$content_counter = $content_counter + 1;
			        $content = isset( $group_value["{$prefix}work_with_us_story_content"] ) ? $group_value["{$prefix}work_with_us_story_content"] : '';
				?>
				<p id="our-story-content-id-<?= $content_counter ?>" class="our-story-content our-story-content-<?= $content_counter ?>"><?= $content; ?></p>
				<?php }} ?>
			</div>
		</div>

		<button onclick="goToAnchor('our-story')" id="story-to-top" class="button-main arrow-btn block none-l"></button>
	</div>
</section>