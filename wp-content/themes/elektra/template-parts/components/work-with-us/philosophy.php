<?php 

$prefix = K_MB_PREFIX;

$title = rwmb_meta("{$prefix}work_with_us_philosophy_title");
$content = rwmb_meta("{$prefix}work_with_us_philosophy_content");

$card_1_title = rwmb_meta("{$prefix}work_with_us_philosophy_card_1_title");
$card_1_first_p = rwmb_meta("{$prefix}work_with_us_philosophy_card_1_content_first");
$card_1_remaining_p = rwmb_meta("{$prefix}work_with_us_philosophy_card_1_content_remaining");

$card_2_title = rwmb_meta("{$prefix}work_with_us_philosophy_card_2_title");
$card_2_first_p = rwmb_meta("{$prefix}work_with_us_philosophy_card_2_content_first");
$card_2_remaining_p = rwmb_meta("{$prefix}work_with_us_philosophy_card_2_content_remaining");

$card_3_title = rwmb_meta("{$prefix}work_with_us_philosophy_card_3_title");
$card_3_first_p = rwmb_meta("{$prefix}work_with_us_philosophy_card_3_content_first");
$card_3_remaining_p = rwmb_meta("{$prefix}work_with_us_philosophy_card_3_content_remaining");

?>

<section id="our-philosophy" class="work-with-us-philosophy">
    <div class="container">
        <div class="row-m">
            <div class="col-12-m">
                <h2><?= $title; ?></h2>
                <p><?= $content; ?></p>
            </div>
        </div>
        <div class="row-m work-with-us-philosophy-columns">
            <div class="work-with-us-philosophy-evolve">
                <img src="<?= get_template_directory_uri(); ?>/dist/images/work-with-us/icons/evolve.svg" alt="Evolve">
                <h4><?= $card_1_title; ?></h4>
                <p><?= $card_1_first_p; ?> <span class="work-with-us-philosophy-addition"><?= $card_1_remaining_p; ?></span></p>
                <button id="find-out-more-evolve" class="find-out-more">Find out more</button>
            </div>
            <div class="work-with-us-philosophy-excite">
                <img src="<?= get_template_directory_uri(); ?>/dist/images/work-with-us/icons/excite.svg" alt="Evolve">
                <h4><?= $card_2_title; ?></h4>
                <p><?= $card_2_first_p; ?> <span class="work-with-us-philosophy-addition"><?= $card_2_remaining_p; ?></span></p>
                <button id="find-out-more-excite" class="find-out-more">Find out more</button>
            </div>
            <div class="work-with-us-philosophy-energise">
                <img src="<?= get_template_directory_uri(); ?>/dist/images/work-with-us/icons/energise.svg" alt="Evolve">
                <h4><?= $card_3_title; ?></h4>
                <p><?= $card_3_first_p; ?> <span class="work-with-us-philosophy-addition"><?= $card_3_remaining_p; ?></span></p>
                <button id="find-out-more-energise" class="find-out-more">Find out more</button>
            </div>
        </div>
    </div>
</section>