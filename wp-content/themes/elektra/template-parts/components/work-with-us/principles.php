<?php 

$prefix = K_MB_PREFIX;

$title = rwmb_meta("{$prefix}work_with_us_principles_title");
$subtext = rwmb_meta("{$prefix}work_with_us_principles_subtext");

$card_1_title = rwmb_meta("{$prefix}work_with_us_principles_card_1_title");
$card_1_content = rwmb_meta("{$prefix}work_with_us_principles_card_1_content");

$card_2_title = rwmb_meta("{$prefix}work_with_us_principles_card_2_title");
$card_2_content = rwmb_meta("{$prefix}work_with_us_principles_card_2_content");

?>

<section id="our-values-principles" class="work-with-us-five-values-principles">
    <div class="container">
        <div class="row-m">
            <div class="col-12-m">
                <h2><?= $title; ?><span><?= $subtext; ?></span></h2>
                <p></p>
            </div>
        </div>

        <div class="row-m values-principles-row">
            <div class="col-6-m work-with-us-five-values">
				<h3><?= $card_1_title; ?></h3>
				<?= $card_1_content; ?>
            </div>
            <div class="col-6-m work-with-us-five-principles">
				<h3><?= $card_2_title; ?></h3>
				<?= $card_2_content; ?>
            </div>
        </div>
    </div>
</section>