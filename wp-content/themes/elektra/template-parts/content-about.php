<?php 

$prefix = K_MB_PREFIX;
$vision = rwmb_meta("{$prefix}about_vision");
$mission = rwmb_meta("{$prefix}about_mission");
$founded = rwmb_meta("{$prefix}about_founded");
$ceo = rwmb_meta("{$prefix}about_ceo");
$employees = rwmb_meta("{$prefix}about_employees");
$our_services_title = rwmb_meta("{$prefix}about_our_services_title");
$our_services = rwmb_meta("{$prefix}about_our_services");
$our_services_button_text = rwmb_meta("{$prefix}about_our_services_button_text");
$our_services_url = rwmb_meta("{$prefix}about_our_services_url");
$our_services_images = rwmb_meta("{$prefix}about_our_services_image", array( 'limit' => 1, 'size'=>'full' ));
$our_services_image = reset($our_services_images);
$our_projects_title = rwmb_meta("{$prefix}about_our_projects_title");
$our_projects = rwmb_meta("{$prefix}about_our_projects");
$our_projects_button_text = rwmb_meta("{$prefix}about_our_projects_button_text");
$our_projects_url = rwmb_meta("{$prefix}about_our_projects_url");
$our_projects_images = rwmb_meta("{$prefix}about_our_projects_image", array( 'limit' => 1, 'size'=>'full' ));
$our_projects_image = reset($our_projects_images);
$shop_title = rwmb_meta("{$prefix}about_shop_title");
$shop = rwmb_meta("{$prefix}about_shop");
$shop_button_text = rwmb_meta("{$prefix}about_shop_button_text");
$shop_url = rwmb_meta("{$prefix}about_shop_url");
$shop_images = rwmb_meta("{$prefix}about_shop_image", array( 'limit' => 1, 'size'=>'full' ));
$shop_image = reset($shop_images);

?>

<main>
    
    <?php get_template_part( 'template-parts/components/inner-top-banner', 'none' ); ?>

    <section class="about-vision-mission">
        <div class="container">
            <div class="row-l">
                <div class="col-8-l">
                    <h2>Our vision</h2>
                    <p><?= $vision; ?></p>
                </div>
                <div class="col-4-l">
                    <h2>Our mission</h2>
                    <p><?= $mission; ?></p>
                </div>
            </div>
        </div>
    </section>

    <section class="about-facts">
        <div class="container">
            <div class="row-m">
                <div class="col-4-m">
                    <img src="<?= get_template_directory_uri(); ?>/dist/images/about-us/icons/founded.svg" alt="Founded">
                    <div class="about-facts-title">Founded</div>
                    <div class="about-facts-content"><?= $founded; ?></div>
                </div>
                <div class="col-4-m">
                        <img src="<?= get_template_directory_uri(); ?>/dist/images/about-us/icons/ceo.svg" alt="Founded">
                        <div class="about-facts-title">CEO</div>
                        <div class="about-facts-content">
                            <div><?= $ceo; ?></div>
                        </div>
                </div>
                <div class="col-4-m">
                    <img src="<?= get_template_directory_uri(); ?>/dist/images/about-us/icons/employees.svg" alt="Founded">
                    <div class="about-facts-title">Employees</div>
                    <div class="about-facts-content"><?= $employees; ?></div>
                </div>
            </div>
        </div>
    </section>

    <?php get_template_part( 'template-parts/components/timeline', 'none' ); ?>
	
	<section class="about-services-projects-shop">
		<div class="container">
			<div class="row-l">
				<div class="col-4-l">
					<img src="<?= $our_services_image["url"]; ?>" alt="<?= $our_services_title; ?>">
					<h2><?= $our_services_title; ?></h2>
					<p><?= $our_services ?></p>
                    <?php if($our_services_url) { ?>
					<button onclick="location.href='<?= $our_services_url ?>';" class="button-main about-us"><?= $our_services_button_text; ?></button>
                    <?php } ?>
				</div>
				<div class="col-4-l">
					<img src="<?= $our_projects_image["url"]; ?>" alt="<?= $our_projects_title; ?>">
					<h2><?= $our_projects_title; ?></h2>
					<p><?= $our_projects ?></p>
                    <?php if($our_projects_url) { ?>
					<button onclick="location.href='<?= $our_projects_url ?>';" class="button-main about-us"><?= $our_projects_button_text; ?></button>
                    <?php } ?>
				</div>
				<div class="col-4-l">
					<img src="<?= $shop_image["url"]; ?>" alt="<?= $shop_title; ?>">
					<h2><?= $shop_title; ?></h2>
					<p><?= $shop; ?></p>
                    <?php if($shop_url) { ?>
                    <button onclick="location.href='<?= $shop_url ?>';" class="button-main about-us"><?= $shop_button_text; ?></button>
                    <?php } ?>
				</div>
			</div>
		</div>
	</section>
</main>