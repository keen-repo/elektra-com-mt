<?php
$prefix = K_MB_PREFIX;

$project = get_post_meta($post->ID);

$slider = unserialize( $project['project-slider'][0] );
$image_id = $slider[0]["{$prefix}project_slider_image"];
$image_url = wp_get_attachment_url($image_id[0]);

?>

<div onclick="location.href='<?= get_permalink(); ?>';" class="col-4-l">
    <div class="blog-image">
		<img src="<?= $image_url; ?>" alt="<?= $slider[0]['_kmeta_project_slider_image'][0]; ?>">
    </div>
    <h4><?php the_title(); ?></h4>
    <button class="button-main read-more-blog">Read more</button>
</div>
