<div onclick="location.href='<?= get_permalink(); ?>';" class="col-4-l">
    <div class="blog-image">
        <img src="<?= get_the_post_thumbnail_url(get_the_ID(), "full"); ?>">
        <div class="blog-post-date"><?= get_the_date("d.m.Y"); ?></div>
    </div>
    <h4><?php the_title(); ?></h4>
    <button class="button-main read-more-blog">Read more</button>
</div>
