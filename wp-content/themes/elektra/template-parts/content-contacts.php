<?php 

$prefix = K_MB_PREFIX;

$street = rwmb_meta("{$prefix}contacts_street");
$town = rwmb_meta("{$prefix}contacts_town");
$postcode = rwmb_meta("{$prefix}contacts_postcode");
$country = rwmb_meta("{$prefix}contacts_country");
$telephone = rwmb_meta("{$prefix}contacts_phone");
$email = rwmb_meta("{$prefix}contacts_email");
$monday_friday = rwmb_meta("{$prefix}contacts_monday-friday");
$saturday = rwmb_meta("{$prefix}contacts_saturday");
$sunday = rwmb_meta("{$prefix}contacts_sunday");
$car_title = rwmb_meta("{$prefix}contacts_by_car_title");
$car_content = rwmb_meta("{$prefix}contacts_by_car_content");
$bus_title = rwmb_meta("{$prefix}contacts_by_bus_title");
$bus_content = rwmb_meta("{$prefix}contacts_by_bus_content");

?>

<?php get_template_part( 'template-parts/components/inner-top-banner', 'none' ); ?>

<section class="contact-requisite">
    <div class="container">
        <div class="row-l">
            <div class="col-6-l contact-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6464.941335046599!2d14.471430071372446!3d35.88648667946136!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x130e5aa70136cdb9%3A0x4bd61e6e886bdfae!2sElektra+Ltd.!5e0!3m2!1sen!2slt!4v1538375623715" width="600" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            
            <div class="col-6-l contact-contact">
                <div class="row-m contact-contact-row">
                    <div class="col-6-m left-column">
                        <h3>Address</h3>
                        <address>
                            <?= $street; ?><br> 
                            <?= $town; ?><br> 
                            <?= $postcode; ?><br> 
                            Malta
                        </address>
                    </div>
                    <div class="col-6-m right-column">
                        <div class="row-m">
                            <div class="col-12-m">
                                <h3>Telephone</h3>
                                <div><?= $telephone; ?></div>
                            </div>
                        </div>
                        <div class="row-m">
                            <div class="col-12-m">
                                <h3>Email</h3>
                                <div class="email"><?= $email; ?></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="horizontal-line"></div>
                
                <div class="row-m contact-hours__wrapper">
                    <div class="col-12-m">
                        <div class="row-m opening-hours-title">
                            <h3>Opening hours:</h3>
                        </div>
                        <div class="row-m opening-hours-row">
                            <div class="col-6-m">
                                <div>Monday to Friday:</div>
                                <div>Saturday:</div>
                                <div>Sunday:</div>
                            </div>
                            <div class="col-6-m">
                                <div><?= $monday_friday; ?></div>
                                <div><?= $saturday; ?></div>
                                <div><?= $sunday; ?></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="horizontal-line"></div>

                <?php get_template_part( 'template-parts/components/contacts-form', 'none' ); ?>

            </div>
        </div>
    </div>
</section>

<section id="contact-directions" class="contact-direction">
    <div class="container-fluid">
        <div class="row-l">
            <div class="col-6-l contact-direction-left">
                <div class="directions">
                    <img src="<?= get_template_directory_uri(); ?>/dist/images/contact-us/icons/car.svg" alt="Car">
                    <h3>Directions by Car</h3>
                    <h4><?= $car_title; ?></h4>
                    <p><?= $car_content; ?></p>
                </div>
            </div>
            <div class="col-6-l contact-direction-right">
                <div class="directions public-transport">
                    <img src="<?= get_template_directory_uri(); ?>/dist/images/contact-us/icons/bus.svg" alt="Car">
                    <h3>Directions by Public Transport</h3>
                    <h4><?= $bus_title; ?></h4>
                    <p><?= $bus_content; ?></p>
                </div>
            </div>
        </div>
    </div>    
</section>
