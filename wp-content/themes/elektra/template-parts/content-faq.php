<main>
    
    <?php get_template_part( 'template-parts/components/inner-top-banner', 'none' ); ?>   
    
    <?php get_template_part( 'template-parts/components/faq-nav', 'none' ); ?>   

    <section class="faq-questions">
        <div class="container">
			
			<?php get_template_part( 'template-parts/components/faq-lighting-design', 'none' ); ?>
			
			<?php get_template_part( 'template-parts/components/faq-general', 'none' ); ?>
			
			<?php get_template_part( 'template-parts/components/faq-wiring-accessories', 'none' ); ?>
			
        </div>
    </section>
</main>