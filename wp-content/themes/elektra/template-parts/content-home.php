<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

$prefix = K_MB_PREFIX;
?>

<section class="slider-main">
	<div class="container-fluid">
		<div class="row-m">
			<div class="col-12-m">
				<div class="home-slider">
					<?php
						$page_banners_group = rwmb_meta( 'page-banners' );
						if ( ! empty( $page_banners_group ) ) {
						    foreach ( $page_banners_group as $group_value ) {
						        $image_id = isset( $group_value["{$prefix}page_banner_image"] ) ? $group_value["{$prefix}page_banner_image"] : '';
						        $text = isset( $group_value["{$prefix}page_banner_text"] ) ? $group_value["{$prefix}page_banner_text"] : '';
						        $button_text = isset( $group_value["{$prefix}page_banner_button_text"] ) ? $group_value["{$prefix}page_banner_button_text"] : '';
						        $button_url = isset( $group_value["{$prefix}page_banner_button_url"] ) ? $group_value["{$prefix}page_banner_button_url"] : '';
						        $image_url = wp_get_attachment_url($image_id[0]);
						     ?>

						     <div class="home-slider-slide-first" style="background: url(<?= $image_url ?>) no-repeat center top;">
								<div class="container">
									<div class="row-m home-slider-row">
										<div class="col-12-m home-slider-column">
											<h2 class="home-slider-column-text">
												<?= $text ?>
											</h2>
										</div>
									</div>

									<div class="row home-slider-button">
										<div class="col-12-m">
											<button onclick="location.href='<?= $button_url ?>';" class="button-main get-free-quote"><?= $button_text ?></button>
										</div>
									</div>
								</div>
							</div>
							<?php

						    }
						}
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about-us">
	<div class="container-fluid">
		<div class="row-m">
			<div class="col-12-m">
				<?php
				$about_us_title = rwmb_meta("{$prefix}about_us_title");
				$about_us_content = rwmb_meta("{$prefix}about_us_content");
				$about_us_button_title = rwmb_meta("{$prefix}about_us_button_title");
				$about_us_button_url = rwmb_meta("{$prefix}about_us_button_url");
				?>
				<h3><?php if($about_us_title) { echo $about_us_title; }else{ echo ""; }?></h3>

				<?php if($about_us_content) { echo $about_us_content; } else { echo ""; } ?>

				<button onclick="location.href='<?= $about_us_button_url; ?>'" class="button-main read-more"><?php if($about_us_button_title) { echo $about_us_button_title; } else { echo "Read more"; } ?></button>
			</div>
		</div>
	</div>
</section>

<?php get_template_part( 'template-parts/components/home-projects', 'none' ); ?>

<section class="our-brands">
	<?php
		$our_brands_title = rwmb_meta("{$prefix}our_brands_title");
		$our_brands_images = rwmb_meta("{$prefix}our_brands_image", array( 'size'=>'full' ));
	?>
	<div class="container">
		<div class="row-m">
			<div class="col-12-m">
				<h3><?php if($our_brands_title) { echo $our_brands_title; } else { echo "OUR BRANDS"; } ?></h3>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row-m">
			<div class="col-12-m">
				<div class="our-brands-slider">
					<?php
					foreach ( $our_brands_images as $image ) {
					?>
				    <div class="our-brands-slide first">
						<img src="<?= $image['url']; ?>">
					</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<!--<?php get_template_part( 'template-parts/components/home-popup', 'none' ); ?>-->
