<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<div class="container" style="margin-top: 40px;margin-bottom: 40px;">
            <div class="col-12-m">
				<?php the_content(); ?>
			</div>
		</div>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
