<?php

$content = get_post(96);
?>

<section class="projects-content">
    <div class="container">
        <div class="row-m">
            <div class="col-12-m">
                <p><?= $content->post_content; ?></p>
            </div>
        </div>
    </div>
</section>

<?php get_template_part( 'template-parts/components/projects-slider', 'none' ); ?>
