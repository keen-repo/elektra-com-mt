<main>

    <?php get_template_part( 'template-parts/components/inner-top-banner', 'none' ); ?>

    <section class="residential-content">
        <div class="container">
            <div class="row-m">
                <div class="col-12-m">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Gallery-->
    <?php get_template_part( 'template-parts/components/gallery', 'none' ); ?>
    <!-- End Gallery -->

    <?php get_template_part( 'template-parts/components/inner-bottom-banner', 'none' ); ?>

</main>
