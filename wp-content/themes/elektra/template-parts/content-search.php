<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

$prefix = K_MB_PREFIX;
if(get_the_post_thumbnail_url(get_the_ID(), "full")) {
	$background = get_the_post_thumbnail_url(get_the_ID(), "full");
} else if(rwmb_meta("{$prefix}inner_banner_image", array( 'limit' => 1, 'size'=>'full' ))) {
	$backgrounds = rwmb_meta("{$prefix}inner_banner_image", array( 'limit' => 1, 'size'=>'full' ));
	$background = reset($backgrounds);	
	$background = $background["url"];	
} else {
	$background = "/wp-content/uploads/2018/09/blog-main-banner-image.jpg";
}
?>

<div class="col-4-l">
    <div class="blog-image image-search">
        <img src="<?= $background; ?>">
        <div class="blog-post-date"><?= get_the_date("d.m.Y"); ?></div>
    </div>
    <h4 class="search-card-title"><?php the_title(); ?></h4>
    <button onclick="location.href='<?= get_permalink(); ?>';" class="button-main read-more-blog search-card-button">Read more</button>
</div>
