<main>
    
    <?php get_template_part( 'template-parts/components/inner-top-banner', 'none' ); ?>

    <section class="services-content">            
        <div class="container">
            <div class="row-m">
                <div class="col-12-m">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>          
    </section>

    <?php get_template_part( 'template-parts/components/services-list', 'none' ); ?>

</main>