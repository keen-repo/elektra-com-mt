<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

$prefix = K_MB_PREFIX;
$highlight_title = rwmb_meta("{$prefix}blog_post_highlight_title");
$highlight_content = rwmb_meta("{$prefix}blog_post_highlight_content");
$summary_content = rwmb_meta("{$prefix}blog_post_summary_content");
$images = rwmb_meta("{$prefix}blog_post_images", array( 'size'=>'full' ));
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single-news blog-post'); ?>>
	<section class="blog-post">
		<div class="container">
			<div class="col-10-m blog-post-header">
				<h1><?php the_title(); ?></h1>
				<div class="blog-post-date"><?= get_the_date('d.m.Y'); ?></div>
				<div class="blog-post-image">
					<img src="<?= esc_url(get_the_post_thumbnail_url(get_the_ID(),'full')); ?>" alt="Light bulb">
				</div>
			</div>
		</div>

		<?php if($highlight_title || $highlight_content) { ?>
			<div class="container-fluid grey-rectangle-upper">
				<div class="container">
					<div class="row-m">
						<div class="col-10-m">
							<h3><?= $highlight_title; ?></h3>
							<p><?= $highlight_content; ?></p>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>

		<div class="container blog-post-description">

			<?php if(count($images) > 0) { ?>
				<div class="row-l">
					<?php
					foreach ( $images as $image ) {
						?>
						<div class="col-5-l blog-post-image">
							<img src="<?= $image["url"]; ?>">
						</div>
					<?php } ?>
				</div>
			<?php } ?>

			<div class="row-m blog-post-description-text">
				<div class="col-10-m">
					<?php the_content(); ?>
				</div>
			</div>
		</div>

		<?php if($summary_content) { ?>
			<div class="container-fluid grey-rectangle-lower">
				<div class="container">
					<div class="row-m">
						<div class="col-10-m">
							<p><?= $summary_content; ?></p>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>


	</section>

	<section class="blog-post-content">
		<div class="container">
			<div class="row-l blog-post-row blog-post-row-first">
				<?php

				$args = array(
					'numberposts' => 3,
					'offset' => 0,
					'post__not_in' => array( get_the_ID() )
				);
				$relatedposts = get_posts($args);
				foreach ($relatedposts as $post) {
					?>
					<div class="col-4-l">
						<div class="blog-image">
							<img src="<?= get_the_post_thumbnail_url($post->ID,"full"); ?>">
							<div class="blog-post-date"><?= get_the_date('d.m.Y',$post->ID); ?></div>
						</div>
						<h4><?= get_the_title($post->ID); ?></h4>
						<button onclick="location.href='<?= get_permalink($post->ID) ?>';" class="button-main read-more-blog">Read more</button>
					</div>
				<?php } ?>

			</div>
		</div>
	</section>
</article><!-- #post-## -->
