<?php 
$prefix = K_MB_PREFIX;
$images = rwmb_meta( '{$prefix}gallery_photos', array( 'size' => 'large' ) );
foreach ( $images as $image ) {
 echo '<a href="', $image['full_url'], '"><img src="', $image['url'], '"></a>';
}

?>

<div class="sgallery">
	<?php foreach ($images as $key => $image): ?>
		<div class="sgallery-item"><img src="<?php $image['url'] ?>" alt="">/div>
	<?php endforeach; ?>
</div>
