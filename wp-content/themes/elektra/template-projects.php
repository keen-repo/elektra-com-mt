<?php /* Template Name: Projects*/

$args = array(
    'posts_per_page'   => -1,
    'post_type'        => 'projects',
);
$the_query = new WP_Query( $args );

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php get_template_part( 'template-parts/components/inner-top-banner', 'none' ); ?>
			<?php
			$counter = 0;
			if ( $the_query->have_posts() ) :
				?>

				<section class="blog-content">
				<?php
				/* Start the Loop */
				while ( $the_query->have_posts() ) : $the_query->the_post();

					$counter = $counter + 1;

					if($counter == 1) {
						get_template_part( 'template-parts/components/newest-project', 'none' );
					} else if($counter == 2) { ?>
						<div class="container">
        					<div class="row-l blog-post-row blog-post-row-first">
							<?php get_template_part( 'template-parts/content-archive-projects', get_post_format() );
							} else {
								get_template_part( 'template-parts/content-archive-projects', get_post_format() );
							}

						endwhile;

					else :

						get_template_part( 'template-parts/content', 'none' ); ?>
						</div>
					</div>
				</section>
			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php
get_footer();

?>