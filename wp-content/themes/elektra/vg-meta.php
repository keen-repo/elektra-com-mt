<?php


add_filter( 'rwmb_meta_boxes', 'vg_register_meta_boxes' );

function vg_register_meta_boxes( $meta_boxes ) {

    global $prefix;

    // Cittadella Landing Metas

    $meta_boxes[] = array(
        'id'         => 'cittadella-about',
        'title'      => __( 'Section: About', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Section Title', 'vg' ),
                'id'    => $prefix . 'about-section-title',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Section Sub Title', 'vg' ),
                'id'    => $prefix . 'about-section-subtitle',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Section Text', 'vg' ),
                'id'    => $prefix . 'about-section-text',
                'type'  => 'wysiwyg',
            ),
            array(
                'name'  => __( 'Section Image', 'vg' ),
                'id'    => $prefix . 'about-section-image',
                'type'  => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'name'  => __( 'Section 2 Text', 'vg' ),
                'id'    => $prefix . 'about-2-section-text',
                'type'  => 'wysiwyg',
            ),
            array(
                'name'  => __( 'Section 2 Button Text', 'vg' ),
                'id'    => $prefix . 'about-2-button-text',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Section 2 Button URL', 'vg' ),
                'id'    => $prefix . 'about-2-button-url',
                'type'  => 'text',
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );

    $meta_boxes[] = array(
        'id'         => 'cittadella-bg-image',
        'title'      => __( 'Section: BG Image', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Section Image', 'vg' ),
                'id'    => $prefix . 'section-bg-image',
                'type'  => 'image_advanced',
                'max_file_uploads' => 1
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );

    $meta_boxes[] = array(
        'id'         => 'cittadella-project',
        'title'      => __( 'Section: Project', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Section Text', 'vg' ),
                'id'    => $prefix . 'project-text',
                'type'  => 'wysiwyg',
            ),
            array(
                'name'  => __( 'Section Images', 'vg' ),
                'id'    => $prefix . 'project-images',
                'type'  => 'image_advanced',
                'max_file_uploads' => 2
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );

    $meta_boxes[] = array(
        'id'         => 'cittadella-history',
        'title'      => __( 'Section: History', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Section Title', 'vg' ),
                'id'    => $prefix . 'history-section-title',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Section Sub Title', 'vg' ),
                'id'    => $prefix . 'history-section-subtitle',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Section Text', 'vg' ),
                'id'    => $prefix . 'history-section-text',
                'type'  => 'wysiwyg',
            ),
            array(
                'name'  => __( 'Section Image', 'vg' ),
                'id'    => $prefix . 'history-section-image',
                'type'  => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'name'  => __( 'Section 2 Text', 'vg' ),
                'id'    => $prefix . 'history-2-section-text',
                'type'  => 'wysiwyg',
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );

    $meta_boxes[] = array(
        'id'         => 'cittadella-vote',
        'title'      => __( 'Section: Vote', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Section Text', 'vg' ),
                'id'    => $prefix . 'vote-text',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Section Button Text', 'vg' ),
                'id'    => $prefix . 'vote-button-text',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Section Button URL', 'vg' ),
                'id'    => $prefix . 'vote-button-url',
                'type'  => 'text',
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );

    $meta_boxes[] = array(
        'id'         => 'cittadella-map',
        'title'      => __( 'Section: Map', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Section Title', 'vg' ),
                'id'    => $prefix . 'map-section-title',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Map Image', 'vg' ),
                'id'    => $prefix . 'map-image',
                'type'  => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'name'  => __( 'BG Image', 'vg' ),
                'id'    => $prefix . 'map-bg-image',
                'type'  => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'name'  => __( 'Section Button Text', 'vg' ),
                'id'    => $prefix . 'map-button-text',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Section Button URL', 'vg' ),
                'id'    => $prefix . 'map-button-url',
                'type'  => 'text',
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );

    $meta_boxes[] = array(
        'id'         => 'cittadella-360',
        'title'      => __( 'Section: 360 Photos', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Section Title', 'vg' ),
                'id'    => $prefix . '360-section-title',
                'type'  => 'text',
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );

    $meta_boxes[] = array(
        'id'         => 'cittadella-webcams',
        'title'      => __( 'Section: Webcams', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Section Title', 'vg' ),
                'id'    => $prefix . 'webcam-section-title',
                'type'  => 'text',
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );

    $meta_boxes[] = array(
        'id'         => 'cittadella-gallery',
        'title'      => __( 'Section: Gallery', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Section Title', 'vg' ),
                'id'    => $prefix . 'gallery-section-title',
                'type'  => 'text',
            ),
            array(
                'id' => 'cittadella-galleries',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Gallery Title:',
                        'id'    => $prefix . 'gallery-title',
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Gallery Images:',
                        'id'    => $prefix . 'gallery-images',
                        'type' => 'image_advanced',
                    ),
                ),
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );

    $meta_boxes[] = array(
        'id'         => 'cittadella-book',
        'title'      => __( 'Section: Book', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Section Text', 'vg' ),
                'id'    => $prefix . 'book-section-text',
                'type'  => 'textarea',
            ),
            array(
                'name'  => __( 'Section Button Text', 'vg' ),
                'id'    => $prefix . 'book-button-text',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Section Button URL', 'vg' ),
                'id'    => $prefix . 'book-button-url',
                'type'  => 'text',
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );

    $meta_boxes[] = array(
        'id'         => 'cittadella-contact',
        'title'      => __( 'Section: Contact', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Address', 'vg' ),
                'id'    => $prefix . 'contact-address-text',
                'type'  => 'textarea',
            ),
            array(
                'name'  => __( 'Phone/Email', 'vg' ),
                'id'    => $prefix . 'contact-phone-text',
                'type'  => 'textarea',
            ),
        ),
        'include' => array(
            'template' => 'page-cittadella.php'
        )
    );


    /* COMMON METAS */

    // global page gallery
    $meta_boxes[] = array(
        'id'         => 'page-banners',
        'title'      => __( 'Page Banners', 'vg' ),
        'post_types' => array( 'page', 'vg_local_artists' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Page Banners', 'vg' ),
                'id'    => $prefix . 'page-banners',
                'type'  => 'image_advanced',
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'         => 'page-gallery',
        'title'      => __( 'Page Gallery', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Page Image Gallery', 'vg' ),
                'id'    => $prefix . 'page-gallery',
                'type'  => 'image_advanced',
                'desc'  => 'Minimum 3 images'
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'         => 'page-video-gallery',
        'title'      => __( 'Page Video Gallery', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Page Video Gallery', 'vg' ),
                'id'    => $prefix . 'page-video-gallery',
                'desc'  => 'Use YouTube Video IDs here pls.',
                'type'  => 'text',
                'clone' => true,
                'sort-clone' => true
            ),
        ),
    );

    if($_GET['action'] == 'edit'){
        $pagesList = vg_get_pages();
    } else {
        $pagesList = array();   
    } 

    $meta_boxes[] = array(
        'id'         => 'page-subpages',
        'title'      => __( 'Subpage Links', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Subpages', 'vg' ),
                'id'    => $prefix . 'subpages',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'clone'  => true,
                'sort_clone' => true,
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'         => 'page-options',
        'title'      => __( 'Other Misc Page Options', 'vg' ),
        'post_types' => array( 'page', 'vg_local_artists' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Hide "Also Check Out" Section?', 'vg' ),
                'id'    => $prefix . 'hide_checkout',
                'type'  => 'checkbox'
            ),
             array(
                'name'  => __( 'Show Child Pages Automatically?', 'vg' ),
                'id'    => $prefix . 'show_child_pages',
                'type'  => 'checkbox'
            ),
        )
    );


    $meta_boxes[] = array(
        'id'         => 'page-listings-widget',
        'title'      => __( 'Listings Widget', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Show Listing Widget?', 'vg' ),
                'id'    => $prefix . 'show_listing_widget',
                'type'  => 'checkbox',
            ),
            array(
                'name'  => __( 'Widget Filters', 'vg' ),
                'id'    => $prefix . 'lwf_heading',
                'type'  => 'heading',
            ),
            array(
                'name'  => __( 'Is this a listing widget or an event widget?', 'vg' ),
                'id'    => $prefix . 'lwf_widget_type',
                'type'  => 'radio',
                'options' => array(
                    'listing'   => 'Listing',
                    'event'     => 'Event',
                ),
                'columns'    => 12,
            ),
            array(
                'name'       => __( 'Default Localities', 'vg' ),
                'id'         => $prefix . 'lwf_default_localities',
                'type'       => 'taxonomy_advanced',
                'taxonomy'   => 'vg_locality_cats',
                'field_type' => 'checkbox_list',
                'query_args' => array('hide_empty' => false),
                'columns'    => 12,
            ),
            array(
                'id' => $prefix . 'lwf_default_categories',
                'type' => 'group',
                'clone'  => true,
                'fields' => array(
                    array(
                        'name'       => __( 'Default Categories', 'vg' ),
                        'id'         => 'categories',
                        'type'       => 'taxonomy_advanced',
                        'taxonomy'   => 'vg_listings_cats',
                        'field_type' => 'select_tree',
                        'placeholder' => 'Select All',
                        'query_args' => array('hide_empty' => false),
                    ),
                ),
                'columns'    => 6,
            ),
            array(
                'id' => $prefix . 'lwf_default_event_categories',
                'type' => 'group',
                'clone'  => true,
                'fields' => array(
                    array(
                        'name'       => __( 'Default Event Categories', 'vg' ),
                        'id'         => 'categories',
                        'type'       => 'taxonomy_advanced',
                        'taxonomy'   => 'vg_events_cats',
                        'field_type' => 'select_tree',
                        'placeholder' => 'Select All',
                        'query_args' => array('hide_empty' => false),
                    ),
                ),
                'columns'    => 6,
            ),
            array(
                'name'  => __( 'Will non selected localities still be available?', 'vg' ),
                'id'    => $prefix . 'lwf_allow_unselected_localities',
                'type'  => 'radio',
                'options' => array(
                    'yes' => 'Yes',
                    'no' => 'No',
                ),
                'columns'    => 6,
            ),
            array(
                'name'  => __( 'Will non selected categories still be available?', 'vg' ),
                'id'    => $prefix . 'lwf_allow_unselected_categories',
                'type'  => 'radio',
                'options' => array(
                    'yes' => 'Yes',
                    'no' => 'No',
                ),
                'columns'    => 6,
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'         => 'page-desc',
        'title'      => __( 'Page Intro Text', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'side',
        'priority'   => 'low',
        // 'validation' => array(
        //     'rules'    => array(
        //         $prefix . 'page-intro-text' => array(
        //             'maxlength' => 150
        //         ),
        //     ),
        // ),
        'fields' => array(
            array(
                'name'  => '',
                'id'    => $prefix . 'page-intro-text',
                'type'  => 'textarea',
                'rows'  => 10,
                'desc'  => 'Maximum 150 characters'
            ),
        ),
        'exclude' => array(
            'ID' => array( 4 )
        )
    );


    /* LISTINGS METAS */

    $post_types = array('page', 'vg_listings');

    foreach ($post_types as $post_type) {

        $include = array();

        if ($post_type == 'page') {
            $include = array(
                'template' => array('single-vg_listings.php')
            );
        }

        /* 
         *
         * Listing Data 
         *
         *
        */
        $mb = array(
            'id'         => 'listing_data_' . $post_type,
            'title'      => __( 'Listing Data', 'vg' ),
            'post_types' => array( $post_type ),
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(
                array(
                    'name'  => __( 'TripAdvisor ID:', 'vg' ),
                    'id'    => $prefix . 'listing-tripadvisor-id',
                    'type'  => 'text',
                ),
                array(
                    'name'  => __( 'TripAdvisor URL:', 'vg' ),
                    'id'    => $prefix . 'listing-tripadvisor-url',
                    'type'  => 'text',
                ),
                array(
                    'name'  => __( 'Latitude:', 'vg' ),
                    'id'    => $prefix . 'listing-latitude',
                    'type'  => 'text',
                ),
                array(
                    'name'  => __( 'Longitude:', 'vg' ),
                    'id'    => $prefix . 'listing-longitude',
                    'type'  => 'text',
                ),
                array(
                    'name'  => __( 'Address 1:', 'vg' ),
                    'id'    => $prefix . 'listing-address-1',
                    'type'  => 'text',
                ),
                array(
                    'name'  => __( 'Address 2:', 'vg' ),
                    'id'    => $prefix . 'listing-address-2',
                    'type'  => 'text',
                ),
                array(
                    'name'  => __( 'Contact No:', 'vg' ),
                    'id'    => $prefix . 'listing-contact-no',
                    'type'  => 'text',
                ),
                array(
                    'name'  => __( 'Email:', 'vg' ),
                    'id'    => $prefix . 'listing-email',
                    'type'  => 'text',
                ),
                array(
                    'name'  => __( 'Website:', 'vg' ),
                    'id'    => $prefix . 'listing-website',
                    'type'  => 'text',
                ),
                array(
                    'name'  => __( 'Video URL:', 'vg' ),
                    'id'    => $prefix . 'listing-video',
                    'type'  => 'text',
                    'desc'  => 'Paste in a YouTube or Vimeo URL'
                ),
                array(
                    'name'  => __( 'Remarks:', 'vg' ),
                    'id'    => $prefix . 'listing-remarks',
                    'type'  => 'textarea',
                    'desc'  => 'Separate multiple items with a new line. Maximum 250 characters.',
                    'rows'  => 10
                ),
            ),
        );

        if ($post_type == 'page') {
            //$mb['include'] = $include;
        }

        $meta_boxes[] = $mb;


         /* 
         *
         * Listing Opening Hours 
         *
         *
        */

        
        $days_of_week = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');

        $days_of_week = array_combine($days_of_week, $days_of_week);

        $mb = array(
            'id'         => 'listing-opening-hours',
            'title'      => __( 'Opening Hours', 'vg' ),
            'post_types' => array( $post_type ),
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(
                array(
                    'id' => $prefix . 'opening-hours',
                    'type' => 'group',
                    'clone'  => true,
                    'sort_clone' => true,
                    'fields' => array(
                        array(
                            'name' => 'Day:',
                            'id' => 'opening-day',
                            //'columns' => 4,
                            'type' => 'select',
                            'options' => $days_of_week,
                        ),
                        array(
                            'name' => 'Opening Hour:',
                            'id' => 'opening-hour',
                            //'columns' => 4,
                            'type' => 'time'
                        ),
                        array(
                            'name' => 'Closing Hour:',
                            'id' => 'closing-hour',
                            //'columns' => 4,
                            'type' => 'time'
                        ),
                    ),
                ),
            ),
        );

        if ($post_type == 'page') {
            $mb['include'] = $include;
        }

        $meta_boxes[] = $mb;
        

         /* 
         *
         * Listing Media 
         *
         *
        */

        
        $mb = array(
            'id'         => 'listing-media',
            'title'      => __( 'Listing Media', 'vg' ),
            'post_types' => array( $post_type ),
            'context'    => 'normal',
            'priority'   => 'high',
            'validation' => array(
                'rules'    => array(
                    $prefix . 'event-gallery' => array(
                        'required'  => true,
                    ),
                ),
            ),
            'fields' => array(
                array(
                    'name'  => __( 'Gallery:', 'vg' ),
                    'id'    => $prefix . 'listing-gallery',
                    'type'  => 'image_advanced',
                    'desc'  => 'Minimum 3 and maximum 9 images. First image is used as the poster image.',
                    'max_file_uploads' => 9
                ),
            ),
        );

        if ($post_type == 'page') {
            $mb['include'] = $include;
        }

        $meta_boxes[] = $mb;
        

        /* 
         *
         * Listing Descriptions 
         *
         *
        */
        $languages = array(
            'es' => 'Spanish', 
            'de' => 'German', 
            'ru' => 'Russian', 
            'it' => 'Italian', 
            'sv' => 'Swedish', 
            'fr' => 'French'
        );

        $fields = array();

        foreach ($languages as $lang_code=>$lang_name) {
            $fields[] = array(
                'name'  => __( $lang_name . ':', 'vg' ),
                'id'    => $prefix . 'listing-' . $lang_code . '-description',
                'type'  => 'textarea',
                'desc'  => 'Separate multiple items with a new line. Maximum 250 characters.',
                'rows'  => 10
            );
        }

        $mb = array(
            'id'         => 'listing-descriptions',
            'title'      => __( 'Listing Descriptions', 'vg' ),
            'post_types' => array( $post_type ),
            'context'    => 'normal',
            'priority'   => 'high',
            'fields'     => $fields,
        );


        if ($post_type == 'page') {
            $mb['include'] = $include;
        }

        $meta_boxes[] = $mb;

    } /* end post type loop */


    /* EVENTS METAS */
    $meta_boxes[] = array(
        'id'         => 'event-data',
        'title'      => __( 'Event Data', 'vg' ),
        'post_types' => array( 'vg_events' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'validation' => array(
            'rules'    => array(
                $prefix . 'event-locality' => array(
                    'required'  => true,
                ),
                $prefix . 'event-venue' => array(
                    'required'  => true,
                ),
                $prefix . 'event-contact-no' => array(
                    'required'  => true,
                ),
                $prefix . 'event-email' => array(
                    'required'  => true,
                    'email'  => true,
                ),
                $prefix . 'event-description' => array(
                    'required'  => true,
                    'minlength' => 200,
                    'maxlength' => 250
                ),
            ),
        ),
        'fields' => array(
            array(
                'name'  => __( 'Venue:', 'vg' ),
                'id'    => $prefix . 'event-venue',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Latitude:', 'vg' ),
                'id'    => $prefix . 'event-latitude',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Longitude:', 'vg' ),
                'id'    => $prefix . 'event-longitude',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Contact No:', 'vg' ),
                'id'    => $prefix . 'event-contact-no',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Email:', 'vg' ),
                'id'    => $prefix . 'event-email',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Website:', 'vg' ),
                'id'    => $prefix . 'event-website',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Video URL:', 'vg' ),
                'id'    => $prefix . 'event-video',
                'type'  => 'text',
                'desc'  => 'Paste in a YouTube or Vimeo URL'
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'event-dates',
        'title'      => __( 'Event Dates', 'vg' ),
        'post_types' => array( 'vg_events' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'validation' => array(
            'rules'    => array(
                $prefix . 'event-date' => array(
                    'required'  => true,
                )
            ),
        ),

        'fields' => array(
             array(
                'name'  => __( 'Does this event occur on?', 'vg' ),
                'id'    => $prefix . 'event-occurance',
                'type'  => 'radio',
                'options' => array(
                    'specific_days' => 'Specific Days',
                    'date_range'    => 'Date Range',
                ),
            ),
            array(
                'name' => 'Start Date:',
                'id' => $prefix . 'event-start-date',
                'type' => 'date',
                'js_options' => array(
                    'dateFormat' => 'dd/M/yy'
                ),
                'timestamp' => true,
                'visible' => array( $prefix . 'event-occurance', '=', 'date_range' ),
            ),
             array(
                'name' => 'End Date:',
                'id' => $prefix . 'event-end-date',
                'type' => 'date',
                'js_options' => array(
                    'dateFormat' => 'dd/M/yy'
                ),
                'timestamp' => true,
                'visible' => array( $prefix . 'event-occurance', '=', 'date_range' ),
            ),
            array(
                'name' => 'Starting Time:',
                'id' => $prefix . 'event-start-time',
                'type' => 'time',
                'visible' => array( $prefix . 'event-occurance', '=', 'date_range' ),
            ),
            array(
                'name' => 'Ending Time:',
                'id' => $prefix . 'event-end-time',
                'type' => 'time',
                'visible' => array( $prefix . 'event-occurance', '=', 'date_range' ),
            ),
            array(
                'id' => $prefix . 'event-dates',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'visible' => array( $prefix . 'event-occurance', '=', 'specific_days' ),
                'fields' => array(
                    array(
                        'name' => 'Event Date:',
                        'id' => 'date',
                        'type' => 'date',
                        'js_options' => array(
                            'dateFormat' => 'dd/M/yy'
                        ),
                        'timestamp' => true
                    ),
                    array(
                        'name' => 'Starting Time:',
                        'id' => 'start-time',
                        'type' => 'time',
                        'js_options' => array(
                            'dateFormat' => 'dd/M/yy'
                        ),
                        'timestamp' => true
                    ),
                    array(
                        'name' => 'Ending Time:',
                        'id' => 'ending-time',
                        'type' => 'time',
                        'js_options' => array(
                            'dateFormat' => 'dd/M/yy'
                        ),
                        'timestamp' => true
                    ),
                ),
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'         => 'event-media',
        'title'      => __( 'Event Media', 'vg' ),
        'post_types' => array( 'vg_events' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'validation' => array(
            'rules'    => array(
                /*
                $prefix . 'event-poster' => array(
                    'required'  => true,
                ),
                */
            ),
        ),
        'fields' => array(
            array(
                'name'  => __( 'Poster:', 'vg' ),
                'id'    => $prefix . 'event-poster',
                'type'  => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'name'  => __( 'Gallery:', 'vg' ),
                'id'    => $prefix . 'event-gallery',
                'type'  => 'image_advanced',
                'desc'  => 'Minimum 3 and maximum 9 images',
                'max_file_uploads' => 9
            ),
        )
    );


    /* HOMEPAGE */ 

    if($_GET['post'] == '4' && $_GET['action'] == 'edit'){
        $pagesList = vg_get_pages();
        $accommList = vg_get_accom();
    } else {
        $pagesList = array();  
        $accomList = array();  
    } 

    $meta_boxes[] = array(
        'id'         => 'homepage-featured-pages',
        'title'      => __( 'Featured Pages', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'tabs'       => array(
            'whattodo' => array(
                'label' => __( 'What to do', 'rwmb' ),
            ),
            'wheretostay'  => array(
                'label' => __( 'Where to stay', 'rwmb' ),
            ),
            'wheretogo'    => array(
                'label' => __( 'Where to go', 'rwmb' ),
            ),
        ),
        'tab_style' => 'left',
        'tab_wrapper' => true,
        'validation' => array(
            'rules'    => array(
                $prefix . 'wtd-featured-leader' => array(
                    'required'  => true,
                ),
                $prefix . 'wtd-featured-page-1' => array(
                    'required'  => true,
                ),
                $prefix . 'wtd-featured-page-2' => array(
                    'required'  => true,
                ),
                $prefix . 'wtd-featured-page-3' => array(
                    'required'  => true,
                ),
                $prefix . 'wtd-featured-page-4' => array(
                    'required'  => true,
                ),
                $prefix . 'wtd-featured-page-5' => array(
                    'required'  => true,
                ),
                $prefix . 'wtd-featured-page-6' => array(
                    'required'  => true,
                ),
                $prefix . 'wts-featured-leader' => array(
                    'required'  => true,
                ),
                $prefix . 'wts-featured-page-1' => array(
                    'required'  => true,
                ),
                $prefix . 'wts-featured-page-2' => array(
                    'required'  => true,
                ),
                $prefix . 'wts-featured-page-3' => array(
                    'required'  => true,
                ),
                $prefix . 'wts-featured-page-4' => array(
                    'required'  => true,
                ),
                $prefix . 'wts-featured-page-5' => array(
                    'required'  => true,
                ),
                $prefix . 'wts-featured-page-6' => array(
                    'required'  => true,
                ),
                $prefix . 'wtg-featured-leader' => array(
                    'required'  => true,
                ),
                $prefix . 'wtg-featured-page-1' => array(
                    'required'  => true,
                ),
                $prefix . 'wtg-featured-page-2' => array(
                    'required'  => true,
                ),
                $prefix . 'wtg-featured-page-3' => array(
                    'required'  => true,
                ),
                $prefix . 'wtg-featured-page-4' => array(
                    'required'  => true,
                ),
                $prefix . 'wtg-featured-page-5' => array(
                    'required'  => true,
                ),
                $prefix . 'wtg-featured-page-6' => array(
                    'required'  => true,
                ),
            ),
        ),
        'fields' => array(
            array(
                'name'  => __( 'Featured Leader Page:', 'vg' ),
                'id'    => $prefix . 'wtd-featured-leader',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'whattodo',
            ),
            array(
                'name'  => __( 'Featured Page 1:', 'vg' ),
                'id'    => $prefix . 'wtd-featured-page-1',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'whattodo',
            ),
            array(
                'name'  => __( 'Featured Page 2:', 'vg' ),
                'id'    => $prefix . 'wtd-featured-page-2',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'whattodo',
            ),
            array(
                'name'  => __( 'Featured Page 3:', 'vg' ),
                'id'    => $prefix . 'wtd-featured-page-3',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'whattodo',
            ),
            array(
                'name'  => __( 'Featured Page 4:', 'vg' ),
                'id'    => $prefix . 'wtd-featured-page-4',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'whattodo',
            ),
            array(
                'name'  => __( 'Featured Page 5:', 'vg' ),
                'id'    => $prefix . 'wtd-featured-page-5',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'whattodo',
            ),
            array(
                'name'  => __( 'Featured Page 6:', 'vg' ),
                'id'    => $prefix . 'wtd-featured-page-6',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'whattodo',
            ),

            array(
                'name'  => __( 'Featured Leader Page:', 'vg' ),
                'id'    => $prefix . 'wts-featured-leader',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretostay',
            ),
            array(
                'name'  => __( 'Featured Page 1:', 'vg' ),
                'id'    => $prefix . 'wts-featured-page-1',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretostay',
            ),
            array(
                'name'  => __( 'Featured Page 2:', 'vg' ),
                'id'    => $prefix . 'wts-featured-page-2',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretostay',
            ),
            array(
                'name'  => __( 'Featured Page 3:', 'vg' ),
                'id'    => $prefix . 'wts-featured-page-3',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretostay',
            ),
            array(
                'name'  => __( 'Featured Page 4:', 'vg' ),
                'id'    => $prefix . 'wts-featured-page-4',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretostay',
            ),
            array(
                'name'  => __( 'Featured Page 5:', 'vg' ),
                'id'    => $prefix . 'wts-featured-page-5',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretostay',
            ),
            array(
                'name'  => __( 'Featured Page 6:', 'vg' ),
                'id'    => $prefix . 'wts-featured-page-6',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretostay',
            ),

            array(
                'name'  => __( 'Featured Leader Page:', 'vg' ),
                'id'    => $prefix . 'wtg-featured-leader',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretogo',
            ),
            array(
                'name'  => __( 'Featured Page 1:', 'vg' ),
                'id'    => $prefix . 'wtg-featured-page-1',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretogo',
            ),
            array(
                'name'  => __( 'Featured Page 2:', 'vg' ),
                'id'    => $prefix . 'wtg-featured-page-2',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretogo',
            ),
            array(
                'name'  => __( 'Featured Page 3:', 'vg' ),
                'id'    => $prefix . 'wtg-featured-page-3',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretogo',
            ),
            array(
                'name'  => __( 'Featured Page 4:', 'vg' ),
                'id'    => $prefix . 'wtg-featured-page-4',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretogo',
            ),
            array(
                'name'  => __( 'Featured Page 5:', 'vg' ),
                'id'    => $prefix . 'wtg-featured-page-5',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretogo',
            ),
            array(
                'name'  => __( 'Featured Page 6:', 'vg' ),
                'id'    => $prefix . 'wtg-featured-page-6',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'placeholder' => 'Select Page',
                'tab'  => 'wheretogo',
            ),
        ),
        'include' => array(
            'ID'    => array( 4 ),
        )
    );

    $meta_boxes[] = array(
        'title'      => 'Locality Settings',
        'taxonomies' => 'vg_locality_cats', // List of taxonomies. Array or string
        'fields' => array(
            array(
                'name' => __( 'TripAdvisor Name', 'vg' ),
                'id'   => $prefix . 'tripadvisor_name',
                'type' => 'text',
            ),
        ),
    );


    /* POOL TRIPS METAS */

    $meta_boxes[] = array(
        'id'         => $prefix.'trip-details',
        'title'      => __( 'Trip Details', 'vg' ),
        'post_types' => array( 'vg_pool_trips' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Travel from', 'vg' ),
                'id'    => $prefix . 'travel_from',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Travel to', 'vg' ),
                'id'    => $prefix . 'travel_to',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Date', 'vg' ),
                'id'    => $prefix . 'date',
                'type'  => 'date',
                'timestamp' => true                
            ),
            array(
                'name'  => __( 'Time', 'vg' ),
                'id'    => $prefix . 'time',
                'type'  => 'time',
            ),
            array(
                'name'  => __( 'No of persons', 'vg' ),
                'id'    => $prefix . 'persons',
                'type'  => 'number',
            ),
            array(
                'name'  => __( 'Transport', 'vg' ),
                'id'    => $prefix . 'transport',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Email', 'vg' ),
                'id'    => $prefix . 'email',
                'type'  => 'text',
            ),
        )
    );


    /* CONTACT US METAS */

    $meta_boxes[] = array(
        'id'         => $prefix.'contact-intro',
        'title'      => __( 'Intro Text', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Intro Text', 'vg' ),
                'id'    => $prefix . 'contact-intro-text',
                'type'  => 'textarea',
            ),
        ),
        'include' => array(
            'template'    => array( 'page-contact-us.php' ),
        )
    );

    /* Friends of Page Template */

    $meta_boxes[] = array(
        'id'         => $prefix.'friends-of',
        'title'      => __( 'Friends List', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
             array(
                'id' => $prefix . 'friends-links',
                'type' => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => 'Name:',
                        'id' => 'name',
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Notes:',
                        'id' => 'notes',
                        'type' => 'textarea',
                    ),
                    array(
                        'name' => 'URL:',
                        'id' => 'url',
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Logo:',
                        'id' => 'logo',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1
                    ),
                ),
            ),
        ),
        'include' => array(
            'template'    => array( 'page-friends-of.php' ),
        )
    );


    /* Plan My Trip Meta */
    $seasons        = vg_get_seasons();
    $accompanied    = vg_get_accompanied();
    $interests      = vg_get_interests();

     $meta_boxes[] = array(
        'id'         => 'plan-my-trip',
        'title'      => __( 'Plan My Trip', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'side',
        'priority'   => 'high',
        'tabs'       => array(
            'config' => array(
                'label' => __( '', 'rwmb' ),
                'icon' => 'dashicons-admin-generic',
            ),
            'season' => array(
                'label' => __( '', 'rwmb' ),
                'icon' => 'dashicons-palmtree',
            ),
            'accompanied'  => array(
                'label' => __( '', 'rwmb' ),
                'icon' => 'dashicons-groups',
            ),
            'interests'    => array(
                'label' => __( '', 'rwmb' ),
                'icon' => 'dashicons-thumbs-up',
            ),
        ),
        'tab_wrapper' => true,
        'fields' => array(
            array(
                'name'  => __( 'Allow this page to be featured in Plan My Trip?', 'vg' ),
                'id'    => $prefix . 'plan-my-trip',
                'type'  => 'radio',
                'options' => array(
                    'yes' => 'Yes',
                    'no' => 'No',
                ),
                'tab'  => 'config',
            ),
            array(
                'name'  => __( 'Season', 'vg' ),
                'id'    => $prefix . 'plan-my-trip-season',
                'type'  => 'checkbox_list',
                'options' => $seasons,
                'tab'  => 'season',
            ),
            array(
                'name'  => __( 'Accompanied', 'vg' ),
                'id'    => $prefix . 'plan-my-trip-accompanied',
                'type'  => 'checkbox_list',
                'options' => $accompanied,
                'tab'  => 'accompanied',
            ),
            array(
                'name'  => __( 'Interests', 'vg' ),
                'id'    => $prefix . 'plan-my-trip-interests',
                'type'  => 'checkbox_list',
                'options' => $interests,
                'tab'  => 'interests',
            ),
            
        ),
    );


    $meta_boxes[] = array(
        'title'      => 'Category Meta',
        'taxonomies' => 'vg_listings_cats', // List of taxonomies. Array or string
        'fields' => array(
            array(
                'name'        => 'Connected Page',
                'id'          => $prefix . 'connected-page',
                'type'        => 'post',
                'post_type'   => 'page',
                'field_type'  => 'select_advanced',
                'placeholder' => esc_html__( 'Select a page', 'your-prefix' ),
                'query_args'  => array(
                    'post_status'    => 'publish',
                    'posts_per_page' => - 1,
                ),
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'         => 'arcgis-connector',
        'title'      => __( 'ARCGIS Map Connector', 'vg' ),
        'post_types' => array( 'page', 'vg_listings', 'vg_events' ),
        'context'    => 'side',
        'priority'   => 'high',
        'fields' => array(
             array(
                'name'  => __( 'MAP Object ID:', 'vg' ),
                'id'    => $prefix . 'map_object_id',
                'type'  => 'number',
            ),
            
        ),
    );


    $meta_boxes[] = array(
        'id'         => 'page-elastic',
        'title'      => __( 'Elastic Search Setting', 'vg' ),
        'post_types' => array( 'page' ),
        'context'    => 'side',
        'priority'   => 'low',
        'fields' => array(
            array(
                'name'  => __( 'Make unsearchable', 'vg' ),
                'id'    => $prefix . 'is_unsearchable',
                'type'  => 'checkbox',
                'std'  => 0
            ),
        ),
    );


    $meta_boxes[] = array(
        'id'             => 'custom_artisan_url',
        'title'          => esc_html__( 'Artisan Custom Link', 'vg' ),
        'post_types'     => array( 'vg_local_artists' ),
        'context'        => 'normal',
        'priority'       => 'default',
        'autosave'       => 'true',
        'fields'         => array(
                                array(
                                    'id'    => $prefix . 'artisan_url',
                                    'type'  => 'url',
                                    'name'  => esc_html__( 'Custom URL', 'vg' ),
                                ),
        ),
    );



    /**
     * Local Artists Map
     * Clone (Repeater) fields that display one or multiple markers
     * Fields:  Longitude, Latitude (map marker)
     *          Title, Street Name, Link (marker popup)
     */

    $meta_boxes[] = array(
        'title'             =>  esc_html__( 'Map with Markers', 'vg' ),
        'id'                =>  'map_markers',
        'post_types'        =>  array( 'vg_local_artists' ),
        'autosave'          =>  'true',
        //'render_template'   =>  get_template_directory() . '/inc/vg-local-artists-render.php',
        'fields'            =>  array(
                                    array(
                                        'id'              => $prefix . 'longitude',
                                        'type'            => 'number',
                                        'name'            => esc_html__( 'Longitude', 'vg' ),
                                        'step'            => 'any',
                                    ),
                                    array(
                                        'id'              => $prefix . 'latitude',
                                        'type'            => 'number',
                                        'name'            => esc_html__( 'Latitude', 'vg' ),
                                        'step'            => 'any',
                                    ),
                                    # Removed since we're taking title from "get_the_title"
                                    # Left here just in case client wants the functionality
                                    /*array(
                                        'id'              => $prefix . 'map_title',
                                        'type'            => 'text',
                                        'name'            => esc_html__( 'Title', 'vg' ),
                                        'desc'            => esc_html__( 'Marker popup - Title', 'vg' ),
                                        'placeholder'     => esc_html__( 'e.g. Lazuli Art', 'vg' ),
                                    ),*/
                                    array(
                                        'id'              => $prefix . 'street',
                                        'type'            => 'text',
                                        'name'            => esc_html__( 'Street address', 'vg' ),
                                        'desc'            => esc_html__( 'Marker popup - Street Name', 'vg' ),
                                        'placeholder'     => esc_html__( 'e.g. Triq Palma, Victoria', 'vg' ),
                                    ),
                                    array(
                                        'id'              => $prefix . 'link',
                                        'type'            => 'url',
                                        'name'            => esc_html__( 'Link', 'vg' ),
                                        'desc'            => esc_html__( 'Marker popup - Link', 'vg' ),
                                        'placeholder'     => esc_html__( 'e.g. http://www.my-link.com', 'vg' ),
                                    ),
                                ),
    );

    /**
     * Meta Box for uploading file for download by website visitor
     */
    $meta_boxes[] = array(
        'id'            => 'brochure',
        'title'         => esc_html__( 'Brochure Download', 'vg' ),
        'post_types'    =>  array( 'page', 'vg_local_artists' ),
        'context'       => 'normal',
        'priority'      => 'default',
        'autosave'      => 'false',
        'fields'        => array(
            array(
                'id'                   => $prefix . 'background_img',
                'type'                 => 'image_advanced',
                'name'                 => esc_html__( 'Background Image', 'vg' ),
                'max_file_uploads'     => '1',
            ),
            array(
                'id'                   => $prefix . 'brochure_title',
                'type'                 => 'text',
                'name'                 => esc_html__( 'Title', 'vg' ),
                'placeholder'          => esc_html__( 'e.g. Brochure', 'vg' ),
            ),
            array(
                'id'                   => $prefix . 'brochure_text',
                'type'                 => 'text',
                'name'                 => esc_html__( 'Text', 'vg' ),
                'desc'                 => esc_html__( 'Please insert short description here.', 'vg' ),
            ),
            array(
                'id'                   => $prefix . 'brochure_image',
                'type'                 => 'image_advanced',
                'name'                 => esc_html__( 'Image', 'vg' ),
                'max_file_uploads'     => 1,
            ),
            array(
                'id'                   => $prefix . 'brochure_file',
                'type'                 => 'file_advanced',
                'name'                 => esc_html__( 'File Advanced', 'vg' ),
                'desc'                 => esc_html__( 'Allowed File Type: PDF', 'vg' ),
                'mime_type'            => 'application/pdf',
                'max_file_uploads'     => 1,
                'max_status'           => 'true',
            ),
        ),
    );


    /**
     * Meta Box for:
     * Title, Text
     * Group: [ Icon / Icon Title / Icon Description ]
     */
    $meta_boxes[] = array(
        'id'           => 'icons_repeater',
        'title'        => esc_html__( 'Icons', 'vg' ),
        'post_types'   =>  array( 'page', 'vg_local_artists' ),
        'context'      => 'normal',
        'priority'     => 'default',
        'autosave'     => 'false',
        'fields'       => array(
            array(
                'id'                    => $prefix . 'icon_title',
                'type'                  => 'text',
                'name'                  => esc_html__( 'Title', 'vg' ),
                'placeholder'           => esc_html__( 'e.g. How To Visit?', 'vg' ),
            ),
            array(
                'id'                    => $prefix . 'icon_text',
                'type'                  => 'textarea',
                'name'                  => esc_html__( 'Text', 'vg' ),
                'desc'                  => esc_html__( 'Please insert short description here.', 'vg' ),
            ),
            'fields'    =>  array(
                                'id'            => 'icons',
                                'name'          => 'Icons',
                                'type'          => 'group',
                                'group_title'   => "Icon {#}",
                                'clone'         => true,
                                'max_clone'     => 3,
                                'sort_clone'    => true,
                                'collapsible'   => true,
                                'fields'        =>  array(
                                                        array(
                                                            'id'                    => $prefix . 'icon_image',
                                                            'type'                  => 'image_advanced',
                                                            'name'                  => esc_html__( 'Icon Image', 'vg' ),
                                                            'max_file_uploads'      => '1',
                                                            'max_status'            => false,
                                                        ),
                                                        array(
                                                            'id'                    => $prefix . 'icon_title',
                                                            'type'                  => 'text',
                                                            'name'                  => esc_html__( 'Icon Title', 'vg' ),
                                                        ),
                                                        array(
                                                            'id'                    => $prefix . 'icon_text',
                                                            'type'                  => 'wysiwyg',
                                                            'name'                  => esc_html__( 'Icon Text', 'vg' ),
                                                        ),
                                                    ),
                            ),
        ),
    );

    return $meta_boxes;

}



?>