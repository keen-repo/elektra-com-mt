WebP Express 0.14.21. Conversion triggered using bulk conversion, 2019-07-12 09:21:33

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.2.19-1+0~20190531112732.22+jessie~1.gbp75765b
- Server software: Apache/2.4.10 (Debian)

Stack converter ignited
Destination folder does not exist. Creating folder: [doc-root]wp-content/webp-express/webp-images/doc-root/wp-content/themes/elektra/src/img/footer

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]wp-content/themes/elektra/src/img/footer/footer_logo.png
- destination: [doc-root]wp-content/webp-express/webp-images/doc-root/wp-content/themes/elektra/src/img/footer/footer_logo.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]wp-content/themes/elektra/src/img/footer/footer_logo.png
- destination: [doc-root]wp-content/webp-express/webp-images/doc-root/wp-content/themes/elektra/src/img/footer/footer_logo.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Locating cwebp binaries
No cwebp binaries where located in common system locations
Checking if we have a supplied binary for OS: Linux... We do.
We in fact have 3
A total of 3 cwebp binaries where found
Detecting versions of the cwebp binaries found (and verifying that they can be executed in the process)
Executing: [doc-root]wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-linux-1.0.2-shared -version
Exec failed (return code: 126)
PS: Return code 126 means "Permission denied". The user that the command was run with does not have permission to execute that binary.
Executing: [doc-root]wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-linux-1.0.2-static -version
Exec failed (return code: 126)
PS: Return code 126 means "Permission denied". The user that the command was run with does not have permission to execute that binary.
Executing: [doc-root]wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-linux-0.6.1 -version
Exec failed (return code: 126)
PS: Return code 126 means "Permission denied". The user that the command was run with does not have permission to execute that binary.

**Error: ** **None of the cwebp files located can be executed. All failed with return code 126 (permission denied)** 
None of the cwebp files located can be executed. All failed with return code 126 (permission denied)
cwebp failed in 84 ms

*Trying: vips* 

**Error: ** **Required Vips extension is not available.** 
Required Vips extension is not available.
vips failed in 0 ms

*Trying: imagemagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]wp-content/themes/elektra/src/img/footer/footer_logo.png
- destination: [doc-root]wp-content/webp-express/webp-images/doc-root/wp-content/themes/elektra/src/img/footer/footer_logo.png.webp
- alpha-quality: 85
- encoding: "auto"
- log-call-arguments: true
- metadata: "none"
- quality: 85
- use-nice: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- low-memory: false
- max-quality: 85
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Version: ImageMagick 6.8.9-9 Q16 x86_64 2019-05-13 http://www.imagemagick.org
Quality: 85. 
using nice
Executing command: nice convert -quality '85' -strip -define webp:alpha-quality=85 -define webp:method=6 '[doc-root]wp-content/themes/elektra/src/img/footer/footer_logo.png' 'webp:[doc-root]wp-content/webp-express/webp-images/doc-root/wp-content/themes/elektra/src/img/footer/footer_logo.png.webp.lossy.webp'
return code: 1

**Error: ** **The exec call failed** 
The exec call failed
imagemagick failed in 46 ms

*Trying: graphicsmagick* 

**Error: ** **gmagick is not installed** 
gmagick is not installed
graphicsmagick failed in 4 ms

*Trying: wpc* 

**Error: ** **Missing URL. You must install Webp Convert Cloud Service on a server, or the WebP Express plugin for Wordpress - and supply the url.** 
Missing URL. You must install Webp Convert Cloud Service on a server, or the WebP Express plugin for Wordpress - and supply the url.
wpc failed in 0 ms

*Trying: ewww* 

**Error: ** **Missing API key.** 
Missing API key.
ewww failed in 0 ms

*Trying: imagick* 

**Error: ** **iMagick was compiled without WebP support.** 
iMagick was compiled without WebP support.
imagick failed in 1 ms

*Trying: gmagick* 

**Error: ** **Required Gmagick extension is not available.** 
Required Gmagick extension is not available.
gmagick failed in 0 ms

*Trying: gd* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]wp-content/themes/elektra/src/img/footer/footer_logo.png
- destination: [doc-root]wp-content/webp-express/webp-images/doc-root/wp-content/themes/elektra/src/img/footer/footer_logo.png.webp
- log-call-arguments: true
- quality: 85

The following options have not been explicitly set, so using the following defaults:
- default-quality: 85
- max-quality: 85
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- alpha-quality
- encoding
- metadata
- near-lossless
- skip-pngs
------------

GD Version: 2.2.5
image is true color
Quality: 85. 
gd succeeded :)

Converted image in 146 ms, reducing file size with 23% (went from 2952 bytes to 2278 bytes)
